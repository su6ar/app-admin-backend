<?php
/**
 * Created by Daniel Bill
 * Date: 23.01.2017
 * Time: 18:33
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new \Dotenv\Dotenv(__DIR__ . '/../config', 'config.env');
$dotenv->load();
$config = new Kominexpres\src\App\Config\Config($dotenv);

$settings = [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'logger' => [
            'name' => 'kominexpres-api',
            'level' => \Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/../logs/app.log',
        ],
    ],
];

$main = new \Kominexpres\src\Slim\Main\Main($config, $settings);

\Kominexpres\src\App\Exceptions\ExceptionHandler::setExceptionHandler();

$main->createRoutes();

$main->runApp();