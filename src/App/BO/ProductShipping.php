<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductShipping
{
    public const SPG_ID = "spg_id";
    public const FIXED_PRICE = "fixed_price";
    public const SHOW_SHIPPING = "show_shipping";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $spgId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var boolean
     */
    public $showShipping;
    /**
     * @var double|null
     */
    public $fixedPrice;
    /**
     * @var string|null
     */
    public $dateTo;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductShipping
     */
    public function setPutId(int $putId): ProductShipping
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpgId(): int
    {
        return $this->spgId;
    }

    /**
     * @param int $spgId
     * @return ProductShipping
     */
    public function setSpgId(int $spgId): ProductShipping
    {
        $this->spgId = $spgId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ProductShipping
     */
    public function setDateFrom(string $dateFrom): ProductShipping
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowShipping(): bool
    {
        return $this->showShipping;
    }

    /**
     * @param bool $showShipping
     * @return ProductShipping
     */
    public function setShowShipping(bool $showShipping): ProductShipping
    {
        $this->showShipping = $showShipping;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFixedPrice(): ?float
    {
        return $this->fixedPrice;
    }

    /**
     * @param float|null $fixedPrice
     * @return ProductShipping
     */
    public function setFixedPrice(?float $fixedPrice): ProductShipping
    {
        $this->fixedPrice = $fixedPrice;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ProductShipping
     */
    public function setDateTo(?string $dateTo): ProductShipping
    {
        $this->dateTo = $dateTo;
        return $this;
    }
}