<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:07
 */

namespace Kominexpres\src\App\BO;


class ImageCategory
{
    public const ICY_ID = "icy_id";
    public const ICY_NAME = "icy_name";

    /**
     * @var integer
     */
    public $icyId;
    /**
     * @var string
     */
    public $icyName;

    /**
     * @return int
     */
    public function getIcyId(): int
    {
        return $this->icyId;
    }

    /**
     * @param int $id
     * @return ImageCategory
     */
    public function setIcyId(int $id): ImageCategory
    {
        $this->icyId = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcyName(): string
    {
        return $this->icyName;
    }

    /**
     * @param string $icyName
     * @return ImageCategory
     */
    public function setIcyName(string $icyName): ImageCategory
    {
        $this->icyName = $icyName;
        return $this;
    }
}