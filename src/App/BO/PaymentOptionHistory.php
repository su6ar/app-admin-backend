<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class PaymentOptionHistory
{
    public const ADD_PERCENTAGE_OOTT_PRICE = "add_percentage_oott_price";
    public const PRICE = "price";
    public const POY_NAME = "poy_name";

    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var integer
     */
    public $pmtId;
    /**
     * @var string
     */
    public $poyName;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var double
     */
    public $addPercentageOottPrice;
    /**
     * @var double
     */
    public $price;

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return PaymentOptionHistory
     */
    public function setDateFrom(string $dateFrom): PaymentOptionHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getPmtId(): int
    {
        return $this->pmtId;
    }

    /**
     * @param int $pmtId
     * @return PaymentOptionHistory
     */
    public function setPmtId(int $pmtId): PaymentOptionHistory
    {
        $this->pmtId = $pmtId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPoyName(): string
    {
        return $this->poyName;
    }

    /**
     * @param string $poyName
     * @return PaymentOptionHistory
     */
    public function setPoyName(string $poyName): PaymentOptionHistory
    {
        $this->poyName = $poyName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return PaymentOptionHistory
     */
    public function setDateTo(?string $dateTo): PaymentOptionHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return float
     */
    public function getAddPercentageOottPrice(): float
    {
        return $this->addPercentageOottPrice;
    }

    /**
     * @param float $addPercentageOottPrice
     * @return PaymentOptionHistory
     */
    public function setAddPercentageOottPrice(float $addPercentageOottPrice): PaymentOptionHistory
    {
        $this->addPercentageOottPrice = $addPercentageOottPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return PaymentOptionHistory
     */
    public function setPrice(float $price): PaymentOptionHistory
    {
        $this->price = $price;
        return $this;
    }
}