<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


class Shipping
{
    public const ID = "id";
    public const DTA_ID = "dta_id";
    public const SHOW_ON_DEFAULT = "show_on_default";
    public const SHOW_TILL_MAX_WEIGHT = "show_till_max_weight";

    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $dtaId;
    /**
     * @var boolean
     */
    public $showOnDefault;
    /**
     * @var double|null
     */
    public $showTillMaxWeight;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Shipping
     */
    public function setId(int $id): Shipping
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getDtaId(): int
    {
        return $this->dtaId;
    }

    /**
     * @param int $dtaId
     * @return Shipping
     */
    public function setDtaId(int $dtaId): Shipping
    {
        $this->dtaId = $dtaId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnDefault(): bool
    {
        return $this->showOnDefault;
    }

    /**
     * @param bool $showOnDefault
     * @return Shipping
     */
    public function setShowOnDefault(bool $showOnDefault): Shipping
    {
        $this->showOnDefault = $showOnDefault;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getShowTillMaxWeight(): ?float
    {
        return $this->showTillMaxWeight;
    }

    /**
     * @param float|null $showTillMaxWeight
     * @return Shipping
     */
    public function setShowTillMaxWeight(?float $showTillMaxWeight): Shipping
    {
        $this->showTillMaxWeight = $showTillMaxWeight;
        return $this;
    }
}