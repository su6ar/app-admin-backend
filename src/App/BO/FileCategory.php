<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:07
 */

namespace Kominexpres\src\App\BO;


class FileCategory
{
    public const FCY_ID = "fcy_id";
    public const FCY_NAME = "fcy_name";

    /**
     * @var integer
     */
    public $fcyId;
    /**
     * @var string
     */
    public $fcyName;

    /**
     * @return int
     */
    public function getFcyId(): int
    {
        return $this->fcyId;
    }

    /**
     * @param int $fcyId
     * @return FileCategory
     */
    public function setFcyId(int $fcyId): FileCategory
    {
        $this->fcyId = $fcyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFcyName(): string
    {
        return $this->fcyName;
    }

    /**
     * @param string $fcyName
     * @return FileCategory
     */
    public function setFcyName(string $fcyName): FileCategory
    {
        $this->fcyName = $fcyName;
        return $this;
    }
}