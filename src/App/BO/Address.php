<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:05
 */

namespace Kominexpres\src\App\BO;


class Address
{
    /**
     * @var string
     */
    public $adsType;
    /**
     * @var integer
     */
    public $actId;
    /**
     * @var integer
     */
    public $adsId;

    /**
     * @return string
     */
    public function getAdsType(): string
    {
        return $this->adsType;
    }

    /**
     * @param string $adsType
     * @return Address
     */
    public function setAdsType(string $adsType): Address
    {
        $this->adsType = $adsType;
        return $this;
    }

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     * @return Address
     */
    public function setActId(int $actId): Address
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return int
     */
    public function getAdsId(): int
    {
        return $this->adsId;
    }

    /**
     * @param int $adsId
     * @return Address
     */
    public function setAdsId(int $adsId): Address
    {
        $this->adsId = $adsId;
        return $this;
    }
}