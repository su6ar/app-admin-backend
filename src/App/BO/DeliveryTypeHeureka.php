<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


class DeliveryTypeHeureka
{
    public const ID = "id";
    public const DTA_NAME = "dta_name";

    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $dtaName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DeliveryTypeHeureka
     */
    public function setId(int $id): DeliveryTypeHeureka
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDtaName(): string
    {
        return $this->dtaName;
    }

    /**
     * @param string $dtaName
     * @return DeliveryTypeHeureka
     */
    public function setDtaName(string $dtaName): DeliveryTypeHeureka
    {
        $this->dtaName = $dtaName;
        return $this;
    }
}