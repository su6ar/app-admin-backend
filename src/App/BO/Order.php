<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:10
 */

namespace Kominexpres\src\App\BO;


class Order
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer|null
     */
    public $actId;
    /**
     * @var int
     */
    public $pmtId;
    /**
     * @var integer
     */
    public $spgId;
    /**
     * @var boolean
     */
    public $deletedOrder;
    /**
     * @var string
     */
    public $odrDate;
    /**
     * @var string|null
     */
    public $note;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getActId(): ?int
    {
        return $this->actId;
    }

    /**
     * @param int|null $actId
     * @return Order
     */
    public function setActId(?int $actId): Order
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPmtId(): int
    {
        return $this->pmtId;
    }

    /**
     * @param int $pmtId
     * @return Order
     */
    public function setPmtId(int $pmtId): Order
    {
        $this->pmtId = $pmtId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpgId(): int
    {
        return $this->spgId;
    }

    /**
     * @param int $spgId
     * @return Order
     */
    public function setSpgId(int $spgId): Order
    {
        $this->spgId = $spgId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeletedOrder(): bool
    {
        return $this->deletedOrder;
    }

    /**
     * @param bool $deletedOrder
     * @return Order
     */
    public function setDeletedOrder(bool $deletedOrder): Order
    {
        $this->deletedOrder = $deletedOrder;
        return $this;
    }

    /**
     * @return string
     */
    public function getOdrDate(): string
    {
        return $this->odrDate;
    }

    /**
     * @param string $odrDate
     * @return Order
     */
    public function setOdrDate(string $odrDate): Order
    {
        $this->odrDate = $odrDate;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param null|string $note
     * @return Order
     */
    public function setNote(?string $note): Order
    {
        $this->note = $note;
        return $this;
    }
}