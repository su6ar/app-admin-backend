<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\POPO\POPOUtils;

class Category
{
    use POPOUtils;

    public const IAE_ID = "iae_id";
    public const CEY_ACTIVE = "cey_active";
    public const CEY_CEY_ID = "cey_cey_id";
    public const CEY_ID = "cey_id";
    public const CEY_PLACE = "cey_place";
    public const CEY_NAME = "cey_name";
    public const SHOW_PARAMETERS = "show_parameters";
    public const PGP_ID = "pgp_id";
    public const CEY_DESCRIPTION = "cey_description";

    /**
     * @var integer|null
     */
    public $iaeId;
    /**
     * @var integer
     */
    public $ceyId;
    /**
     * @var integer|null
     */
    public $ceyCeyId;
    /**
     * @var integer|null
     */
    public $pgpId;
    /**
     * @var string
     */
    public $ceyUrl;
    /**
     * @var string
     */
    public $ceyName;
    /**
     * @var integer
     */
    public $ceyPlace;
    /**
     * @var boolean
     */
    public $showParameters;
    /**
     * @var bool
     */
    public $ceyActive;
    /**
     * @var string|null
     */
    public $ceyDescription;

    /**
     * @return int|null
     */
    public function getIaeId(): ?int
    {
        return $this->iaeId;
    }

    /**
     * @param int|null $iaeId
     * @return Category
     */
    public function setIaeId(?int $iaeId): Category
    {
        $this->iaeId = $iaeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCeyId(): int
    {
        return $this->ceyId;
    }

    /**
     * @param int $ceyId
     * @return Category
     */
    public function setCeyId(int $ceyId): Category
    {
        $this->ceyId = $ceyId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCeyCeyId(): ?int
    {
        return $this->ceyCeyId;
    }

    /**
     * @param int|null $ceyCeyId
     * @return Category
     */
    public function setCeyCeyId(?int $ceyCeyId): Category
    {
        $this->ceyCeyId = $ceyCeyId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPgpId(): ?int
    {
        return $this->pgpId;
    }

    /**
     * @param int|null $pgpId
     * @return Category
     */
    public function setPgpId(?int $pgpId): Category
    {
        $this->pgpId = $pgpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCeyUrl(): string
    {
        return $this->ceyUrl;
    }

    /**
     * @param string $ceyUrl
     * @return Category
     */
    public function setCeyUrl(string $ceyUrl): Category
    {
        $this->ceyUrl = $ceyUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getCeyName(): string
    {
        return $this->ceyName;
    }

    /**
     * @param string $ceyName
     * @return Category
     */
    public function setCeyName(string $ceyName): Category
    {
        $this->ceyName = $ceyName;
        $this->setCeyUrl(self::generateUrl($ceyName));
        return $this;
    }

    /**
     * @return int
     */
    public function getCeyPlace(): int
    {
        return $this->ceyPlace;
    }

    /**
     * @param int $ceyPlace
     * @return Category
     */
    public function setCeyPlace(int $ceyPlace): Category
    {
        $this->ceyPlace = $ceyPlace;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowParameters(): bool
    {
        return $this->showParameters;
    }

    /**
     * @param bool $showParameters
     * @return Category
     */
    public function setShowParameters(bool $showParameters): Category
    {
        $this->showParameters = $showParameters;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCeyActive(): bool
    {
        return $this->ceyActive;
    }

    /**
     * @param bool $ceyActive
     * @return Category
     */
    public function setCeyActive(bool $ceyActive): Category
    {
        $this->ceyActive = $ceyActive;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCeyDescription(): ?string
    {
        return $this->ceyDescription;
    }

    /**
     * @param null|string $ceyDescription
     * @return Category
     */
    public function setCeyDescription(?string $ceyDescription): Category
    {
        $this->ceyDescription = $ceyDescription;
        return $this;
    }

    /**
     * @param $list
     * @return array
     */
    public static function generateList($list): array
    {
        $resList = [];
        $hashMap = [];

        for ($i = 0; $i < count($list); $i++)
        {
            $item = $list[$i];
            $item["items"] = [];
            $iaePath = $list[$i][Image::IAE_PATH];
            $iaeType = $list[$i][Image::IAE_TYPE];
            $ceyId = $list[$i][Category::CEY_ID];
            $ceyCeyId = $list[$i][Category::CEY_CEY_ID];
            $ceyPlace = $list[$i][Category::CEY_PLACE];
            $ceyName = $list[$i][Category::CEY_NAME];

            if ($ceyCeyId === null)
            {
                if (isset($item[CategoryGroupDiscount::DISCOUNT]))
                {
                    $item[CategoryGroupDiscount::DISCOUNT] = doubleval($item[CategoryGroupDiscount::DISCOUNT]);
                }
                $index = array_push($resList, $item) - 1;
                $hashMap[$ceyId] = [$index];
                unset($list[$i]);
                $list = array_values($list);
                $i = -1;
            }
            else
            {
                if (array_key_exists($ceyCeyId, $hashMap))
                {
                    $arrIndex = $hashMap[$ceyCeyId];
                    $index = '$resList';
                    foreach ($arrIndex as $in)
                    {
                        $index .= "[$in]['items']";
                    }
                    $pos = eval("return count($index);");
                    if (isset($list[$i][CategoryGroupDiscount::DISCOUNT]))
                    {
                        $discount = doubleval($list[$i][CategoryGroupDiscount::DISCOUNT]);
                        eval("array_push($index, ['cey_id' => $ceyId, 'cey_cey_id' => $ceyCeyId, 'cey_name' => '$ceyName', 'cey_place' => $ceyPlace, 'iae_path' => '$iaePath', 'iae_type' => '$iaeType', 'discount' => $discount, 'items' => []]);");
                    }
                    else
                    {
                        eval("array_push($index, ['cey_id' => $ceyId, 'cey_cey_id' => $ceyCeyId, 'cey_name' => '$ceyName', 'cey_place' => $ceyPlace, 'iae_path' => '$iaePath', 'iae_type' => '$iaeType', 'items' => []]);");
                    }
                    array_push($arrIndex, $pos);
                    $hashMap[$ceyId] = $arrIndex;
                    unset($list[$i]);
                    $list = array_values($list);
                    $i = -1;
                }
                else
                {
                    foreach ($list as $key => $item)
                    {
                        if ($ceyId == $ceyCeyId)
                        {
                            $i = $key;
                            break;
                        }
                    }
                }
            }
        }
        return $resList;
    }
}