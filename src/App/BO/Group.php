<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:07
 */

namespace Kominexpres\src\App\BO;


class Group
{
    public const GRP_ID = "grp_id";
    public const GRP_NAME = "grp_name";
    public const GRP_DEFAULT = "grp_default";
    public const GRP_ACTIVE = "grp_active";

    /**
     * @var integer
     */
    public $grpId;
    /**
     * @var string
     */
    public $grpName;
    /**
     * @var boolean
     */
    public $grpActive;
    /**
     * @var boolean|null
     */
    public $grpDefault;

    /**
     * @return int
     */
    public function getGrpId(): int
    {
        return $this->grpId;
    }

    /**
     * @param int $grpId
     * @return Group
     */
    public function setGrpId(int $grpId): Group
    {
        $this->grpId = $grpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getGrpName(): string
    {
        return $this->grpName;
    }

    /**
     * @param string $grpName
     * @return Group
     */
    public function setGrpName(string $grpName): Group
    {
        $this->grpName = $grpName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGrpActive(): bool
    {
        return $this->grpActive;
    }

    /**
     * @param bool $grpActive
     * @return Group
     */
    public function setGrpActive(bool $grpActive): Group
    {
        $this->grpActive = $grpActive;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isGrpDefault(): ?bool
    {
        return $this->grpDefault;
    }

    /**
     * @param bool|null $grpDefault
     * @return Group
     */
    public function setGrpDefault(?bool $grpDefault): Group
    {
        $this->grpDefault = $grpDefault;
        return $this;
    }
}