<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class ProductHistory
{
    public const PRICE_VAT = "price_vat";
    public const PRICE_NO_VAT = "price_no_vat";
    public const DATE_FROM = "date_from";
    public const ACT_ID = "act_id";
    public const PHY_NAME = "phy_name";
    public const WEIGHT = "weight";
    public const DATE_TO = "date_to";
    public const OPTION_TITLE = "option_title";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var integer
     */
    public $actId;
    /**
     * @var string
     */
    public $phyName;
    /**
     * @var double
     */
    public $priceVat;
    /**
     * @var double
     */
    public $priceNoVat;
    /**
     * @var double|null
     */
    public $weight;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var string|null
     */
    public $optionTitle;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductHistory
     */
    public function setPutId(int $putId): ProductHistory
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ProductHistory
     */
    public function setDateFrom(string $dateFrom): ProductHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     * @return ProductHistory
     */
    public function setActId(int $actId): ProductHistory
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhyName(): string
    {
        return $this->phyName;
    }

    /**
     * @param string $phyName
     * @return ProductHistory
     */
    public function setPhyName(string $phyName): ProductHistory
    {
        $this->phyName = $phyName;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceVat(): float
    {
        return $this->priceVat;
    }

    /**
     * @param float $priceVat
     * @return ProductHistory
     */
    public function setPriceVat(float $priceVat): ProductHistory
    {
        $this->priceVat = $priceVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceNoVat(): float
    {
        return $this->priceNoVat;
    }

    /**
     * @param float $priceNoVat
     * @return ProductHistory
     */
    public function setPriceNoVat(float $priceNoVat): ProductHistory
    {
        $this->priceNoVat = $priceNoVat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     * @return ProductHistory
     */
    public function setWeight(?float $weight): ProductHistory
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ProductHistory
     */
    public function setDateTo(?string $dateTo): ProductHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getOptionTitle(): ?string
    {
        return $this->optionTitle;
    }

    /**
     * @param null|string $optionTitle
     * @return ProductHistory
     */
    public function setOptionTitle(?string $optionTitle): ProductHistory
    {
        $this->optionTitle = $optionTitle;
        return $this;
    }
}