<?php
/**
 * Created by Daniel Bill
 * Date: 23.07.2018
 * Time: 15:34
 */

namespace Kominexpres\src\App\BO;


class ImageCategoryMarking
{
    /**
     * @var integer
     */
    public $icyId;
    /**
     * @var string
     */
    public $migId;

    /**
     * @return int
     */
    public function getIcyId(): int
    {
        return $this->icyId;
    }

    /**
     * @param int $icyId
     * @return ImageCategoryMarking
     */
    public function setIcyId(int $icyId): ImageCategoryMarking
    {
        $this->icyId = $icyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMigId(): string
    {
        return $this->migId;
    }

    /**
     * @param string $migId
     * @return ImageCategoryMarking
     */
    public function setMigId(string $migId): ImageCategoryMarking
    {
        $this->migId = $migId;
        return $this;
    }
}