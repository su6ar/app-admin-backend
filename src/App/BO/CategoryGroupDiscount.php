<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\Exceptions\DiscountValueException;
use Kominexpres\src\App\POPO\StatusObject;

class CategoryGroupDiscount
{
    public const DISCOUNT = "discount";

    /**
     * @var integer
     */
    public $grpId;
    /**
     * @var integer
     */
    public $ceyId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var double
     */
    public $discount;
    /**
     * @var string|null
     */
    public $dateTo;

    /**
     * @return int
     */
    public function getGrpId(): int
    {
        return $this->grpId;
    }

    /**
     * @param int $grpId
     * @return CategoryGroupDiscount
     */
    public function setGrpId(int $grpId): CategoryGroupDiscount
    {
        $this->grpId = $grpId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCeyId(): int
    {
        return $this->ceyId;
    }

    /**
     * @param int $ceyId
     * @return CategoryGroupDiscount
     */
    public function setCeyId(int $ceyId): CategoryGroupDiscount
    {
        $this->ceyId = $ceyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return CategoryGroupDiscount
     */
    public function setDateFrom(string $dateFrom): CategoryGroupDiscount
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     * @return CategoryGroupDiscount
     * @throws DiscountValueException
     */
    public function setDiscount(float $discount): CategoryGroupDiscount
    {
        if ($discount > 100)
            throw new DiscountValueException($discount);
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return CategoryGroupDiscount
     */
    public function setDateTo(?string $dateTo): CategoryGroupDiscount
    {
        $this->dateTo = $dateTo;
        return $this;
    }
}