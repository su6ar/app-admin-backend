<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


class CompanyDetails
{
    /**
     * @var integer
     */
    public $cdlId;
    /**
     * @var string
     */
    public $cdlName;
    /**
     * @var string
     */
    public $cdlTin;
    /**
     * @var string
     */
    public $cdlVatin;
    /**
     * @var string
     */
    public $websiteName;
    /**
     * @var string
     */
    public $websiteUrl;

    /**
     * @return int
     */
    public function getCdlId(): int
    {
        return $this->cdlId;
    }

    /**
     * @param int $cdlId
     * @return CompanyDetails
     */
    public function setCdlId(int $cdlId): CompanyDetails
    {
        $this->cdlId = $cdlId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCdlName(): string
    {
        return $this->cdlName;
    }

    /**
     * @param string $cdlName
     * @return CompanyDetails
     */
    public function setCdlName(string $cdlName): CompanyDetails
    {
        $this->cdlName = $cdlName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCdlTin(): string
    {
        return $this->cdlTin;
    }

    /**
     * @param string $cdlTin
     * @return CompanyDetails
     */
    public function setCdlTin(string $cdlTin): CompanyDetails
    {
        $this->cdlTin = $cdlTin;
        return $this;
    }

    /**
     * @return string
     */
    public function getCdlVatin(): string
    {
        return $this->cdlVatin;
    }

    /**
     * @param string $cdlVatin
     * @return CompanyDetails
     */
    public function setCdlVatin(string $cdlVatin): CompanyDetails
    {
        $this->cdlVatin = $cdlVatin;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsiteName(): string
    {
        return $this->websiteName;
    }

    /**
     * @param string $websiteName
     * @return CompanyDetails
     */
    public function setWebsiteName(string $websiteName): CompanyDetails
    {
        $this->websiteName = $websiteName;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsiteUrl(): string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string $websiteUrl
     * @return CompanyDetails
     */
    public function setWebsiteUrl(string $websiteUrl): CompanyDetails
    {
        $this->websiteUrl = $websiteUrl;
        return $this;
    }
}