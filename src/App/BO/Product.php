<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\POPO\POPOUtils;

class Product
{
    use POPOUtils;

    public const SYNC_FEED_STATUS = 'sync_feed_status';
    public const PUT_ID = "put_id";
    public const PGP_ID = "pgp_id";
    public const MUR_ID = "mur_id";
    public const CODE = "code";
    public const AVAILABILITY = "availability";
    public const ACTIVE = "active";
    public const BARCODE = "barcode";
    public const DESCRIPTION_SHORT = "description_short";
    public const DESCRIPTION_LONG = "description_long";
    public const WARRANTY = "warranty";
    public const DESCRIPTION_META = "description_meta";
    public const PUT_URL = "put_url";
    public const SELLABLE = "sellable";
    public const SALE = "sale";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer|null
     */
    public $pgpId;
    /**
     * @var integer
     */
    public $murId;
    /**
     * @var string
     */
    public $code;
    /**
     * @var integer
     */
    public $availability;
    /**
     * @var boolean
     */
    public $active;
    /**
     * @var string|null
     */
    public $barcode;
    /** @var boolean */
    public $syncFeedStatus;
    /**
     * @var string|null
     */
    public $descriptionShort;
    /**
     * @var string|null
     */
    public $descriptionLong;
    /**
     * @var integer|null
     */
    public $warranty;
    /**
     * @var string|null
     */
    public $descriptionMeta;
    /**
     * @var string
     */
    public $putUrl;
    /**
     * @var boolean
     */
    public $sellable;
    /**
     * @var boolean
     */
    public $sale;

    /**
     * @return bool
     */
    public function isSyncFeedStatus(): bool
    {
        return $this->syncFeedStatus;
    }

    /**
     * @param bool $syncFeedStatus
     * @return Product
     */
    public function setSyncFeedStatus(bool $syncFeedStatus): self
    {
        $this->syncFeedStatus = $syncFeedStatus;
        return $this;
    }

    /**
 * @return int
 */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return Product
     */
    public function setPutId(int $putId): Product
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPgpId(): ?int
    {
        return $this->pgpId;
    }

    /**
     * @param int|null $pgpId
     * @return Product
     */
    public function setPgpId(?int $pgpId): Product
    {
        $this->pgpId = $pgpId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMurId(): int
    {
        return $this->murId;
    }

    /**
     * @param int $murId
     * @return Product
     */
    public function setMurId(int $murId): Product
    {
        $this->murId = $murId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Product
     */
    public function setCode(string $code): Product
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return int
     */
    public function getAvailability(): int
    {
        return $this->availability;
    }

    /**
     * @param int $availability
     * @return Product
     */
    public function setAvailability(int $availability): Product
    {
        $this->availability = $availability;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Product
     */
    public function setActive(bool $active): Product
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    /**
     * @param null|string $barcode
     * @return Product
     */
    public function setBarcode(?string $barcode): Product
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescriptionShort(): ?string
    {
        return $this->descriptionShort;
    }

    /**
     * @param null|string $descriptionShort
     * @return Product
     */
    public function setDescriptionShort(?string $descriptionShort): Product
    {
        $this->descriptionShort = $descriptionShort;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescriptionLong(): ?string
    {
        return $this->descriptionLong;
    }

    /**
     * @param null|string $descriptionLong
     * @return Product
     */
    public function setDescriptionLong(?string $descriptionLong): Product
    {
        $this->descriptionLong = $descriptionLong;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWarranty(): ?int
    {
        return $this->warranty;
    }

    /**
     * @param int|null $warranty
     * @return Product
     */
    public function setWarranty(?int $warranty): Product
    {
        $this->warranty = $warranty;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescriptionMeta(): ?string
    {
        return $this->descriptionMeta;
    }

    /**
     * @param null|string $descriptionMeta
     * @return Product
     */
    public function setDescriptionMeta(?string $descriptionMeta): Product
    {
        $this->descriptionMeta = $descriptionMeta;
        return $this;
    }

    /**
     * @return string
     */
    public function getPutUrl(): string
    {
        return $this->putUrl;
    }

    /**
     * @param string $putUrl
     * @return Product
     */
    public function setPutUrl(string $putUrl): Product
    {
        $this->putUrl = $putUrl;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSellable(): bool
    {
        return $this->sellable;
    }

    /**
     * @param bool $sellable
     * @return Product
     */
    public function setSellable(bool $sellable): Product
    {
        $this->sellable = $sellable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSale(): bool
    {
        return $this->sale;
    }

    /**
     * @param bool $sale
     * @return Product
     */
    public function setSale(bool $sale): Product
    {
        $this->sale = $sale;
        return $this;
    }

    /**
     * @param array $json
     * @return Product
     */
    public static function createFromJson(array $json): Product
    {
        $item = new Product();
        $item->setWarranty($json[self::WARRANTY])
            ->setMurId($json[self::MUR_ID])
            ->setPutId($json[self::PUT_ID])
            ->setPgpId($json[self::PGP_ID])
            ->setPutUrl($json[self::PUT_URL])
            ->setSale($json[self::SALE])
            ->setSellable($json[self::SELLABLE])
            ->setDescriptionMeta($json[self::DESCRIPTION_META])
            ->setDescriptionLong($json[self::DESCRIPTION_LONG])
            ->setDescriptionShort($json[self::DESCRIPTION_SHORT])
            ->setBarcode($json[self::BARCODE])
            ->setCode($json[self::CODE])
            ->setActive($json[self::ACTIVE])
            ->setAvailability($json[self::AVAILABILITY])
            ->setSyncFeedStatus($json[self::SYNC_FEED_STATUS] ?? true);
        return $item;
    }
}