<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:05
 */

namespace Kominexpres\src\App\BO;


class AddressHistory
{
    /**
     * @var integer
     */
    public $adsId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string|null
     */
    public $street;
    /**
     * @var string|null
     */
    public $city;
    /**
     * @var string|null
     */
    public $zip;
    /**
     * @var string|null
     */
    public $country;
    /**
     * @var string|null
     */
    public $enterprise;
    /**
     * @var string|null
     */
    public $firstName;
    /**
     * @var string|null
     */
    public $lastName;
    /**
     * @var string|null
     */
    public $tin;
    /**
     * @var string|null
     */
    public $vatin;
    /**
     * @var string|null
     */
    public $cellnumber;
    /**
     * @var string|null
     */
    public $dateTo;

    /**
     * @return int
     */
    public function getAdsId(): int
    {
        return $this->adsId;
    }

    /**
     * @param int $adsId
     * @return AddressHistory
     */
    public function setAdsId(int $adsId): AddressHistory
    {
        $this->adsId = $adsId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return AddressHistory
     */
    public function setDateFrom(string $dateFrom): AddressHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param null|string $street
     * @return AddressHistory
     */
    public function setStreet(?string $street): AddressHistory
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param null|string $city
     * @return AddressHistory
     */
    public function setCity(?string $city): AddressHistory
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param null|string $zip
     * @return AddressHistory
     */
    public function setZip(?string $zip): AddressHistory
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param null|string $country
     * @return AddressHistory
     */
    public function setCountry(?string $country): AddressHistory
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getEnterprise(): ?string
    {
        return $this->enterprise;
    }

    /**
     * @param null|string $enterprise
     * @return AddressHistory
     */
    public function setEnterprise(?string $enterprise): AddressHistory
    {
        $this->enterprise = $enterprise;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     * @return AddressHistory
     */
    public function setFirstName(?string $firstName): AddressHistory
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     * @return AddressHistory
     */
    public function setLastName(?string $lastName): AddressHistory
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTin(): ?string
    {
        return $this->tin;
    }

    /**
     * @param null|string $tin
     * @return AddressHistory
     */
    public function setTin(?string $tin): AddressHistory
    {
        $this->tin = $tin;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getVatin(): ?string
    {
        return $this->vatin;
    }

    /**
     * @param null|string $vatin
     * @return AddressHistory
     */
    public function setVatin(?string $vatin): AddressHistory
    {
        $this->vatin = $vatin;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCellnumber(): ?string
    {
        return $this->cellnumber;
    }

    /**
     * @param null|string $cellnumber
     * @return AddressHistory
     */
    public function setCellnumber(?string $cellnumber): AddressHistory
    {
        $this->cellnumber = $cellnumber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return AddressHistory
     */
    public function setDateTo(?string $dateTo): AddressHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }
}