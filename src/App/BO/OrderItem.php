<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:09
 */

namespace Kominexpres\src\App\BO;


class OrderItem
{
    /**
     * @var integer
     */
    public $odrId;
    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer|null
     */
    public $pmnId;
    /**
     * @var integer
     */
    public $quantity;

    /**
     * @return int
     */
    public function getOdrId(): int
    {
        return $this->odrId;
    }

    /**
     * @param int $odrId
     * @return OrderItem
     */
    public function setOdrId(int $odrId): OrderItem
    {
        $this->odrId = $odrId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return OrderItem
     */
    public function setPutId(int $putId): OrderItem
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPmnId(): ?int
    {
        return $this->pmnId;
    }

    /**
     * @param int|null $pmnId
     * @return OrderItem
     */
    public function setPmnId(?int $pmnId): OrderItem
    {
        $this->pmnId = $pmnId;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return OrderItem
     */
    public function setQuantity(int $quantity): OrderItem
    {
        $this->quantity = $quantity;
        return $this;
    }
}