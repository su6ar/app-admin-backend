<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:10
 */

namespace Kominexpres\src\App\BO;


class Parameter
{
    public const PRR_ID = "prr_id";
    public const PRR_NAME = "prr_name";
    public const PRR_UNIT = "prr_unit";
    /**
     * @var integer
     */
    public $prrId;
    /**
     * @var string
     */
    public $prrName;
    /**
     * @var string|null
     */
    public $prrUnit;

    /**
     * @return int
     */
    public function getPrrId(): int
    {
        return $this->prrId;
    }

    /**
     * @param int $prrId
     * @return Parameter
     */
    public function setPrrId(int $prrId): Parameter
    {
        $this->prrId = $prrId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrrName(): string
    {
        return $this->prrName;
    }

    /**
     * @param string $prrName
     * @return Parameter
     */
    public function setPrrName(string $prrName): Parameter
    {
        $this->prrName = $prrName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPrrUnit(): ?string
    {
        return $this->prrUnit;
    }

    /**
     * @param null|string $prrUnit
     * @return Parameter
     */
    public function setPrrUnit(?string $prrUnit): Parameter
    {
        $this->prrUnit = $prrUnit;
        return $this;
    }
}