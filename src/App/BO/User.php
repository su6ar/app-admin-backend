<?php
/**
 * Created by Daniel Bill
 * Date: 08.07.2017
 * Time: 15:03
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\POPO\POPOUtils;

class User extends RegisteredUser
{
    use POPOUtils;

    const UER_ACTIVE = 'uer_active';
    const UER_DATE_CREATED = 'uer_date_created';
    const UER_NEWSLETTER = 'uer_newsletter';
    const UER_CODE_ACTIVATION = 'uer_code_activation';

    /** @var bool */
    private $uerActive;

    /** @var string */
    private $uerDateCreated;

    /** @var bool */
    private $uerNewsletter;

    /** @var string */
    private $uerCodeActivation;

    /**
     * @return bool
     */
    public function isUerActive(): bool
    {
        return $this->uerActive;
    }

    /**
     * @param bool $uerActive
     * @return $this
     */
    public function setUerActive(bool $uerActive)
    {
        $this->uerActive = $uerActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getUerDateCreated(): string
    {
        return $this->uerDateCreated;
    }

    /**
     * @param string $uerDateCreated
     * @return $this
     */
    public function setUerDateCreated(string $uerDateCreated)
    {
        $this->uerDateCreated = $uerDateCreated;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUerNewsletter(): bool
    {
        return $this->uerNewsletter;
    }

    /**
     * @param bool $uerNewsletter
     * @return $this
     */
    public function setUerNewsletter(bool $uerNewsletter)
    {
        $this->uerNewsletter = $uerNewsletter;
        return $this;
    }

    /**
     * @return string
     */
    public function getUerCodeActivation(): string
    {
        return $this->uerCodeActivation;
    }

    /**
     * @param string $uerCodeActivation
     * @return $this
     */
    public function setUerCodeActivation(string $uerCodeActivation)
    {
        $this->uerCodeActivation = $uerCodeActivation;
        return $this;
    }
}