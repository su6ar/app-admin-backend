<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductTypeOption
{
    /**
     * @var integer
     */
    public $pteId;
    /**
     * @var integer
     */
    public $putId;

    /**
     * @return int
     */
    public function getPteId(): int
    {
        return $this->pteId;
    }

    /**
     * @param int $pteId
     * @return ProductTypeOption
     */
    public function setPteId(int $pteId): ProductTypeOption
    {
        $this->pteId = $pteId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductTypeOption
     */
    public function setPutId(int $putId): ProductTypeOption
    {
        $this->putId = $putId;
        return $this;
    }
}