<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:05
 */

namespace Kominexpres\src\App\BO;


class ArticleHistory
{
    public const AHY_CONTENT = "ahy_content";

    /**
     * @var integer
     */
    public $ateId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var integer
     */
    public $actId;
    /**
     * @var string
     */
    public $ahyContent;
    /**
     * @var string|null
     */
    public $dateTo;

    /**
     * @return int
     */
    public function getAteId(): int
    {
        return $this->ateId;
    }

    /**
     * @param int $ateId
     * @return ArticleHistory
     */
    public function setAteId(int $ateId): ArticleHistory
    {
        $this->ateId = $ateId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ArticleHistory
     */
    public function setDateFrom(string $dateFrom): ArticleHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     * @return ArticleHistory
     */
    public function setActId(int $actId): ArticleHistory
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAhyContent(): string
    {
        return $this->ahyContent;
    }

    /**
     * @param string $ahyContent
     * @return ArticleHistory
     */
    public function setAhyContent(string $ahyContent): ArticleHistory
    {
        $this->ahyContent = $ahyContent;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ArticleHistory
     */
    public function setDateTo(?string $dateTo): ArticleHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }
}