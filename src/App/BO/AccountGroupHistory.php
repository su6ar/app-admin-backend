<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 13:50
 */

namespace Kominexpres\src\App\BO;


/**
 * Class AccountGroupHistory
 * @package Kominexpres\src\App\BO
 */
class AccountGroupHistory
{
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var int
     */
    public $grpId;
    /**
     * @var int
     */
    public $actId;
    /**
     * @var string|null
     */
    public $dateTo;


    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     */
    public function setDateFrom(string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return int
     */
    public function getGrpId(): int
    {
        return $this->grpId;
    }

    /**
     * @param int $grpId
     */
    public function setGrpId(int $grpId): void
    {
        $this->grpId = $grpId;
    }

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     */
    public function setActId(int $actId): void
    {
        $this->actId = $actId;
    }

    /**
     * @return string|null
     */
    public function getDateTo() : ?string
    {
        return $this->dateTo;
    }

    /**
     * @param string|null $dateTo
     */
    public function setDateTo(?string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }
}