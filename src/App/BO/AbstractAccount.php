<?php
/**
 * Created by Danie
 * Date: 03.07.2017
 * Time: 20:38
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\Exceptions\JsonInvalidFormatException;
use Kominexpres\src\App\POPO\JsonDeserializable;
use Kominexpres\src\App\POPO\POPOUtils;

abstract class AbstractAccount
{
    use POPOUtils;

    const ACT_ID = 'act_id';
    const ACT_EMAIL = 'act_email';
    const ACT_TYPE = 'act_type';

    /** @var int */
    private $actId;

    /** @var string */
    private $actEmail;

    /** @var string */
    private $actType;

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     * @return $this
     */
    public function setActId(?int $actId)
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return string
     */
    public function getActEmail(): string
    {
        return $this->actEmail;
    }

    /**
     * @param string $actEmail
     * @return $this
     */
    public function setActEmail(string $actEmail)
    {
        $this->actEmail = strtolower($actEmail);
        return $this;
    }

    /**
     * @return string
     */
    public function getActType(): string
    {
        return $this->actType;
    }

    /**
     * @param string $actType
     * @return $this
     */
    public function setActType(?string $actType)
    {
        $this->actType = $actType;
        return $this;
    }
}