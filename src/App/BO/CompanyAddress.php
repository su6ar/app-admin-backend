<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


class CompanyAddress
{
    /**
     * @var integer
     */
    public $cdlId;
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $street;
    /**
     * @var string
     */
    public $city;
    /**
     * @var string
     */
    public $zip;
    /**
     * @var string
     */
    public $country;
    /**
     * @var boolean
     */
    public $isHeadquarters;
    /**
     * @var string
     */
    public $name;

    /**
     * @return int
     */
    public function getCdlId(): int
    {
        return $this->cdlId;
    }

    /**
     * @param int $cdlId
     * @return CompanyAddress
     */
    public function setCdlId(int $cdlId): CompanyAddress
    {
        $this->cdlId = $cdlId;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CompanyAddress
     */
    public function setId(int $id): CompanyAddress
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return CompanyAddress
     */
    public function setStreet(string $street): CompanyAddress
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return CompanyAddress
     */
    public function setCity(string $city): CompanyAddress
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return CompanyAddress
     */
    public function setZip(string $zip): CompanyAddress
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return CompanyAddress
     */
    public function setCountry(string $country): CompanyAddress
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHeadquarters(): bool
    {
        return $this->isHeadquarters;
    }

    /**
     * @param bool $isHeadquarters
     * @return CompanyAddress
     */
    public function setIsHeadquarters(bool $isHeadquarters): CompanyAddress
    {
        $this->isHeadquarters = $isHeadquarters;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CompanyAddress
     */
    public function setName(string $name): CompanyAddress
    {
        $this->name = $name;
        return $this;
    }
}