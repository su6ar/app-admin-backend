<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


class ShippingHistory
{
    public const SPG_ID = "spg_id";
    public const SHY_NAME = "shy_name";
    public const DATE_FROM = "date_from";
    public const FREE_PRICE_ABOVE = "free_price_above";
    public const DATE_TO = "date_to";
    public const SUM_WEIGHT_PRICE = "sum_weight_price";
    public const SHY_ID = "shy_id";

    /**
     * @var integer
     */
    public $spgId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var double|null
     */
    public $freePriceAbove;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var boolean
     */
    public $sumWeightPrice;
    /**
     * @var integer
     */
    public $shyId;
    /**
     * @var string
     */
    public $shyName;

    /**
     * @return string
     */
    public function getShyName(): string
    {
        return $this->shyName;
    }

    /**
     * @param string $shyName
     * @return ShippingHistory
     */
    public function setShyName(string $shyName): ShippingHistory
    {
        $this->shyName = $shyName;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpgId(): int
    {
        return $this->spgId;
    }

    /**
     * @param int $spgId
     * @return ShippingHistory
     */
    public function setSpgId(int $spgId): ShippingHistory
    {
        $this->spgId = $spgId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ShippingHistory
     */
    public function setDateFrom(string $dateFrom): ShippingHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFreePriceAbove(): ?float
    {
        return $this->freePriceAbove;
    }

    /**
     * @param float|null $freePriceAbove
     * @return ShippingHistory
     */
    public function setFreePriceAbove(?float $freePriceAbove): ShippingHistory
    {
        $this->freePriceAbove = $freePriceAbove;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ShippingHistory
     */
    public function setDateTo(?string $dateTo): ShippingHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSumWeightPrice(): bool
    {
        return $this->sumWeightPrice;
    }

    /**
     * @param bool $sumWeightPrice
     * @return ShippingHistory
     */
    public function setSumWeightPrice(bool $sumWeightPrice): ShippingHistory
    {
        $this->sumWeightPrice = $sumWeightPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getShyId(): int
    {
        return $this->shyId;
    }

    /**
     * @param int $shyId
     * @return ShippingHistory
     */
    public function setShyId(int $shyId): ShippingHistory
    {
        $this->shyId = $shyId;
        return $this;
    }
}