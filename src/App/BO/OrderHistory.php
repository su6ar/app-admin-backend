<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:09
 */

namespace Kominexpres\src\App\BO;


class OrderHistory
{
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var integer
     */
    public $odrId;
    /**
     * @var integer|null
     */
    public $actId;
    /**
     * @var string
     */
    public $status;
    /**
     * @var string
     */
    public $dateTo;

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return OrderHistory
     */
    public function setDateFrom(string $dateFrom): OrderHistory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return int
     */
    public function getOdrId(): int
    {
        return $this->odrId;
    }

    /**
     * @param int $odrId
     * @return OrderHistory
     */
    public function setOdrId(int $odrId): OrderHistory
    {
        $this->odrId = $odrId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getActId(): ?int
    {
        return $this->actId;
    }

    /**
     * @param int|null $actId
     * @return OrderHistory
     */
    public function setActId(?int $actId): OrderHistory
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return OrderHistory
     */
    public function setStatus(string $status): OrderHistory
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateTo(): string
    {
        return $this->dateTo;
    }

    /**
     * @param string $dateTo
     * @return OrderHistory
     */
    public function setDateTo(string $dateTo): OrderHistory
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @param string $status
     * @param int $odrId
     * @return array
     */
    public static function getStatusWord(string $status, int $odrId): array
    {
        switch ($status)
        {
            case '0':
                return [
                    "subject" => "Storno objednávky č. {$odrId} - Kominexpres.cz",
                    "description" => "Objednávka byla stornována",
                    "status" => "Storno"
                ];
            case '1':
                return [
                    "subject" => "Přijatá objednávka č. {$odrId} - Kominexpres.cz",
                    "description" => "Objednávka byla přijatá",
                    "status" => "Přijatá"
                ];
            case '2':
                return [
                    "subject" => "Objednávka s č. {$odrId} se vyřizuje - Kominexpres.cz",
                    "description" => "Objednávka se vyřizuje",
                    "status" => "Vyřizuje se"
                ];
            case '3':
                return [
                    "subject" => "Odeslaná objednávka č. {$odrId} - Kominexpres.cz",
                    "description" => "Objednávka byla odeslána",
                    "status" => "Odeslaná"
                ];
            case '4':
                return [
                    "subject" => "Vyřízená objednávka č. {$odrId} - Kominexpres.cz",
                    "description" => "Objednávka byla vyřízená",
                    "status" => "Vyřízená"
                ];
            default:
                return [];
        }
    }
}