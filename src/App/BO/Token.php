<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


use Firebase\JWT\JWT;
use Tuupola\Base62;

class Token
{
    /**
     * @var string
     */
    public $jwt;
    /**
     * @var boolean
     */
    public $invalid;
    /**
     * @var string
     */
    public $sub;
    /**
     * @var integer
     */
    public $id;

    private $decoded;
    private $iat;
    private $exp;
    private $jti;
    private $scope;

    /**
     * @return string
     */
    public function getJwt(): string
    {
        return $this->jwt;
    }

    /**
     * @param string $jwt
     * @return Token
     */
    public function setJwt(string $jwt): Token
    {
        $this->jwt = $jwt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInvalid(): bool
    {
        return $this->invalid;
    }

    /**
     * @param bool $invalid
     * @return Token
     */
    public function setInvalid(bool $invalid): Token
    {
        $this->invalid = $invalid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSub(): string
    {
        return $this->sub;
    }

    /**
     * @param string $sub
     * @return Token
     */
    public function setSub(string $sub): Token
    {
        $this->sub = $sub;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Token
     */
    public function setId(int $id): Token
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIat()
    {
        return $this->iat;
    }

    /**
     * @param mixed $iat
     * @return Token
     */
    public function setIat($iat): Token
    {
        $this->iat = $iat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @param mixed $exp
     * @return Token
     */
    public function setExp($exp): Token
    {
        $this->exp = $exp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJti()
    {
        return $this->jti;
    }

    /**
     * @param mixed $jti
     * @return Token
     */
    public function setJti($jti): Token
    {
        $this->jti = $jti;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     * @return Token
     */
    public function setScope($scope): Token
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return [
            "iat" => $this->getIat(),
            "exp" => $this->getExp(),
            "jti" => $this->getJti(),
            "sub" => $this->getSub(),
            "scope" => $this->getScope()
        ];
    }

    /**
     * @param array $scope
     * @return bool
     */
    public function hasScope(array $scope): bool
    {
        return !!count(array_intersect($scope, $this->decoded->scope));
    }

    /**
     * @param string $email
     * @param array $scope
     * @return Token
     * @throws \Exception
     */
    public static function createToken(string $email, array $scope): Token
    {
        $token = new Token();
        $now = new \DateTime();
        $future = new \DateTime("now +3 weeks");
        $token->setIat($now->getTimestamp())
            ->setInvalid(false)
            ->setExp($future->getTimestamp())
            ->setJti((new Base62())->encode(random_bytes(16)))
            ->setSub($email)
            ->setScope($scope)
            ->setJwt(JWT::encode($token->getPayload(), getenv("SECRET_KEY"), "HS256"));

        return $token;
    }

    /**
     * @param $decoded
     */
    public function setDecoded($decoded)
    {
        $this->decoded = $decoded;
        $this->setIat($decoded->iat)
            ->setExp($decoded->exp)
            ->setJti($decoded->jti)
            ->setSub($decoded->sub)
            ->setScope($decoded->scope)
            ->setJwt(JWT::encode($this->getPayload(), getenv("SECRET_KEY"), "HS256"));
    }
}