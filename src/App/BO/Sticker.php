<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


class Sticker
{
    /**
     * @var integer
     */
    public $serId;
    /**
     * @var string
     */
    public $serName;
    /**
     * @var string
     */
    public $serBackgroundColor;
    /**
     * @var string
     */
    public $serTextColor;
    /**
     * @var string
     */
    public $serPosition;
    /**
     * @var integer
     */
    public $serPlace;

    /**
     * @return int
     */
    public function getSerId(): int
    {
        return $this->serId;
    }

    /**
     * @param int $serId
     * @return Sticker
     */
    public function setSerId(int $serId): Sticker
    {
        $this->serId = $serId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerName(): string
    {
        return $this->serName;
    }

    /**
     * @param string $serName
     * @return Sticker
     */
    public function setSerName(string $serName): Sticker
    {
        $this->serName = $serName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerBackgroundColor(): string
    {
        return $this->serBackgroundColor;
    }

    /**
     * @param string $serBackgroundColor
     * @return Sticker
     */
    public function setSerBackgroundColor(string $serBackgroundColor): Sticker
    {
        $this->serBackgroundColor = $serBackgroundColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerTextColor(): string
    {
        return $this->serTextColor;
    }

    /**
     * @param string $serTextColor
     * @return Sticker
     */
    public function setSerTextColor(string $serTextColor): Sticker
    {
        $this->serTextColor = $serTextColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSerPosition(): string
    {
        return $this->serPosition;
    }

    /**
     * @param string $serPosition
     * @return Sticker
     */
    public function setSerPosition(string $serPosition): Sticker
    {
        $this->serPosition = $serPosition;
        return $this;
    }

    /**
     * @return int
     */
    public function getSerPlace(): int
    {
        return $this->serPlace;
    }

    /**
     * @param int $serPlace
     * @return Sticker
     */
    public function setSerPlace(int $serPlace): Sticker
    {
        $this->serPlace = $serPlace;
        return $this;
    }
}