<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:10
 */

namespace Kominexpres\src\App\BO;


class ParameterGroupParameter
{
    /**
     * @var integer
     */
    public $prrId;
    /**
     * @var integer
     */
    public $pgpId;

    /**
     * @return int
     */
    public function getPrrId(): int
    {
        return $this->prrId;
    }

    /**
     * @param int $prrId
     * @return ParameterGroupParameter
     */
    public function setPrrId(int $prrId): ParameterGroupParameter
    {
        $this->prrId = $prrId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPgpId(): int
    {
        return $this->pgpId;
    }

    /**
     * @param int $pgpId
     * @return ParameterGroupParameter
     */
    public function setPgpId(int $pgpId): ParameterGroupParameter
    {
        $this->pgpId = $pgpId;
        return $this;
    }
}