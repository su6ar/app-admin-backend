<?php
/**
 * Created by Daniel Bill
 * Date: 04.07.2017
 * Time: 16:34
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\Exceptions\AccountVerificationException;
use Kominexpres\src\App\POPO\POPOUtils;

class Administrator extends RegisteredUser
{
    use POPOUtils;

    const AMR_RIGHTS = 'amr_rights';
    const AMR_DELETED_ADMIN_USER = 'amr_deleted_admin_user';
    const AMR_DELETED_ORDER_SHOW = 'amr_deleted_order_show';
    const AMR_AVATAR = 'amr_avatar';
    const AMR_FIRST_NAME = 'amr_first_name';
    const AMR_LAST_NAME = 'amr_last_name';
    const AMR_PRIMARY_COLOR = 'amr_primary_color';
    const AMR_HEADER_STYLE = 'amr_header_style';

    const ACT_TYPE_NAME_VALUE = 'AMR';
    const AMR_RIGHTS_DEFAULT_VALUE = 0;
    const AMR_DELETED_ADMIN_USER_DEFAULT_VALUE = true;
    const AMR_DELETED_ORDER_SHOW_DEFAULT_VALUE = true;

    public const USER_BASIC = "user.basic";
    public const USER_SUPER = "user.super";
    public const USER_GUEST = "user.guest";

    /** @var string */
    private $amrRights;

    /** @var bool */
    private $amrDeletedAdminUser;

    /** @var bool */
    private $amrDeletedOrderShow;

    /** @var string */
    private $amrAvatar;

    /** @var string */
    private $amrFirstName;

    /** @var string */
    private $amrLastName;

    /** @var string|null */
    private $amrPrimaryColor;

    /** @var string|null */
    private $amrHeaderStyle;

    public function __construct()
    {
        parent::setActType(self::ACT_TYPE_NAME_VALUE);
        $this->setAmrRights(self::AMR_RIGHTS_DEFAULT_VALUE);
        $this->setAmrDeletedAdminUser(self::AMR_DELETED_ADMIN_USER_DEFAULT_VALUE);
        $this->setAmrDeletedOrderShow(self::AMR_DELETED_ORDER_SHOW_DEFAULT_VALUE);
        $this->setAmrPrimaryColor(self::AMR_PRIMARY_COLOR);
        $this->setAmrHeaderStyle(self::AMR_HEADER_STYLE);
    }

    /**
     * @return null|string
     */
    public function getAmrPrimaryColor(): ?string
    {
        return $this->amrPrimaryColor;
    }

    /**
     * @param null|string $amrPrimaryColor
     * @return Administrator
     */
    public function setAmrPrimaryColor(?string $amrPrimaryColor): Administrator
    {
        $this->amrPrimaryColor = $amrPrimaryColor;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAmrHeaderStyle(): ?string
    {
        return $this->amrHeaderStyle;
    }

    /**
     * @param null|string $amrHeaderStyle
     * @return Administrator
     */
    public function setAmrHeaderStyle(?string $amrHeaderStyle): Administrator
    {
        $this->amrHeaderStyle = $amrHeaderStyle;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmrRights(): string
    {
        return $this->amrRights;
    }

    /**
     * @param string $amrRights
     * @return $this
     */
    public function setAmrRights(string $amrRights)
    {
        $this->amrRights = $amrRights;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAmrDeletedAdminUser(): bool
    {
        return $this->amrDeletedAdminUser;
    }

    /**
     * @param bool $amrDeletedAdminUser
     * @return $this
     */
    public function setAmrDeletedAdminUser(bool $amrDeletedAdminUser)
    {
        $this->amrDeletedAdminUser = $amrDeletedAdminUser;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAmrDeletedOrderShow(): bool
    {
        return $this->amrDeletedOrderShow;
    }

    /**
     * @param bool $amrDeletedOrderShow
     * @return $this
     */
    public function setAmrDeletedOrderShow(bool $amrDeletedOrderShow)
    {
        $this->amrDeletedOrderShow = $amrDeletedOrderShow;
        return $this;
    }

    /**
     * @return bool
     */
    public function getAmrDeletedAdminUser()
    {
        return $this->amrDeletedAdminUser;
    }

    /**
     * @return bool
     */
    public function getAmrDeletedOrderShow()
    {
        return $this->amrDeletedOrderShow;
    }

    /**
     * @return string
     */
    public function getAmrAvatar(): string
    {
        return $this->amrAvatar;
    }

    /**
     * @param string $amrAvatar
     * @return $this
     */
    public function setAmrAvatar(string $amrAvatar)
    {
        $this->amrAvatar = $amrAvatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmrFirstName(): string
    {
        return $this->amrFirstName;
    }

    /**
     * @param string $amrFirstName
     * @return $this
     */
    public function setAmrFirstName(string $amrFirstName)
    {
        $this->amrFirstName = $amrFirstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmrLastName(): string
    {
        return $this->amrLastName;
    }

    /**
     * @param string $amrLastName
     * @return $this
     */
    public function setAmrLastName(string $amrLastName)
    {
        $this->amrLastName = $amrLastName;
        return $this;
    }

    /**
     * @param string $password
     * @throws AccountVerificationException
     */
    public function verifyPassword(string $password): void
    {
        if (!password_verify($password, $this->getRurPassword()))
            throw new AccountVerificationException();
    }

    /**
     * @param string $rights
     * @return array
     */
    public static function getScope(string $rights): array
    {
        switch ($rights)
        {
            case '1':
                $scope = [self::USER_BASIC];
                break;
            case '2':
                $scope = [self::USER_SUPER];
                break;
            default:
                $scope = [self::USER_GUEST];
                break;
        }
        return $scope;
    }

    /**
     * @param array $json
     * @throws \Kominexpres\src\App\Exceptions\JsonInvalidFormatException
     */
    public static function validateUserCredentialsJson(array $json)
    {
        self::jsonValidation($json, [self::RUR_PASSWORD, self::ACT_EMAIL]);
    }

    public static function createFromJson($json)
    {
        $administrator = new Administrator();
        $administrator->setActId($json[parent::ACT_ID])
            ->setAmrDeletedOrderShow($json[self::AMR_DELETED_ORDER_SHOW] === true ? $json[self::AMR_DELETED_ORDER_SHOW] : false)
            ->setAmrDeletedAdminUser($json[self::AMR_DELETED_ADMIN_USER] === true ? $json[self::AMR_DELETED_ADMIN_USER] : false)
            ->setAmrRights($json[self::AMR_RIGHTS])
            ->setActType($json[parent::ACT_TYPE])
            ->setAmrLastName($json[self::AMR_LAST_NAME])
            ->setAmrFirstName($json[self::AMR_FIRST_NAME])
            ->setActEmail($json[parent::ACT_EMAIL])
            ->setRurPassword($json[self::RUR_PASSWORD])
            ->setAmrAvatar($json[self::AMR_AVATAR])
            ->setAmrHeaderStyle($json[self::AMR_HEADER_STYLE])
            ->setAmrPrimaryColor($json[self::AMR_PRIMARY_COLOR])
            ->setRurCodeRefresh($json[self::RUR_CODE_REFRESH]);
        return $administrator;
    }
}