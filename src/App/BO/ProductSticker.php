<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductSticker
{
    /**
     * @var integer
     */
    public $serId;
    /**
     * @var integer
     */
    public $putId;

    /**
     * @return int
     */
    public function getSerId(): int
    {
        return $this->serId;
    }

    /**
     * @param int $serId
     * @return ProductSticker
     */
    public function setSerId(int $serId): ProductSticker
    {
        $this->serId = $serId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductSticker
     */
    public function setPutId(int $putId): ProductSticker
    {
        $this->putId = $putId;
        return $this;
    }
}