<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:10
 */

namespace Kominexpres\src\App\BO;


class OrderNote
{
    public const ODR_ID = "odr_id";
    public const ONE_DATE = "one_date";
    public const ACT_ID = "act_id";
    public const TITLE = "title";
    public const ONE_CONTENT = "one_content";
    /**
     * @var integer
     */
    public $odrId;
    /**
     * @var string
     */
    public $oneDate;
    /**
     * @var integer
     */
    public $actId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $oneContent;

    /**
     * @return int
     */
    public function getOdrId(): int
    {
        return $this->odrId;
    }

    /**
     * @param int $odrId
     * @return OrderNote
     */
    public function setOdrId(int $odrId): OrderNote
    {
        $this->odrId = $odrId;
        return $this;
    }

    /**
     * @return string
     */
    public function getOneDate(): string
    {
        return $this->oneDate;
    }

    /**
     * @param string $oneDate
     * @return OrderNote
     */
    public function setOneDate(string $oneDate): OrderNote
    {
        $this->oneDate = $oneDate;
        return $this;
    }

    /**
     * @return int
     */
    public function getActId(): int
    {
        return $this->actId;
    }

    /**
     * @param int $actId
     * @return OrderNote
     */
    public function setActId(int $actId): OrderNote
    {
        $this->actId = $actId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return OrderNote
     */
    public function setTitle(string $title): OrderNote
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getOneContent(): string
    {
        return $this->oneContent;
    }

    /**
     * @param string $oneContent
     * @return OrderNote
     */
    public function setOneContent(string $oneContent): OrderNote
    {
        $this->oneContent = $oneContent;
        return $this;
    }
}