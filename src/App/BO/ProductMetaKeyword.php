<?php
/**
 * Created by Daniel Bill
 * Date: 28.08.2018
 * Time: 19:53
 */

namespace Kominexpres\src\App\BO;


class ProductMetaKeyword
{
    public const PUT_ID = "put_id";
    public const MKD_ID = "mkd_id";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $mkdId;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductMetaKeyword
     */
    public function setPutId(int $putId): ProductMetaKeyword
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMkdId(): int
    {
        return $this->mkdId;
    }

    /**
     * @param int $mkdId
     * @return ProductMetaKeyword
     */
    public function setMkdId(int $mkdId): ProductMetaKeyword
    {
        $this->mkdId = $mkdId;
        return $this;
    }
}