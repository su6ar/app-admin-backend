<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class ProductGroupDiscount
{
    public const PUT_ID = "put_id";
    public const GRP_ID = "grp_id";
    public const DATE_FROM = "date_from";
    public const DISCOUNT = "discount";
    public const DATE_TO = "date_to";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $grpId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var double
     */
    public $discount;
    /**
     * @var string|null
     */
    public $dateTo;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductGroupDiscount
     */
    public function setPutId(int $putId): ProductGroupDiscount
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGrpId(): int
    {
        return $this->grpId;
    }

    /**
     * @param int $grpId
     * @return ProductGroupDiscount
     */
    public function setGrpId(int $grpId): ProductGroupDiscount
    {
        $this->grpId = $grpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ProductGroupDiscount
     */
    public function setDateFrom(string $dateFrom): ProductGroupDiscount
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     * @return ProductGroupDiscount
     */
    public function setDiscount(float $discount): ProductGroupDiscount
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ProductGroupDiscount
     */
    public function setDateTo(?string $dateTo): ProductGroupDiscount
    {
        $this->dateTo = $dateTo;
        return $this;
    }
}