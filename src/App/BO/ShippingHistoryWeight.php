<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


class ShippingHistoryWeight
{
    public const SHY_ID = "shy_id";
    public const WEIGHT_MIN = "weight_min";
    public const WEIGHT_MAX = "weight_max";
    public const PRICE = "price";
    public const ACTIVE = "active";
    public const DATE_FROM = "date_from";
    public const DATE_TO = "date_to";
    public const SHT_ID = "sht_id";

    /**
     * @var integer
     */
    public $shyId;
    /**
     * @var int
     */
    public $weightMin;
    /**
     * @var int
     */
    public $weightMax;
    /**
     * @var double
     */
    public $price;
    /**
     * @var boolean
     */
    public $active;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var integer
     */
    public $shtId;

    /**
     * @return int
     */
    public function getShyId(): int
    {
        return $this->shyId;
    }

    /**
     * @param int $shyId
     * @return ShippingHistoryWeight
     */
    public function setShyId(int $shyId): ShippingHistoryWeight
    {
        $this->shyId = $shyId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeightMin(): int
    {
        return $this->weightMin;
    }

    /**
     * @param int $weightMin
     * @return ShippingHistoryWeight
     */
    public function setWeightMin(int $weightMin): ShippingHistoryWeight
    {
        $this->weightMin = $weightMin;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeightMax(): int
    {
        return $this->weightMax;
    }

    /**
     * @param int $weightMax
     * @return ShippingHistoryWeight
     */
    public function setWeightMax(int $weightMax): ShippingHistoryWeight
    {
        $this->weightMax = $weightMax;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ShippingHistoryWeight
     */
    public function setPrice(float $price): ShippingHistoryWeight
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return ShippingHistoryWeight
     */
    public function setActive(bool $active): ShippingHistoryWeight
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ShippingHistoryWeight
     */
    public function setDateFrom(string $dateFrom): ShippingHistoryWeight
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param string $dateTo
     * @return ShippingHistoryWeight
     */
    public function setDateTo(?string $dateTo): ShippingHistoryWeight
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getShtId(): int
    {
        return $this->shtId;
    }

    /**
     * @param int $shtId
     * @return ShippingHistoryWeight
     */
    public function setShtId(int $shtId): ShippingHistoryWeight
    {
        $this->shtId = $shtId;
        return $this;
    }
}