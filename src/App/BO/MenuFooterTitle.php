<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:08
 */

namespace Kominexpres\src\App\BO;


class MenuFooterTitle
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $mfeName;
    /**
     * @var integer
     */
    public $place;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MenuFooterTitle
     */
    public function setId(int $id): MenuFooterTitle
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMfeName(): string
    {
        return $this->mfeName;
    }

    /**
     * @param string $mfeName
     * @return MenuFooterTitle
     */
    public function setMfeName(string $mfeName): MenuFooterTitle
    {
        $this->mfeName = $mfeName;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlace(): int
    {
        return $this->place;
    }

    /**
     * @param int $place
     * @return MenuFooterTitle
     */
    public function setPlace(int $place): MenuFooterTitle
    {
        $this->place = $place;
        return $this;
    }
}