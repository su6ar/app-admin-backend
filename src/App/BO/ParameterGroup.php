<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:10
 */

namespace Kominexpres\src\App\BO;


class ParameterGroup
{
    public const PGP_ID = "pgp_id";
    public const PGP_NAME = "pgp_name";

    /**
     * @var integer
     */
    public $pgpId;
    /**
     * @var string
     */
    public $pgpName;

    /**
     * @return int
     */
    public function getPgpId(): int
    {
        return $this->pgpId;
    }

    /**
     * @param int $pgpId
     * @return ParameterGroup
     */
    public function setPgpId(int $pgpId): ParameterGroup
    {
        $this->pgpId = $pgpId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPgpName(): string
    {
        return $this->pgpName;
    }

    /**
     * @param string $pgpName
     * @return ParameterGroup
     */
    public function setPgpName(string $pgpName): ParameterGroup
    {
        $this->pgpName = $pgpName;
        return $this;
    }
}