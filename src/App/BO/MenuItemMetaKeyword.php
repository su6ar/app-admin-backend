<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:08
 */

namespace Kominexpres\src\App\BO;


class MenuItemMetaKeyword
{
    /**
     * @var integer
     */
    public $mkdId;
    /**
     * @var integer
     */
    public $mimId;

    /**
     * @return int
     */
    public function getMkdId(): int
    {
        return $this->mkdId;
    }

    /**
     * @param int $mkdId
     * @return MenuItemMetaKeyword
     */
    public function setMkdId(int $mkdId): MenuItemMetaKeyword
    {
        $this->mkdId = $mkdId;
        return $this;
    }

    /**
     * @return int
     */
    public function getMimId(): int
    {
        return $this->mimId;
    }

    /**
     * @param int $mimId
     * @return MenuItemMetaKeyword
     */
    public function setMimId(int $mimId): MenuItemMetaKeyword
    {
        $this->mimId = $mimId;
        return $this;
    }
}