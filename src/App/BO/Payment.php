<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class Payment
{
    public const ID = "id";
    public const ACTIVE = "active";
    public const CASH_ON_DELIVERY = "cash_on_delivery";

    /**
     * @var integer
     */
    public $id;
    /**
     * @var boolean|null
     */
    public $active;
    /**
     * @var boolean|null
     */
    public $cashOnDelivery;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Payment
     */
    public function setId(int $id): Payment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     * @return Payment
     */
    public function setActive(?bool $active): Payment
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCashOnDelivery(): ?bool
    {
        return $this->cashOnDelivery;
    }

    /**
     * @param bool|null $cashOnDelivery
     * @return Payment
     */
    public function setCashOnDelivery(?bool $cashOnDelivery): Payment
    {
        $this->cashOnDelivery = $cashOnDelivery;
        return $this;
    }
}