<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductMenuOption
{
    public const PMN_NAME = "pmn_name";

    /**
     * @var string
     */
    public $pmnName;
    /**
     * @var integer
     */
    public $putId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var integer
     */
    public $pmnId;

    /**
     * @return string
     */
    public function getPmnName(): string
    {
        return $this->pmnName;
    }

    /**
     * @param string $pmnName
     * @return ProductMenuOption
     */
    public function setPmnName(string $pmnName): ProductMenuOption
    {
        $this->pmnName = $pmnName;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductMenuOption
     */
    public function setPutId(int $putId): ProductMenuOption
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ProductMenuOption
     */
    public function setDateFrom(string $dateFrom): ProductMenuOption
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ProductMenuOption
     */
    public function setDateTo(?string $dateTo): ProductMenuOption
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return int
     */
    public function getPmnId(): int
    {
        return $this->pmnId;
    }

    /**
     * @param int $pmnId
     * @return ProductMenuOption
     */
    public function setPmnId(int $pmnId): ProductMenuOption
    {
        $this->pmnId = $pmnId;
        return $this;
    }
}