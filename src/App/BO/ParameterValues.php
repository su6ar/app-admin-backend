<?php
/**
 * Created by Daniel Bill
 * Date: 28.02.2019
 * Time: 16:59
 */

namespace Kominexpres\src\App\BO;


/**
 * Class ParameterValues
 * @package Kominexpres\src\App\BO
 */
class ParameterValues
{
    public const PRR_ID = "prr_id";
    public const PVE_ID = "pve_id";
    public const PVE_VALUE = "pve_value";

    /**
     * @var integer
     */
    public $prrId;
    /**
     * @var integer
     */
    public $pveId;
    /**
     * @var string|null
     */
    public $pveValue;

    /**
     * @return int
     */
    public function getPrrId(): int
    {
        return $this->prrId;
    }

    /**
     * @param int $prrId
     * @return ParameterValues
     */
    public function setPrrId(int $prrId): ParameterValues
    {
        $this->prrId = $prrId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPveId(): int
    {
        return $this->pveId;
    }

    /**
     * @param int $pveId
     * @return ParameterValues
     */
    public function setPveId(int $pveId): ParameterValues
    {
        $this->pveId = $pveId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPveValue(): ?string
    {
        return $this->pveValue;
    }

    /**
     * @param null|string $pveValue
     * @return ParameterValues
     */
    public function setPveValue(?string $pveValue): ParameterValues
    {
        $this->pveValue = $pveValue;
        return $this;
    }
}