<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class ProductFile
{
    public const FIE_ID = "fie_id";
    public const PUT_ID = "put_id";

    /**
     * @var integer
     */
    public $fieId;
    /**
     * @var integer
     */
    public $putId;

    /**
     * @return int
     */
    public function getFieId(): int
    {
        return $this->fieId;
    }

    /**
     * @param int $fieId
     * @return ProductFile
     */
    public function setFieId(int $fieId): ProductFile
    {
        $this->fieId = $fieId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductFile
     */
    public function setPutId(int $putId): ProductFile
    {
        $this->putId = $putId;
        return $this;
    }
}