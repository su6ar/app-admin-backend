<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


class CompanyContact
{
    /**
     * @var integer
     */
    public $cctId;
    /**
     * @var string
     */
    public $cctEmail;
    /**
     * @var string
     */
    public $cctCellnumber;
    /**
     * @var integer
     */
    public $cdlId;
    /**
     * @var boolean
     */
    public $isEshop;

    /**
     * @return int
     */
    public function getCctId(): int
    {
        return $this->cctId;
    }

    /**
     * @param int $cctId
     * @return CompanyContact
     */
    public function setCctId(int $cctId): CompanyContact
    {
        $this->cctId = $cctId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCctEmail(): string
    {
        return $this->cctEmail;
    }

    /**
     * @param string $cctEmail
     * @return CompanyContact
     */
    public function setCctEmail(string $cctEmail): CompanyContact
    {
        $this->cctEmail = $cctEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getCctCellnumber(): string
    {
        return $this->cctCellnumber;
    }

    /**
     * @param string $cctCellnumber
     * @return CompanyContact
     */
    public function setCctCellnumber(string $cctCellnumber): CompanyContact
    {
        $this->cctCellnumber = $cctCellnumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getCdlId(): int
    {
        return $this->cdlId;
    }

    /**
     * @param int $cdlId
     * @return CompanyContact
     */
    public function setCdlId(int $cdlId): CompanyContact
    {
        $this->cdlId = $cdlId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEshop(): bool
    {
        return $this->isEshop;
    }

    /**
     * @param bool $isEshop
     * @return CompanyContact
     */
    public function setIsEshop(bool $isEshop): CompanyContact
    {
        $this->isEshop = $isEshop;
        return $this;
    }
}