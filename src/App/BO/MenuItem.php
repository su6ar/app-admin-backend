<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:08
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\POPO\POPOUtils;

class MenuItem
{
    use POPOUtils;

    public const URL_HOME = "";
    public const URL_ECOMMERCE = "eshop";

    public const ATE_ID = "ate_id";
    public const CCT_ID = "cct_id";
    public const URL = "url";
    public const FER_PLACE = "fer_place";
    public const META_DESCRIPTION = "meta_description";
    public const MFE_ID = "mfe_id";
    public const MIM_ECOMMERCE = "mim_ecommerce";
    public const MIM_HOME = "mim_home";
    public const MIM_ID = "mim_id";
    public const MIM_MIM_ID = "mim_mim_id";
    public const MIM_NAME = "mim_name";
    public const MAN_PLACE = "man_place";
    public const MIM_TYPE = "mim_type";
    public const MIM_TYPE_FER = "FER";
    public const MIM_TYPE_MAN = "MAN";
    public const MIM_TYPE_MFR = "MFR";

    /**
     * @var integer
     */
    public $mimId;
    /**
     * @var integer|null
     */
    public $ateId;
    /**
     * @var integer|null
     */
    public $mfeId;
    /**
     * @var integer|null
     */
    public $mimMimId;
    /**
     * @var string
     */
    public $mimType;
    /**
     * @var string
     */
    public $mimName;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $metaDescription;
    /**
     * @var integer|null
     */
    public $ferPlace;
    /**
     * @var integer|null
     */
    public $manPlace;
    /**
     * @var boolean|null
     */
    public $mimEcommerce;
    /**
     * @var integer
     */
    public $cctId;
    /** @var boolean|null */
    public $mimHome;

    /**
     * @return bool|null
     */
    public function isMimHome(): ?bool
    {
        return $this->mimHome;
    }

    /**
     * @param bool|null $mimHome
     * @return MenuItem
     */
    public function setMimHome(?bool $mimHome): MenuItem
    {
        $this->mimHome = $mimHome;
        return $this;
    }

    /**
     * @return int
     */
    public function getMimId(): ?int
    {
        return $this->mimId;
    }

    /**
     * @param int $mimId
     * @return MenuItem
     */
    public function setMimId(int $mimId): MenuItem
    {
        $this->mimId = $mimId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAteId(): ?int
    {
        return $this->ateId;
    }

    /**
     * @param int|null $ateId
     * @return MenuItem
     */
    public function setAteId(?int $ateId): MenuItem
    {
        $this->ateId = $ateId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMfeId(): ?int
    {
        return $this->mfeId;
    }

    /**
     * @param int|null $mfeId
     * @return MenuItem
     */
    public function setMfeId(?int $mfeId): MenuItem
    {
        $this->mfeId = $mfeId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMimMimId(): ?int
    {
        return $this->mimMimId;
    }

    /**
     * @param int|null $mimMimId
     * @return MenuItem
     */
    public function setMimMimId(?int $mimMimId): MenuItem
    {
        $this->mimMimId = $mimMimId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMimType(): string
    {
        return $this->mimType;
    }

    /**
     * @param string $mimType
     * @return MenuItem
     */
    public function setMimType(string $mimType): MenuItem
    {
        $this->mimType = $mimType;
        return $this;
    }

    /**
     * @return string
     */
    public function getMimName(): string
    {
        return $this->mimName;
    }

    /**
     * @param string $mimName
     * @return MenuItem
     */
    public function setMimName(string $mimName): MenuItem
    {
        $this->mimName = $mimName;
        $this->setUrl(self::generateUrl($mimName));
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return MenuItem
     */
    public function setUrl(string $url): MenuItem
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     * @return MenuItem
     */
    public function setMetaDescription(string $metaDescription): MenuItem
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFerPlace(): ?int
    {
        return $this->ferPlace;
    }

    /**
     * @param int|null $ferPlace
     * @return MenuItem
     */
    public function setFerPlace(?int $ferPlace): MenuItem
    {
        $this->ferPlace = $ferPlace;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getManPlace(): ?int
    {
        return $this->manPlace;
    }

    /**
     * @param int|null $manPlace
     * @return MenuItem
     */
    public function setManPlace(?int $manPlace): MenuItem
    {
        $this->manPlace = $manPlace;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMimEcommerce(): ?bool
    {
        return $this->mimEcommerce;
    }

    /**
     * @param bool|null $mimEcommerce
     * @return MenuItem
     */
    public function setMimEcommerce(?bool $mimEcommerce): MenuItem
    {
        $this->mimEcommerce = $mimEcommerce;
        return $this;
    }

    /**
     * @return int
     */
    public function getCctId(): int
    {
        return $this->cctId;
    }

    /**
     * @param int $cctId
     * @return MenuItem
     */
    public function setCctId(int $cctId): MenuItem
    {
        $this->cctId = $cctId;
        return $this;
    }

    /**
     * @param $list
     * @return array
     */
    public static function generateList($list): array
    {
        $resList = [];
        $hashMap = [];

        for ($i = 0; $i < count($list); $i++)
        {
            $item = $list[$i];
            $item["items"] = [];
            $mimId = $list[$i][MenuItem::MIM_ID];
            $mimMimId = $list[$i][MenuItem::MIM_MIM_ID];
            $manPlace = $list[$i][MenuItem::MAN_PLACE];
            $mimName = $list[$i][MenuItem::MIM_NAME];

            if ($mimMimId === null)
            {
                $index = array_push($resList, $item) - 1;
                $hashMap[$mimId] = [$index];
                unset($list[$i]);
                $list = array_values($list);
                $i = -1;
            }
            else
            {
                if (array_key_exists($mimMimId, $hashMap))
                {
                    $arrIndex = $hashMap[$mimMimId];
                    $index = '$resList';
                    foreach ($arrIndex as $in)
                    {
                        $index .= "[$in]['items']";
                    }
                    $pos = eval("return count($index);");
                    eval("array_push($index, ['mim_id' => $mimId, 'mim_mim_id' => $mimMimId, 'mim_name' => '$mimName', 'man_place' => $manPlace, 'items' => []]);");
                    array_push($arrIndex, $pos);
                    $hashMap[$mimId] = $arrIndex;
                    unset($list[$i]);
                    $list = array_values($list);
                    $i = -1;
                }
                else
                {
                    foreach ($list as $key => $item)
                    {
                        if ($mimId == $mimMimId)
                        {
                            $i = $key;
                            break;
                        }
                    }
                }
            }
        }
        return $resList;
    }
}