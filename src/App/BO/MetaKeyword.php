<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:09
 */

namespace Kominexpres\src\App\BO;


class MetaKeyword
{
    public const ID = "id";
    public const MKD_NAME = "mkd_name";

    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $mkdName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MetaKeyword
     */
    public function setId(int $id): MetaKeyword
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMkdName(): string
    {
        return $this->mkdName;
    }

    /**
     * @param string $mkdName
     * @return MetaKeyword
     */
    public function setMkdName(string $mkdName): MetaKeyword
    {
        $this->mkdName = $mkdName;
        return $this;
    }
}