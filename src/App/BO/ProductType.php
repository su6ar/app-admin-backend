<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductType
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $pteName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductType
     */
    public function setId(int $id): ProductType
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPteName(): string
    {
        return $this->pteName;
    }

    /**
     * @param string $pteName
     * @return ProductType
     */
    public function setPteName(string $pteName): ProductType
    {
        $this->pteName = $pteName;
        return $this;
    }
}