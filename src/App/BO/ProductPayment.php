<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductPayment
{
    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $pmtId;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductPayment
     */
    public function setPutId(int $putId): ProductPayment
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPmtId(): int
    {
        return $this->pmtId;
    }

    /**
     * @param int $pmtId
     * @return ProductPayment
     */
    public function setPmtId(int $pmtId): ProductPayment
    {
        $this->pmtId = $pmtId;
        return $this;
    }
}