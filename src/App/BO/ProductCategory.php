<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:11
 */

namespace Kominexpres\src\App\BO;


class ProductCategory
{
    public const CEY_ID = "cey_id";
    public const PCE_MAIN = "pce_main";
    public const PUT_ID = "put_id";

    /**
     * @var string
     */
    public $ceyId;
    /**
     * @var integer
     */
    public $putId;
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string|null
     */
    public $dateTo;
    /**
     * @var boolean|null
     */
    public $pceMain;

    /**
     * @return string
     */
    public function getCeyId(): string
    {
        return $this->ceyId;
    }

    /**
     * @param string $ceyId
     * @return ProductCategory
     */
    public function setCeyId(string $ceyId): ProductCategory
    {
        $this->ceyId = $ceyId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductCategory
     */
    public function setPutId(int $putId): ProductCategory
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom(): string
    {
        return $this->dateFrom;
    }

    /**
     * @param string $dateFrom
     * @return ProductCategory
     */
    public function setDateFrom(string $dateFrom): ProductCategory
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDateTo(): ?string
    {
        return $this->dateTo;
    }

    /**
     * @param null|string $dateTo
     * @return ProductCategory
     */
    public function setDateTo(?string $dateTo): ProductCategory
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPceMain(): ?bool
    {
        return $this->pceMain;
    }

    /**
     * @param bool|null $pceMain
     * @return ProductCategory
     */
    public function setPceMain(?bool $pceMain): ProductCategory
    {
        $this->pceMain = $pceMain;
        return $this;
    }
}