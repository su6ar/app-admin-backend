<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductImage
{
    public const PUT_ID = "put_id";
    public const IAE_ID = "iae_id";
    public const IAE_MAIN = "iae_main";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $iaeId;
    /**
     * @var boolean|null
     */
    public $iaeMain;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductImage
     */
    public function setPutId(int $putId): ProductImage
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getIaeId(): int
    {
        return $this->iaeId;
    }

    /**
     * @param int $iaeId
     * @return ProductImage
     */
    public function setIaeId(int $iaeId): ProductImage
    {
        $this->iaeId = $iaeId;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isIaeMain(): ?bool
    {
        return $this->iaeMain;
    }

    /**
     * @param bool|null $iaeMain
     * @return ProductImage
     */
    public function setIaeMain(?bool $iaeMain): ProductImage
    {
        $this->iaeMain = $iaeMain;
        return $this;
    }
}