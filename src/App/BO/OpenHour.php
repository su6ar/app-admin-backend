<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:09
 */

namespace Kominexpres\src\App\BO;


class OpenHour
{
    /**
     * @var string
     */
    public $ohrDay;
    /**
     * @var string
     */
    public $openHour;
    /**
     * @var string
     */
    public $closeHour;
    /**
     * @var integer
     */
    public $cdlId;

    /**
     * @return string
     */
    public function getOhrDay(): string
    {
        return $this->ohrDay;
    }

    /**
     * @param string $ohrDay
     * @return OpenHour
     */
    public function setOhrDay(string $ohrDay): OpenHour
    {
        $this->ohrDay = $ohrDay;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpenHour(): string
    {
        return $this->openHour;
    }

    /**
     * @param string $openHour
     * @return OpenHour
     */
    public function setOpenHour(string $openHour): OpenHour
    {
        $this->openHour = $openHour;
        return $this;
    }

    /**
     * @return string
     */
    public function getCloseHour(): string
    {
        return $this->closeHour;
    }

    /**
     * @param string $closeHour
     * @return OpenHour
     */
    public function setCloseHour(string $closeHour): OpenHour
    {
        $this->closeHour = $closeHour;
        return $this;
    }

    /**
     * @return int
     */
    public function getCdlId(): int
    {
        return $this->cdlId;
    }

    /**
     * @param int $cdlId
     * @return OpenHour
     */
    public function setCdlId(int $cdlId): OpenHour
    {
        $this->cdlId = $cdlId;
        return $this;
    }
}