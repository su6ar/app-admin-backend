<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\BO;


class ProductParameterValue
{
    public const PUT_ID = "put_id";
    public const PVE_ID = "pve_id";

    /**
     * @var integer
     */
    public $putId;
    /**
     * @var integer
     */
    public $pveId;

    /**
     * @return int
     */
    public function getPutId(): int
    {
        return $this->putId;
    }

    /**
     * @param int $putId
     * @return ProductParameterValue
     */
    public function setPutId(int $putId): ProductParameterValue
    {
        $this->putId = $putId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPveId(): int
    {
        return $this->pveId;
    }

    /**
     * @param int $pveId
     * @return ProductParameterValue
     */
    public function setPveId(int $pveId): ProductParameterValue
    {
        $this->pveId = $pveId;
        return $this;
    }
}