<?php
/**
 * Created by Daniel Bill
 * Date: 08.07.2017
 * Time: 15:00
 */

namespace Kominexpres\src\App\BO;


use Kominexpres\src\App\Exceptions\PasswordLengthException;

Abstract class RegisteredUser extends AbstractAccount
{
    public const RUR_PASSWORD = 'rur_password';
    public const RUR_CODE_REFRESH = 'rur_code_refresh';

    /** @var string */
    private $rurPassword;
    /** @var string */
    private $rurCodeRefresh;

    /**
     * @return string
     */
    public function getRurPassword(): string
    {
        return $this->rurPassword;
    }

    /**
     * @param string $rurPassword
     * @return $this
     */
    public function setRurPassword(string $rurPassword)
    {
        $this->rurPassword = $rurPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getRurCodeRefresh(): string
    {
        return $this->rurCodeRefresh;
    }

    /**
     * @param string|null $rurCodeRefresh
     * @return $this
     */
    public function setRurCodeRefresh(?string $rurCodeRefresh)
    {
        $this->rurCodeRefresh = $rurCodeRefresh;
        return $this;
    }

    /**
     * @param string|null $password
     * @throws PasswordLengthException
     */
    public function setHashedPassword(string $password = null)
    {
        $newPassword = is_null($password) ? $this->getRurPassword() : $password;
        if (strlen($newPassword) > 7)
        {
            $this->rurPassword = self::generatePassword($newPassword);
        }
        else throw new PasswordLengthException();
    }

    /**
     * @throws \Exception
     */
    public function setGeneratedRurCodeRefresh()
    {
        $this->rurCodeRefresh = self::generateBytes(32);
    }
}