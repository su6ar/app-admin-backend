<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:08
 */

namespace Kominexpres\src\App\BO;


class Manufactory
{
    public const ID = "id";
    public const MUR_NAME = "mur_name";

    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $murName;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Manufactory
     */
    public function setId(int $id): Manufactory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMurName(): string
    {
        return $this->murName;
    }

    /**
     * @param string $murName
     * @return Manufactory
     */
    public function setMurName(string $murName): Manufactory
    {
        $this->murName = $murName;
        return $this;
    }
}