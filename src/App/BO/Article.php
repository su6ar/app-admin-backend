<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:06
 */

namespace Kominexpres\src\App\BO;


class Article
{
    public const ATE_ACTIVE = "ate_active";
    public const ATE_ID = "ate_id";
    public const ATE_NAME = "ate_name";

    /**
     * @var integer
     */
    public $ateId;
    /**
     * @var string
     */
    public $ateName;
    /**
     * @var boolean
     */
    public $ateActive;

    /**
     * @return int
     */
    public function getAteId(): int
    {
        return $this->ateId;
    }

    /**
     * @param int $ateId
     * @return Article
     */
    public function setAteId(int $ateId): Article
    {
        $this->ateId = $ateId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAteName(): string
    {
        return $this->ateName;
    }

    /**
     * @param string $ateName
     * @return Article
     */
    public function setAteName(string $ateName): Article
    {
        $this->ateName = $ateName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAteActive(): bool
    {
        return $this->ateActive;
    }

    /**
     * @param bool $ateActive
     * @return Article
     */
    public function setAteActive(bool $ateActive): Article
    {
        $this->ateActive = $ateActive;
        return $this;
    }
}