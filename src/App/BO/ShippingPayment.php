<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\BO;


class ShippingPayment
{
    public const PMT_ID = "pmt_id";
    public const SPG_ID = "spg_id";

    /**
     * @var integer
     */
    public $pmtId;
    /**
     * @var integer
     */
    public $spgId;

    /**
     * @return int
     */
    public function getPmtId(): int
    {
        return $this->pmtId;
    }

    /**
     * @param int $pmtId
     * @return ShippingPayment
     */
    public function setPmtId(int $pmtId): ShippingPayment
    {
        $this->pmtId = $pmtId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpgId(): int
    {
        return $this->spgId;
    }

    /**
     * @param int $spgId
     * @return ShippingPayment
     */
    public function setSpgId(int $spgId): ShippingPayment
    {
        $this->spgId = $spgId;
        return $this;
    }
}