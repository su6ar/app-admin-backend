<?php
/**
 * Created by Daniel Bill
 * Date: 03.07.2017
 * Time: 19:48
 */

namespace Kominexpres\src\App\Logger;


use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

class LOGGER
{
    /** @var \Monolog\Logger */
    private static $logger;

    /** @return \Monolog\Logger */
    public static function getLogger()
    {
        if(null === static::$logger)
            throw new \RuntimeException("LOGGER not set!");
        return static::$logger;
    }

    /**
     * @param array $settings
     * @throws \Exception
     */
    public static function setLogger(array $settings)
    {
        static::$logger = new \Monolog\Logger($settings['name']);
        static::$logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct() { }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone() { }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup() { }
}