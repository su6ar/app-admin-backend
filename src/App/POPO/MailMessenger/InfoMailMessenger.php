<?php
/**
 * Created by Daniel Bill
 * Date: 08.07.2017
 * Time: 21:33
 */

namespace Kominexpres\src\App\POPO\MailMessenger;


use Kominexpres\src\App\Config\Config;

/**
 * Class InfoMailMessenger
 * @package Kominexpres\src\App\POPO\MailMessenger
 */
class InfoMailMessenger extends AbstractMailMessenger
{
    /**
     * @param Config $config
     * @return InfoMailMessenger|mixed
     * @throws \phpmailerException
     */
    public static function createMail(Config $config)
    {
        $infoMail = new InfoMailMessenger($config);
        $infoMail->phpMailer->Username = $config->getMailInfoUsername();
        $infoMail->phpMailer->Password = $config->getMailInfoPassword();
        $infoMail->phpMailer->setFrom($config->getMailInfoUsername());
        return $infoMail;
    }
}