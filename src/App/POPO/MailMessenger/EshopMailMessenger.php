<?php
/**
 * Created by Daniel Bill
 * Date: 08.07.2017
 * Time: 22:27
 */

namespace Kominexpres\src\App\POPO\MailMessenger;


use Kominexpres\src\App\Config\Config;

/**
 * Class EshopMailMessenger
 * @package Kominexpres\src\App\POPO\MailMessenger
 */
class EshopMailMessenger extends AbstractMailMessenger
{
    /**
     * @param Config $config
     * @return EshopMailMessenger|mixed
     * @throws \phpmailerException
     */
    public static function createMail(Config $config)
    {
        $eshopMail = new EshopMailMessenger($config);
        $eshopMail->phpMailer->Username = $config->getMailEshopUsername();
        $eshopMail->phpMailer->Password = $config->getMailEshopPassword();
        $eshopMail->phpMailer->setFrom($config->getMailEshopUsername());

        return $eshopMail;
    }
}