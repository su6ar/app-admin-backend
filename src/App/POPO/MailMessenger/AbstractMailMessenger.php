<?php
/**
 * Created by Daniel Bill
 * Date: 08.07.2017
 * Time: 19:25
 */

namespace Kominexpres\src\App\POPO\MailMessenger;


use Kominexpres\src\App\Config\Config;
use Kominexpres\src\App\Exceptions\MailClientException;
use PHPMailer;

/**
 * Class AbstractMailMessenger
 * @package Kominexpres\src\App\POPO\MailMessenger
 */
abstract class AbstractMailMessenger
{
    /** @var PHPMailer */
    protected $phpMailer;
    /** @var Config */
    private $config;

    /**
     * AbstractMailMessenger constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->phpMailer = new PHPMailer();
        $this->phpMailer->isSMTP();
        $this->phpMailer->Host = $this->config->getMailHost();
        $this->phpMailer->CharSet = $this->config->getMailCharSet();
        $this->phpMailer->SMTPDebug = $this->config->getMailSmtpDebug();
        $this->phpMailer->SMTPAuth = $this->config->getMailSmtpAuth();
        $this->phpMailer->SMTPSecure = $this->config->getMailSmtpSecure();
        $this->phpMailer->Port = $this->config->getMailPort();
        $this->phpMailer->isHTML($this->config->getMailHtml());
        $this->phpMailer->SMTPOptions = array(
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        );
    }

    /**
     * @param Config $config
     * @return mixed
     */
    abstract public static function createMail(Config $config);

    /**
     * @param string $body
     * @param string $email
     * @param string $subject
     * @throws MailClientException
     * @throws \phpmailerException
     */
    public function sendMail(string $body, string $email, string $subject): void
    {
        $this->phpMailer->Body = $body;
        $this->phpMailer->Subject = $subject;
        $this->phpMailer->AddAddress($email);
        if(!$this->phpMailer->send())
            throw new MailClientException($this->phpMailer->ErrorInfo);
    }
}