<?php
/**
 * Created by Daniel Bill
 * Date: 11.07.2017
 * Time: 20:14
 */

namespace Kominexpres\src\App\POPO;

use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * Class TwigObject
 * @package Kominexpres\src\App\POPO
 */
class TwigObject
{
    private const TEMPLATE_PATH_ROOT = "/Templates";
    public const TEMPLATE_PATH_USERS = "/Users";
    public const TEMPLATE_PATH_ORDERS = "/Orders";

    public const FILENAME_REFRESH_PASS = "RefreshPassEmailTemplate.twig";
    public const FILENAME_CHANGE_STATUS = "ChangeStatusOrderTemplate.twig";

    /** @var Twig_Loader_Filesystem */
    private $loader;
    /** @var Twig_Environment */
    private $environment;

    /**
     * TwigObject constructor.
     * @param string|null $path
     */
    public function __construct(string $path = null)
    {
        $this->loader = new Twig_Loader_Filesystem(dirname(__DIR__) . self::TEMPLATE_PATH_ROOT . $path);
        $this->environment = new Twig_Environment($this->loader);
    }

    /**
     * @return Twig_Loader_Filesystem
     */
    public function getLoader(): Twig_Loader_Filesystem
    {
        return $this->loader;
    }

    /**
     * @param Twig_Loader_Filesystem $loader
     */
    public function setLoader(Twig_Loader_Filesystem $loader)
    {
        $this->loader = $loader;
    }

    /**
     * @return Twig_Environment
     */
    public function getEnvironment(): Twig_Environment
    {
        return $this->environment;
    }

    /**
     * @param Twig_Environment $environment
     */
    public function setEnvironment(Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param string $folderName
     * @param string $fileName
     * @param array $context
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function renderTwig(string $folderName, string $fileName, array $context): string
    {
        $twig = new TwigObject($folderName);
        return $twig->getEnvironment()->render($fileName, $context);
    }
}