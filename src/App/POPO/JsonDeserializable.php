<?php
/**
 * Created by Danie
 * Date: 03.07.2017
 * Time: 21:07
 */

namespace Kominexpres\src\App\POPO;


interface JsonDeserializable extends \JsonSerializable
{
    /**
     * @param array $json
     * @return self Object created from json
     */
    public static function createFromJson(array $json);
}