<?php
/**
 * Created by PhpStorm.
 * User: Daniel Bill
 * Date: 03.07.2017
 * Time: 16:17
 */

namespace Kominexpres\src\App\POPO;


class StatusObject
{
    const OK = 200;
    const CREATED = 201;
    const NO_CONTENT = 204;

    const NOT_MODIFIED = 304;

    const INVALID_INPUT = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const CONFLICT = 409;

    const INTERNAL_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
    const UNKNOWN_ERROR = 520;

    /** @var  integer */
    private $code;

    /** @var  array */
    private $data;

    /**
     * Create object with initialized data
     * @param array|object $data
     * @param int $code Default 200 OK
     * @return StatusObject
     */
    public static function create($data, $code=self::OK)
    {
        $so = new StatusObject();
        $so->setCode($code)->setData($data);
        return $so;
    }

    /**
     * Create StatusObject with CREATED response
     * @return StatusObject
     */
    public static function created()
    {
        $so = new StatusObject();
        $so->setCode(self::CREATED);
        return $so;
    }

    /**
     * Create StatusObject with OK response
     * @return StatusObject
     */
    public static function ok()
    {
        $so = new StatusObject();
        $so->setCode(self::OK);
        return $so;
    }

    /**
     * Create StatusObject with NotImplemented response
     * @return StatusObject
     */
    public static function notImplemented()
    {
        $so = new StatusObject();
        $so->setCode(self::NOT_IMPLEMENTED);
        $so->setData([]);
        return $so;
    }

    /**
     * Create StatusObject with set code
     * @param $code int
     * @return StatusObject
     */
    public static function error($code)
    {
        $so = new StatusObject();
        $so->setCode($code);
        return $so;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return StatusObject
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return StatusObject
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param array $data
     * @return StatusObject
     */
    public function addData($data)
    {
        $this->data[] = $data;
        return $this;
    }
}