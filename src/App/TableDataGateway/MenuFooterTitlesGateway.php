<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 20:25
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class MenuFooterTitlesGateway extends AbstractGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM menu_footer_titles order by place";

    /**
     * MenuFooterGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll()
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }
}