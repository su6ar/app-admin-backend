<?php
/**
 * Created by Daniel Bill
 * Date: 26.02.2018
 * Time: 16:09
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Token;
use Kominexpres\src\App\Exceptions\AccountNotFoundException;
use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\TokenNotFoundException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\Storage\Database;

/**
 * Class AccountGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class AccountGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM accounts";

    private const SQL_FIND_WHERE = "SELECT * FROM accounts WHERE act_id = ?";
    private const SQL_FIND_BY_TOKEN = "SELECT act_id, act_email, amr_first_name, amr_last_name, amr_rights, amr_avatar, amr_header_style, amr_primary_color, amr_deleted_order_show, amr_deleted_admin_user FROM tokens JOIN accounts ON tokens.sub = accounts.act_email AND sub = ? AND (invalid IS NULL OR invalid = FALSE)";
    private const SQL_FIND_BY_EMAIL = "SELECT * FROM accounts WHERE act_email = ?";
    private const SQL_FIND_BY_CODE_REFRESH = "SELECT * FROM accounts WHERE rur_code_refresh = ?";

    private const SQL_UPDATE = "UPDATE accounts SET act_email = ?, amr_avatar = ?, amr_last_name = ?, amr_first_name = ?, amr_deleted_admin_user = ?, amr_deleted_order_show = ?, rur_password = ?, amr_header_style = ?, amr_primary_color = ? WHERE act_id = ?";
    private const SQL_UPDATE_CODE_REFRESH = "UPDATE accounts SET rur_code_refresh = ? WHERE act_email = ?";
    private const SQL_UPDATE_PASSWORD = "UPDATE accounts SET rur_password = ?, rur_code_refresh = ? WHERE act_id = ?";

    private const SQL_EXISTS_BY_EMAIL = "SELECT act_id FROM accounts WHERE act_email = ?";
    private const SQL_EXISTS_BY_CODE_REFRESH = "SELECT act_id FROM accounts WHERE rur_code_refresh = ?";

    private const SQL_INSERT = "INSERT INTO accounts (act_email, act_type, rur_password, amr_rights, amr_deleted_admin_user, amr_deleted_order_show, amr_avatar, amr_first_name, amr_last_name, amr_header_style, amr_primary_color) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

    /**
     * CompanyGateway constructor.
     * @param Database $pgsql
     * @param Database $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @return array
     */
    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL);
    }

    /**
     * @param $actId
     * @return array
     * @throws AccountNotFoundException
     */
    public function findWhere($actId): array
    {
        $data = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$actId]);
        if (!empty($data))
            return $data;
        else
            throw new AccountNotFoundException("Account with id: '{$actId}' was not found!");
    }

    /**
     * @param Token $token
     * @return array
     * @throws TokenNotFoundException
     */
    public function findByToken(Token $token): array
    {
        $data = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_BY_TOKEN, [$token->getSub()]);
        if (!empty($data))
            return $data;
        else
            throw new TokenNotFoundException("Token was not found!");
    }

    /**
     * @param string $actEmail
     * @return array
     * @throws AccountNotFoundException
     */
    public function findByEmail(string $actEmail) : array
    {
        $data = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_BY_EMAIL, [$actEmail]);
        if (!empty($data))
            return $data;
        else
            throw new AccountNotFoundException("Account with email: '{$actEmail}' was not found!");
    }

    /**
     * @param string $actEmail
     * @throws AccountNotFoundException
     */
    public function existsByEmail(string $actEmail): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_EMAIL, [$actEmail]);
        if($rowCount == 0)
            throw new AccountNotFoundException("The email '{$actEmail}' doesn't exist.");
    }

    /**
     * @param string $rurCodeRefresh
     * @return array
     * @throws AccountNotFoundException
     */
    public function findByCodeRefresh(string $rurCodeRefresh) : array
    {
        $data = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_BY_CODE_REFRESH, [$rurCodeRefresh]);
        if (!empty($data))
            return $data;
        else
            throw new AccountNotFoundException("Account with refresh code: '{$rurCodeRefresh}' was not found!");
    }

    /**
     * @param string $rurCodeRefresh
     * @throws AccountNotFoundException
     */
    public function existsByCodeRefresh(string $rurCodeRefresh): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_CODE_REFRESH, [$rurCodeRefresh]);
        if($rowCount == 0)
            throw new AccountNotFoundException("The refresh code '{$rurCodeRefresh}' doesn't exist.");
    }

    /**
     * @param Administrator $account
     */
    public function updateCodeRefresh(Administrator $account): void
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_CODE_REFRESH, [$account->getRurCodeRefresh(), $account->getActEmail()]);
    }

    /**
     * @param Administrator $account
     */
    public function updatePassword(Administrator $account): void
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_PASSWORD, [$account->getRurPassword(), null, $account->getActId()]);
    }

    /**
     * @param object $account
     * @return bool
     * @throws PostgreSQLDatabaseException
     */
    public function insert(object $account)
    {
        try
        {
            $this->pgsql->beginTransaction();
            $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$account->getActEmail(), $account->getActType(), $account->getRurPassword(), $account->getAmrRights(), (int)$account->isAmrDeletedAdminUser(), (int)$account->isAmrDeletedOrderShow(), $account->getAmrAvatar(), $account->getAmrFirstName(), $account->getAmrLastName(), $account->getAmrHeaderStyle(), $account->getAmrPrimaryColor()]);
            return $this->pgsql->commit();
        }
        catch (\PDOException $e)
        {
            $this->pgsql->rollBack();
            $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " account. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
            throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " account");
        }
    }

    /**
     * @param $id
     * @throws NotImplementedException
     */
    public function delete($id)
    {
        throw new NotImplementedException();
    }

    /**
     * @param object $account
     * @throws PostgreSQLDatabaseException
     */
    public function update(object $account)
    {
        try
        {
            $this->pgsql->beginTransaction();
            $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$account->getActEmail(), $account->getAmrAvatar(), $account->getAmrLastName(), $account->getAmrFirstName(), (int)$account->isAmrDeletedAdminUser(), (int)$account->isAmrDeletedOrderShow(), $account->getRurPassword(), $account->getAmrHeaderStyle(), $account->getAmrPrimaryColor(), $account->getActId()]);
            $this->pgsql->commit();
        }
        catch (\PDOException $e)
        {
            $this->pgsql->rollBack();
            $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " account. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
            throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " account");
        }
    }
}