<?php
/**
 * Created by Daniel Bill
 * Date: 16.08.2018
 * Time: 20:37
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class MenuItemGateway extends AbstractGateway
{
    private const SQL_DELETE_BY_ATE_ID = "DELETE FROM menu_items WHERE ate_id = ?";
    private const SQL_UPDATE_ATE_ID = "UPDATE menu_items SET ate_id = ? WHERE ate_id = ?";
    private const SQL_SELECT_MAIN_ALL = "SELECT mim_id, mim_mim_id, mim_name, man_place FROM menu_items WHERE mim_type IN ('MAN', 'MFR') ORDER BY  man_place ASC";
    private const SQL_SELECT_FOOTER_ALL = "SELECT mim_id, mfe_id, mim_name, fer_place FROM menu_items WHERE mim_type IN ('FER', 'MFR') ORDER BY fer_place ASC";
    private const SQL_SELECT_FOOTER_ONLY_ALL = "SELECT mim_id, mim_name FROM menu_items WHERE mim_type = 'FER' ORDER BY fer_place ASC";
    private const SQL_SELECT_MAIN_ONLY_ALL = "SELECT mim_id, mim_name FROM menu_items WHERE mim_type = 'MAN' ORDER BY man_place ASC";
    private const SQL_SELECT_MAIN_AVAILABLE_PLACE = "SELECT (case when max(man_place) + 1 is null then 1 else max(man_place) + 1 end) man_place_max, (CASE WHEN min(man_place) IS NULL THEN 1 ELSE min(man_place) END) man_place_min FROM menu_items WHERE mim_mim_id = ?";
    private const SQL_SELECT_MAIN_AVAILABLE_PLACE_NULL = "SELECT (case when max(man_place) + 1 is null then 1 else max(man_place) + 1 end) man_place_max, (CASE WHEN min(man_place) IS NULL THEN 1 ELSE min(man_place) END) man_place_min FROM menu_items WHERE mim_mim_id IS NULL";
    private const SQL_SELECT_FOOTER_AVAILABLE_PLACE = "SELECT (case when max(fer_place) + 1 is null then 1 else max(fer_place) + 1 end) fer_place_max, (CASE WHEN min(fer_place) IS NULL THEN 1 ELSE min(fer_place) END) fer_place_min FROM menu_items WHERE mfe_id = ?";
    private const SQL_SELECT_FOOTER_AVAILABLE_PLACE_NULL = "SELECT (case when max(fer_place) + 1 is null then 1 else max(fer_place) + 1 end) fer_place_max, (CASE WHEN min(fer_place) IS NULL THEN 1 ELSE min(fer_place) END) fer_place_min FROM menu_items WHERE mfe_id IS NULL";
    private const SQL_SELECT_PARENT = "SELECT mim_mim_id, mim_name FROM menu_items WHERE mim_id = ?";
    private const SQL_UPDATE_SORT = "UPDATE menu_items SET mim_mim_id = ?, man_place = ?, url = ? WHERE mim_id = ?";
    private const SQL_UPDATE_SORT_FOOTER = "UPDATE menu_items SET mfe_id = ?, fer_place = ? WHERE mim_id = ?";
    private const SQL_UPDATE_URL = "UPDATE menu_items SET url = ? WHERE mim_id = ?";
    private const SQL_FIND_WHERE = "SELECT * FROM menu_items WHERE mim_id = ?";
    private const SQL_SELECT_BY_MIM_MIM_ID = "SELECT mim_id,ate_id FROM menu_items WHERE mim_id = ? OR mim_mim_id = ?";
    private const SQL_SELECT_CHILDREN = "SELECT * FROM menu_items WHERE mim_mim_id = ?";
    private const SQL_EXISTS_BY_URL = "SELECT * FROM menu_items WHERE url = ?";
    private const SQL_INSERT = "INSERT INTO menu_items (ate_id, mfe_id, mim_mim_id, mim_type, mim_name, url, meta_description, fer_place, man_place, mim_ecommerce, mim_home, cct_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) RETURNING mim_id";
    private const SQL_UPDATE = "UPDATE menu_items SET ate_id = ?, mfe_id = ?, mim_mim_id = ?, mim_type = ?, mim_name = ?, url = ?, meta_description = ?, fer_place = ?, man_place = ?, mim_ecommerce = ?, mim_home = ?, cct_id = ? WHERE mim_id = ?";
    private const SQL_DELETE = "DELETE FROM menu_items WHERE mim_id = ?";


    /**
     * ArticleGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectMainAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_MAIN_ALL, []);
    }

    public function updateUrl($url, $mimId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_URL, [$url, $mimId]);
    }

    public function selectChildren($mimMimId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_CHILDREN, [$mimMimId]);
    }

    public function selectFooterAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_FOOTER_ALL, []);
    }

    public function selectMainOnlyAll()
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_MAIN_ONLY_ALL, []);
    }

    public function selectFooterOnlyAll()
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_FOOTER_ONLY_ALL, []);
    }

    public function selectByMimMimId($id)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_MIM_MIM_ID, [$id, $id]);
    }

    /**
     * @param $url
     * @return mixed
     */
    public function existsByUrl($url)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_URL, [$url]);
    }

    public function selectParent($mimId)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_PARENT, [$mimId]);
    }

    public function updateSort(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_SORT, [$object->getMimMimId(), $object->getManPlace(), $object->getUrl(), $object->getMimId()]);
    }

    public function updateSortFooter(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_SORT_FOOTER, [$object->getMfeId(), $object->getFerPlace(), $object->getMimId()]);
    }

    public function selectAvailablePlace($mimMimId = -1, $mfeId = -1)
    {
        if ($mimMimId === null)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_MAIN_AVAILABLE_PLACE_NULL, []);
        }
        elseif ($mimMimId > -1)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_MAIN_AVAILABLE_PLACE, [$mimMimId]);
        }

        if ($mfeId === null)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_FOOTER_AVAILABLE_PLACE_NULL, []);
        }
        elseif ($mfeId > -1)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_FOOTER_AVAILABLE_PLACE, [$mfeId]);
        }
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Menu", $object);
        return $arr;
    }

    public function findByAteName($ateName)
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getAteId(), $object->getMfeId(), $object->getMimMimId(), $object->getMimType(), $object->getMimName(), $object->getUrl(), $object->getMetaDescription(), $object->getFerPlace(), $object->getManPlace(), $object->getMimEcommerce() === null ? null : (int)$object->getMimEcommerce(), $object->isMimHome() === null ? null : (int)$object->isMimHome(), $object->getCctId()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function deleteByAteId($ateId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_ATE_ID, [$ateId]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getAteId(), $object->getMfeId(), $object->getMimMimId(), $object->getMimType(), $object->getMimName(), $object->getUrl(), $object->getMetaDescription(), $object->getFerPlace(), $object->getManPlace(), $object->getMimEcommerce() === null ? null : (int)$object->getMimEcommerce(), $object->isMimHome() === null ? null : (int)$object->isMimHome(), $object->getCctId(), $object->getMimId()]);
    }

    public function updateAteId($newAteId, $oldAteId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_ATE_ID, [$newAteId, $oldAteId]);
    }
}