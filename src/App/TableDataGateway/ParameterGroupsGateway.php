<?php
/**
 * Created by Daniel Bill
 * Date: 07.08.2018
 * Time: 14:12
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class ParameterGroupsGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM parameter_groups";
    private const SQL_FIND_WHERE = "SELECT * FROM parameter_groups WHERE pgp_id = ?";
    private const SQL_INSERT = "INSERT INTO parameter_groups (pgp_name) VALUES (?) RETURNING pgp_id";
    private const SQL_UPDATE = "UPDATE parameter_groups SET pgp_name = ? WHERE pgp_id = ?";
    private const SQL_DELETE = "DELETE FROM parameter_groups WHERE pgp_id = ?";

    /**
     * ParameterGroupsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Parameter group", $object);
        return $arr;
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getPgpName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getPgpName(), $object->getPgpId()]);
    }
}