<?php
/**
 * Created by Daniel Bill
 * Date: 14.07.2018
 * Time: 14:56
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\Image;
use Kominexpres\src\App\Exceptions\ImageCategoryAlreadyExistsException;
use Kominexpres\src\App\Exceptions\ImageCategoryNotFoundException;
use Kominexpres\src\App\Storage\Database;


class ImageCategoriesGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM image_categories ORDER BY icy_name ASC";
    private const SQL_SELECT_MARKINGS_BY_CATEGORY = "SELECT * FROM markings WHERE mig_id IN (SELECT mig_id FROM image_category_markings WHERE icy_id = ?)";
    private const SQL_DELETE = "DELETE FROM image_categories WHERE icy_id = ?";
    private const SQL_INSERT = "INSERT INTO image_categories (icy_name) VALUES (?) RETURNING icy_id";
    private const SQL_EXISTS_BY_ICY_NAME = "SELECT * FROM image_categories WHERE icy_name = ?";
    private const SQL_EXISTS_BY_ICY_ID = "SELECT * FROM image_categories WHERE icy_id = ?";
    private const SQL_FIND_WHERE = "SELECT * FROM image_categories WHERE icy_id = ?";
    private const SQL_UPDATE = "UPDATE image_categories SET icy_name = ? WHERE icy_id = ?";

    /**
     * ImageCategoriesGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectMarkingsByCategory($icyId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_MARKINGS_BY_CATEGORY, [$icyId]);
    }

    public function delete($icyId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$icyId]);
    }

    public function insert(Object $icy)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$icy->getIcyName()]);
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getIcyName(), $object->getIcyId()]);
    }

    /**
     * @param $icyName
     * @throws ImageCategoryAlreadyExistsException
     */
    public function existsByIcyName($icyName)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_ICY_NAME, [$icyName]);
        if ($rowCount > 0)
        {
            throw new ImageCategoryAlreadyExistsException($icyName);
        }
    }

    /**
     * @param $icyId
     * @throws ImageCategoryNotFoundException
     */
    public function existsByIcyId($icyId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_ICY_ID, [$icyId]);
        if ($rowCount == 0)
        {
            throw new ImageCategoryNotFoundException($icyId);
        }
    }
}