<?php
/**
 * Created by Daniel Bill
 * Date: 10.08.2018
 * Time: 19:28
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ProductParameterValuesGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM product_parameter_values";
    private const SQL_DELETE_BY_PVE_ID = "DELETE FROM product_parameter_values WHERE pve_id = ?";
    private const SQL_INSERT = "INSERT INTO product_parameter_values (put_id, pve_id) VALUES (?,?)";
    private const SQL_DELETE = "DELETE FROM product_parameter_values WHERE put_id = ?";
    private const SQL_INSERT_BULK = "INSERT INTO product_parameter_values (put_id, pve_id) SELECT put_id, ? FROM products WHERE pgp_id = ?";
    private const SQL_DELETE_BY_PRR_ID_AND_PGP_ID = "DELETE FROM product_parameter_values WHERE put_id IN (SELECT put_id FROM products WHERE pgp_id = ?) AND pve_id IN (SELECT pve_id FROM parameter_values WHERE prr_id = ?)";

    /**
     * ParameterGroupParametersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPutId(), $object->getPveId()]);
    }

    public function insertBulk($pveId, $pgpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT_BULK, [$pveId, $pgpId]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function deleteByPgpIdAndPrrId($pgpId, $prrId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PRR_ID_AND_PGP_ID, [$pgpId, $prrId]);
    }

    public function deleteByPveId($pveId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PVE_ID, [$pveId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}