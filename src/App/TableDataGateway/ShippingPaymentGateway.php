<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 16:15
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\ShippingPayment;
use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\Storage\Database;

/**
 * Class ShippingPaymentGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class ShippingPaymentGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO shipping_payments (pmt_id, spg_id) VALUES (?,?)";
    private const SQL_DELETE = "DELETE FROM shipping_payments WHERE spg_id = ?";
    private const SQL_FIND_WHERE = "SELECT spt.pmt_id FROM shipping_payments spt WHERE spt.spg_id = ?";

    /**
     * ShippingPaymentGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $id
     * @return array
     */
    public function findWhere($id): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE, [$id]);
    }

    public function delete($spgId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$spgId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPmtId(), $object->getSpgId()]);
    }
}