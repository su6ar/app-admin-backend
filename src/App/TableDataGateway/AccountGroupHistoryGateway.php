<?php
/**
 * Created by Daniel Bill
 * Date: 11.08.2018
 * Time: 17:07
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class AccountGroupHistoryGateway extends AbstractGateway implements IGateway
{
    private const SQL_DELETE_BY_GRP_ID = "UPDATE account_group_histories SET date_to = now() WHERE grp_id = ? AND date_to IS NULL";
    private const SQL_DELETE_BY_ACT_ID = "UPDATE account_group_histories SET date_to = now() WHERE act_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO account_group_histories  (date_from, grp_id, act_id, date_to) VALUES (now(), ?,?,null)";

    /**
     * ParameterGroupParametersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getGrpId(), $object->getActId()]);
    }

    public function delete($pgpId)
    {
        throw new NotImplementedException();
    }

    public function deleteByActId($actId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_ACT_ID, [$actId]);
    }

    public function deleteByGrpId($grpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_GRP_ID, [$grpId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}