<?php
/**
 * Created by Daniel Bill
 * Date: 07.08.2018
 * Time: 12:56
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\DefaultGroupException;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class GroupsGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM groups WHERE grp_active = true";
    private const SQL_FIND_WHERE = "SELECT * FROM groups WHERE grp_id = ? AND grp_active = true";
    private const SQL_DELETE = "UPDATE groups SET grp_active = false, grp_default = false WHERE grp_id = ?";
    private const SQL_INSERT = "INSERT INTO groups (grp_name, grp_active, grp_default) VALUES (?,TRUE,?) RETURNING grp_id";
    private const SQL_EXISTS_BY_GRP_NAME_NULL = "SELECT grp_id, grp_active FROM groups WHERE grp_name = ?";
    private const SQL_EXISTS_BY_GRP_NAME = "SELECT grp_id, grp_active FROM groups WHERE grp_name = ? AND grp_id <> ?";
    private const SQL_UPDATE_DEFAULT = "UPDATE groups SET grp_default = false";
    private const SQL_EXISTS_DEFAULT = "SELECT grp_id FROM groups WHERE grp_default = true AND grp_id = ?";
    private const SQL_UPDATE = "UPDATE groups SET grp_name = ?, grp_default = ?, grp_active = true WHERE grp_id = ?";

    /**
     * GroupsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Group", $object);
        return $arr;
    }

    /**
     * @param $grpId
     * @throws DefaultGroupException
     */
    public function existsDefault($grpId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_DEFAULT, [$grpId]);
        if ($rowCount > 0)
            throw new DefaultGroupException();
    }

    public function updateDefault()
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_DEFAULT, []);
    }

    /**
     * @param $grpName
     * @param null $grpId
     * @return mixed
     */
    public function existsByGrpName($grpName, $grpId = null)
    {
        if ($grpId === null)
            return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_GRP_NAME_NULL, [$grpName]);
        else
            return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_GRP_NAME, [$grpName, $grpId]);
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getGrpName(), (int)$object->isGrpDefault()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getGrpName(), (int)$object->isGrpDefault(), $object->getGrpId()]);
    }
}