<?php
/**
 * Created by Daniel Bill
 * Date: 26.02.2018
 * Time: 20:03
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class SyncGateway extends AbstractGateway
{
    /**
     * SyncGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function dbSync() : void
    {
        // categories
        $stmt = $this->mysql->sendQueryAndFetch(/** @lang MySQL */ "SELECT MAX(id) max FROM cats_main_sort WHERE cat_id in (SELECT id from categorylist)", []);
        $catcount = intval($stmt["max"]);
        $sql = /** @lang MySQL */ "SELECT categorylist.id, name, substr(url, length('kominexpres.cz/eshop/')+1) 'url', pid, popis, popisProdukty, cats_main_sort.id 'place'
                    FROM categorylist
                    LEFT JOIN cats_main_sort ON categorylist.id = cats_main_sort.cat_id
                    WHERE categorylist.id <> 1";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $categories_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($categories_arr as $category)
        {
            $place = $category['place'];
            if($place === null) {
                $place = ++$catcount;
            }

            $prp1 = $this->pgsql->prepare("INSERT INTO categories 
                      (cey_id, cey_cey_id, pgp_id, cey_url, cey_name, cey_place, show_parameters, cey_active, description_category, description_product)  
                      VALUES 
                      (?,?,?,?,?,?,?,?,?,?)");
            $prp1->execute(array($category['id'], $category['pid'] == 1 ? null : $category['pid'], null, $category['url'],
                $category['name'], $place, 'false', 'true', empty(trim($category['popis'])) ? null : trim($category['popis']), empty(trim($category['popisProdukty'])) ? null : trim($category['popisProdukty'])));
        }

        // manufacturers
        $sql = /** @lang MySQL */"SELECT * FROM vyrobci";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $manufacturers_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($manufacturers_arr as $manufacturer)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO manufacturers (id, mur_name) VALUES (?,?)");
            $prp1->execute(array($manufacturer['id'], $manufacturer['nazev']));
        }

        // groups
        $sql = /** @lang MySQL */ "SELECT * FROM `group`";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $groups_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($groups_arr as $group)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO groups (grp_id, grp_name, grp_active, grp_default) VALUES (?,?,?,?)");
            $prp1->execute(array($group['id'], $group['nazev'], 'true', strcmp($group['nazev'], "Běžná") == 0 ? 'true' : 'false'));
        }

        // files
        $sql = /** @lang MySQL */ "SELECT * FROM files_category";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $file_categories_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($file_categories_arr as $file_category)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO file_categories (fcy_id, fcy_name) VALUES (?,?)");
            $prp1->execute(array($file_category['id'], $file_category['name']));
        }

        $sql = /** @lang MySQL */"SELECT files.id, name, type, size, files_cats_ids.file_cat_id
                    FROM files
                    LEFT JOIN files_cats_ids on files.id = files_cats_ids.file_id 
                    GROUP BY path";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $files_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($files_arr as $file)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO files (fie_id, fcy_id, fie_name, fie_path, fie_type, fie_size) VALUES (?,?,?,?,?,?)");
            $prp1->execute(array($file['id'], $file['file_cat_id'], $file['name'], $file['name'], $file['type'], $file['size']));
        }

        $this->pgsql->sendQueryOnly("INSERT INTO company_details (cdl_id, cdl_name, cdl_tin, cdl_vatin, website_name, website_url) VALUES (?,?,?,?,?,?)", [1,"Komínexpres s.r.o.",'03684903',"CZ03684903	","www.kominexpres.cz","https://www.kominexpres.cz"]);
        $this->pgsql->sendQueryOnly("INSERT INTO company_addresses (cdl_id, cas_id, street, city, zip, country, is_headquarters, cas_name) VALUES (?,?,?,?,?,?,?,?)", [1,2,"Proskovická 701","Krmelín",73924,"Česká republika",0,"Pobočka"]);
        $this->pgsql->sendQueryOnly("INSERT INTO company_addresses (cdl_id, cas_id, street, city, zip, country, is_headquarters, cas_name) VALUES (?,?,?,?,?,?,?,?)", [1	,1,"Sládkova 372/8","Ostrava",70200,"Česká republika",1,"Fakturační adresa"]);
        $this->pgsql->sendQueryOnly("INSERT INTO company_contacts (cct_id, cct_email, cct_cellnumber, cdl_id, is_eshop) VALUES (?,?,?,?,?)", [4,"eshop@kominexpres.cz","+420604126626",1,1]);
        $this->pgsql->sendQueryOnly("INSERT INTO company_contacts (cct_id, cct_email, cct_cellnumber, cdl_id, is_eshop) VALUES (?,?,?,?,?)", [1,"info@kominexpres.cz","+420604806106",1,0]);

        $this->pgsql->sendQueryOnly("INSERT INTO open_hours (ohr_day, open_hour, close_hour, cdl_id) VALUES (?,?,?,?)", ['MON','08:00:00','17:00:00',1]);
        $this->pgsql->sendQueryOnly("INSERT INTO open_hours (ohr_day, open_hour, close_hour, cdl_id) VALUES (?,?,?,?)", ['TUE','08:00:00','17:00:00',1]);
        $this->pgsql->sendQueryOnly("INSERT INTO open_hours (ohr_day, open_hour, close_hour, cdl_id) VALUES (?,?,?,?)", ['WED','08:00:00','17:00:00',1]);
        $this->pgsql->sendQueryOnly("INSERT INTO open_hours (ohr_day, open_hour, close_hour, cdl_id) VALUES (?,?,?,?)", ['THU','08:00:00','17:00:00',1]);
        $this->pgsql->sendQueryOnly("INSERT INTO open_hours (ohr_day, open_hour, close_hour, cdl_id) VALUES (?,?,?,?)", ['FRY','08:00:00','17:00:00',1]);

        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [1	,'m',274,1170]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,null)", [2,'r',140]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,null,null)", [3,'1']);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [4	,'2',300,300]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [5	,'3',200,200]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [6	,'4',130,180]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [7	,'5',140,140]);
        $this->pgsql->sendQueryOnly("INSERT INTO markings (mig_id, mig_marking, mig_max_height, mig_max_width) VALUES (?,?,?,?)", [8	,'6',85,85]);

        $this->pgsql->sendQueryOnly("INSERT INTO image_category_markings (icy_id, mig_id) SELECT icy_id, mig_id FROM image_categories, markings");
        $this->pgsql->sendQueryOnly("INSERT INTO image_markings (mig_id, iae_id) SELECT mig_id, iae_id FROM images, markings");

        // images
        $sql = /** @lang MySQL */"SELECT * FROM imagecategory";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $image_categories_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($image_categories_arr as $image_category)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO image_categories (icy_id, icy_name) VALUES (?,?)");
            $prp1->execute(array($image_category['id'], $image_category['name']));
        }

        $sql = /** @lang MySQL */"SELECT images.id, name, type, size, images_cats_ids.img_cat_id
                    FROM images
                      LEFT JOIN images_cats_ids on images.id = images_cats_ids.img_id
                    GROUP BY path";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $images_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($images_arr as $image)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO images (iae_id, icy_id, iae_path, iae_name, iae_type, iae_size) VALUES (?,?,?,?,?,?)");
            $prp1->execute(array($image['id'], $image['img_cat_id'], $image['name'], $image['name'], $image['type'], $image['size']));
        }

        // shippings
        $sql = /** @lang MySQL */"SELECT * FROM delivery_id;";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $heureka_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($heureka_arr as $heureka)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO delivery_type_heurekas (id, dta_name) VALUES (?,?)");
            $prp1->execute(array($heureka['delivery_id'], $heureka['delivery_name']));
        }

        $sql = /** @lang MySQL */"SELECT * FROM shipping";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $shipping_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($shipping_arr as $shipping)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO shippings (id, dta_id, show_on_default, show_till_max_weight) VALUES (?,?,?,?)");
            $prp1->execute(array($shipping['ship_id'], $shipping['delivery_id'], $shipping['all_default'], $shipping['top_weight']));


            $sql = /** @lang MySQL */"SELECT * FROM shipping_weight_price WHERE shipping_id = ?";
            $stmt = $this->mysql->prepare($sql);
            $stmt->execute([$shipping['ship_id']]);
            $shipping_weight_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($shipping_weight_arr as $shipping_weight)
            {
                $prp1 = $this->pgsql->prepare("INSERT INTO shipping_histories (spg_id, date_from, shy_name, free_price_above, date_to, sum_weight_price) VALUES (?,?,?,?,?,?) RETURNING shy_id");
                $prp1->execute(array($shipping['ship_id'], date('Y-m-d H:i:s', strtotime("2001-01-01")), $shipping['name'], $shipping['free_price'], null, $shipping['sum']));
                $shipping_history = $prp1->fetch(\PDO::FETCH_ASSOC);

                $prp2 = $this->pgsql->prepare("INSERT INTO shipping_history_weights (date_from, weight_min, weight_max, price, active, shy_id, date_to) VALUES (?,?,?,?,?,?,?)");
                $prp2->execute(array(date('Y-m-d H:i:s', strtotime("2001-01-01")), $shipping_weight['min_weight'], $shipping_weight['max_weight'], $shipping_weight['price'], true, $shipping_history["shy_id"], null));
            }
        }

        // parameter_groups
        $prp1 = $this->pgsql->prepare("INSERT INTO parameter_groups (pgp_id, pgp_name) VALUES (?,?)");
        $prp1->execute(array(1, "Pro všechny"));

        // parameters
        $prp1 = $this->pgsql->prepare("INSERT INTO parameters (prr_id, prr_name, prr_unit) VALUES (?,?,?)");
        foreach ([["id" => 1, "prr_name" => "Barva", "unit" => null], ["id" => 2, "prr_name" => "Materiál", "unit" => null], ["id" => 3, "prr_name" => "Průměr", "unit" => "mm"], ["id" => 4, "prr_name" => "Typ", "unit" => null]] as $parameter) {
            $prp1->execute(array($parameter['id'], $parameter['prr_name'], $parameter['unit']));
        }

        // parameter_group_parameters
        $prp1 = $this->pgsql->prepare("INSERT INTO parameter_group_parameters (prr_id, pgp_id) VALUES (?,?)");
        foreach ([["prr_id" => 1, "pgp_id" => 1], ["prr_id" => 2, "pgp_id" => 1], ["prr_id" => 3, "pgp_id" => 1], ["prr_id" => 4, "pgp_id" => 1]] as $parameter) {
            $prp1->execute(array($parameter['prr_id'], $parameter['pgp_id']));
        }

        // payments
        $payments_arr = [
            ["id"=>1, "payment"=>"V hotovosti při osobním odběru (Proskovická 701, Krmelín)", "price"=>0, "date_from"=>'2016-10-10 22:33:30', "date_to" => null, "cod"=>"false", "add_percentage_oott_price" => 0],
            ["id"=>1, "payment"=>"V hotovosti při osobním odběru", "price"=>0, "date_from"=>'2015-11-05 22:34:22', "date_to" => '2016-10-10 22:33:29', "cod"=>"false", "add_percentage_oott_price" => 0],
            ["id"=>2, "payment"=>"Bankovním převodem – proforma faktura", "price"=>0, "date_from"=>'2015-09-07 15:03:44', "date_to" => '2016-11-17 17:16:36', "cod"=>"false", "add_percentage_oott_price" => 0],
            ["id"=>2, "payment"=>"Bankovním převodem na účet předem (proforma faktura)", "price"=>0, "date_from"=>'2016-11-17 17:16:37', "date_to" => null, "cod"=>"false", "add_percentage_oott_price" => 0],
            ["id"=>3, "payment"=>"Dobírkou", "price"=>50, "date_from"=>'2015-11-18 18:50:30', "date_to" => '2016-03-21 19:18:34', "cod"=>"true", "add_percentage_oott_price" => 0.000],
            ["id"=>3, "payment"=>"V hotovosti na dobírku (platba řidiči)", "price"=>50, "date_from"=>'2016-03-21 19:18:35', "date_to" => null, "cod"=>"true", "add_percentage_oott_price" => 0.000],
            ["id"=>5, "payment"=>"Platební kartou při osobním odběru", "price"=>0, "date_from"=>'2017-04-05 22:25:57', "date_to" => null, "cod"=>"true", "add_percentage_oott_price" => 1.4],
            ["id"=>6, "payment"=>"Platební kartou na dobírku", "price"=>50, "date_from"=>'2015-11-18 12:59:00', "date_to" => '2016-12-21 13:17:04', "cod"=>"true", "add_percentage_oott_price" => 1.4]
        ];
        foreach ($payments_arr as $payment)
        {
            $prp1 = $this->pgsql->prepare("SELECT payments_prc(p_id := ?, p_price := ?, p_cod := ?, p_add_percentage_oott_price := ?, p_date_from := ?, p_date_to := ?, p_name := ?)");
            $prp1->execute(array($payment['id'], $payment['price'], $payment['cod'], $payment['add_percentage_oott_price'], $payment['date_from'], $payment['date_to'], $payment['payment']));
        }

        // shipping_payments
        $sql = /** @lang MySQL */"SELECT * FROM shipping_payment";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $ship_payments_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($ship_payments_arr as $ship_payment)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO shipping_payments (spg_id, pmt_id) VALUES (?,?)");
            $prp1->execute(array($ship_payment['shipping_id'], $ship_payment['payment_id']));
        }

        // accounts
        $sql = /** @lang MySQL */"SELECT * FROM kominexprescz.signup";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute(array());
        $users_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($users_arr as $user) {
            $prp = $this->pgsql->prepare("SELECT accounts_prc(p_act_email:=?, p_act_type:=?, p_rur_password:=?, p_uer_active:=?, p_uer_date_created:=?, p_uer_newsletter:=?, p_uer_code_activation:=?, p_uer_code_refresh:=?)");
            $date = date('Y-m-d H:i:s', $user['date']);
            $prp->execute(array(strtolower($user['email']), 'UER', $user['password'],
                $user['active'], $date, $user['newsletter'],
                trim($user['activation_code']) == '' ? null : $user['activation_code'], trim($user['refresh_code']) == '' ? null : $user['refresh_code']));

            $prp = $this->pgsql->prepare("SELECT act_id, act_email FROM accounts WHERE act_email = ?");
            $prp->execute(array(strtolower($user['email'])));
            $act_email_array = $prp->fetchAll(\PDO::FETCH_ASSOC);

            $prp = $this->pgsql->prepare("SELECT addresses__prc(?,?,?)");
            $prp->execute(array($act_email_array[0]['act_email'], 'ACT', null));

            $prp = $this->pgsql->prepare("SELECT ads_id FROM addresses WHERE ads_type = ? AND act_id = ?");
            $prp->execute(array('ACT', $act_email_array[0]['act_id']));
            $ads_id_array = $prp->fetchAll(\PDO::FETCH_ASSOC);

            if(isset($ads_id_array[0]['ads_id'])) {
                // ACT address
                $prp = $this->pgsql->prepare("INSERT INTO address_histories (ads_id, date_from, street, city, zip,
                    country, enterprise, first_name, last_name, tin, vatin, cellnumber, date_to) 
                    VALUES
                    (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $prp->execute(array($ads_id_array[0]['ads_id'], $date, $user['street'], $user['city'],
                    str_replace(' ', '', $user['zip']), $user['country'], empty(trim($user['enterprise'])) ? null : trim($user['enterprise']),
                    empty(trim($user['name'])) ? null : trim($user['name']), empty(trim($user['surname'])) ? null : trim($user['surname']),
                    empty(trim($user['tin'])) ? null : str_replace(' ', '', $user['tin']),
                    empty(trim($user['vatin'])) ? null : str_replace(' ', '', $user['vatin']), empty(trim($user['cellnumber'])) ? null : str_replace(' ', '', $user['cellnumber']), null));
            }
        }

        $sql = /** @lang MySQL */"SELECT * FROM kominexprescz.login";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute(array());
        $admins_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($admins_arr as $admin) {
            $avatar = 'assets/images/avatars-vector/boy-blue-shirt.svg';
            $admin_type = 'AMR';

            $prp = $this->pgsql->prepare("SELECT act_id FROM accounts WHERE act_email = ?");
            $prp->execute(array(strtolower($admin['email'])));
            $act_id_admin = $prp->fetchAll(\PDO::FETCH_ASSOC);

            if (isset($act_id_admin[0]['act_id'])) {
                $prp = $this->pgsql->prepare("UPDATE accounts SET act_type = ? WHERE act_email = ?");
                $prp->execute(array($admin_type, strtolower($admin['email'])));

                $prp = $this->pgsql->prepare("SELECT addresses__prc(?,?,?)");
                $prp->execute(array(strtolower($admin['email']), 'ACT', null));
            } else {
                $prp = $this->pgsql->prepare("SELECT accounts_prc(p_act_email:=?, p_act_type:=?, p_amr_rights:=?, p_amr_deleted_admin_user:=?, p_amr_deleted_order_show:=?, p_amr_avatar:=?, p_amr_first_name:=?, p_amr_last_name:=?)");
                $prp->execute(array(strtolower($admin['email']), $admin_type, $admin['prava'], 'true', 'true', $avatar,
                    trim($admin['name']), trim($admin['surname'])));
            }
        }

        $sql = /** @lang MySQL */"SELECT * FROM orders WHERE userid NOT IN (SELECT id FROM signup) group by email";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute(array());
        $customers_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($customers_arr as $customer) {
            $prp = $this->pgsql->prepare("SELECT accounts_prc(p_act_email:=?, p_act_type:=?)");
            $prp->execute(array(strtolower($customer['email']), 'CTR'));
        }

        $sql = /** @lang MySQL */"SELECT * FROM orders WHERE length(namefak) <= 30";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute(array());
        $customers_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($customers_arr as $customer) {
            $prp = $this->pgsql->prepare("SELECT addresses__prc(?, ?, ?)");
            $prp->execute(array(strtolower($customer['email']), 'BIG', 'DLY'));
            $ic = str_replace(' ', '', $customer['ic']);
            $dic = str_replace([' ', '-'], '', $customer['dic']);
            $prp1 = $this->pgsql->prepare("SELECT ADDRESSES_PRC(p_email:= ?, p_cellnumber:= ?, p_big_first_name:= ?, p_big_last_name:= ?, p_big_street:= ?, p_big_city:= ?, p_big_zip:= ?, p_big_country:= ?, p_dly_first_name:= ?, p_dly_last_name:= ?, p_dly_street:= ?, p_dly_city:= ?, p_dly_zip:= ?, p_dly_country:= ?, p_enterprise:= ?, p_tin:= ?, p_vatin:= ?, p_date_from:= ?)");
            $prp1->execute(array(strtolower($customer['email']), empty(trim($customer['cellnumber'])) ? null : str_replace(' ', '', $customer['cellnumber']),
                empty(trim($customer['namefak'])) ? null : trim($customer['namefak']), empty(trim($customer['surnamefak'])) ? null : trim($customer['surnamefak']),
                trim($customer['streetfak']), trim($customer['cityfak']), str_replace(' ', '', $customer['zipfak']), trim($customer['countryfak']),
                empty(trim($customer['namedor'])) ? null : trim($customer['namedor']), empty(trim($customer['surnamedor'])) ? null : trim($customer['surnamedor']), trim($customer['streetdor']),
                $customer['citydor'], str_replace(' ', '', $customer['zipdor']), $customer['countrydor'], empty(trim($customer['enterprise'])) ? null : trim($customer['enterprise']),
                empty($ic) || strlen($ic) != 8 ? null : $ic, empty($dic) ? null : $dic, date('Y-m-d H:i:s', $customer['date'])));
        }

        // accounts



        // account_group_histories
        $sql = "SELECT * FROM accounts";
        $stmt = $this->pgsql->prepare($sql);
        $stmt->execute([]);
        $acc_grs_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($acc_grs_arr as $acc_gr)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO account_group_histories 
                          (date_from, grp_id, act_id, date_to)  
                          SELECT ?, grp_id, ?, ? FROM groups WHERE grp_default is true");
            $prp1->execute(array(date('Y-m-d H:i:s', strtotime("2001-01-01")), $acc_gr['act_id'], null));
        }

        // category_group_discounts
        $sql = /** @lang MySQL */"SELECT * FROM cat_sleva_group WHERE id_cat IN (SELECT id FROM categorylist) and id_cat <> 1";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $cgds_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($cgds_arr as $cgd_gr)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO category_group_discounts 
                              (grp_id, cey_id, date_from, discount, date_to) 
                              VALUES 
                              (?,?,?,?,?)");
            $prp1->execute(array($cgd_gr['id_skupiny'], $cgd_gr['id_cat'], date('Y-m-d H:i:s', strtotime("2001-01-01")), $cgd_gr['sleva'], null));
        }

        // articles
        $pages_arr = [
            ["id" => 1, "name" => "Kominictví Komínexpres", "text" => '<p>Naše kominictví z Ostravy Vám nabízí kompletní kominické práce a služby.</p> <p>Moderní, inovativní a zodpovědný přístup naší mladé dynamické společnosti Vám přináší vysokou&nbsp;kvalitu.</p> <p>Kvalitně, profesionálně a pečlivě odvedená práce pro nás není pouhá fráze! Zakládáme si na odborném komínovém servisu, při navrhování a provádění spalinových cest používáme výpočtový software KESA-ALADIN. Tento postup je zárukou toho, že spalinová cesta bude funkční a vyhovující při následné revizi spalinové cesty.</p> <p>Využíváme nejnovějších technologických postupů, které nám umožňují stavět, upravovat, opravovat a udržovat komíny kvalitně při dodržení platných norem, zákonů, vyhlášek, nařízení a technických předpisů.</p> <p>Můžeme tedy hrdě prohlásit, že patříme mezi společnosti, které nabízejí v dnešní době ojedinělý a odlišný přístup ke své práci.</p><p>Ve spolupráci s naším spřáteleným kamnářstvím <a href="http://vasekrby.cz/" target="_blank">Radim Mazurek</a> Vám zajistíme stavbu krbů, kamen, sporáků, pecí, venkovních grilů nebo udíren včetně komínu na klíč.</p><p>Ke komínu Vám také dodáme a připojíme nový plynový kotel nebo kotel na pevná paliva ve spolupráci s naším partnerem v oboru instalatérství a topenářství, <a href="https://www.firmy.cz/detail/1987651-lukas-konecny-brusperk.html" target="_blank">Lukášem Konečným</a>.</p> <p>Komínové vložky, kouřovody, komíny, komínové systémy, krbová kamna, krbové vložky, hlásiče požáru a kouře, detektory CO a další zboží ve vysoké kvalitě můžete zakoupit také v našem <a href="https://www.kominexpres.cz/eshop" rel="nofollow" target="_blank"><b>e-shopu</b></a>.</p>  <h2>Krédo naší společnosti: kvalitně a pečlivě za příznivou cenu!</h2>  <h3>Naše služby:</h3> <ul> <li><a href="https://www.kominexpres.cz/vlozkovani" rel="nofollow" target="_blank">Vložkování komínů</a></li> <li><a href="https://www.kominexpres.cz/frezovani" rel="nofollow" target="_blank">Frézování komínů</a></li> <li><a href="https://www.kominexpres.cz/opravy" rel="nofollow" target="_blank">Opravy komínů</a></li> <li><a href="https://www.kominexpres.cz/stavba" rel="nofollow" target="_blank">Stavba komínů</a></li> <li><a href="https://www.kominexpres.cz/revize" rel="nofollow" target="_blank">Revize komínů</a></li> <li><a href="https://www.kominexpres.cz/revize/cisteni-a-kontrola-spalinovych-cest" rel="nofollow" target="_blank">Čištění komínů</a></li> <li><a href="https://www.kominexpres.cz/revize/revize-spalinovych-cest" rel="nofollow" target="_blank">Kontrola komínů</a></li><li><a href="http://vasekrby.cz/" target="_blank">Stavba krbů, kamen a pecí</a></li><li><a href="https://www.firmy.cz/detail/1987651-lukas-konecny-brusperk.html" target="_blank">Montáž kotlů</a>, <a href="http://vasekrby.cz/" target="_blank">krbových kamen a krbových vložek</a></li><li>Montáž a revize zařízení ADS</li><li>Montáž a revize požárního a elektronického zabezpečení</li> </ul>  <h3>Prodej a montáž:</h3> <ul> <li>Nerezové a plastové komínové vložky<br></li> <li>Nerezové, plastové a ocelové kouřovody</li> <li>Plastové, keramické a nerezové komíny</li><li>Komínové systémy</li> <li>Komínové stříšky, nástavce, hlavice a dvířka</li><li>Regulátory tahu a spalinové ventilátory</li><li>Plynové tepelné zářiče</li> <li>Krbová kamna a krbové vložky</li><li>Plynové kotle a kotle na pevná paliva<br></li><li>Hlásiče požáru a kouře</li><li>Detektory CO, hořlavých a výbušných plynů</li><li>Detektory plamene a tabákového kouře</li><li>Poplachové zabezpečovací a tísňové systémy</li><li>Alarmy, kamerové systémy, videotelefony a digitální dveřní kukátka</li></ul>'],
            ["id" => 2, "name" => "Vložkování komínů", "text" => '<p>Vložkování komínů se provádí u nevyhovujících stávajících komínů. Např. při instalaci nového spotřebiče, změně spotřebiče nebo media, např. přechod na plyn, nebo při problémech se stávajícím komínem, např. špatný tah komínu, unikání spalin z komínu, zavlhávání komínu, atd.</p><p>Vložkování provádíme kyselinovzdornými nerezovými pevnými či ohebnými vložkami, ke kterým jsou speciální komponenty. Pro dopojení na kotel jsou různé typy kouřovodů pevných i atypických pro dosažení ideálního vedení kouřovodu. Druh, jakost, tloušťka nerezi a těsnění se odvíjí podle typu paliva a spotřebiče. Veškerý tento materiál má všechny potřebné atesty a zkoušky a je schválen a certifikován pro celou EU. Kompletní vysledek odpovídá požadavkům všech platných předpisů, zejména ČSN 73 4201:2010.</p><h2>Vložkování komínů pro spotřebiče na plynná paliva s atmosférickým h:</h2><p>Komíny pro tyto spotřebiče se dle ČSN 73 4201:2010 vložkují kyselinovzdornými nerezovými pevnými vložkami tř. 1.4301 o tl. 0,5 mm či nerezovými ohebnými vložkami tř. 1.4301 o tl. 0,3 mm.</p><p>Dopojení kouřovodů provádíme také z nerezi stejné kvality.</p><p>Vzhledem ke zkušenostem, technologii a specializovanému vybavení tyto práce v 99% probíhají bez problémů a narušení komfortu uživatele. Doba celé kompletní realizace je 4 - 6 hodin. Zbývající 1%, kdy je komín např. zasypán nebo je v něm nějaká překážka, je i tak v převážné většině případů kompletně dokončena ve stejný den.</p><h3>Orientační ceny za vložkování komínů pro rodinné domy:</h3><p><strong>Vložkování komínu pro spotřebič na plynná paliva s atmosférickým hořákem: 1000 - 1400 Kč/m</strong></p><p><strong>Vložkování komínu pro spotřebič na pevná (tuhá) paliva: 1600 - 2000 Kč/m</strong></p><p><strong>Vložkování komínu pro spotřebič na pevná (tuhá) paliva včetně frézování zděného komínu z plných pálených cihel: 2600 - 3500 Kč/m</strong></p><p><strong>Vložkování komínu pro spotřebič na pevná (tuhá) paliva včetně frézování zděného komínu s&nbsp;azbestocementovými (osinkocementovými) nebo keramickými (šamotovými) rourami: 3400 - 4300 Kč/m</strong></p><p><span style="font-weight: 700; line-height: 1.42857;"><br></span></p><p>Ceny jsou včetně DPH v rozmezí dle průměru a délky vložkování komínu a vzdálenosti zakázky.<strong><br></strong></p><p><br></p><p><strong>Před každým vložkováním komínu Vás navštíví náš technik, který pečlivě prohlédne Váš komín, navrhne nejvhodnější řešení, popřípadě několik variant a vyhotoví přesné cenové nabídky.</strong></p><p><br></p><h4>Pro domluvení konzultace, osobní prohlídky a bližší informace nás neváhejte <a href="https://www.kominexpres.cz/kontaktujte-nas">kontaktovat</a>!</h4>'],
            ["id" => 3, "name" => "Frézování komínů", "text" => '<h3>Používá se tam, kde je potřeba zvětšit průřez stávajícího komínového průduchu, a to z důvodů:</h3><ul><li>vyžaduje to připojovaný spotřebič paliv (krb, krbová vložka, krbová kamna, kotel na pevná paliva, plynový kotel většího výkonu)</li><li>komínové zdivo je netěsné a je potřeba komínový průduch vyvložkovat komínovou vložkou</li><li>komín má nízkou účinnou výšku (vzdálenost od osy sopouchu po ústí komínu)</li><li>použítí komínového průduchu pro výkonnou ventilaci provozovny</li><li>komín je zadehtovaný a vzhledem k technickému stavu jej nelze vypálit</li><li>jiný problém, který frézování komínu vyřeší</li></ul><p>Frézování komínu je rychlejší a levnější varianta než zbourání stávajícího komínového tělesa a postavení nového. Navíc, při použití vhodných materiálů pro vložkování komínu získá zákazník stejnou záruku a životnost jako u nového komínu. Náklady na kompletní úpravu včetně vložkování komínu jsou podstatně nižší než při zbourání stávajícího a postavení nového komínového tělesa, a to o 25 až 40 %. Frézováním komínu a jeho následným vyvložkováním získá Váš komín nové vlastnosti a parametry, které by měl v dnešní vyspělé době splňovat každý komín, například vyšší tah komínu, jednodušší čištění komínu, atd.<br></p><h2>Princip frézování komínu:</h2><p>Hydraulické čerpadlo roztáčí rotor frézy v komínovém průduchu do vysokých otáček. Na obvodu rotoru jsou upevněny řetízky z vysoce tvrzené oceli. Tyto řetízky odstředivou silou narážejí do komínového zdiva a drtí jej postupně na jemný prach. Délkou řetízků se nastavuje výsledný průměr komínového průduchu. Frézuje se od půdice komínu k jeho hlavě.&nbsp;Komínová fréza se skládá z výkonného kompresoru a vlastního hydraulického motoru s frézovací hlavicí. Stávající otvory v komíně (sopouch, vymetací, vybírací otvory apod.) se uzavřou a utěsní pevnými deskami s těsněním Tato metoda sanace komínu je velmi šetrná a je používána i na dožilém komínovém zdivu bez nebezpečí posunu jednotlivých cihel.</p><h3>Frézovat lze:</h3><ul><li>komíny rovné i mírně uhýbané</li><li>všechny průřezy - kruhový, čtvercový i obdélníkový komínový průduch<br></li><li>zděné komínové průduchy z cihel a betonu</li><li>azbestocementové (osinkocementové) a keramické (šamotové) komínové vložky lze frézovat obtížně <br></li><li>vestavěné a přistavěné komíny, u samostatně stojících komínů je nutno posoudit statiku</li><li>nelze frézovat kovové (plechové) komínové vložky, komínové vložky z glazované keramiky a železobetonu (železobetonový věnec frézovat lze, je však nutno montážním otvorem po odfrézování betonu vyřezat armatury)</li></ul><p>Při frézování komínu je možné odebrat u vestavěných a přistavěných komínů až 1/3 tloušťky komínového zdiva. Tedy např. u běžného komínového průduchu o průměru 150 mm a tloušťce komínového zdiva 150 mm, lze odebrat až 50 mm komínového zdiva a tím zvětšit průměr komínového průduchu na 250 mm, což postačí na vyvložkování komínovou vložkou o průměru až 230 mm. Bude-li komínový průduch použit k odtahu spalin, je nutno vyfrézovaný komínový průduch vždy vyvložkovat. Komínové průduchy, které budou použity jako ventilace, se vložkovat nemusí, vzhledem k možným netěsnostem komínového zdiva se to však doporučuje.</p><h2>Požadavky na zajištění pracoviště pro frézování komínů:</h2><ul> <li>komín musí být průchozí po celé délce - po dohodě může zprůchodnění komínu provést naše kominická firma</li><li>musí být zajištěn bezpečný přístup k ústí komínu<br></li><li>všechny místnosti v domě, jimiž prochází frézované komínové těleso, musí být po dobu frézování komínu přístupné</li><li>komín nesmí být odkloněný od svislice v úhlech větších než 15 stupňů (při větších úhlech je nutno v místě odklonu vybourat montážní otvor - zajistí naše kominická firma)</li><li>zákazník bere na vědomí, že se při práci může uvnitř objektu prášit a objekt podle toho zajistí</li></ul><p>&nbsp;</p><h3>Orientační ceny za frézování komínů pro rodinné domy:</h3><p><b>Frézování zděného komínu z plných pálených cihel: 700 - 1200 Kč/m</b></p><p><b>Frézování zděného komínu s azbestocementovými (osinkocementovými) nebo keramickými (šamotovými) rourami: 1500 - 2000 Kč/m</b></p><p><b>Frézování zděného komínu&nbsp;z plných pálených cihel&nbsp;pro spotřebič na pevná (tuhá) paliva včetně vložkování komínu: 2600 - 3500 Kč/m</b></p><p><b>Frézování zděného komínu&nbsp;s azbestocementovými (osinkocementovými) nebo keramickými (šamotovými) rourami&nbsp;pro spotřebič na pevná (tuhá) paliva včetně vložkování komínu: 3400 - 4300 Kč/m</b></p><p>&nbsp;</p><p> Ceny jsou včetně DPH v rozmezí dle průměru a délky frézování komínu a vzdálenosti zakázky.&nbsp;V&nbsp;ceně je vyfrézování komínového průduchu na potřebný nebo požadovaný průměr včetně vybourání a zazdění případných montážních otvorů.</p><p>&nbsp;</p><p><strong>Před každým frézováním komínu Vás navštíví náš technik, který pečlivě prohlédne Váš komín, navrhne nejvhodnější řešení, popřípadě několik variant a vyhotoví přesné cenové nabídky.</strong></p><p>&nbsp;</p><h4>Pro domluvení konzultace, osobní prohlídky a pro bližší informace nás neváhejte <a href="https://www.kominexpres.cz/kontaktujte-nas">kontaktovat</a>!</h4>'],
            ["id" => 4, "name" => "Opravy komínů", "text" => '<h3>Provádíme:</h3><ul><li>bourání a zdění komínů nad střechou nebo v podkroví z kvalitních lícových cihel Klinker</li><li>zpevňování a omítání komínů nad střechou nebo v podkroví</li><li>oplechování komínů</li><li>tepelná izolace komínů</li><li>montáž komínových stříšek a dvířek</li></ul><p>&nbsp;</p><p><b>Před každou opravou komínu Vás navštíví náš technik, který pečlivě prohlédne Váš komín, navrhne nejvhodnější řešení, popřípadě několik variant a vyhotoví přesné cenové nabídky.</b><br></p><p>&nbsp;</p><h4>Pro domluvení konzultace, osobní prohlídky a bližší informace nás neváhejte <a href="https://www.kominexpres.cz/kontaktujte-nas">kontaktovat</a>!</h4>'],
            ["id" => 5, "name" => "Stavba komínů", "text" => '<h3>Provádíme:</h3><ul><li><span style="line-height: 1.42857;">montáž všech typů kvalitních komínových systémů, plastových, keramických a nerezových komínů</span></li><li>montáž nerezových komínových nástavců<br></li><li>stavíme komíny pro rodinné domy, bytové domy, obce, města a firmy<br></li></ul><p><br></p><p><b>Před každou stavbou komínu Vás navštíví náš technik, který pečlivě prohlédne místo stavby, navrhne nejvhodnější řešení, popřípadě několik variant a vyhotoví přesné cenové nabídky.</b></p><p>&nbsp;</p><h4>Pro domluvení konzultace, osobní prohlídky a bližší informace nás neváhejte <a href="https://www.kominexpres.cz/kontaktujte-nas">kontaktovat</a>!</h4>'],
            ["id" => 6, "name" => "Kominické práce", "text" => '<h2>Zajišťujeme veškeré kominické práce dle zákona č. 320/2015:</h2><ul><li>Čištění spalinových cest</li><li>Kontrola spalinových cest</li><li><span>Revize spalinových cest</span></li><li><span>Výpočet spalinových cest</span></li><li><span><span>Prohlídka spalinových cest kamerou</span><br></span></li></ul><h2>V případě provádění kominických prací je potřeba mít zajištěn přístup ke:</h2><ul><li>komínu nad střechou (v případě čištění a kontroly ústím komínu)</li><li>komínovým dvířkám</li><li>kouřovodu</li></ul><h2>Ceník kominických prací pro soukromé osoby:</h2><p><b>Čištění a kontrola spalinové cesty: &nbsp;</b>550&nbsp;Kč/ks, od 2 ks 400 Kč/ks</p><p><b>Kontrola spalinové cesty:</b>&nbsp;450 Kč/ks, od 2 ks 350 Kč/ks</p><p><b>Čištění spalinové cesty:&nbsp;</b>400 Kč/ks, od 2 ks 300 Kč/ks</p><p><b>Revize spalinové cesty včetně výpočtu: </b>2300 Kč/ks, od 2 ks 1650 Kč/ks</p><p><b>Revize spalinové cesty bez výpočtu: </b>1800 Kč/ks, od 2 ks 1150 Kč/ks</p><p><b>Výpočet spalinové cesty: </b>1300&nbsp;Kč/ks, od 2 ks 850 Kč/ks</p><p><b>Prohlídka spalinové cesty kamerou: </b>2300 Kč/ks, od 2 ks 1650 Kč/ks</p><p>Ceny jsou včetně DPH&nbsp;za 1 spalinovou cestu od spotřebiče na všechny druhy paliv o jmenovitém výkonu do 50 kW.</p><p>Pro všechny naše stálé zákazníky hlídání lhůt čištění a kontrol spalinové cesty v ceně!</p><br/><h2>Ceník kominických prací pro bytové domy a právnické osoby:</h2><p><b>Čištění a kontrola spalinové cesty: &nbsp;</b>700&nbsp;Kč/ks, od 2 ks 500 Kč/ks</p><p><b>Kontrola spalinové cesty: </b>600&nbsp;Kč/ks, od 2 ks 450 Kč/ks</p><p><b>Čištění spalinové cesty:</b>&nbsp;400&nbsp;Kč/ks, od 2 ks 300 Kč/ks</p><p><b>Revize spalinové cesty včetně výpočtu: </b>2300&nbsp;Kč/ks, od 2 ks 1650 Kč/ks</p><p><b>Revize spalinové cesty bez výpočtu: </b>1800&nbsp;Kč/ks, od 2 ks 1150 Kč/ks</p><p><b>Výpočet spalinové cesty:&nbsp;</b>1300 Kč/ks, od 2 ks 850 Kč/ks</p><p><b>Prohlídka spalinové cesty kamerou: </b>2300&nbsp;Kč/ks, od 2 ks 1650 Kč/ks</p><p>Ceny jsou včetně DPH&nbsp;za 1 spalinovou cestu od spotřebiče na všechny druhy paliv o jmenovitém výkonu do 50 kW.</p> <p>Pro všechny naše stálé smluvní zákazníky garance ceny po dobu 2 let a hlídání lhůt čištění a kontrol spalinové cesty v ceně!</p><br/><h2>Poučení o postupu při zjištění nedostatků při čištění nebo kontrole spalinové cesty:</h2><p>Rádi bychom Vás informovali o některých nových povinnostech, které nám od 01.01.2016 ukládá zákon č. 320/2015 Sb. o Hasičském záchranném sboru České republiky a vyhláška č. 34/2016 Sb. o čištění, kontrole a revizi spalinové cesty:</p><br/><p><b>§ 46 Postup při zjištění nedostatků:</b></p> <p>Pokud oprávněná osoba při čištění nebo kontrole spalinové cesty nebo revizní technik spalinových cest při revizi spalinové cesty zjistí nedostatek, který bezprostředně ohrožuje zdraví, život nebo majetek osob a který nelze odstranit na místě, neprodleně, nejpozději do 10 pracovních dnů ode dne zjištění nedostatku, oznámí tuto skutečnost písemně v případě nedostatku způsobeného nedodržením technických požadavků na stavbu příslušnému stavebnímu úřadu a v případě nedostatku týkajícího se nedodržení požadavků na požární bezpečnost příslušnému orgánu státního požárního dozoru.</p> <p><b>§ 76a:</b></p><p>(2) Oprávněné osobě nebo reviznímu technikovi spalinových cest, který v rozporu s § 46 neoznámí zjištěné nedostatky příslušnému stavebnímu úřadu nebo orgánu státního požárního dozoru nebo je oznámí opožděně, se uloží pokuta do 50 000 Kč.</p><br/><h4>Pro domluvení termínu a bližší informace nás neváhejte&nbsp;<a href="https://www.kominexpres.cz/kontaktujte-nas">kontaktovat</a>!</h4>'],
            ["id" => 7, "name" => "Ostatní", "text" => '<h5>Níže naleznete novinky a informace ze světa kominictví a také naše reference.</h5>'],
            ["id" => 8, "name" => "Aktuality", "text" => '<h5>Zde Vás informujeme o novinkách v naší společnosti, ze světa komínů, o nových předpisech, zákonech a vyhláškách a přidáváme zajímavé tématické články. Své náměty, nápady, postřehy a dotazy nám klidně posílejte na info@kominexpres.cz, budeme za ně rádi.<br><br><span style="color: inherit; font-family: inherit; line-height: 1.1;">Děkujeme za Vaši přízeň a přejeme Vám krásný den, čistý komín a dobrý tah!<br><br></span>Váš tým Komínexpres :-)</h5><p><br></p><p><span style="font-weight: 700;"><u>3. července 2017, 7:00:<br></u></span></p><p><span style="font-family: inherit; color: rgb(29, 33, 41); white-space: pre-wrap;"><b>Nepodceňujte pravidelnou roční kontrolu a čištění spalinové cesty!</b></span></p><div class="" data-block="true" data-editor="6lfl7" data-offset-key="cjj37-0-0" style="font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); white-space: pre-wrap;"><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="1t04r-0-0" style="font-family: inherit;">Nezapomeňte, že k bezpečnému provozu plynového kotle s průtokovým ohřevem vody (i všem ostatním spotřebičům paliv) je potřeba mít správně navrženou, provedenou a udržovanou spalinovou cestu, dále také pravidelný servis kotle a v neposlední řadě je důležité zajistit dostatečný přívod venkovního spalovacího vzduchu ke kotli a větrání místnosti s kotlem.</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="1vefi-0-0" style="font-family: inherit;">Mohlo by se Vám totiž stát ještě něco horšího než našim přátelům z Nového Jičína, kteří si v těchto letních vedrech ohřívali vodu k mytí nádobí a sprchování pomocí plynového kotle s průtokovým ohřevem vody :-(</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="8qnhg-0-0" style="font-family: inherit;">Naštěstí je na únik spalin z plynového kotle, a to hlavně jedovatého oxidu uhelnatého, upozornil náš detektor CO (<a href="https://www.kominexpres.cz/produkt/detektor-co-kidde-10lldco/2601" target="_blank">Kidde 10LLDCO</a>), který si do koupelny nainstalovali loni v prosinci. Díky včasné výstraze naším detektorem CO nedošlo k neštěstí a na místo úniku CO jsme po odstavení plynového kotle z provozu a vyvětrání koupelny byli přivoláni my.</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="77ueg-0-0" style="font-family: inherit;">Kontrolou spalinové cesty jsme zjistili, že již nevyhovuje bezpečnému provozu, jelikož stávající hliníková ohebná komínová vložka a kouřovod z pozinkovaného plechu je zoxidován a spalinová cesta je tím pádem netěsná.<br></span><span data-offset-key="6lri4-0-0" style="font-family: inherit;">Za komínovými dvířky v předsíni nás čekala hromada hliníkové silice vzniklá oxidací hliníkové komínové vložky, suti a prachu z komínového průduchu.<br></span><span data-offset-key="eetkl-0-0" style="font-family: inherit;">Plynový kotel byl nevyčištěný a očividně potřeboval kvalitní servis. K plynovému kotli nebyl zajištěný dostatečný neuzavíratelný přívod spalovacího vzduchu a větrání z venkovního ovzduší dle TPG 704 01.<br></span><span data-offset-key="2q7td-0-0" style="font-family: inherit;">Za dvířky na půdě nás čekalo další překvapení v podobě rozpojené komínové vložky a kontrolního otvoru bez víčka.</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="cueen-0-0" style="font-family: inherit;">Když se k tomu přičte krátká svislá část kouřovodu nad přerušovačem tahu (pouze 20 cm; min. je 40 cm) a celková délka kouřovodu delší než jedna čtvrtina účinné výšky komína (účinná výška komína 9,4 m, celková délka kouřovodu 3 m), pak se není čemu divit, když v těchto vysokých letních teplotách dochází ke zpětnému komínovému tahu a vracení se spalin.<br></span><span data-offset-key="k7p5-0-0" style="font-family: inherit;">V létě jsou na spalinovou cestu kladeny vyšší nároky než v zimě. V létě je totiž rozdíl teploty spalin a venkovní teploty menší než v zimě, tedy i rozdíl hustot je menší a tím pádem dochází k menšímu komínovému tahu.</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="3r3aa-0-0" style="font-family: inherit;">Vždy si prosím pečlivě vybírejte mezi kominíky a pozvěte si domů pouze ověřené a osvědčené odborně způsobilé osoby v oboru kominictví! V dnešní době, se řemeslníky skoro na každém rohu, to platí dvojnásobně!<br></span><span data-offset-key="1rqjd-0-0" style="font-family: inherit;">Nejlevnější neznamená nejkvalitnější! Kvalitní a profesionálně odvedená práce stojí čas, energii, znalosti, vzdělání, nástroje i peníze!<br></span><span data-offset-key="dddlg-0-0" style="font-family: inherit;">Kvalitně a poctivě provedená kontrola 1 spalinové cesty včetně vystavení zprávy zabere přibližně 40 - 60 minut. Pryč jsou ty doby, kdy se kominík podíval zrcátkem do komína, vypsal paragon, předal kalendář a měl za 10 minut hotovo :-)</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="9bbpi-0-0" style="font-family: inherit;">Myslete prosím na to, že jde především o zdraví, život a majetek Vás a Vašich blízkých! ;-)

</span></p><p style="position: relative; direction: ltr; font-family: inherit;"><img src="https://www.kominexpres.cz/uploads/img-1621.jpg"><img src="https://www.kominexpres.cz/uploads/img-1606.jpg"><img src="https://www.kominexpres.cz/uploads/img-1601.jpg"><img src="https://www.kominexpres.cz/uploads/img-1602.jpg"></p><p style="position: relative; direction: ltr; font-family: inherit;">&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1621-orig.jpg" target="_blank">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1606-orig.jpg" target="_blank">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1601-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1602-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a></p><p style="position: relative; direction: ltr; font-family: inherit;"><img src="https://www.kominexpres.cz/uploads/img-1603.jpg"><img src="https://www.kominexpres.cz/uploads/img-1605.jpg"><img src="https://www.kominexpres.cz/uploads/img-1619.jpg"><img src="https://www.kominexpres.cz/uploads/img-1625.jpg"></p><p style="position: relative; direction: ltr; font-family: inherit;">&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1603-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1605-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1619-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1625-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a><br></p><p style="position: relative; direction: ltr; font-family: inherit;"><img src="https://www.kominexpres.cz/uploads/img-1622.jpg"><img src="https://www.kominexpres.cz/uploads/img-1623.jpg"><img src="https://www.kominexpres.cz/uploads/img-1626.jpg"><img src="https://www.kominexpres.cz/uploads/img-1628.jpg"></p><p style="position: relative; direction: ltr; font-family: inherit;">&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1622-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1623-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1626-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1628-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a><br></p><p style="position: relative; direction: ltr; font-family: inherit;"><img src="https://www.kominexpres.cz/uploads/img-1616.jpg"><img src="https://www.kominexpres.cz/uploads/img-1610.jpg"><img src="https://www.kominexpres.cz/uploads/img-1611.jpg"><img src="https://www.kominexpres.cz/uploads/img-1612.jpg"><span data-offset-key="9bbpi-0-0" style="font-family: inherit;"><br></span></p><p style="position: relative; direction: ltr; font-family: inherit;"><span data-offset-key="9bbpi-0-0" style="font-family: inherit;">&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1616-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1610-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1611-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="https://www.kominexpres.cz/uploads/img-1612-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a><br></span></p><p style="position: relative; direction: ltr; font-family: inherit;"><br></p></div><p><span style="font-weight: 700;"><u>8. června 2017, 21:06:</u></span></p><p>Jsme hrdým sponzorem malých krmelínských házenkářů :-) Dresy se opravdu povedly! Přejeme spoustu radosti ze hry! :-)&nbsp;</p><p><img src="https://www.kominexpres.cz/uploads/dsc-0325.jpg"><br><a href="http://www.kominexpres.cz/uploads/dsc-0325-orig.jpg" target="_blank" style="background-color: rgb(255, 255, 255);">Plná velikost fotky</a><br></p><p><br></p><p><img src="https://www.kominexpres.cz/uploads/dsc-0396.jpg"><br><a href="http://www.kominexpres.cz/uploads/dsc-0396-orig.jpg" target="_blank">Plná velikost fotky</a><br></p><p><br></p><p><b><u>15. března 2017, 23:15:</u></b></p><p>Dnes jsme zažili televizní premiéru :-D Česká televize s námi strávila 1,5 hodiny na montáži detektorů CO v městských bytech Ostrava-Jih a zkušenost to byla veliká! Dívat se do kamery a mluvit na mikrofon je pro nezkušeného a neotrkaného člověka opravdový zážitek! Fuj, to byly nervy! :-D</p><p>Živý vstup s naším technikem, čas 2:00 zde:</p><p><a href="http://www.ceskatelevize.cz/ivysilani/10320835092-udalosti-v-regionech-plus-ostrava/417231100020315-udalosti-v-regionech-plus" target="_blank">http://www.ceskatelevize.cz/ivysilani/10320835092-udalosti-v-regionech-plus-ostrava/417231100020315-udalosti-v-regionech-plus</a></p><p>Reportáž v hlavních zprávách, čas 45:05 zde:</p><p><a href="http://www.ceskatelevize.cz/ivysilani/1097181328-udalosti" target="_blank">http://www.ceskatelevize.cz/ivysilani/1097181328-udalosti</a></p><p><br></p><p><b><u>15. března 2017, 22:51:</u></b></p><p>První článek o montáží detektorů CO naší firmou ve 234 městských bytech Ostrava-Jih je na světě ;-)</p><p><a href="http://www.patriotmagazin.cz/kvuli-mnozstvi-otrav-plynem-budou-v-bytech-v-ostrave-jihu-hlasice/" target="_blank">http://www.patriotmagazin.cz/kvuli-mnozstvi-otrav-plynem-budou-v-bytech-v-ostrave-jihu-hlasice/</a></p><p><br></p><p><b><u>15. března 2017, 22:49:</u></b></p><p>V pondělí jsme zahájili montáž detektorů CO značky Kidde 10 LLDCO do 234 městských bytů Ostrava-Jih. Montáž budeme provádět dle harmonogramu prací až do konce prvního týdne v dubnu, bude to fuška, už teď nás z toho bolí nohy, ale věříme, že je to perfektní způsob prevence otrav jedovatým oxidem uhelnatým.</p><p>Detektor Kidde 10LLDCO je ten nejkvalitnější produkt pro domácí použití, který se v současné době na trhu nachází.</p><p>Pro více informací o detektoru klikněte zde: <a href="https://www.kominexpres.cz/produkt/detektor-co-kidde-10lldco/2601">https://www.kominexpres.cz/produkt/detektor-co-kidde-10lldco/2601</a></p><p><br></p>'],
            ["id" => 9, "name" => "Reference", "text" => '<h5> Společnost <strong>Komínexpres s.r.o.</strong>&nbsp;v současnosti úspěšně působí po celé ČR, zejména v Moravskoslezském kraji na Ostravsku a Frýdecko-Místecku. Zde naleznete reference našich montáží a prací.</h5><br><div id="reference">Tady budou "Reference"</div>'],
            ["id" => 10, "name" => "Často kladené otázky", "text" => '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"> <div class="panel panel-default"> <div class="panel-heading" role="tab" id="heading1"> <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">Jakou výšku musí mít komín pro podtlakový spotřebič nad rovnou střechou?</a> </h4> </div><div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1"> <div class="panel-body">Výška komínu pro podtlakový spotřebič musí být minimálně 1,0 m nad atikou rovné střechy.</div></div></div></div>'],
            ["id" => 11, "name" => "Obchodní podmínky", "text" => '<h2>Úvodní ustanovení</h2>
        <ol>
            <li>Tyto obchodní podmínky (dále jen „obchodní podmínky“) obchodní společnosti Komínexpres s.r.o. se sídlem Sládkova 372/8, 702 00 Ostrava, identifikační číslo: 03684903, zapsané v obchodním rejstříku vedeném Krajským soudem v Ostravě, oddíl C, vložka 60936 (dále jen „prodávající“) upravují v souladu s ustanovením § 1751 odst. 1 zákona č. 89/2012 Sb., občanský zákoník (dále jen „občanský zákoník“) vzájemná práva a povinnosti smluvních stran vzniklé v souvislosti nebo na základě kupní smlouvy (dále jen „kupní smlouva“) uzavírané mezi prodávajícím a jinou fyzickou osobou (dále jen „kupující“) prostřednictvím internetového obchodu prodávajícího, tak také i smlouvy darovací uzavřené vedle uzavírané smlouvy kupní. Internetový obchod je prodávajícím provozován na webové stránce umístěné na internetové adrese https://www.kominexpres.cz (dále jen „webová stránka“), a to prostřednictvím rozhraní webové stránky (dále jen „webové rozhraní obchodu“).</li>
            <li>Obchodní podmínky se nevztahují na případy, kdy osoba, která má v úmyslu nakoupit zboží od prodávajícího, je právnickou osobou či osobou, jež jedná při objednávání zboží v rámci své podnikatelské činnosti nebo v rámci svého samostatného výkonu povolání.</li>
            <li>Ustanovení odchylná od obchodních podmínek je možné sjednat v kupní smlouvě. Odchylná ujednání v kupní smlouvě mají přednost před ustanoveními obchodních podmínek.</li>
            <li>Ustanovení obchodních podmínek jsou nedílnou součástí kupní smlouvy. Kupní smlouva a obchodní podmínky jsou vyhotoveny v českém jazyce. Kupní smlouvu lze uzavřít v českém jazyce.</li>
            <li>Znění obchodních podmínek může prodávající měnit či doplňovat. Tímto ustanovením nejsou dotčena práva a povinnosti vzniklá po dobu účinnosti předchozího znění obchodních podmínek.</li>
            <li>Kupující učiněním závazné objednávky stvrzuje, že akceptuje obchodní podmínky pro dodávku zboží vyhlášené prodávajícím. Vztahy mezi kupujícím a prodávajícím se řídí těmito obchodními podmínkami, které jsou zároveň pro obě strany závazné.</li>
        </ol>
        <h2>Uživatelský účet</h2>
        <ol>
            <li>Na základě registrace kupujícího provedené na webové stránce může kupující přistupovat do svého uživatelského rozhraní. Ze svého uživatelského rozhraní může kupující provádět objednávání zboží (dále jen „uživatelský účet“). V případě, že to webové rozhraní obchodu umožňuje, může kupující provádět objednávání zboží též bez registrace přímo z webového rozhraní obchodu.</li>
            <li>Při registraci na webové stránce a při objednávání zboží je kupující povinen uvádět správně a pravdivě všechny údaje. Údaje uvedené v uživatelském účtu je kupující při jakékoliv jejich změně povinen aktualizovat. Údaje uvedené kupujícím v uživatelském účtu a při objednávání zboží jsou prodávajícím považovány za správné.</li>
            <li>Přístup k uživatelskému účtu je zabezpečen uživatelským jménem a heslem. Kupující je povinen zachovávat mlčenlivost ohledně informací nezbytných k přístupu do jeho uživatelského účtu.</li>
            <li>Kupující není oprávněn umožnit využívání uživatelského účtu třetím osobám.</li>
            <li>Prodávající může zrušit uživatelský účet, a to zejména v případě, kdy kupující svůj uživatelský účet déle než 12 měsíců nevyužívá, či v případě, kdy kupující poruší své povinnosti z kupní smlouvy (včetně obchodních podmínek).</li>
            <li>Kupující bere na vědomí, že uživatelský účet nemusí být dostupný nepřetržitě, a to zejména s ohledem na nutnou údržbu hardwarového a softwarového vybavení prodávajícího, popř. nutnou údržbu hardwarového a softwarového vybavení třetích osob.</li>
        </ol>
        <h2>Uzavření kupní smlouvy</h2>
        <ol>
            <li>Veškerá prezentace zboží umístěná ve webovém rozhraní obchodu je informativního charakteru a prodávající není povinen uzavřít kupní smlouvu ohledně tohoto zboží. Ustanovení § 1732 odst. 2 občanského zákoníku se nepoužije.</li>
            <li>Webové rozhraní obchodu obsahuje informace o zboží, a to včetně uvedení cen jednotlivého zboží a nákladů za navrácení zboží, jestliže toto zboží ze své podstaty nemůže být navráceno obvyklou poštovní cestou. Ceny zboží jsou uvedeny včetně daně z přidané hodnoty a všech souvisejících poplatků. Ceny zboží zůstávají v platnosti po dobu, kdy jsou zobrazovány ve webovém rozhraní obchodu. Tímto ustanovením není omezena možnost prodávajícího uzavřít kupní smlouvu za individuálně sjednaných podmínek.</li>
            <li>Webové rozhraní obchodu obsahuje také informace o nákladech spojených s balením a dodáním zboží. Informace o nákladech spojených s balením a dodáním zboží uvedené ve webovém rozhraní obchodu platí pouze v případech, kdy je zboží doručováno v rámci území České republiky.</li>
            <li>Pro objednání zboží vyplní kupující objednávkový formulář ve webovém rozhraní obchodu. Objednávkový formulář obsahuje zejména informace o:
                <ol>
                    <li>objednávaném zboží (objednávané zboží „vloží“ kupující do elektronického nákupního košíku webového rozhraní obchodu),</li>
                    <li>způsobu úhrady kupní ceny zboží, údaje o požadovaném způsobu doručení objednávaného zboží a</li>
                    <li>informace o nákladech spojených s dodáním zboží (dále společně jen jako „objednávka“).</li>
                </ol>
            </li>
            <li>Před zasláním objednávky prodávajícímu je kupujícímu umožněno zkontrolovat a měnit údaje, které do objednávky kupující vložil, a to i s ohledem na možnost kupujícího zjišťovat a opravovat chyby vzniklé při zadávání dat do objednávky. Objednávku odešle kupující prodávajícímu kliknutím na tlačítko „Odeslat objednávku“. Údaje uvedené v objednávce jsou prodávajícím považovány za správné. Prodávající neprodleně po obdržení objednávky potvrdí kupujícímu doručení této objednávky elektronickou poštou, a to na adresu elektronické pošty kupujícího uvedenou v uživatelském účtu či v objednávce (dále jen „elektronická adresa kupujícího“). Toto potvrzení doručení objednávky není považováno za její akceptaci, kdy k akceptaci dochází až v souladu s bodem 3.9. těchto podmínek.</li>
            <li>Předmětem kupní smlouvy je pouze zboží uvedené v objednávce.</li>
            <li>V případě, že v průběhu doby, kdy bylo zboží objednáno, došlo k výrazné změně kurzu zahraniční měny nebo ke změně ceny či dodávaného sortimentu ze strany dodavatele prodávajícího, má prodávající právo objednávku po dohodě s kupujícím modifikovat nebo od ní jednostranně s okamžitou platností odstoupit. Stejné oprávnění si prodávající vyhrazuje i v případě, kdy výrobce přestane vyrábět nebo dodávat objednaný produkt nebo uvede na trh novou verzi produktu, popřípadě výrazným způsobem změní cenu produktu.</li>
            <li>Prodávající je vždy oprávněn v závislosti na charakteru objednávky (množství zboží, výše kupní ceny, předpokládané náklady na dopravu) požádat kupujícího o dodatečné potvrzení objednávky (osobně, písemně či telefonicky) a o úhradu finanční zálohy. Úhradu finanční zálohy bude prodávající zpravidla požadovat, pokud kupní cena přesáhne částku 30.000,- Kč, popřípadě kupující bude v prodlení s úhradou svých finančních závazků vůči prodávajícímu. Výše zálohy může dosáhnout až 100 % kupní ceny zboží (bez zohlednění ceny balení). V případě neuhrazení zálohy má prodávající právo od smlouvy odstoupit.</li>
            <li>Smluvní vztah mezi prodávajícím a kupujícím vzniká doručením akceptace objednávky (pouhé potvrzení doručení objednávky se za akceptaci nepovažuje), jež je prodávajícím zasláno kupujícímu elektronickou poštou, a to na adresu elektronické pošty kupujícího, popřípadě doručením objednaného plnění prodávajícím kupujícímu.</li>
            <li>Přijetí objednávky (akceptace) obsahuje celkovou cenu objednávky a přehled o druhu a množství zboží, náklady na dopravu a manipulaci se zbožím (množství palet a výše záloh na ně, vykládka hydraulickou rukou apod.). Počet palet, které budou zákazníkovi fakticky účtovány, může být odlišný od původně objednaného počtu palet, a bude vyplývat z reálného zabalení předmětu smlouvy tak, aby tento nebyl poškozen při dodání kupujícímu. Zvýšené množství palet je prodávající oprávněn kupujícímu doúčtovat.</li>
            <li>Kupující souhlasí s použitím komunikačních prostředků na dálku při uzavírání kupní smlouvy. Náklady vzniklé kupujícímu při použití komunikačních prostředků na dálku v souvislosti s uzavřením kupní smlouvy (náklady na internetové připojení, náklady na telefonní hovory) si hradí smluvní strany samy, přičemž tyto náklady se neliší od základní sazby.</li>
        </ol>
        <h2>Cena zboží a platební podmínky</h2>
        <ol>
            <li>Cenu zboží a případné náklady spojené s dodáním zboží dle kupní smlouvy může kupující uhradit prodávajícímu následujícími způsoby:
                <ol>
                    <li>v hotovosti v provozovně prodávajícího na adrese Proskovická 701, 739 24 Krmelín, a to při převzetí zboží;</li>
                    <li>v hotovosti na dobírku v místě určeném kupujícím v objednávce;</li>
                    <li>platební kartou na dobírku v místě určeném kupujícím v objednávce;</li>
                    <li>bezhotovostně předem bankovním převodem na účet prodávajícího č. 222250005/0600, vedený u společnosti MONETA Money Bank, a.s. (dále jen „účet prodávajícího“), a to na základě vystavené proforma faktury.</li>
                </ol>
            </li>
            <li>Společně s kupní cenou je kupující povinen zaplatit prodávajícímu také náklady spojené s balením a dodáním zboží ve smluvené výši. Není-li uvedeno výslovně jinak, rozumí se dále kupní cenou i náklady spojené s dodáním zboží.</li>
            <li>Prodávající nepožaduje od kupujícího zálohu či jinou obdobnou platbu, pokud z těchto podmínek (čl. 3.8) popřípadě zvoleného způsobu úhrady (bezhotovostní převod) nevyplývá jinak.</li>
            <li>V případě platby v hotovosti či v případě platby na dobírku je kupní cena splatná při převzetí zboží.</li>
            <li>V případě bezhotovostní platby je kupující povinen uhrazovat kupní cenu zboží společně s uvedením variabilního symbolu platby, a to dle proforma faktury. V případě bezhotovostní platby je závazek kupujícího uhradit kupní cenu splněn okamžikem připsání příslušné částky na účet prodávajícího.</li>
            <li>Prodávající je oprávněn, zejména v případě, že ze strany kupujícího nedojde k dodatečnému potvrzení objednávky (čl. 3.8), bude-li prodávajícím vyžadováno, požadovat uhrazení celé kupní ceny ještě před odesláním zboží kupujícímu. Ustanovení § 2119 odst. 1 občanského zákoníku se nepoužije.</li>
            <li>Případné slevy z ceny zboží poskytnuté prodávajícím kupujícímu nelze vzájemně kombinovat.</li>
            <li>Je-li to v obchodním styku obvyklé nebo je-li tak stanoveno obecně závaznými právními předpisy, vystaví prodávající ohledně plateb prováděných na základě kupní smlouvy kupujícímu daňový doklad – fakturu. Prodávající je plátcem daně z přidané hodnoty. Daňový doklad – fakturu vystaví prodávající kupujícímu po uhrazení ceny zboží a zašle jej v elektronické podobě na elektronickou adresu kupujícího.<br><br><p>Vzorový formulář pro odstoupení od kupní smlouvy naleznete <a href="https://www.kominexpres.cz/uploads/vzorovy-formular-pro-odstoupeni-od-kupni-smlouvy.docx">ZDE</a>.</p></li>
        </ol>
        <h2>Odstoupení od kupní smlouvy</h2>
        <ol>
            <li>Kupující bere na vědomí, že dle ustanovení § 1837 občanského zákoníku, nelze mimo jiné (dále uvedený výčet je pouze příkladný) odstoupit od kupní smlouvy o dodávce zboží, které bylo upraveno podle přání kupujícího nebo pro jeho osobu, od kupní smlouvy o dodávce zboží, které podléhá rychlé zkáze, jakož i zboží, které bylo po dodání nenávratně smíseno s jiným zbožím, od kupní smlouvy o dodávce zboží v uzavřeném obalu, které spotřebitel z obalu vyňal a z hygienických důvodů jej není možné vrátit a od kupní smlouvy o dodávce zvukové nebo obrazové nahrávky nebo počítačového programu, pokud porušil jejich původní obal.</li>
            <li>Nejedná-li se o případ uvedený v čl. 5.1 obchodních podmínek či o jiný případ, kdy nelze od kupní smlouvy odstoupit, má kupující v souladu s ustanovením § 1829 odst. 1 občanského zákoníku právo od kupní smlouvy odstoupit, a to do čtrnácti (14) dnů od převzetí zboží, přičemž v případě, že předmětem kupní smlouvy je několik druhů zboží nebo dodání několika částí, běží tato lhůta ode dne převzetí poslední dodávky zboží. Odstoupení od kupní smlouvy musí být prodávajícímu odesláno ve lhůtě uvedené v předchozí větě. Pro odstoupení od kupní smlouvy může kupující využít vzorový formulář poskytovaný prodávajícím, jenž tvoří přílohu obchodních podmínek. Odstoupení od kupní smlouvy může kupující zasílat mimo jiné na adresu provozovny prodávajícího či na adresu elektronické pošty prodávajícího eshop@kominexpres.cz.</li>
            <li>V případě odstoupení od kupní smlouvy dle čl. 5.2 obchodních podmínek se kupní smlouva od počátku ruší. Zboží musí být prodávajícímu vráceno do čtrnácti (14) dnů od odstoupení od smlouvy prodávajícímu. Odstoupí-li kupující od kupní smlouvy, nese kupující náklady spojené s navrácením zboží prodávajícímu, a to i v tom případě, kdy zboží nemůže být vráceno pro svou povahu obvyklou poštovní cestou.</li>
            <li>V případě odstoupení od smlouvy dle čl. 5.2 obchodních podmínek vrátí prodávající peněžní prostředky přijaté od kupujícího do čtrnácti (14) dnů od odstoupení od kupní smlouvy kupujícím, a to stejným způsobem, jakým je prodávající od kupujícího přijal. Prodávající je taktéž oprávněn vrátit plnění poskytnuté kupujícím již při vrácení zboží kupujícím či jiným způsobem, pokud s tím kupující bude souhlasit a nevzniknou tím kupujícímu další náklady. Odstoupí-li kupující od kupní smlouvy, prodávající není povinen vrátit přijaté peněžní prostředky kupujícímu dříve, než mu kupující zboží vrátí nebo prokáže, že zboží prodávajícímu odeslal.</li>
            <li>Nárok na úhradu škody vzniklé na zboží je prodávající oprávněn jednostranně započíst proti nároku kupujícího na vrácení kupní ceny.</li>
            <li>Prodávající je oprávněn odstoupit od kupní smlouvy v případě, že zboží se již nevyrábí nebo nedodává nebo dojde ke zvýšení ceny zboží na straně dodavatele prodávajícího anebo pokud není schopen zboží za jím uvedenou cenu v danou chvíli dodat z jiných důvodů. V takovém případě bude prodávající neprodleně kontaktovat kupujícího za účelem dohody o dalším postupu. Prodávající vrátí kupujícímu kupní cenu bez zbytečného odkladu, a to bezhotovostně na účet určený kupujícím.</li>
            <li>Je-li společně se zbožím poskytnut kupujícímu dárek, je darovací smlouva mezi prodávajícím a kupujícím uzavřena s rozvazovací podmínkou, že dojde-li k odstoupení od kupní smlouvy kupujícím, pozbývá darovací smlouva ohledně takového dárku účinnosti a kupující je povinen spolu se zbožím prodávajícímu vrátit i poskytnutý dárek.</li>
        </ol>
        <h2>Další práva a povinnosti smluvních stran</h2>
        <ol>
            <li>Kupující nabývá vlastnictví ke zboží zaplacením celé kupní ceny zboží.</li>
            <li>Prodávající není ve vztahu ke kupujícímu vázán žádnými kodexy chování ve smyslu ustanovení § 1826 odst. 1 písm. e) občanského zákoníku.</li>
            <li>K mimosoudnímu řešení spotřebitelských sporů z kupní smlouvy je příslušná Česká obchodní inspekce, se sídlem Štěpánská 567/15, 120 00 Praha 2, IČ: 00020869, internetová adresa: http://www.coi.cz.</li>
            <li>Prodávající je oprávněn k prodeji zboží na základě živnostenského oprávnění. Živnostenskou kontrolu provádí v rámci své působnosti příslušný živnostenský úřad. Dozor nad oblastí ochrany osobních údajů vykonává Úřad pro ochranu osobních údajů. Česká obchodní inspekce vykonává ve vymezeném rozsahu mimo jiné dozor nad dodržováním zákona č. 634/1992 Sb., o ochraně spotřebitele, ve znění pozdějších předpisů.</li>
            <li>Kupující tímto přebírá na sebe nebezpečí změny okolností ve smyslu § 1765 odst. 2 občanského zákoníku.</li>
        </ol>
        <h2>Evidence tržeb</h2>
        <ol>
            <li>Podle zákona o evidenci tržeb je prodávající povinen vystavit kupujícímu účtenku. Zároveň je povinen zaevidovat přijatou tržbu u správce daně online; v případě technického výpadku pak nejpozději do 48 hodin.</li>
            <li>Prodávající vystavuje účtenky dle zákona o evidenci tržeb v elektronické podobě zasílané na elektronickou adresu kupujícího či v papírové podobě v závislosti na zvolené platební metodě a způsobu přepravy.</li>
        </ol>
        <h2>Ochrana osobních údajů</h2>
        <ol>
            <li>Ochrana osobních údajů kupujícího, který je fyzickou osobou, je poskytována zákonem č. 101/2000 Sb., o ochraně osobních údajů, ve znění pozdějších předpisů.</li>
            <li>Kupující souhlasí se zpracováním těchto svých osobních údajů: jméno a příjmení, adresa bydliště, identifikační číslo, daňové identifikační číslo, adresa elektronické pošty a telefonní číslo (dále společně vše jen jako „osobní údaje“).</li>
            <li>Kupující souhlasí se zpracováním osobních údajů prodávajícím, a to pro účely realizace práv a povinností z kupní smlouvy a pro účely vedení uživatelského účtu. Nezvolí-li kupující jinou možnost, souhlasí se zpracováním osobních údajů prodávajícím také pro účely zasílání informací a obchodních sdělení kupujícímu. Souhlas se zpracováním osobních údajů v celém rozsahu dle tohoto článku není podmínkou, která by sama o sobě znemožňovala uzavření kupní smlouvy.</li>
            <li>Kupující bere na vědomí, že je povinen své osobní údaje (při registraci, ve svém uživatelském účtu, při objednávce provedené z webového rozhraní obchodu) uvádět správně a pravdivě a že je povinen bez zbytečného odkladu informovat prodávajícího o změně ve svých osobních údajích.</li>
            <li>Zpracováním osobních údajů kupujícího může prodávající pověřit třetí osobu, jakožto zpracovatele. Kromě osob dopravujících zboží nebudou osobní údaje prodávajícím bez předchozího souhlasu kupujícího předávány třetím osobám.</li>
            <li>Osobní údaje budou zpracovávány po dobu neurčitou. Osobní údaje budou zpracovávány v elektronické podobě automatizovaným způsobem nebo v tištěné podobě neautomatizovaným způsobem.</li>
            <li>Kupující potvrzuje, že poskytnuté osobní údaje jsou přesné a že byl poučen o tom, že se jedná o dobrovolné poskytnutí osobních údajů.</li>
            <li>V případě, že by se kupující domníval, že prodávající nebo zpracovatel (čl. 9.5) provádí zpracování jeho osobních údajů, které je v rozporu s ochranou soukromého a osobního života kupujícího nebo v rozporu se zákonem, zejména jsou-li osobní údaje nepřesné s ohledem na účel jejich zpracování, může:
                <ol>
                    <li>požádat prodávajícího nebo zpracovatele o vysvětlení,</li>
                    <li>požadovat, aby prodávající nebo zpracovatel odstranil takto vzniklý stav.</li>
                </ol>
            <li>Požádá-li kupující o informaci o zpracování svých osobních údajů, je mu prodávající povinen tuto informaci předat. Prodávající má právo za poskytnutí informace podle předchozí věty požadovat přiměřenou úhradu nepřevyšující náklady nezbytné na poskytnutí informace.</li>
        </ol>
        <h2>Zasílání obchodních sdělení a ukládání cookies</h2>
        <ol>
            <li>Kupující souhlasí se zasíláním informací souvisejících se zbožím, službami nebo podnikem prodávajícího na elektronickou adresu kupujícího a dále souhlasí se zasíláním obchodních sdělení prodávajícím na elektronickou adresu kupujícího. Kupující bere na vědomí, že tento souhlas může kdykoliv vzít zpět, a to písemně (e-mailem).</li>
            <li>Kupující souhlasí s ukládáním tzv. cookies na jeho počítač. V případě, že je nákup na webové stránce možné provést a závazky prodávajícího z kupní smlouvy plnit, aniž by docházelo k ukládání tzv. cookies na počítač kupujícího, může kupující souhlas podle předchozí věty kdykoliv odvolat.</li>
        </ol>
        <h2>Doručování</h2>
        <ol>
            <li>Kupujícímu může být doručováno na elektronickou adresu kupujícího.</li>
        </ol>
        <h2>Závěrečná ustanovení</h2>
        <ol>
            <li>Pokud vztah založený kupní smlouvou obsahuje mezinárodní (zahraniční) prvek, pak strany sjednávají, že vztah se řídí českým právem. Tímto nejsou dotčena práva spotřebitele vyplývající z obecně závazných právních předpisů.</li>
            <li>Je-li některé ustanovení obchodních podmínek neplatné nebo neúčinné, nebo se takovým stane, namísto neplatných ustanovení nastoupí ustanovení, jehož smysl se neplatnému ustanovení co nejvíce přibližuje. Neplatností nebo neúčinností jednoho ustanovení není dotknuta platnost ostatních ustanovení.</li>
            <li>Kupní smlouva včetně obchodních podmínek je archivována prodávajícím v elektronické podobě a není přístupná.</li>
            <li>Přílohu obchodních podmínek tvoří vzorový formulář pro odstoupení od kupní smlouvy.</li>
            <li>Kontaktní údaje prodávajícího: adresa pro doručování Proskovická 701, 739 24 Krmelín, adresa elektronické pošty eshop@kominexpres.cz, telefon +420 604 126 626.</li>
            <li>Momentem uzavření kupní smlouvy kupující přijímá veškerá ustanovení obchodních podmínek ve znění platném v den odeslání objednávky včetně ceny objednaného zboží uvedenou v potvrzené objednávce, nebylo-li v konkrétním případě prokazatelně dohodnuto jinak.</li>
            <li>Tyto obchodní podmínky nabývají platnosti a účinnosti dne 08.07.2017.</li>
        </ol>'],
            ["id" => 12, "name" => "Doprava a platba", "text" => '<h2>Přeprava a dodání zboží</h2>
            <ol>
                <li>Termín dodání zboží sděluje prodávající kupujícímu telefonicky nebo elektronickou poštou. Dodací lhůta je 2 až 45 pracovních dnů od přijetí objednávky, dle typu zboží. V případě, že požadované zboží není momentálně skladem ani u dodavatele prodávajícího, bude prodávající neprodleně o tomto informovat kupujícího a domluví se s kupujícím případně na jiném termínu dodání zboží.</li>
                <li>V případě, že je způsob dopravy smluven na základě zvláštního požadavku kupujícího, nese kupující riziko a případné dodatečné náklady spojené s tímto způsobem dopravy.</li>
                <li>Je-li prodávající podle kupní smlouvy povinen dodat zboží na místo určené kupujícím v objednávce, je kupující povinen převzít a zkontrolovat (množství, druh, jakost, stav palet apod.) zboží při dodání a potvrdit převzetí zboží na dodacím listu prodávajícího.</li>
                <li>V případě, že je z důvodů na straně kupujícího nutno zboží doručovat opakovaně nebo jiným způsobem, než bylo uvedeno v objednávce, je kupující povinen uhradit náklady spojené s opakovaným doručováním zboží, resp. náklady spojené s jiným způsobem doručení.</li>
                <li>Při převzetí zboží od přepravce je kupující povinen zkontrolovat neporušenost obalů zboží a v případě jakýchkoliv závad toto neprodleně oznámit přepravci. V případě shledání porušení obalu svědčícího o neoprávněném vniknutí do zásilky nemusí kupující zásilku od přepravce převzít.</li>
                <li>Další práva a povinnosti stran při přepravě zboží mohou upravit zvláštní dodací podmínky prodávajícího, jsou-li prodávajícím vydány.</li>
            </ol>'],
                ["id" => 13, "name" => "Reklamační řád", "text" => '<h2>Práva z vadného plnění</h2>
            <ol>
                <li>Práva a povinnosti smluvních stran ohledně práv z vadného plnění se řídí příslušnými obecně závaznými právními předpisy (zejména ustanoveními § 1914 až 1925, § 2099 až 2117 a § 2161 až 2174 občanského zákoníku a zákonem č. 634/1992 Sb., o ochraně spotřebitele, ve znění pozdějších předpisů).</li>
                <li>Prodávající odpovídá kupujícímu, že zboží při převzetí nemá vady. Zejména prodávající odpovídá kupujícímu, že v době, kdy kupující zboží převzal:
                    <ol>
                        <li>má zboží vlastnosti, které si strany ujednaly, a chybí-li ujednání, má takové vlastnosti, které prodávající nebo výrobce popsal nebo které kupující očekával s ohledem na povahu zboží a na základě reklamy jimi prováděné,</li>
                        <li>se zboží hodí k účelu, který pro jeho použití prodávající uvádí nebo ke kterému se zboží tohoto druhu obvykle používá,</li>
                        <li>zboží odpovídá jakostí nebo provedením smluvenému vzorku nebo předloze, byla-li jakost nebo provedení určeno podle smluveného vzorku nebo předlohy,</li>
                        <li>je zboží v odpovídajícím množství, míře nebo hmotnosti a</li>
                        <li>zboží vyhovuje požadavkům právních předpisů.</li>
                    </ol>
                </li>
                <li>Ustanovení uvedená v čl. 7.2 obchodních podmínek se nepoužijí u zboží prodávaného za nižší cenu na vadu, pro kterou byla nižší cena ujednána, na opotřebení zboží způsobené jeho obvyklým užíváním, u použitého zboží na vadu odpovídající míře používání nebo opotřebení, kterou zboží mělo při převzetí kupujícím, nebo vyplývá-li to z povahy zboží.</li>
                <li>Projeví-li se vada v průběhu šesti měsíců od převzetí, má se za to, že zboží bylo vadné již při převzetí. Kupující je oprávněn uplatnit právo z vady, která se vyskytne u spotřebního zboží v době dvaceti čtyř měsíců od převzetí.</li>
                <li>Práva z vadného plnění uplatňuje kupující u prodávajícího na adrese jeho provozovny, v níž je přijetí reklamace možné s ohledem na sortiment prodávaného zboží, případně i v sídle nebo místě podnikání.</li>
                <li>Další práva a povinnosti stran související s odpovědností prodávajícího za vady upravuje reklamační řád prodávajícího.</li>
            </ol>'],
            ["id" => 14, "name" => "Komínexpres s.r.o", "text" => '<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <h2>Fakturační adresa</h2>
                <ul>
                    <li>Sládkova 372/8, 702 00 Ostrava</li>
                    <li>IČ: 03684903<b><br></b></li>
                    <li>DIČ: CZ03684903</li>
                    <li>Společnost je zapsána v obchodním rejstříku vedeném Krajským soudem v Ostravě, oddíl C, vložka 60936
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h2>Pobočka</h2>
                <ul>
                    <li>Proskovická 701, 739 24 Krmelín</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <h2>Kominictví:</h2>
                <ul>
                    <li><b>Telefon:</b> +420 558 712 529</li>
                    <li><b>Mobil:</b> +420 604 806 106</li>
                    <li><b>E-mail:</b> <a href="mailto:info@kominexpres.cz">info@kominexpres.cz</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h2>E-shop:</h2>
                <ul>
                    <li><span style="font-weight: 700;">Mobil:</span>&nbsp;+420 604 126 626</li>
                    <li><span style="font-weight: 700;">E-mail:</span>&nbsp;<a href="mailto:eshop@kominexpres.cz">eshop@kominexpres.cz</a>
                    </li>
                </ul>
            </div>
        </div><p><br></p>
    </div>
</div><div id="contactForm">Tady bude "Kontaktní Formulář"</div>'],
            ["id" => 15, "name" => "Plynná paliva", "text" => '<h2>Plynové spotřebiče s tlakovými hořáky, turbo kotle, kondenzační kotle nebo zářiče:</h2><h3>Odkouření pro tyto spotřebiče provádíme z plastových nebo nerezových systémů. Odtah spalin může být:</h3><ul><li>samostatný - pouze pro odtah spalin</li><li>koaxiální - pro samostatný přívod vzduchu a samostatný odtah spalin<br></li></ul><p>Pro jeden spotřebič nebo pro kaskády pro více spotřebičů. Pro přívod vzduchu a odtah spalin od těchto spotřebičů je celá škála řešení a možností provedení. Všechny jsou opět stejně bezproblémové a většinou velmi rychle vyřešeny v době 4 - 6 hodin včetně potřebných bouracích a zednických prací a úklidu.</p>'],
            ["id" => 16, "name" => "Pevná paliva", "text" => '<h2>Vložkování komínů pro spotřebiče na pevná (tuhá) a kapalná paliva, pro krby, kamna, kotle na dřevo, uhlí, dřevoplyn, biomasu atd.:</h2><p>Komíny pro tyto spotřebiče se dle ČSN 73 4201:2010 vložkují kyselinovzdornými nerezovými pevnými vložkami tř. 1.4404 o tl. 0,8 mm.<br></p><p>Dopojení kouřovodů provádíme také z nerezi stejné kvality. Pokud se jedná o spotřebiče s tlakovými hořáky, jsou tyto vložky doplněny o speciální vysokoteplotní těsnění, tlaková dvířka, komín je doplněn o vzduchový průduch atd. Tento typ vložkování je již o něco komplikovanější.</p><p>Samotné vložkování trvá 1/2 - 1 den. Protože pro uvedené spotřebiče na pevná (tuhá) a jiná paliva musí být průměr vložky již větší, ve většině případů se musí provádět také frézování komínů. Celý proces tedy trvá 1 - 2 dny, ovšem vzhledem ke zkušenostem, technologii a specializovanému vybavení i toto vložkování provádíme v co nejkratší době a tak, aby došlo k co nejmenšímu narušení komfortu uživatele.</p><p>&nbsp;</p>']
        ];
        foreach ($pages_arr as $page)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO articles (ate_id, ate_name, ate_active) VALUES (?,?,?)");
            $prp1->execute(array($page['id'], $page['name'], true));
            $prp2 = $this->pgsql->prepare("INSERT INTO article_histories (ate_id, date_from, act_id, ahy_content, date_to)
                                                      SELECT ?,?,act_id,?,? FROM accounts WHERE act_email = 'tomas.bill@kominexpres.cz'");
            $prp2->execute(array($page['id'], date('Y-m-d H:i:s', strtotime("2001-01-01")), $page['text'], null));
        }

        // menu_footer_titles
        $menu_footer_titles = [
            ["id" => 2, "mfe_name" => "Informace", "place" => 1],
            ["id" => 3, "mfe_name" => "Služby", "place" => 2]
        ];
        foreach ($menu_footer_titles as $title)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO menu_footer_titles (id,mfe_name,place) VALUES (?,?,?)");
            $prp1->execute(array($title['id'], $title['mfe_name'], $title['place']));
        }

        // meta_keywords
        $sql = /** @lang MySQL */"SELECT meta_keywords FROM pages";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $keywords_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($keywords_arr as $keywords)
        {
            $ex = explode(",", $keywords['meta_keywords']);
            foreach ($ex as $keyword) {
                $word = trim($keyword);
                if (empty($word)) {continue;}

                $sql = $this->pgsql->prepare("SELECT count(*) ct FROM meta_keywords WHERE mkd_name = ?");
                $sql->execute(array($word));
                $sd = $sql->fetchColumn(0);
                if (strcmp($sd, "1") == 0) {continue;}

                $prp1 = $this->pgsql->prepare("INSERT INTO meta_keywords (mkd_name) VALUES (?)");
                $prp1->execute(array($word));
            }
        }

        // menu_items
        $menu_items = [
            ["id" => 1, "ate_id" => 1, "fer_mfe_id" => null, "man_id" => null, "mim_type" => "MAN", "mim_name" => "Domů", "url" => "", "meta_description" => "Kominictví Komínexpres z Ostravy Vám nabízí kompletní kominické práce, služby a internetový obchod s nabídkou kouřovodů, komínových vložek a další", "fer_place" => null, "man_place" => 1, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 2, "ate_id" => null, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "E-shop", "url" => "eshop", "meta_description" => "V našem e-shopu naleznete široký výběr komínových vložek, kouřovodů, krbů a krbových kamen", "fer_place" => 1, "man_place" => 2, "mim_ecommerce" => 'true', "cct_id" => 4],
            ["id" => 3, "ate_id" => 2, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Vložkování", "url" => "vlozkovani", "meta_description" => "Vložkování komínů pro spotřebiče na plynná paliva, pevná (tuhá) a kapalná paliva, pro krby, kamna, kotle na dřevo, uhlí, dřevo a dalších", "fer_place" => 5, "man_place" => 3, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 4, "ate_id" => 15, "fer_mfe_id" => null, "man_id" => 3, "mim_type" => "MAN", "mim_name" => "Plynná paliva", "url" => "vlozkovani/plynna-paliva", "meta_description" => "Vložkování komínů pro spotřebiče na plynná paliva, pro krby, kamna, kotle na dřevo, uhlí, dřevo a dalších", "fer_place" => null, "man_place" => 1, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 5, "ate_id" => 16, "fer_mfe_id" => null, "man_id" => 3, "mim_type" => "MAN", "mim_name" => "Pevná paliva", "url" => "vlozkovani/pevna-paliva", "meta_description" => "Vložkování komínů pro spotřebiče na pevná (tuhá) paliva, pro krby, kamna, kotle na dřevo, uhlí, dřevo a dalších", "fer_place" => null, "man_place" => 2, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 6, "ate_id" => 3, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Frézování", "url" => "frezovani", "meta_description" => "Frézování komínu je rychlejší a levnější varianta než zbourání stávajícího komínového tělesa a postavení nového", "fer_place" => 2, "man_place" => 4, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 7, "ate_id" => 4, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Opravy", "url" => "opravy", "meta_description" => "Specialisté na opravy komínů", "fer_place" => 4, "man_place" => 5, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 8, "ate_id" => 5, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Stavba", "url" => "stavba", "meta_description" => "Specialisté na stavby komínů", "fer_place" => 6, "man_place" => 6, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 9, "ate_id" => 6, "fer_mfe_id" => 3, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Revize", "url" => "revize", "meta_description" => "Specialisté na revize komínů", "fer_place" => 3, "man_place" => 7, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 10, "ate_id" => 7, "fer_mfe_id" => 2, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Ostatní", "url" => "ostatni", "meta_description" => "Novinky, reference a ostatní informace ze světa kominictví", "fer_place" => 5, "man_place" => 8, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 11, "ate_id" => 8, "fer_mfe_id" => null, "man_id" => 10, "mim_type" => "MAN", "mim_name" => "Aktuality", "url" => "ostatni/aktuality", "meta_description" => "Novinky ze světa kominictví", "fer_place" => null, "man_place" => 1, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 12, "ate_id" => 14, "fer_mfe_id" => null, "man_id" => null, "mim_type" => "MFR", "mim_name" => "Kontakt", "url" => "kontakt", "meta_description" => "Kontakt - Komínexpres s.r.o. - Sládkova 372/8, 702 00 Moravská Ostrava a Přívoz", "fer_place" => 3, "man_place" => 9, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 13, "ate_id" => 10, "fer_mfe_id" => 2, "man_id" => null, "mim_type" => "FER", "mim_name" => "Často kladené otázky", "url" => "casto-kladene-otazky", "meta_description" => "Často kladené otázky", "fer_place" => 1, "man_place" => null, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 14, "ate_id" => 9, "fer_mfe_id" => null, "man_id" => 10, "mim_type" => "MAN", "mim_name" => "Reference", "url" => "ostatni/reference", "meta_description" => "Reference našich montáží a prací", "fer_place" => null, "man_place" => 2, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 15, "ate_id" => 12, "fer_mfe_id" => 2, "man_id" => null, "mim_type" => "FER", "mim_name" => "Doprava a platba", "url" => "obchodni-podminky/doprava-a-platba", "meta_description" => "Obchodní podmínky - Doprava a platba", "fer_place" => 2, "man_place" => null, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 16, "ate_id" => 11, "fer_mfe_id" => 2, "man_id" => null, "mim_type" => "FER", "mim_name" => "Obchodní podmínky", "url" => "obchodni-podminky", "meta_description" => "Obchodní podmínky", "fer_place" => 4, "man_place" => null, "mim_ecommerce" => 'false', "cct_id" => 1],
            ["id" => 17, "ate_id" => 13, "fer_mfe_id" => 2, "man_id" => null, "mim_type" => "FER", "mim_name" => "Reklamační řád", "url" => "obchodni-podminky/reklamacni-rad", "meta_description" => "Obchodní podmínky - Reklamační řád", "fer_place" => 6, "man_place" => null, "mim_ecommerce" => 'false', "cct_id" => 1],
        ];
        foreach ($menu_items as $menu_item)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO menu_items
                      (mim_id, ate_id, mfe_id, mim_mim_id, mim_type, mim_name, url, meta_description, fer_place, man_place, mim_ecommerce,cct_id) 
                      VALUES
                      (?,?,?,?,?,?,?,?,?,?,?,?)");
            $prp1->execute(array($menu_item['id'], $menu_item['ate_id'], $menu_item['fer_mfe_id'], $menu_item['man_id'], $menu_item['mim_type'], $menu_item['mim_name'], $menu_item['url'], $menu_item['meta_description'], $menu_item['fer_place'], $menu_item['man_place'], $menu_item['mim_ecommerce'], $menu_item['cct_id']));
        }

        // menu_items_meta_keywords
        $prp1 = $this->pgsql->prepare("INSERT INTO menu_items_meta_keywords (mkd_id, mim_id) SELECT meta_keywords.id, menu_items.mim_id FROM meta_keywords, menu_items");
        $prp1->execute(array());

        $dan = $this->pgsql->sendQueryAndFetch("SELECT act_id FROM accounts WHERE act_email = ?", ['danielbill95@gmail.com'])["act_id"];

        // products
        $sql = /** @lang MySQL */"SELECT *
            FROM products
            LEFT JOIN products_menu_option ON products.id = products_menu_option.id_product";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $products_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($products_arr as $product)
        {
            $barcode = $product['barcode'];
            if(strlen($product['barcode']) == 7) {
                $barcode = '0'.$barcode;
            } else if (strlen($product['barcode']) < 7) {
                $barcode = null;
            }
            $warranty = empty(trim($product['zaruka'])) ? null : intval($product['zaruka']);
            if (strpos($product['zaruka'], 'let') !== false) {
                $warranty *= 12;
            }

            $prp1 = $this->pgsql->prepare("SELECT products_prc(p_id:=?, p_code:=?, p_sellable:=?, p_availability:=?, p_active:=?, p_barcode:=?, p_description_short:=?, p_description_long:=?, p_warranty:=?, p_mur_name:=?, p_url:=?, p_sale:=?)");
            $prp1->execute(array($product['id'], $product['kod'], $product["saleable"] == 1 ? 1 : 0, $product['dostupnost'], $product['status'] == 0 ? 1 : 0, $barcode, $product['maly_popis'], $product['popis'], $warranty, $product['vyrobce'], $product['url'], $product['akce'] == 1 ? 1: 0));

            $prp1 = $this->pgsql->prepare("INSERT INTO product_histories (put_id, date_from, act_id, phy_name, price_vat, price_no_vat, weight, date_to, option_title) VALUES (?,?,?,?,?,?,?,?,?)");
            $prp1->execute(array($product['id'], date('Y-m-d H:i:s', strtotime("2001-01-01")), $dan, $product['nazev'], $product['cenasdph'], $product['cenabezdph'], $product['hmotnost'], null, $product['name']));

        }

        // product_images
        $sql = /** @lang MySQL */"SELECT product_id, images.id, mainImage, substr(images.path, 9, length(images.path)-9-length(images.type)) path
FROM images_files
  JOIN images ON images_files.image = images.path
                 AND image is not null
                 AND image <> ''
                 AND product_id IN (SELECT id FROM products)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_images_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_images_arr as $put_iae)
        {
            $prp1 = $this->pgsql->prepare("SELECT product_images_prc(?, ?, ?, ?)");
            $prp1->execute(array($put_iae['product_id'], $put_iae['id'], $put_iae['mainImage'], $put_iae['path']));
        }

        // product_files
        $sql = /** @lang MySQL */"SELECT product_id, files.id
                FROM images_files
                JOIN files ON images_files.file = files.path
                  AND file is not null 
                  AND file <> ''
                  AND product_id IN (SELECT id FROM products)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_files_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_files_arr as $put_fie)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_files (fie_id, put_id) VALUES (?,?)");
            $prp1->execute(array($put_fie['id'], $put_fie['product_id']));
        }

        // product_payments
        $sql = /** @lang MySQL */"SELECT * FROM product_payments WHERE put_id IN (SELECT id FROM products)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_payments_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_payments_arr as $ppt)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_payments (put_id, pmt_id) VALUES (?,?)");
            $prp1->execute(array($ppt['put_id'], $ppt['pmt_id']));
        }

        // product_shippings
        $sql = /** @lang MySQL */"SELECT *
                FROM shipping_product
                WHERE product_id IN (SELECT id FROM products) AND shipping_id IN (SELECT ship_id FROM shipping)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_shippings_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_shippings_arr as $psg)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_shippings (put_id, spg_id, date_from, show_shipping, fixed_price, date_to) VALUES (?,?,?,?,?,?)");
            $prp1->execute(array($psg['product_id'], $psg['shipping_id'], date('Y-m-d H:i:s', strtotime("2001-01-01")), $psg['ship_hide'] == 1 ? 0 : 1, $psg['price'], null));
        }

        // product_categories
        $sql = /** @lang MySQL */"SELECT product_id, cat_id
                FROM cats_ids
                WHERE product_id IN (SELECT id FROM products)
                  AND cat_id IN (SELECT id FROM categorylist)
                  AND cat_id <> 1";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_categories_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_categories_arr as $pce)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_categories (cey_id, put_id, date_from, date_to, pce_main) VALUES (?,?,?,?,?)");
            $prp1->execute(array($pce['cat_id'], $pce['product_id'], date('Y-m-d H:i:s', strtotime("2001-01-01")), null, 0));
        }

        // product_group_discounts
        $sql = /** @lang MySQL */"SELECT product_id, group_id, sleva
                FROM sleva_product
                WHERE group_id IN (SELECT id FROM `group`)
                  AND product_id IN (SELECT id FROM products)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_group_discounts_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_group_discounts_arr as $pgt)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_group_discounts (put_id, grp_id, discount, date_from, date_to) VALUES (?,?,?,?,?)");
            $prp1->execute(array($pgt['product_id'], $pgt['group_id'], $pgt['sleva'], date('Y-m-d H:i:s', strtotime("2001-01-01")), null));
        }

        // product_menu_options
        $sql = /** @lang MySQL */"SELECT products_menu_options.name, id_product
                FROM products_menu_options
                JOIN products_menu_option ON products_menu_options.id_option = products_menu_option.id_option
                    AND id_product IN (SELECT id FROM products)";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_menu_options_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_menu_options_arr as $pmn)
        {
            $prp1 = $this->pgsql->prepare("INSERT INTO product_menu_options (pmn_name, put_id, date_from, date_to) VALUES (?,?,?,?)");
            $prp1->execute(array($pmn['name'], $pmn['id_product'], date('Y-m-d H:i:s', strtotime("2001-01-01")), null));
        }

        // product_parameter_values
        $sql = /** @lang MySQL */"SELECT id, null color, TRIM(material) material, TRIM(type) type, diameter
                FROM products
                WHERE color = ''
                UNION
                SELECT id, TRIM(color), TRIM(material), TRIM(type), diameter
                FROM products
                WHERE color <> ''";
        $stmt = $this->mysql->prepare($sql);
        $stmt->execute([]);
        $product_parameter_values_arr = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($product_parameter_values_arr as $ppe)
        {
            if($ppe['color'] !== null) {
                $prp1 = $this->pgsql->prepare("SELECT product_parameter_values_prc(?,?,?)");
                $prp1->execute(array($ppe['id'], $ppe['color'], 'Barva'));
            }

            $prp1 = $this->pgsql->prepare("SELECT product_parameter_values_prc(?,?,?)");
            $prp1->execute(array($ppe['id'], $ppe['material'], 'Materiál'));

            $prp1 = $this->pgsql->prepare("SELECT product_parameter_values_prc(?,?,?)");
            $prp1->execute(array($ppe['id'], $ppe['diameter'], 'Průměr'));

            $prp1 = $this->pgsql->prepare("SELECT product_parameter_values_prc(?,?,?)");
            $prp1->execute(array($ppe['id'], $ppe['type'], 'Typ'));
        }

        $this->pgsql->sendQueryOnly("SELECT setval('articles_id_seq', COALESCE((SELECT MAX(ate_id)+1 FROM articles), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('categories_id_seq', COALESCE((SELECT MAX(cey_id)+1 FROM categories), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('company_addresses_id_seq', COALESCE((SELECT MAX(cas_id)+1 FROM company_addresses), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('company_contacts_cct_id_seq', COALESCE((SELECT MAX(cct_id)+1 FROM company_contacts), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('company_details_cdl_id_seq', COALESCE((SELECT MAX(cdl_id)+1 FROM company_details), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('delivery_type_heurekas_id_seq', COALESCE((SELECT MAX(id)+1 FROM delivery_type_heurekas), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('file_categories_id_seq', COALESCE((SELECT MAX(fcy_id)+1 FROM file_categories), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('files_id_seq', COALESCE((SELECT MAX(fie_id)+1 FROM files), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('groups_id_seq', COALESCE((SELECT MAX(grp_id)+1 FROM groups), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('image_categories_id_seq', COALESCE((SELECT MAX(icy_id)+1 FROM image_categories), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('images_id_seq', COALESCE((SELECT MAX(iae_id)+1 FROM images), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('manufacturers_id_seq', COALESCE((SELECT MAX(id)+1 FROM manufacturers), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('marking_mig_id_seq', COALESCE((SELECT MAX(mig_id)+1 FROM markings), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('menu_footer_titles_id_seq', COALESCE((SELECT MAX(id)+1 FROM menu_footer_titles), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('menu_items_id_seq', COALESCE((SELECT MAX(mim_id)+1 FROM menu_items), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('meta_keywords_id_seq', COALESCE((SELECT MAX(id)+1 FROM meta_keywords), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('parameter_groups_id_seq', COALESCE((SELECT MAX(pgp_id)+1 FROM parameter_groups), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('parameters_id_seq', COALESCE((SELECT MAX(prr_id)+1 FROM parameters), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('payments_id_seq', COALESCE((SELECT MAX(id)+1 FROM payments), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('products_id_seq', COALESCE((SELECT MAX(put_id)+1 FROM products), 1), false)", []);
        $this->pgsql->sendQueryOnly("SELECT setval('shippings_id_seq', COALESCE((SELECT MAX(id)+1 FROM shippings), 1), false)", []);
    }
}