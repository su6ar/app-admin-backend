<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 21:07
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\DeliveryTypeHeurekaAlreadyExistsException;
use Kominexpres\src\App\Exceptions\DeliveryTypeHeurekaNotFoundException;
use Kominexpres\src\App\Exceptions\DeliveryTypeHeurekaIsUsedException;
use Kominexpres\src\App\Storage\Database;

/**
 * Class DeliveryTypeHeurekaGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class DeliveryTypeHeurekaGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM delivery_type_heurekas ORDER BY id ASC";
    private const SQL_EXISTS_BY_ID = "SELECT id FROM delivery_type_heurekas WHERE id = ?";
    private const SQL_FIND_WHERE = "SELECT * FROM delivery_type_heurekas WHERE id = ?";
    private const SQL_INSERT = "INSERT INTO delivery_type_heurekas (dta_name) VALUES (?)";
    private const SQL_DELETE = "DELETE FROM delivery_type_heurekas WHERE id = ?";
    private const SQL_UPDATE = "UPDATE delivery_type_heurekas SET dta_name = ? WHERE id = ?";
    private const SQL_EXISTS_BY_NAME = "SELECT dta_name FROM delivery_type_heurekas WHERE dta_name = ?";
    private const SQL_IS_USED = "SELECT id FROM shippings WHERE dta_id = ?";

    /**
     * DeliveryTypeHeurekaGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getDtaName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getDtaName(), $object->getId()]);
    }

    /**
     * @param int $id
     * @throws DeliveryTypeHeurekaNotFoundException
     */
    public function existsById(int $id)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_ID, [$id]);
        if($rowCount == 0)
            throw new DeliveryTypeHeurekaNotFoundException("Heureka's delivery type with id: '{$id}' was not found!");
    }

    /**
     * @param string $dtaName
     * @throws DeliveryTypeHeurekaAlreadyExistsException
     */
    public function existsByName(string $dtaName)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_NAME, [$dtaName]);
        if($rowCount > 0)
            throw new DeliveryTypeHeurekaAlreadyExistsException($dtaName);
    }

    /**
     * @param $id
     * @throws DeliveryTypeHeurekaIsUsedException
     */
    public function isUsed($id)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_IS_USED, [$id]);
        if($rowCount > 0)
            throw new DeliveryTypeHeurekaIsUsedException($id);
    }
}