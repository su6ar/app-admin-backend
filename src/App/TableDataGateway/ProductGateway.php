<?php
/**
 * Created by Daniel Bill
 * Date: 24.08.2018
 * Time: 15:21
 */

namespace Kominexpres\src\App\TableDataGateway;

use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;


class ProductGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM products";
    private const SQL_SELECT_ALL_BY_CEY_ID_COUNT = "SELECT count(*) count FROM products put JOIN product_histories phy ON put.put_id = phy.put_id AND date_to IS NULL JOIN product_categories pcy ON put.put_id = pcy.put_id AND pcy.cey_id = ? AND pcy.date_to IS NULL";
    private const SQL_SELECT_ALL_BY_CEY_ID = "SELECT phy.put_id, put.barcode, put.code, phy.phy_name, phy.price_no_vat, put.active FROM products put JOIN product_histories phy ON put.put_id = phy.put_id AND date_to IS NULL JOIN product_categories pcy ON put.put_id = pcy.put_id AND pcy.cey_id = ? AND pcy.date_to IS NULL";
    private const SQL_DELETE = "UPDATE products SET active = false WHERE put_id = ?";
    private const SQL_FIND_WHERE = "SELECT *, (SELECT ARRAY_TO_JSON(ARRAY(select row(psg.put_id, psg.spg_id, psg.date_from, psg.show_shipping, psg.fixed_price, psg.date_to)::product_shippings FROM product_shippings psg WHERE psg.put_id = ? AND psg.date_to IS NULL))) shippings, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pcy.cey_id, pcy.put_id, pcy.date_from, pcy.date_to, pcy.pce_main)::product_categories FROM product_categories pcy WHERE pcy.put_id = ? AND pcy.date_to IS NULL))) categories, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pfe.fie_id, pfe.put_id)::product_files FROM product_files pfe WHERE pfe.put_id = ?))) files, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pie.put_id, pie.iae_id, pie.iae_main)::product_images FROM product_images pie WHERE pie.put_id = ?))) images, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pgt.put_id, pgt.grp_id, pgt.date_from, pgt.discount, pgt.date_to)::product_group_discounts FROM product_group_discounts pgt WHERE pgt.put_id = ? AND pgt.date_to IS NULL))) group_discounts, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(ppt.put_id, ppt.pmt_id)::product_payments FROM product_payments ppt WHERE ppt.put_id = ?))) payments, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pmo.pmn_name, pmo.put_id, pmo.date_from, pmo.date_to, pmo.pmn_id)::product_menu_options FROM product_menu_options pmo WHERE pmo.put_id = ? AND pmo.date_to IS NULL))) menu_options, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pve.pve_id, pve.prr_id, pve.pve_value)::parameter_values FROM parameter_values pve WHERE pve.pve_id in (select ppe.pve_id from product_parameter_values ppe where ppe.put_id = ?)))) parameter_values, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(ptn.pte_id, ptn.put_id)::product_type_options FROM product_type_options ptn WHERE ptn.put_id = ?))) type_options, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(psr.ser_id, psr.ser_id)::product_stickers FROM product_stickers psr WHERE psr.put_id = ?))) stickers, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(act.act_id, act.amr_avatar, act.amr_first_name, act.amr_last_name, phy.date_from, phy.date_to)::product_history_account_t FROM accounts act JOIN product_histories phy ON act.act_id = phy.act_id AND phy.put_id = ?))) history, " .
                                      "(SELECT ARRAY_TO_JSON(ARRAY(select row(pmd.put_id, pmd.mkd_id)::product_meta_keywords FROM product_meta_keywords pmd WHERE pmd.put_id = ?))) meta_keywords ".
                                    "FROM products put " .
                                      "LEFT JOIN product_histories phy USING (put_id) " .
                                        "WHERE put.put_id = ? " .
                                          "AND phy.date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO products (mur_id, pgp_id, code, availability, active, barcode, description_short, description_long, warranty, description_meta, put_url, sellable, sale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING put_id";
    private const SQL_EXISTS_BY_CODE = "SELECT put_id FROM products WHERE code = ? AND put_id <> ?";
    private const SQL_EXISTS_BY_CODE_NULL = "SELECT put_id FROM products WHERE code = ?";
    private const SQL_UPDATE = "UPDATE products SET sync_feed_status = ?, mur_id = ?, code = ?, pgp_id = ?, availability = ?, active = ?, barcode = ?, description_short = ?, description_long = ?, warranty = ?, description_meta = ?, put_url = ?, sellable = ?, sale = ? WHERE put_id = ?";
    private const SQL_EXISTS_BY_PUT_ID = "SELECT put_id FROM products WHERE put_id = ?";
    private const SQL_SELECT_BY_PUT_ID = "SELECT * FROM products WHERE put_id = ?";
    private const SQL_DELETE_PGP_ID = "UPDATE products SET pgp_id = NULL WHERE pgp_id = ?";
    private const SQL_SELECT_BY_PGP_ID = "SELECT put_id FROM products WHERE pgp_id = ?";

    /**
     * ProductGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @param $putId
     * @return array
     * @throws EntityNotFoundException
     */
    public function existsByPutId($putId): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_PUT_ID, [$putId]);
        if (empty($arr))
            throw new EntityNotFoundException("Product", $putId);
        return $arr;
    }

    /**
     * @param $putId
     * @return array
     * @throws EntityNotFoundException
     */
    public function selectByPutId($putId): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_BY_PUT_ID, [$putId]);
        if (empty($arr))
            throw new EntityNotFoundException("Product", $putId);
        return $arr;
    }

    /**
     * @param $pgpId
     * @return array
     */
    public function selectByPgpId($pgpId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_PGP_ID, [$pgpId]);
    }

    public function deletePgpId($pgpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_PGP_ID, [$pgpId]);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectAllByCeyId($ceyId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL_BY_CEY_ID, [$ceyId]);
    }

    public function selectAllByCeyIdCount($ceyId): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_ALL_BY_CEY_ID_COUNT, [$ceyId]);
    }

    /**
     * @param $code
     * @param null $putId
     * @throws EntityAlreadyExistsException
     */
    public function existsByCode($code, $putId = null)
    {
        if ($putId === null)
        {
            $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_CODE_NULL, [$code]);
        }
        else $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_CODE, [$code, $putId]);
        if ($rowCount > 0)
            throw new EntityAlreadyExistsException("Product code", $code);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object, $object, $object, $object, $object, $object, $object, $object, $object, $object, $object, $object, $object]);
        if (empty($arr))
            throw new EntityNotFoundException("Product", $object);
        return $arr;
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getMurId(), $object->getPgpId(), $object->getCode(), $object->getAvailability(), (int)$object->isActive(), $object->getBarcode(), $object->getDescriptionShort(), $object->getDescriptionLong(), $object->getWarranty(), $object->getDescriptionMeta(), $object->getPutUrl(), (int)$object->isSellable(), (int)$object->isSale()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryAndFetchAll(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryAndFetch(self::SQL_UPDATE, [(int)$object->isSyncFeedStatus(), $object->getMurId(), $object->getCode(), $object->getPgpId(), $object->getAvailability(), (int)$object->isActive(), $object->getBarcode(), $object->getDescriptionShort(), $object->getDescriptionLong(), $object->getWarranty(), $object->getDescriptionMeta(), $object->getPutUrl(), (int)$object->isSellable(), (int)$object->isSale(), $object->getPutId()]);
    }
}