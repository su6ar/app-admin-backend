<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 15:56
 */

namespace Kominexpres\src\App\TableDataGateway;


interface IGateway
{
    public function selectAll() : array;
    public function findWhere($object) : array;
    public function insert(object $object);
    public function delete($id);
    public function update(object $object);
}