<?php
/**
 * Created by Daniel Bill
 * Date: 28.08.2018
 * Time: 19:39
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ProductPaymentsGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO product_payments (put_id, pmt_id) VALUES (?,?)";
    private const SQL_DELETE = "DELETE FROM product_payments WHERE put_id = ?";
    private const SQL_DELETE_BY_PMT_ID = "DELETE FROM product_payments WHERE put_id = ? AND pmt_id = ?";

    /**
     * ProductPaymentsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getPutId(), $object->getPmtId()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function deleteByPmtId($id, $pmtId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PMT_ID, [$id, $pmtId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}