<?php
/**
 * Created by Daniel Bill
 * Date: 11.04.2018
 * Time: 15:52
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\PaymentNotFoundException;
use Kominexpres\src\App\Storage\Database;

class PaymentOptionHistoryGateway extends AbstractGateway implements IGateway
{
    private const SQL_DELETE = "UPDATE payment_option_histories SET date_to = now() WHERE pmt_id = ? AND date_to IS NULL";
    private const SQL_UPDATE = "INSERT INTO payment_option_histories (date_from, pmt_id, poy_name, date_to, add_percentage_oott_price, price) VALUES (now(), ?, ?, null, ?, ?)";
    private const SQL_EXISTS_BY_PMT_ID = "SELECT pmt_id FROM payment_option_histories WHERE pmt_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO payment_option_histories (date_from, pmt_id, poy_name, date_to, add_percentage_oott_price, price) VALUES (now(), ?, ?, null, ?, ?)";

    /**
     * PaymentGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function delete($pmtId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$pmtId]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getPmtId(), $object->getPoyName(), $object->getAddPercentageOottPrice(), $object->getPrice()]);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPmtId(), $object->getPoyName(), $object->getAddPercentageOottPrice(), $object->getPrice()]);
    }

    /**
     * @param $pmtId
     * @throws PaymentNotFoundException
     */
    public function existsByPmtId($pmtId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_PMT_ID, [$pmtId]);
        if($rowCount == 0)
            throw new PaymentNotFoundException("Payment with id: '{$pmtId}' was not found!");
    }
}