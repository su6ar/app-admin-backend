<?php
/**
 * Created by Daniel Bill
 * Date: 19.02.2018
 * Time: 17:51
 */

namespace Kominexpres\src\App\TableDataGateway;

use Kominexpres\src\App\Exceptions\ShippingAlreadyExistsException;
use Kominexpres\src\App\Exceptions\ShippingNotFoundException;
use Kominexpres\src\App\Storage\Database;

/**
 * Class ShippingHistoryWeightsGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class ShippingHistoryWeightsGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM getshippinglist()";
    private const SQL_FIND_WHERE = "SELECT spg.id as spg_id, sht.sht_id, shy.shy_id, dta_name, show_on_default, show_till_max_weight, active, shy_name, free_price_above, sum_weight_price, weight_min, weight_max, price FROM shippings spg JOIN shipping_histories shy on spg.id = shy.spg_id JOIN delivery_type_heurekas dta on spg.dta_id = dta.id JOIN shipping_history_weights sht on shy.shy_id = sht.shy_id AND sht.sht_id = ? ORDER BY spg.id, weight_min";
    private const SQL_DELETE = "UPDATE shipping_history_weights SET active = false, date_to = now() WHERE sht_id = ?";
    private const SQL_DELETE_BY_SPG_ID = "UPDATE shipping_history_weights SET active = false, date_to = now() WHERE shy_id = (SELECT shy_id FROM shipping_histories WHERE spg_id = ? AND shipping_histories.date_to IS NULL)";
    private const SQL_INSERT = "INSERT INTO shipping_history_weights (shy_id, weight_min, weight_max, price, active, date_from, date_to) VALUES (?,?,?,?,?,now(),null)";
    private const SQL_EXISTS_BY_SHT_ID = "SELECT sht_id FROM shipping_history_weights WHERE sht_id = ?";
    private const SQL_EXISTS_BY_SHY_ID_AND_WEIGHT = "SELECT existsbyshyidandweight as result FROM existsbyshyidandweight(?,?,?)";
    private const SQL_EXISTS_BY_SHY_ID_AND_WEIGHT_NOT_SHT_ID = "SELECT existsbyshyidandweightnotshtid as result FROM existsbyshyidandweightnotshtid(?,?,?,?)";
    private const SQL_UPDATE = "UPDATE shipping_history_weights SET date_to = now() WHERE sht_id = ? AND date_to IS NULL";

    /**
     * ShippingHistoryWeightsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @return array
     */
    public function selectAll(): array
    {
        $data = $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL);
        for($key = 0; $key < count($data); $key++)
        {
            $data[$key]['items'] = json_decode($data[$key]['items']);
        }
        return $data;
    }

    /**
     * @param $shtId
     * @return array
     */
    public function findWhere($shtId): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$shtId]);
    }

    /**
     * @param object $sht
     */
    public function insert(object $sht)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$sht->getShyId(), $sht->getWeightMin(), $sht->getWeightMax(), $sht->getPrice(), (int)$sht->isActive()]);
    }

    /**
     * @param $shtId
     */
    public function delete($shtId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$shtId]);
    }

    /**
     * @param $spgId
     */
    public function deleteBySpgId($spgId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_SPG_ID, [$spgId]);
    }

    /**
     * @param object $sht
     */
    public function update(object $sht)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$sht->getShtId()]);
    }

    /**
     * @param string $shtId
     * @throws ShippingNotFoundException
     */
    public function existsByShtId(string $shtId): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_SHT_ID, [$shtId]);
        if($rowCount == 0)
            throw new ShippingNotFoundException("Shipping with id: '{$shtId}' was not found!");
    }

    /**
     * @param int $shyId
     * @param int $weightMin
     * @param int $weightMax
     * @throws ShippingAlreadyExistsException
     */
    public function existsByShyIdAndWeight(int $shyId, int $weightMin, int $weightMax)
    {
        $result = $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_SHY_ID_AND_WEIGHT, [$shyId, $weightMin, $weightMax]);
        if ($result["result"] === true)
        {
            throw new ShippingAlreadyExistsException("in range from {$weightMin} g to {$weightMax} g");
        }
    }

    /**
     * @param int $shyId
     * @param int $weightMin
     * @param int $weightMax
     * @param int $shtId
     * @throws ShippingAlreadyExistsException
     */
    public function existsByShyIdAndWeightNotShtId(int $shyId, int $weightMin, int $weightMax, int $shtId)
    {
        $result = $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_SHY_ID_AND_WEIGHT_NOT_SHT_ID, [$shyId, $weightMin, $weightMax, $shtId]);
        if ($result["result"] === true)
        {
            throw new ShippingAlreadyExistsException("in range from {$weightMin} g to {$weightMax} g");
        }
    }
}