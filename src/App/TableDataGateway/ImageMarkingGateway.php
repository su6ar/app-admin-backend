<?php
/**
 * Created by Daniel Bill
 * Date: 19.07.2018
 * Time: 23:42
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ImageMarkingGateway extends AbstractGateway
{
    private const SQL_INSERT = "INSERT INTO image_markings (mig_id, iae_id) SELECT mig_id, ? FROM markings WHERE mig_marking = ?";
    private const SQL_INSERT_BY_ICY_ID = "INSERT INTO image_markings (mig_id, iae_id) SELECT mig_id, ? FROM image_category_markings WHERE icy_id = ?";
    private const SQL_DELETE = "DELETE FROM image_markings WHERE iae_id = ?";

    /**
     * ImageCategoriesGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function delete($iaeId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$iaeId]);
    }

    public function insert($iaeId, $migMarking)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$iaeId, $migMarking]);
    }

    public function insertByIcyId($iaeId, $icyId)
    {
        return $this->pgsql->sendQueryOnly(self::SQL_INSERT_BY_ICY_ID, [$iaeId, $icyId]);
    }
}