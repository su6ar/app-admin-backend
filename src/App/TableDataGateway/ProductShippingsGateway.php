<?php
/**
 * Created by Daniel Bill
 * Date: 26.08.2018
 * Time: 16:45
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\ProductShipping;
use Kominexpres\src\App\Storage\Database;

class ProductShippingsGateway extends AbstractGateway implements IGateway
{
    private const SQL_UPDATE_PRICE = "INSERT INTO product_shippings (put_id, spg_id, date_from, show_shipping, fixed_price, date_to) VALUES (?, ?, now(), ?, ?, null)";
    private const SQL_DELETE = "UPDATE product_shippings SET date_to = now() WHERE put_id = ? AND date_to IS NULL";
    private const SQL_DELETE_BY_SPG_ID = "UPDATE product_shippings SET date_to = now() WHERE put_id = ? AND spg_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO product_shippings (put_id, spg_id, date_from, show_shipping, fixed_price, date_to) VALUES (?,?,now(),?,?,null)";
    private const SQL_DELETE_ONLY_BY_SPG_ID = "UPDATE product_shippings SET date_to = now() WHERE spg_id = ? AND date_to IS NULL";

    /**
     * ProductShippingsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPutId(), $object->getSpgId(), (int)$object->isShowShipping(), $object->getFixedPrice()]);
    }

    public function delete($id)
    {
        foreach ($id as $putId)
        {
            $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$putId]);
        }
    }

    public function deleteOnlyBySpgId($spgId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_ONLY_BY_SPG_ID, [$spgId]);
    }

    public function deleteBySpgId($id, $spgId)
    {
        foreach ($id as $putId)
        {
            $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_SPG_ID, [$putId, $spgId]);
        }
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }

    public function updateShipping($items, ProductShipping $psg)
    {
        foreach ($items as $item)
        {
            $this->pgsql->sendQueryOnly(self::SQL_UPDATE_PRICE, [$item, $psg->getSpgId(), (int)$psg->isShowShipping(), $psg->getFixedPrice()]);
        }
    }
}