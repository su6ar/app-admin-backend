<?php
/**
 * Created by Daniel Bill
 * Date: 11.08.2018
 * Time: 17:08
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ProductGroupDiscountsGateway extends AbstractGateway implements IGateway
{
    private const SQL_DELETE_BY_GRP_ID = "UPDATE product_group_discounts SET date_to = now() WHERE date_to IS NULL AND grp_id = ?";
    private const SQL_INSERT = "INSERT INTO product_group_discounts (put_id, grp_id, date_from, discount, date_to) VALUES (?,?,now(),?,null)";
    private const SQL_DELETE = "UPDATE product_group_discounts SET date_to = now() WHERE date_to IS NULL AND put_id = ?";
    private const SQL_DELETE_MULTIPLE = "UPDATE product_group_discounts SET date_to = now() WHERE date_to IS NULL AND grp_id = ? AND put_id = ?";

    /**
     * ParameterGroupParametersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object, array $items = null)
    {
        if ($items !== NULL)
        {
            foreach ($items as $item)
            {
                $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$item, $object->getGrpId(), $object->getDiscount()]);
            }
        }
        else
        {
            $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPutId(), $object->getGrpId(), $object->getDiscount()]);
        }
    }

    public function delete($id, $grpId = null)
    {
        if ($grpId !== NULL)
        {
            foreach ($id as $putId)
            {
                $this->pgsql->sendQueryOnly(self::SQL_DELETE_MULTIPLE, [$grpId, $putId]);
            }
        }
        else
        {
            $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
        }
    }

    public function deleteByGrpId($grpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_GRP_ID, [$grpId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}