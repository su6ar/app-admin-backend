<?php
/**
 * Created by Daniel Bill
 * Date: 14.07.2018
 * Time: 14:56
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\Image;
use Kominexpres\src\App\Exceptions\ImageCategoryStillInUseException;
use Kominexpres\src\App\Storage\Database;


class ImageGateway extends AbstractGateway
{
    private const SQL_SELECT_ALL = "SELECT iae.iae_id, iae_name, icy_name, icy_id, iae.iae_size, iae.iae_type, iae.iae_path FROM images iae JOIN image_categories USING (icy_id) JOIN image_markings img ON iae.iae_id = img.iae_id AND mig_id = (SELECT mig_id FROM markings WHERE mig_marking = '6') ORDER BY iae.iae_id";
    private const SQL_DELETE = "DELETE FROM images WHERE iae_id = ?";
    private const SQL_INSERT = "INSERT INTO images (icy_id, iae_path, iae_name, iae_type, iae_size) VALUES (?, ?, ?, ?, ?) RETURNING iae_id";
    private const SQL_UPDATE = "UPDATE images SET icy_id = ? WHERE iae_id = ?";
    private const SQL_SELECT_BY_ICY_ID = "SELECT iae_path, iae_id, iae_type, iae_name FROM images WHERE icy_id = ?";

    /**
     * ImageGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function update($iaeId, $icyId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$icyId, $iaeId]);
    }

    public function insert(Image $image)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$image->getIcyId(), $image->getIaePath(), $image->getIaeName(), $image->getIaeType(), $image->getIaeSize()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function selectByIcyId(int $icyId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_ICY_ID, [$icyId]);
    }

    /**
     * @param int $icyId
     * @throws ImageCategoryStillInUseException
     */
    public function existsByIcyId(int $icyId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_SELECT_BY_ICY_ID, [$icyId]);
        if($rowCount > 0)
            throw new ImageCategoryStillInUseException($icyId);
    }
}