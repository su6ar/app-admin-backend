<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:38
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Storage\Database;

class ProductCategoriesGateway extends AbstractGateway implements IGateway
{
    private const SQL_DELETE = "UPDATE product_categories SET date_to = now() WHERE cey_id = ? AND date_to IS NULL";
    private const SQL_SELECT_BY_CEY_ID = "SELECT * FROM product_categories WHERE cey_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO product_categories (cey_id, put_id, date_from, date_to, pce_main) VALUES (?,?,now(),null,?)";
    private const SQL_DELETE_BY_PUT_ID = "UPDATE product_categories SET date_to = now() WHERE put_id = ? AND date_to IS NULL";
    private const SQL_FIND_WHERE = "SELECT * FROM product_categories WHERE put_id = ? AND cey_id = ? AND date_to IS NULL";

    /**
     * ProductCategoriesGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectByCeyId($ceyId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_CEY_ID, [$ceyId]);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object->getPutId(), $object->getCeyId()]);
        if (empty($arr))
            throw new EntityNotFoundException("Product category", "putId: ". $object->getPutId() . ", ceyId: " . $object->getCeyId());
        return $arr;
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getCeyId(), $object->getPutId(), (int)$object->isPceMain()]);
    }

    public function delete($ceyId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$ceyId]);
    }

    public function deleteByPutId($putId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PUT_ID, [$putId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}