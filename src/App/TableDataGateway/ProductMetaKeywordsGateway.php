<?php
/**
 * Created by Daniel Bill
 * Date: 28.08.2018
 * Time: 19:57
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ProductMetaKeywordsGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO product_meta_keywords (put_id, mkd_id) VALUES (?,?)";
    private const SQL_DELETE = "DELETE FROM product_meta_keywords WHERE put_id = ?";

    /**
     * ProductMetaKeywordsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPutId(), $object->getMkdId()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}