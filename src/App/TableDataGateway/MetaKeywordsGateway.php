<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 0:50
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class MetaKeywordsGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM meta_keywords";
    private const SQL_FIND_WHERE_MIM_ID = "SELECT * FROM meta_keywords WHERE id IN (SELECT mkd_id FROM menu_items_meta_keywords WHERE mim_id = ?)";
    private const SQL_FIND_WHERE = "SELECT * FROM meta_keywords WHERE id = ?";
    private const SQL_INSERT = "INSERT INTO meta_keywords (mkd_name) VALUES (?)";
    private const SQL_UPDATE = "UPDATE meta_keywords SET mkd_name = ? WHERE id = ?";
    private const SQL_DELETE = "DELETE FROM meta_keywords WHERE id = ?";
    private const SQL_EXISTS_BY_NAME = "SELECT id FROM meta_keywords WHERE mkd_name = ?";

    /**
     * MetaKeywordsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhereMimId($mimId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE_MIM_ID, [$mimId]);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("meta keyword", $object);
        return $arr;
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getMkdName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getMkdName(), $object->getId()]);
    }

    public function existsByName($mkdName)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_NAME, [$mkdName]);
    }
}