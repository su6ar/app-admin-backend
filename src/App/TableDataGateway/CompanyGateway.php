<?php
/**
 * Created by Daniel Bill
 * Date: 26.02.2018
 * Time: 15:06
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class CompanyGateway extends AbstractGateway
{
    private const SQL_GET_COMPANY_INFO = "SELECT cdl.cdl_id, cct_cellnumber, cct_email, street, city, zip, country, cas_name, cdl_name, cdl_tin, cdl_vatin, website_name, website_url FROM company_addresses cas JOIN company_details cdl on cas.cdl_id = cdl.cdl_id AND cas.is_headquarters = ? JOIN company_contacts cct On cdl.cdl_id = cct.cdl_id AND cct.is_eshop = ?";
    private const SQL_SELECT_COMPANY_CONTACTS = "SELECT * FROM company_contacts";

    /**
     * CompanyGateway constructor.
     * @param Database $pgsql
     * @param Database $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @param bool $isHeadquarters
     * @param bool $isEshop
     * @return array
     */
    public function getCompanyInfo(bool $isHeadquarters = false, bool $isEshop = false)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_GET_COMPANY_INFO, [$isHeadquarters, $isEshop]);
    }

    public function selectCompanyContacts()
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_COMPANY_CONTACTS, []);
    }
}