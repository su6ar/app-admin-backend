<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:45
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\PaymentNotFoundException;
use Kominexpres\src\App\Storage\Database;

class PaymentGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT id, active, cash_on_delivery, date_to, date_from, poy_name, add_percentage_oott_price, price FROM payments pmt JOIN payment_option_histories poy ON pmt.id = poy.pmt_id AND pmt.active = true AND date_to IS NULL";
    private const SQL_EXISTS_BY_ID = "SELECT id FROM payments pmt JOIN payment_option_histories poy ON pmt.id = poy.pmt_id AND pmt.id = ? AND pmt.active = true AND date_to IS NULL";
    private const SQL_FIND_WHERE = "SELECT id, active, cash_on_delivery, date_to, date_from, poy_name, add_percentage_oott_price, price FROM payments pmt JOIN payment_option_histories poy ON pmt.id = poy.pmt_id AND pmt.id = ? AND pmt.active = true AND date_to IS NULL";
    private const SQL_DELETE = "UPDATE payments SET active = false WHERE id = ?";
    private const SQL_UPDATE = "UPDATE payments SET cash_on_delivery = ? WHERE id = ?";
    private const SQL_INSERT = "INSERT INTO payments (active, cash_on_delivery) VALUES (true, ?) RETURNING id";

    /**
     * PaymentGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [(int)$object->isCashOnDelivery()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [(int)$object->isCashOnDelivery(), $object->getId()]);
    }

    /**
     * @param int $id
     * @throws PaymentNotFoundException
     */
    public function existsById(int $id)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_ID, [$id]);
        if($rowCount == 0)
            throw new PaymentNotFoundException("Payment with id: '{$id}' was not found!");
    }
}