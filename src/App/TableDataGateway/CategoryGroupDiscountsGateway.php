<?php
/**
 * Created by Daniel Bill
 * Date: 07.08.2018
 * Time: 12:40
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class CategoryGroupDiscountsGateway extends AbstractGateway
{
    private const SQL_SELECT_BY_CEY_ID = "SELECT grp.grp_id, discount, grp_name FROM groups grp JOIN category_group_discounts cgd ON cgd.grp_id = grp.grp_id AND grp_active = TRUE AND cey_id = ? AND date_to IS NULL";
    private const SQL_UPDATE = "UPDATE category_group_discounts SET date_to = now() WHERE cey_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO category_group_discounts (cey_id, grp_id, date_from, discount, date_to) VALUES (?, ?, now(), ?, null)";
    private const SQL_DELETE_BY_GRP_ID = "UPDATE category_group_discounts SET date_to = now() WHERE grp_id = ? AND date_to IS NULL";
    private const SQL_SELECT_BY_GRP_ID = "SELECT cey_id, cey_cey_id, cey_name, cey_place, discount, iae_path, iae_type FROM category_group_discounts JOIN categories USING (cey_id) LEFT JOIN images USING (iae_id) WHERE grp_id = ? AND date_to IS NULL ORDER BY (CASE WHEN cey_cey_id IS NULL THEN 1 ELSE cey_cey_id END), cey_place ASC";

    /**
     * FileGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectByCeyId($ceyId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_CEY_ID, [$ceyId]);
    }

    public function selectByGrpId($grpId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_GRP_ID, [$grpId]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getCeyId(), $object->getGrpId(), $object->getDiscount()]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getCeyId()]);
    }

    public function deleteByGrpId($grpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_GRP_ID, [$grpId]);
    }
}