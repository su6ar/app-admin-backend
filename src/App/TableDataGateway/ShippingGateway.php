<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 14:50
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\Shipping;
use Kominexpres\src\App\Storage\Database;

/**
 * Class ShippingGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class ShippingGateway extends AbstractGateway
{
    private const SQL_INSERT = "INSERT INTO shippings (dta_id, show_on_default, show_till_max_weight) VALUES (?, ?, ?) RETURNING id";
    private const SQL_UPDATE = "SELECT * FROM update_shipping_prc(?, ?, ?, ?, ?, ?, ?)";

    /**
     * ShippingGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @param Shipping $shipping
     * @return mixed
     */
    public function insert(Shipping $shipping)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$shipping->getDtaId(), (int)$shipping->isShowOnDefault(), $shipping->getShowTillMaxWeight()]);
    }

    /**
     * @param array $spg
     * @param int $spgId
     */
    public function update(array $spg, int $spgId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$spgId, $spg["dta_id"], $spg["shy_name"], (int)$spg["show_on_default"], (int)$spg["sum_weight_price"], $spg["free_price_above"], $spg["show_till_max_weight"]]);
    }
}