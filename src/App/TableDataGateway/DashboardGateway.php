<?php
/**
 * Created by Daniel Bill
 * Date: 19.02.2018
 * Time: 17:32
 */

namespace Kominexpres\src\App\TableDataGateway;

use Kominexpres\src\App\Storage\Database;

class DashboardGateway extends AbstractGateway
{
    private const SQL_GET_DASHBOARD_STATS = "SELECT (SELECT count(*) FROM accounts WHERE act_type = 'UER' AND uer_active = TRUE AND DATE_PART('day', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) <= 0 AND DATE_PART('hours', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) <= 0 AND DATE_PART('minutes', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) <= 0 AND DATE_PART('day', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) >= 0 AND DATE_PART('hours', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) >= 0 AND DATE_PART('minutes', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - uer_date_created) >= 0) users_count, count(*) orders_count, COALESCE(sum(order_total),0) orders_total_sum FROM getorders() WHERE deleted_order <> TRUE AND DATE_PART('day', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) <= 0 AND DATE_PART('hours', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) <= 0 AND DATE_PART('minutes', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) <= 0 AND DATE_PART('day', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) >= 0 AND DATE_PART('hours', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) >= 0 AND DATE_PART('minutes', (TIMESTAMP 'epoch' + ? * INTERVAL '1 second') - ord_date) >= 0 AND id not in (SELECT odr_id FROM order_histories WHERE status = '0' AND date_to IS NULL)";

    /**
     * DashboardGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @param string $date_from
     * @param string $date_to
     * @return array
     */
    public function getDashboardStats(string $date_from, string $date_to) : array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_GET_DASHBOARD_STATS, [$date_from, $date_from, $date_from, $date_to, $date_to, $date_to, $date_from, $date_from, $date_from, $date_to, $date_to, $date_to]);
    }
}