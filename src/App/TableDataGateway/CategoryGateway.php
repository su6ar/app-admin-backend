<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:38
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class CategoryGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT cey_id, cey_cey_id, cey_name, cey_place, iae.iae_path, iae.iae_type FROM categories cey LEFT JOIN images iae ON cey.iae_id = iae.iae_id WHERE cey_active = true OR cey_active IS NULL ORDER BY cey_place ASC";
    private const SQL_SELECT_ALL_CHILDREN = "SELECT cey_id FROM categories WHERE cey_cey_id = ?";
    private const SQL_SELECT_PARENT = "SELECT cey_cey_id, cey_name FROM categories WHERE cey_id = ?";
    private const SQL_SELECT_AVAILABLE_PLACE = "SELECT (case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end) cey_place_max, (CASE WHEN min(cey_place) IS NULL THEN 1 ELSE min(cey_place) END) cey_place_min FROM categories WHERE cey_cey_id = ?";
    private const SQL_SELECT_AVAILABLE_PLACE_NULL = "SELECT (case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end) cey_place_max, (CASE WHEN min(cey_place) IS NULL THEN 1 ELSE min(cey_place) END) cey_place_min FROM categories WHERE cey_cey_id IS NULL";
    private const SQL_DELETE = "UPDATE categories SET cey_active = ? WHERE cey_id = ?";
    private const SQL_FIND_WHERE = "SELECT * FROM categories cey LEFT JOIN images iae ON cey.iae_id = iae.iae_id WHERE cey_id = ?";
    private const SQL_EXISTS_BY_URL = "SELECT cey_id, cey_active FROM categories WHERE cey_url = ? AND cey_id <> ?";
    private const SQL_UPDATE = "UPDATE categories SET iae_id = ?, cey_active = true, pgp_id = ?, cey_name = ?, cey_url = ?, show_parameters = ?, cey_description = ? WHERE cey_id = ?";
    private const SQL_UPDATE_ALL = "UPDATE categories SET iae_id = ?, cey_active = true, cey_place = (SELECT (case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end) FROM categories WHERE cey_cey_id = ?), cey_cey_id = ?, pgp_id = ?, cey_name = ?, cey_url = ?, show_parameters = ?, cey_description = ? WHERE cey_id = ?";
    private const SQL_UPDATE_ALL_NULL = "UPDATE categories SET iae_id = ?, cey_active = true, cey_place = (SELECT (case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end) FROM categories WHERE cey_cey_id IS NULL), cey_cey_id = ?, pgp_id = ?, cey_name = ?, cey_url = ?, show_parameters = ?, cey_description = ? WHERE cey_id = ?";
    private const SQL_INSERT = "INSERT INTO categories (iae_id, cey_cey_id, pgp_id, cey_url, cey_name, cey_place, show_parameters, cey_active, cey_description) (SELECT ?,?,?,?,?, case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end, ?, true, ? FROM categories WHERE cey_cey_id = ?) RETURNING cey_id";
    private const SQL_INSERT_NULL = "INSERT INTO categories (iae_id, cey_cey_id, pgp_id, cey_url, cey_name, cey_place, show_parameters, cey_active, cey_description) (SELECT ?,?,?,?,?, case when max(cey_place) + 1 is null then 1 else max(cey_place) + 1 end, ?, true, ? FROM categories WHERE cey_cey_id IS NULL) RETURNING cey_id";
    private const SQL_UPDATE_SORT = "UPDATE categories SET cey_cey_id = ?, cey_place = ?, cey_url = ? WHERE cey_id = ?";
    private const SQL_DELETE_IAE_ID = "UPDATE categories SET iae_id = null WHERE iae_id = ?";

    /**
     * FileGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function deleteIaeId($iaeId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_IAE_ID, [$iaeId]);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectParent($ceyId)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_PARENT, [$ceyId]);
    }

    public function selectAllChildren($id): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL_CHILDREN, [$id], \PDO::FETCH_COLUMN);
    }

    public function selectAvailablePlace($ceyId)
    {
        if ($ceyId === NULL)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_AVAILABLE_PLACE_NULL, []);
        }
        else
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_SELECT_AVAILABLE_PLACE, [$ceyId]);
        }
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
        {
            throw new EntityNotFoundException("Category", $object);
        }
        return $arr;
    }

    /**
     * @param $url
     * @param $ceyId
     * @return mixed
     */
    public function existsByUrl($url, $ceyId)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_URL, [$url, $ceyId]);
    }

    public function insert(object $object)
    {
        if ($object->getCeyCeyId() === null)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT_NULL, [$object->getIaeId(), $object->getCeyCeyId(), $object->getPgpId(), $object->getCeyUrl(), $object->getCeyName(), (int)$object->isShowParameters(), $object->getCeyDescription()]);
        }
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getIaeId(), $object->getCeyCeyId(), $object->getPgpId(), $object->getCeyUrl(), $object->getCeyName(), (int)$object->isShowParameters(), $object->getCeyDescription(), $object->getCeyCeyId()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [0, $id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getIaeId(), $object->getPgpId(), $object->getCeyName(), $object->getCeyUrl(), (int)$object->isShowParameters(), $object->getCeyDescription(), $object->getCeyId()]);
    }

    public function updateOne(object $object, $ceyCeyIdChanged)
    {
        if ($ceyCeyIdChanged)
        {
            if ($object->getCeyCeyId() === null)
            {
                $this->pgsql->sendQueryOnly(self::SQL_UPDATE_ALL_NULL, [$object->getIaeId(), $object->getCeyCeyId(), $object->getPgpId(), $object->getCeyName(), $object->getCeyUrl(), (int)$object->isShowParameters(), $object->getCeyDescription(), $object->getCeyId()]);
            }
            else
            {
                $this->pgsql->sendQueryOnly(self::SQL_UPDATE_ALL, [$object->getIaeId(), $object->getCeyCeyId(), $object->getCeyCeyId(), $object->getPgpId(), $object->getCeyName(), $object->getCeyUrl(), (int)$object->isShowParameters(), $object->getCeyDescription(), $object->getCeyId()]);
            }
        }
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getIaeId(), $object->getPgpId(), $object->getCeyName(), $object->getCeyUrl(), (int)$object->isShowParameters(), $object->getCeyDescription(), $object->getCeyId()]);
    }

    public function updateSort(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_SORT, [$object->getCeyCeyId(), $object->getCeyPlace(), $object->getCeyUrl(), $object->getCeyId()]);
    }
}