<?php
/**
 * Created by Daniel Bill
 * Date: 10.08.2018
 * Time: 19:26
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ParameterGroupParametersGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM parameter_group_parameters";
    private const SQL_DELETE_BY_PRR_ID = "DELETE FROM parameter_group_parameters WHERE prr_id = ?";
    private const SQL_SELECT_BY_PGP_ID = "SELECT prr_id FROM parameter_group_parameters WHERE pgp_id = ?";
    private const SQL_DELETE = "DELETE FROM parameter_group_parameters WHERE pgp_id = ?";
    private const SQL_INSERT = "INSERT INTO parameter_group_parameters (prr_id, pgp_id) VALUES (?,?)";

    /**
     * ParameterGroupParametersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectByPgpId($pgpId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_PGP_ID, [$pgpId]);
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPrrId(), $object->getPgpId()]);
    }

    public function delete($pgpId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$pgpId]);
    }

    public function deleteByPrrId($prrId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PRR_ID, [$prrId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}