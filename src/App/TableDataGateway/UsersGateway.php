<?php
/**
 * Created by Daniel Bill
 * Date: 13.08.2018
 * Time: 15:45
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Storage\Database;

class UsersGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT act.act_id, grp.grp_name, act_email, uer_active FROM accounts act JOIN account_group_histories gry ON act.act_id = gry.act_id AND uer_active IS NOT NULL AND gry.date_to IS NULL JOIN groups grp ON gry.grp_id = grp.grp_id";
    private const SQL_SELECT_ALL_ADMINS = "SELECT act_id, act_email, amr_avatar, amr_first_name, amr_last_name, amr_rights FROM accounts WHERE act_type = 'AMR'";
    private const SQL_FIND_WHERE = "SELECT act.act_id, grp.grp_id, grp.grp_name, act_email, uer_active, uer_newsletter, uer_date_created, street, city, zip, country, enterprise, first_name, last_name, tin, vatin, cellnumber FROM accounts act JOIN account_group_histories gry ON act.act_id = gry.act_id AND uer_active IS NOT NULL AND gry.date_to IS NULL AND act.act_id = ? JOIN groups grp ON gry.grp_id = grp.grp_id JOIN addresses ads ON act.act_id = ads.act_id AND ads_type = 'ACT' JOIN address_histories ahy ON ads.ads_id = ahy.ads_id AND ahy.date_to IS NULL";
    private const SQL_FIND_WHERE_ADMIN = "SELECT act_id, act_email, amr_avatar, amr_first_name, amr_last_name, amr_rights FROM accounts WHERE act_type = 'AMR' AND act_id = ?";
    private const SQL_UPDATE_ADMIN_RIGHTS = "UPDATE accounts SET amr_rights = ? WHERE act_id = ?";

    /**
     * UsersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectAllAdmins(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL_ADMINS, []);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Customer", $object);
        return $arr;
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhereAdmin($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE_ADMIN, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Admin", $object);
        return $arr;
    }

    public function insert(object $object)
    {
        throw new NotImplementedException();
    }

    public function delete($id)
    {
        throw new NotImplementedException();
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }

    public function updateAdminRights($amrRights, $actId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_ADMIN_RIGHTS, [$amrRights, $actId]);
    }
}