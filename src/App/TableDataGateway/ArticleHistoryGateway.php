<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2018
 * Time: 19:38
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Storage\Database;

class ArticleHistoryGateway extends AbstractGateway implements IGateway
{
    private const SQL_FIND_WHERE = "SELECT * FROM article_histories WHERE ate_id = ? ORDER BY date_to DESC";
    private const SQL_INSERT = "INSERT INTO article_histories (ate_id, date_from, act_id, ahy_content, date_to) VALUES (?,now(),?,?,NULL)";
    private const SQL_DELETE = "UPDATE article_histories SET act_id = ?, date_to = now() WHERE ate_id = ? AND date_to IS NULL";

    /**
     * ArticleHistoryGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE, [$object]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getAteId(), $object->getActId(), $object->getAhyContent()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id->getActId(), $id->getAteId()]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}