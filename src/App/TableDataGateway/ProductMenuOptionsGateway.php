<?php
/**
 * Created by Daniel Bill
 * Date: 28.08.2018
 * Time: 19:47
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class ProductMenuOptionsGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO product_menu_options (pmn_name, put_id, date_from, date_to) VALUES (?,?,now(),null)";
    private const SQL_DELETE = "UPDATE product_menu_options SET date_to = now() WHERE date_to IS NULL AND put_id = ?";

    /**
     * ProductGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getPmnName(), $object->getPutId()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}