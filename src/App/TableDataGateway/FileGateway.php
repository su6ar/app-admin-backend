<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 17:25
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\EntityStillInUseException;
use Kominexpres\src\App\Storage\Database;

class FileGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM files JOIN file_categories USING (fcy_id) ORDER BY fie_id";
    private const SQL_DELETE = "DELETE FROM files WHERE fie_id = ?";
    private const SQL_UPDATE = "UPDATE files SET fcy_id = ? WHERE fie_id = ?";
    private const SQL_EXISTS_BY_FIE_ID = "SELECT fie_id FROM files WHERE fie_id = ?";
    private const SQL_INSERT = "INSERT INTO files (fcy_id, fie_path, fie_type, fie_size, fie_name) VALUES (?, ?, ?, ?, ?)";
    private const SQL_EXISTS_BY_FCY_ID = "SELECT fie_id FROM files WHERE fcy_id = ?";

    /**
     * FileGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getFcyId(), $object->getFiePath(), $object->getFieType(), $object->getFieSize(), $object->getFieName()]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getFcyId(), $object->getFieId()]);
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $fieId
     * @throws EntityNotFoundException
     */
    public function existsByFieId($fieId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_FIE_ID, [$fieId]);
        if ($rowCount == 0)
        {
            throw new EntityNotFoundException("File", $fieId);
        }
    }

    /**
     * @param int $fcyId
     * @throws EntityStillInUseException
     */
    public function existsByFcyId(int $fcyId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_FCY_ID, [$fcyId]);
        if($rowCount > 0)
            throw new EntityStillInUseException("File category", $fcyId);
    }
}