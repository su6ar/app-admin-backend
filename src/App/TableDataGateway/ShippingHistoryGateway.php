<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 15:47
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\ShippingHistory;
use Kominexpres\src\App\Exceptions\ShippingAlreadyExistsException;
use Kominexpres\src\App\Exceptions\ShippingNotFoundException;
use Kominexpres\src\App\Storage\Database;

/**
 * Class ShippingHistoryGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class ShippingHistoryGateway extends AbstractGateway
{
    private const SQL_INSERT = "INSERT INTO shipping_histories (spg_id, date_from, free_price_above, date_to, sum_weight_price, shy_name) VALUES (?, now(), ?, null, ?, ?) RETURNING shy_id";
    private const SQL_EXISTS_BY_SHY_NAME = "SELECT shy_id FROM shipping_histories WHERE shy_name = ?";
    private const SQL_EXISTS_BY_SHY_ID = "SELECT shy_id FROM shipping_histories WHERE shy_id = ?";
    private const SQL_EXISTS_BY_SPG_ID = "SELECT spg_id FROM shipping_histories WHERE spg_id = ? AND date_to IS NULL";
    private const SQL_DELETE = "UPDATE shipping_histories SET date_to = now() WHERE spg_id = ? AND date_to IS NULL";
    private const SQL_FIND_WHERE = "SELECT spg.id as spg_id, shy.shy_id, dta_name, show_on_default, show_till_max_weight, shy_name, free_price_above, sum_weight_price FROM shippings spg JOIN shipping_histories shy on spg.id = shy.spg_id AND spg.id = ? AND shy.date_to IS NULL JOIN delivery_type_heurekas dta on spg.dta_id = dta.id";

    /**
     * ShippingHistoryGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }


    /**
     * @param $spgId
     * @return array
     */
    public function findWhere($spgId): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$spgId]);
    }

    /**
     * @param $spgId
     */
    public function delete($spgId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$spgId]);
    }

    /**
     * @param ShippingHistory $shy
     * @return mixed
     */
    public function insert(ShippingHistory $shy)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$shy->getSpgId(), $shy->getFreePriceAbove(), (int)$shy->isSumWeightPrice(), $shy->getShyName()]);
    }

    /**
     * @param string $shyName
     * @throws ShippingAlreadyExistsException
     */
    public function existsByShyName(string $shyName): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_SHY_NAME, [$shyName]);
        if($rowCount > 0)
            throw new ShippingAlreadyExistsException($shyName);
    }

    /**
     * @param int $shyId
     * @throws ShippingNotFoundException
     */
    public function existsByShyId(int $shyId): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_SHY_ID, [$shyId]);
        if($rowCount == 0)
            throw new ShippingNotFoundException("Shipping with id: '{$shyId}' was not found!");
    }

    /**
     * @param int $spgId
     * @throws ShippingNotFoundException
     */
    public function existsBySpgId(int $spgId): void
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_SPG_ID, [$spgId]);
        if($rowCount == 0)
            throw new ShippingNotFoundException("Shipping with id: '{$spgId}' was not found!");
    }
}