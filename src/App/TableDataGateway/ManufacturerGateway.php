<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:45
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\ManufacturerAlreadyExistsException;
use Kominexpres\src\App\Exceptions\ManufacturerIsUsedException;
use Kominexpres\src\App\Exceptions\ManufacturerNotFoundException;
use Kominexpres\src\App\Storage\Database;

class ManufacturerGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT id, mur_name FROM manufacturers";
    private const SQL_EXISTS_BY_ID = "SELECT id FROM manufacturers WHERE id = ?";
    private const SQL_FIND_WHERE = "SELECT id, mur_name FROM manufacturers WHERE id = ?";
    private const SQL_DELETE = "DELETE FROM manufacturers WHERE id = ?";
    private const SQL_UPDATE = "UPDATE manufacturers SET mur_name = ? WHERE id = ?";
    private const SQL_INSERT = "INSERT INTO manufacturers (mur_name) VALUES (?)";
    private const SQL_IS_USED = "SELECT put_id FROM products WHERE mur_id = ?";
    private const SQL_EXISTS_BY_NAME = "SELECT mur_name FROM manufacturers WHERE mur_name = ?";

    /**
     * PaymentGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getMurName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getMurName(), $object->getId()]);
    }

    /**
     * @param int $id
     * @throws ManufacturerNotFoundException
     */
    public function existsById(int $id)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_ID, [$id]);
        if($rowCount == 0)
            throw new ManufacturerNotFoundException("Manufacturer with id: '{$id}' was not found!");
    }

    /**
     * @param string $murName
     * @throws ManufacturerAlreadyExistsException
     */
    public function existsByName(string $murName)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_NAME, [$murName]);
        if($rowCount > 0)
            throw new ManufacturerAlreadyExistsException($murName);
    }

    /**
     * @param $id
     * @throws ManufacturerIsUsedException
     */
    public function isUsed($id)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_IS_USED, [$id]);
        if($rowCount > 0)
            throw new ManufacturerIsUsedException($id);
    }
}