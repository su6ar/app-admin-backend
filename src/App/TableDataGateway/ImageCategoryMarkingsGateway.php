<?php
/**
 * Created by Daniel Bill
 * Date: 23.07.2018
 * Time: 15:31
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Storage\Database;

class ImageCategoryMarkingsGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO image_category_markings (icy_id, mig_id) SELECT ?, mig_id FROM markings WHERE mig_marking = ?";
    private const SQL_SELECT_MARKINGS_BY_ICY_ID = "SELECT mig_marking FROM markings WHERE mig_id IN (SELECT mig_id FROM image_category_markings WHERE icy_id = ?)";
    private const SQL_DELETE = "DELETE FROM image_category_markings WHERE icy_id = ?";

    /**
     * imageCategoryMarkingsGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectMarkingsByIcyId($icyId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_MARKINGS_BY_ICY_ID, [$icyId]);
    }

    public function insert(Object $icg)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$icg->getIcyId(), $icg->getMigId()]);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function delete($icyId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$icyId]);
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}