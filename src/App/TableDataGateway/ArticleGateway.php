<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2018
 * Time: 19:38
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class ArticleGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM articles WHERE ate_active = true";
    private const SQL_FIND_WHERE = "SELECT * FROM articles WHERE ate_id = ? AND ate_active = true";
    private const SQL_EXISTS_BY_ATE_NAME_NULL = "SELECT ate_id, ate_active FROM articles WHERE ate_name = ?";
    private const SQL_EXISTS_BY_ATE_NAME = "SELECT ate_id, ate_active FROM articles WHERE ate_name = ? AND ate_id <> ?";
    private const SQL_INSERT = "INSERT INTO articles (ate_name, ate_active) VALUES (?,true) RETURNING ate_id";
    private const SQL_UPDATE = "UPDATE articles SET ate_active = true, ate_name = ? WHERE ate_id = ?";
    private const SQL_DELETE = "UPDATE articles SET ate_active = false WHERE ate_id = ?";

    /**
     * ArticleGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Article", $object);
        return $arr;
    }

    public function existsByAteName($ateName, $ateId = null)
    {
        if ($ateId === null)
            return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_ATE_NAME_NULL, [$ateName]);
        else
            return $this->pgsql->sendQueryAndFetch(self::SQL_EXISTS_BY_ATE_NAME, [$ateName, $ateId]);
    }

    public function insert(object $object)
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getAteName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getAteName(), $object->getAteId()]);
    }
}