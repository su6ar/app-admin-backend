<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 23:00
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class FileCategoryGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM file_categories ORDER BY fcy_name";
    private const SQL_EXISTS_BY_FCY_ID = "SELECT fcy_id FROM file_categories WHERE fcy_id = ?";
    private const SQL_EXISTS_BY_FCY_NAME = "SELECT fcy_id FROM file_categories WHERE fcy_name = ?";
    private const SQL_FIND_WHERE = "SELECT * FROM file_categories WHERE fcy_id = ?";
    private const SQL_UPDATE = "UPDATE file_categories SET fcy_name = ? WHERE fcy_id = ?";
    private const SQL_INSERT = "INSERT INTO file_categories (fcy_name) VALUES (?)";
    private const SQL_DELETE = "DELETE FROM file_categories WHERE fcy_id = ?";

    /**
     * FileGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function findWhere($object): array
    {
        return $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getFcyName()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getFcyName(), $object->getFcyId()]);
    }

    /**
     * @param $fcyId
     * @throws EntityNotFoundException
     */
    public function existsByFcyId($fcyId)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_FCY_ID, [$fcyId]);
        if ($rowCount == 0)
        {
            throw new EntityNotFoundException("File category", $fcyId);
        }
    }

    /**
     * @param $fcyName
     * @throws EntityAlreadyExistsException
     */
    public function existsByFcyName($fcyName)
    {
        $rowCount = $this->pgsql->sendQueryOnly(self::SQL_EXISTS_BY_FCY_NAME, [$fcyName]);
        if ($rowCount > 0)
        {
            throw new EntityAlreadyExistsException("File category", $fcyName);
        }
    }
}