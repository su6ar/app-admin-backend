<?php
/**
 * Created by Daniel Bill
 * Date: 07.08.2018
 * Time: 14:13
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class ParametersGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM parameters";
    private const SQL_FIND_WHERE = "SELECT * FROM parameters WHERE prr_id = ?";
    private const SQL_DELETE = "DELETE FROM parameters WHERE prr_id = ?";
    private const SQL_INSERT = "INSERT INTO parameters (prr_name, prr_unit) VALUES (?,?)";
    private const SQL_UPDATE = "UPDATE parameters SET prr_name = ?, prr_unit = ? WHERE prr_id = ?";
    private const SQL_SELECT_BY_PGP_ID = "SELECT *,array_to_json(array(SELECT pve.pve_value FROM parameter_values pve WHERE pve.prr_id = prr.prr_id order by pve.pve_value asc)) as options FROM parameters prr WHERE prr.prr_id IN (SELECT pgr.prr_id FROM parameter_group_parameters pgr WHERE pgr.pgp_id = ?)";

    /**
     * ParametersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    public function selectByPgpId($pgpId)
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_BY_PGP_ID, [$pgpId]);
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if (empty($arr))
            throw new EntityNotFoundException("Parameter", $object);
        return $arr;
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPrrName(), $object->getPrrUnit()]);
    }

    public function delete($id)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$id]);
    }

    public function update(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE, [$object->getPrrName(), $object->getPrrUnit(), $object->getPrrId()]);
    }
}