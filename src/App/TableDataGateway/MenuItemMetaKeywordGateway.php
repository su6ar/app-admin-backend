<?php
/**
 * Created by Daniel Bill
 * Date: 18.08.2018
 * Time: 18:10
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\MenuItemMetaKeyword;
use Kominexpres\src\App\Exceptions\NotImplementedException;
use Kominexpres\src\App\Storage\Database;

class MenuItemMetaKeywordGateway extends AbstractGateway implements IGateway
{
    private const SQL_FIND_WHERE = "";
    private const SQL_DELETE_BY_MKD_ID = "DELETE FROM menu_items_meta_keywords WHERE mkd_id = ?";
    private const SQL_DELETE_BY_MIM_ID = "DELETE FROM menu_items_meta_keywords WHERE mim_id = ?";
    private const SQL_INSERT = "INSERT INTO menu_items_meta_keywords (mkd_id, mim_id) VALUES (?,?)";
    private const SQL_UPDATE = "";

    /**
     * MenuItemMetaKeywordGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    public function findWhere($object): array
    {
        throw new NotImplementedException();
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getMkdId(), $object->getMimId()]);
    }

    public function delete($object)
    {
        try
        {
            $id = $object->getMimId();
            $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_MIM_ID, [$object->getMimId()]);
        }
        catch (\TypeError $e)
        {
            $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_MKD_ID, [$object->getMkdId()]);
        }
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}