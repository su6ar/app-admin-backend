<?php
/**
 * Created by Daniel Bill
 * Date: 26.02.2018
 * Time: 16:56
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Exceptions\TokenNotFoundException;
use Kominexpres\src\App\Storage\Database;

class TokenGateway extends AbstractGateway implements IGateway
{
    private const SQL_INSERT = "INSERT INTO tokens (jwt, invalid, sub) VALUES (?,?,?)";
    private const SQL_FIND_WHERE = "SELECT tokens.id token_id, jwt, invalid, sub, amr_rights FROM tokens JOIN accounts ON tokens.sub = accounts.act_email AND tokens.jwt = ?";
    private const SQL_PATCH = "UPDATE tokens SET invalid = ? WHERE id = ?";
    private const SQL_DELETE = "DELETE FROM tokens WHERE sub = ? AND invalid = FALSE";
    private const SQL_DELETE_FORCE = "DELETE FROM tokens WHERE sub = ?";

    /**
     * CompanyGateway constructor.
     * @param Database $pgsql
     * @param Database $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $jwt
     * @return array
     * @throws TokenNotFoundException
     */
    public function findWhere($jwt): array
    {
        $data = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$jwt]);
        if (!empty($data))
            return $data;
        else
            throw new TokenNotFoundException("Token '{$jwt}' was not found!");
    }

    /**
     * @param object $token
     */
    public function patch(object $token)
    {
        $this->pgsql->sendQueryOnly(self::SQL_PATCH, [(int)$token->isInvalid(), $token->getId()]);
    }

    /**
     * @param object $token
     */
    public function insert(object $token)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$token->getJwt(), (int)$token->isInvalid(), $token->getSub()]);
    }

    public function delete($object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$object]);
    }

    public function deleteForce($sub)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_FORCE, [$sub]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}