<?php
/**
 * Created by Daniel Bill
 * Date: 19.02.2018
 * Time: 17:45
 */

namespace Kominexpres\src\App\TableDataGateway;

use Kominexpres\src\App\Exceptions\OrderNotFoundException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\Storage\Database;

/**
 * Class OrdersGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class OrdersGateway extends AbstractGateway implements IGateway
{
    private const SQL_SELECT_ALL = "SELECT odr.id, to_char(ord_date, 'YYYY-MM-DD HH24:MI:SS') AS ord_date, customer, is_company, status, note, deleted_order, act_email, cellnumber, order_total, poy.poy_name FROM getorders() odr JOIN payment_option_histories poy ON poy.pmt_id = (SELECT pmt_id FROM orders WHERE id = odr.id) AND DATE_PART('second', poy.date_from - odr.ord_date) <= 0 AND (DATE_PART('second', poy.date_to - odr.ord_date) > 0 OR poy.date_to IS NULL)";

    private const SQL_FIND_WHERE_INFO = "SELECT enterprise, odr_id, to_char(odr_date, 'YYYY-MM-DD HH24:MI:SS') as odr_date, big_tin, big_vatin, big_cellnumber, status, note, act_email, big_customer, big_street, big_city, big_zip, big_country, dly_customer, dly_street, dly_city, dly_zip, dly_country, spg_name, spg_price, pmt_name, pmt_price, put_weight, put_total, ord_total, deleted_order FROM getorderinfo(?)";
    private const SQL_FIND_WHERE_ITEMS = "SELECT * FROM getorderitems(?)";
    private const SQL_FIND_WHERE_NOTES = "SELECT odr_id, one_content, to_char(one_date, 'YYYY-MM-DD HH24:MI:SS') as one_date, title, accounts.amr_first_name, accounts.amr_last_name FROM order_notes JOIN accounts on order_notes.act_id = accounts.act_id AND odr_id = ? ORDER BY one_date ASC";
    private const SQL_FIND_WHERE_HISTORIES = "SELECT amr_first_name, amr_last_name, amr_avatar, to_char(date_from, 'YYYY-MM-DD HH24:MI:SS') as date_from, status FROM (select * from order_histories WHERE odr_id = ?) t1 LEFT JOIN accounts ON t1.act_id = accounts.act_id ORDER BY date_from ASC";
    private const SQL_FIND_WHERE_ACT_ID = "SELECT id, to_char(odr_date, 'YYYY-MM-DD HH24:MI:SS') as odr_date FROM orders WHERE act_id = ?";

    private const SQL_INSERT = "INSERT INTO order_notes (odr_id, one_date, act_id, title, one_content) VALUES (?,now(),?,?,?)";
    private const SQL_INSERT_NOTE = "SELECT odr_id, one_content, to_char(one_date, 'YYYY-MM-DD HH24:MI:SS') as one_date, title, accounts.amr_first_name, accounts.amr_last_name FROM order_notes JOIN accounts on order_notes.act_id = accounts.act_id AND odr_id = ? ORDER BY one_date ASC";

    private const SQL_DELETE = "UPDATE orders SET deleted_order = ? WHERE id = ?";

    private const SQL_UPDATE = "SELECT * FROM update_status_order_prc(?,?,?);";

    /**
     * OrdersGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    /**
     * @return array
     */
    public function selectAll() : array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }

    /**
     * @param $odrId
     * @return array
     * @throws OrderNotFoundException
     */
    public function findWhere($odrId): array
    {
        $dataInfo = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE_INFO, [$odrId]);
        $dataItems = $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE_ITEMS, [$odrId]);
        $dataNotes = $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE_NOTES, [$odrId]);
        $dataHistories = $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE_HISTORIES, [$odrId]);
        if (!empty($dataInfo) && !empty($dataItems)) return ["order_histories" => $dataHistories, "order_notes" => $dataNotes, "order_info" => $dataInfo, "order_items" => $dataItems];
        throw new OrderNotFoundException("Order with id: '{$odrId}' was not found!");
    }

    public function findWhereActId($actId): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_FIND_WHERE_ACT_ID, [$actId]);
    }

    /**
     * @param object $orderNote
     * @return array
     * @throws PostgreSQLDatabaseException
     */
    public function insert(object $orderNote)
    {
        try
        {
            $this->pgsql->beginTransaction();
            $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$orderNote->getOdrId(), $orderNote->getActId(), $orderNote->getTitle(), $orderNote->getOneContent()]);
            $this->pgsql->commit();
            return $this->pgsql->sendQueryAndFetchAll(self::SQL_INSERT_NOTE, [$orderNote->getOdrId()]);
        }
        catch (\PDOException $e)
        {
            $this->pgsql->rollBack();
            $this->logger->Error("Something went wrong when inserting note. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
            throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, 'Something went wrong when inserting a new note');
        }
    }

    /**
     * @param $data
     * @throws PostgreSQLDatabaseException
     */
    public function delete($data)
    {
        try
        {
            $this->pgsql->beginTransaction();
            foreach ($data as $id)
            {
                $this->pgsql->sendQueryOnly(self::SQL_DELETE, ['true', $id]);
            }
            $this->pgsql->commit();
        }
        catch (\PDOException $e)
        {
            $this->pgsql->rollBack();
            $this->logger->Error("Something went wrong while deleting order. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
            throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, 'Something went wrong while deleting order');
        }
    }

    /**
     * @param object $orderHistory
     * @return mixed
     * @throws PostgreSQLDatabaseException
     */
    public function update(object $orderHistory)
    {
        try
        {
            $this->pgsql->beginTransaction();
            $rowCount = $this->pgsql->sendQueryAndFetch(self::SQL_UPDATE, [$orderHistory->getOdrId(), $orderHistory->getStatus(), $orderHistory->getActId()]);
            $this->pgsql->commit();
            return $rowCount;
        }
        catch (\PDOException $e)
        {
            $this->pgsql->rollBack();
            $this->logger->Error("Something went wrong while updating order status. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
            throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, 'Something went wrong while updating order status');
        }
    }
}