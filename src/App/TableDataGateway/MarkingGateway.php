<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 14:31
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Storage\Database;

class MarkingGateway extends AbstractGateway
{
    private const SQL_SELECT_ALL = "SELECT * FROM markings";

    /**
     * MarkingGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        return $this->pgsql->sendQueryAndFetchAll(self::SQL_SELECT_ALL, []);
    }
}