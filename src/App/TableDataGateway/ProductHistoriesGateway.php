<?php
/**
 * Created by Daniel Bill
 * Date: 26.08.2018
 * Time: 15:53
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\BO\ProductHistory;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;

class ProductHistoriesGateway extends AbstractGateway implements IGateway
{
    private const SQL_UPDATE_PRICE = "INSERT INTO product_histories (put_id, date_from, act_id, phy_name, price_vat, price_no_vat, weight, date_to, option_title) SELECT ?, now(), act_id, phy_name, ?, ?, weight, null, option_title FROM product_histories WHERE put_id = ? ORDER BY date_to DESC LIMIT 1";
    private const SQL_UPDATE_WEIGHT = "INSERT INTO product_histories (put_id, date_from, act_id, phy_name, price_vat, price_no_vat, weight, date_to, option_title) SELECT ?, now(), act_id, phy_name, price_vat, price_no_vat, ?, null, option_title FROM product_histories WHERE put_id = ? ORDER BY date_to DESC LIMIT 1";
    private const SQL_DELETE = "UPDATE product_histories SET date_to = now() WHERE put_id = ? AND date_to IS NULL";
    private const SQL_INSERT = "INSERT INTO product_histories (put_id, date_from, act_id, phy_name, price_vat, price_no_vat, weight, date_to, option_title) VALUES (?,now(),?,?,?,?,?,null,?)";
    private const SQL_FIND_WHERE = "SELECT phy_name, price_vat, price_no_vat, weight, option_title FROM product_histories WHERE put_id = ? AND date_to IS NULL";

    /**
     * ProductHistoriesGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        $rowCount = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object]);
        if($rowCount == 0)
            throw new EntityNotFoundException("Product", $object);
        return $rowCount;
    }

    public function insert(object $object)
    {
        $this->pgsql->sendQueryOnly(self::SQL_INSERT, [$object->getPutId(), $object->getActId(), $object->getPhyName(), $object->getPriceVat(), $object->getPriceNoVat(), $object->getWeight(), $object->getOptionTitle()]);
    }

    public function delete($id)
    {
        foreach ($id as $putId)
        {
            $this->pgsql->sendQueryOnly(self::SQL_DELETE, [$putId]);
        }
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }

    public function updateWeight(ProductHistory $phy)
    {
        $this->pgsql->sendQueryOnly(self::SQL_UPDATE_WEIGHT, [$phy->getPutId(), $phy->getWeight(), $phy->getPutId()]);
    }

    public function updatePrice($items, ProductHistory $phy)
    {
        foreach ($items as $item)
        {
            $this->pgsql->sendQueryOnly(self::SQL_UPDATE_PRICE, [$item, $phy->getPriceVat(), $phy->getPriceNoVat(), $item]);
        }
    }
}