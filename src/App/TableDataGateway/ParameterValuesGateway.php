<?php
/**
 * Created by Daniel Bill
 * Date: 28.02.2019
 * Time: 17:42
 */

namespace Kominexpres\src\App\TableDataGateway;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Storage\Database;


/**
 * Class ParameterValuesGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class ParameterValuesGateway extends AbstractGateway implements IGateway
{
    private const SQL_FIND_WHERE = "SELECT * FROM parameter_values WHERE pve_value = ? AND prr_id = ?";
    private const SQL_FIND_WHERE_NULL = "SELECT * FROM parameter_values WHERE pve_value IS NULL AND prr_id = ?";
    private const SQL_INSERT = "INSERT INTO parameter_values (prr_id, pve_value) VALUES (?,?) RETURNING pve_id";
    private const SQL_INSERT_NULL = "INSERT INTO parameter_values (prr_id, pve_value) VALUES (?,null) RETURNING pve_id";
    private const SQL_DELETE_BY_PRR_ID = "DELETE FROM parameter_values WHERE prr_id = ?";

    /**
     * ParameterValuesGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        parent::__construct($pgsql, $mysql);
    }

    public function selectAll(): array
    {
        throw new NotImplementedException();
    }

    /**
     * @param $object
     * @return array
     * @throws EntityNotFoundException
     */
    public function findWhere($object): array
    {
        if ($object->getPveValue() === NULL || strlen(trim($object->getPveValue())) == 0)
        {
            $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE_NULL, [$object->getPrrId()]);
        }
        else
        {
            $arr = $this->pgsql->sendQueryAndFetch(self::SQL_FIND_WHERE, [$object->getPveValue(), $object->getPrrId()]);
        }
        if (empty($arr))
            throw new EntityNotFoundException("Parameter value", $object->getPrrId());
        return $arr;
    }

    public function insert(object $object)
    {
        if ($object->getPveValue() === NULL || strlen(trim($object->getPveValue())) == 0)
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT_NULL, [$object->getPrrId()]);
        }
        else
        {
            return $this->pgsql->sendQueryAndFetch(self::SQL_INSERT, [$object->getPrrId(), $object->getPveValue()]);
        }
    }

    public function delete($id)
    {
        throw new NotImplementedException();
    }

    public function deleteByPrrId($prrId)
    {
        $this->pgsql->sendQueryOnly(self::SQL_DELETE_BY_PRR_ID, [$prrId]);
    }

    public function update(object $object)
    {
        throw new NotImplementedException();
    }
}