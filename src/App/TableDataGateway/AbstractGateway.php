<?php
/**
 * Created by Daniel Bill
 * Date: 20.02.2018
 * Time: 15:53
 */

namespace Kominexpres\src\App\TableDataGateway;


use Kominexpres\src\App\Logger\LOGGER;
use Kominexpres\src\App\Storage\Database;

/**
 * Class AbstractGateway
 * @package Kominexpres\src\App\TableDataGateway
 */
class AbstractGateway
{
    /** @var Database|null */
    protected $pgsql;
    /** @var Database|null */
    protected $mysql;
    /** @var \Monolog\Logger */
    protected $logger;

    /**
     * AbstractGateway constructor.
     * @param Database $pgsql
     * @param Database|null $mysql
     */
    public function __construct(Database $pgsql, ?Database $mysql)
    {
        $this->pgsql = $pgsql;
        $this->mysql = $mysql;
        $this->logger = LOGGER::getLogger();
    }
}