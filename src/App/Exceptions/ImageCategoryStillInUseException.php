<?php
/**
 * Created by Daniel Bill
 * Date: 22.07.2018
 * Time: 14:26
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class ImageCategoryStillInUseException extends StatusCodeException
{
    /**
     * ImageCategoryStillInUseException constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct(StatusObject::FORBIDDEN, "Image category '{$id}' is in use.");
    }
}