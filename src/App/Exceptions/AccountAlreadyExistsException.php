<?php
/**
 * Created by Daniel Bill
 * Date: 04.07.2017
 * Time: 20:56
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class AccountAlreadyExistsException extends StatusCodeException
{
    /**
     * AccountAlreadyExistsException constructor.
     * @param string $actEmail
     */
    public function __construct(string $actEmail)
    {
        parent::__construct(StatusObject::CONFLICT, "Account with email: '{$actEmail}' already exists.");
    }
}