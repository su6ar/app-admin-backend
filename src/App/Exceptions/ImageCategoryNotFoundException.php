<?php
/**
 * Created by Daniel Bill
 * Date: 23.07.2018
 * Time: 21:00
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class ImageCategoryNotFoundException extends StatusCodeException
{
    /**
     * ImageCategoryNotFoundException constructor.
     * @param int $icyId
     */
    public function __construct(int $icyId)
    {
        parent::__construct(StatusObject::NOT_FOUND, "Image category '{$icyId}' not found.");
    }
}