<?php
/**
 * Created by Daniel Bill
 * Date: 10.08.2018
 * Time: 13:32
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class DiscountValueException extends StatusCodeException
{
    /**
     * DiscountValueException constructor.
     * @param $value
     */
    public function __construct($value)
    {
        parent::__construct(StatusObject::INVALID_INPUT, "Discount value is too high: " . $value);
    }
}