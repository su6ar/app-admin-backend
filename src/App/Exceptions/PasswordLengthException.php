<?php
/**
 * Created by Daniel Bill
 * Date: 04.07.2017
 * Time: 14:29
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class PasswordLengthException
 * @package Kominexpres\src\App\Exceptions
 */
class PasswordLengthException extends StatusCodeException
{
    /**
     * PasswordLengthException constructor.
     */
    public function __construct()
    {
        parent::__construct(StatusObject::INVALID_INPUT, "The password length must be greater than 7 characters!");
    }
}