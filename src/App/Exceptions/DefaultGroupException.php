<?php
/**
 * Created by Daniel Bill
 * Date: 11.08.2018
 * Time: 20:42
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class DefaultGroupException extends StatusCodeException
{
    /**
     * DefaultGroupException constructor.
     */
    public function __construct()
    {
        parent::__construct(StatusObject::FORBIDDEN, "You can't delete default group.");
    }
}