<?php
/**
 * Created by Daniel Bill
 * Date: 03.07.2017
 * Time: 19:52
 */

namespace Kominexpres\src\App\Exceptions\HTTP;


use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;

class RouteNotFoundException extends StatusCodeException
{
    public function __construct()
    {
        parent::__construct(StatusObject::NOT_FOUND, 'Route not found');
    }
}