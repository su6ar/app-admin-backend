<?php
/**
 * Created by Daniel Bill
 * Date: 03.07.2017
 * Time: 19:52
 */

namespace Kominexpres\src\App\Exceptions\HTTP;


use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;

class MethodNotAllowedException extends StatusCodeException
{
    public function __construct(array $methods)
    {
        parent::__construct(StatusObject::METHOD_NOT_ALLOWED, 'Method not allowed. Must be one of '.implode(', ', $methods));
    }
}