<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 23:42
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class EntityNotFoundException extends StatusCodeException
{
    /**
     * EntityNotFoundException constructor.
     * @param string $entity
     * @param $object
     */
    public function __construct(string $entity, $object)
    {
        parent::__construct(StatusObject::NOT_FOUND, $entity . " '{$object}' not found.");
    }
}