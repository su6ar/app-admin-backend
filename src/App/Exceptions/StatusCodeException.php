<?php
/**
 * Created by Daniel Bill
 * Date: 03.07.2017
 * Time: 19:54
 */

namespace Kominexpres\src\App\Exceptions;


class StatusCodeException extends \Exception implements \JsonSerializable
{
    /** @var  int */
    private $statusCode;

    /**
     * StatusCodeException constructor.
     * @param int $statusCode
     * @param string $message
     */
    public function __construct($statusCode, $message)
    {
        parent::__construct($message, $statusCode);
        $this->statusCode = $statusCode;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param $string
     * @return bool
     */
    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'code'=>dechex($this->getCode()),
            'message'=>$this->isJson($this->getMessage())?json_decode($this->getMessage()):$this->getMessage()
        ];
    }
}