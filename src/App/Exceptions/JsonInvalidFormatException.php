<?php
/**
 * Created by Danie
 * Date: 03.07.2017
 * Time: 20:39
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class JsonInvalidFormatException extends StatusCodeException
{
    /**
     * JsonInvalidFormatException constructor.
     * @param int $message
     */
    public function __construct($message)
    {
        parent::__construct(StatusObject::INVALID_INPUT, $message);
    }
}