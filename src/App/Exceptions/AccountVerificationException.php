<?php
/**
 * Created by Daniel Bill
 * Date: 27.07.2017
 * Time: 20:45
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class AccountVerificationException
 * @package Kominexpres\src\App\Exceptions
 */
class AccountVerificationException extends StatusCodeException
{
    /**
     * AccountVerificationException constructor.
     */
    public function __construct()
    {
        parent::__construct(StatusObject::INVALID_INPUT, "The credentials are incorrect!");
    }
}