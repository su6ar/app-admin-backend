<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 20:18
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class ShippingAlreadyExistsException
 * @package Kominexpres\src\App\Exceptions
 */
class ShippingAlreadyExistsException extends StatusCodeException
{
    /**
     * ShippingAlreadyExistsException constructor.
     * @param string $shyName
     */
    public function __construct(string $shyName)
    {
        parent::__construct(StatusObject::CONFLICT, "Shipping '{$shyName}' already exists.");
    }
}