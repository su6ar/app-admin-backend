<?php
/**
 * Created by Daniel Bill
 * Date: 23.07.2018
 * Time: 15:27
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class ImageCategoryAlreadyExistsException extends StatusCodeException
{
    /**
     * ImageCategoryAlreadyExistsException constructor.
     * @param string $icyName
     */
    public function __construct(string $icyName)
    {
        parent::__construct(StatusObject::CONFLICT, "Image category '{$icyName}' already exists.");
    }
}