<?php
/**
 * Created by Daniel Bill
 * Date: 17.08.2017
 * Time: 17:50
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class TokenNotFoundException extends StatusCodeException
{
    /**
     * TokenNotFoundException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::NOT_FOUND, $message);
    }
}