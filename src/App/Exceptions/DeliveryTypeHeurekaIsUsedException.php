<?php
/**
 * Created by Daniel Bill
 * Date: 06.04.2018
 * Time: 20:40
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class DeliveryTypeHeurekaIsUsedException
 * @package Kominexpres\src\App\TableDataGateway
 */
class DeliveryTypeHeurekaIsUsedException extends StatusCodeException
{

    /**
     * DeliveryTypeHeurekaIsUsedException constructor.
     * @param $id
     */
    public function __construct(int $id)
    {
        parent::__construct(StatusObject::FORBIDDEN, "Delivery '{$id}' is in use.");
    }
}