<?php
/**
 * Created by Daniel Bill
 * Date: 11.07.2017
 * Time: 22:45
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class MailClientException
 * @package Kominexpres\src\App\Exceptions
 */
class MailClientException extends StatusCodeException
{
    /**
     * MailClientException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::INVALID_INPUT, $message);
    }
}