<?php
/**
 * Created by Daniel Bill
 * Date: 13.08.2017
 * Time: 1:59
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class UnauthorizedException
 * @package Kominexpres\src\App\Exceptions
 */
class UnauthorizedException extends StatusCodeException
{
    /**
     * UnauthorizedException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::UNAUTHORIZED, $message);
    }
}