<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 20:18
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class ManufacturerAlreadyExistsException
 * @package Kominexpres\src\App\Exceptions
 */
class ManufacturerAlreadyExistsException extends StatusCodeException
{
    /**
     * ManufacturerAlreadyExistsException constructor.
     * @param string $rurName
     */
    public function __construct(string $rurName)
    {
        parent::__construct(StatusObject::CONFLICT, "Manufacturer '{$rurName}' already exists.");
    }
}