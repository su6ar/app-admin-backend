<?php
/**
 * Created by Daniel Bill
 * Date: 06.04.2018
 * Time: 20:40
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class ManufacturerIsUsedException
 * @package Kominexpres\src\App\Exceptions
 */
class ManufacturerIsUsedException extends StatusCodeException
{

    /**
     * ManufacturerIsUsedException constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct(StatusObject::FORBIDDEN, "Manufacturer '{$id}' is in use.");
    }
}