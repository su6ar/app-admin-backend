<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 20:24
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class ShippingNotFoundException
 * @package Kominexpres\src\App\Exceptions
 */
class ShippingNotFoundException extends StatusCodeException
{
    /**
     * ShippingNotFoundException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::NOT_FOUND, $message);
    }
}