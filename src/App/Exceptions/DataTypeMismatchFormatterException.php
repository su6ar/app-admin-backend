<?php
/**
 * Created by Danie
 * Date: 03.07.2017
 * Time: 20:37
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class DataTypeMismatchFormatterException extends StatusCodeException
{
    /**
     * DataTypeMismatchFormatterException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::INTERNAL_ERROR, $message);
    }
}