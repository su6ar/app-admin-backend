<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 15:45
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class EntityAlreadyExistsException extends StatusCodeException
{
    /**
     * EntityAlreadyExistsException constructor.
     * @param string $entity
     * @param $object
     */
    public function __construct(string $entity, $object)
    {
        parent::__construct(StatusObject::CONFLICT, $entity . " '{$object}' already exists.");
    }
}