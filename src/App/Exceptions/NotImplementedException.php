<?php
/**
 * Created by Daniel Bill
 * Date: 25.10.2017
 * Time: 18:34
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class NotImplementedException
 * @package Kominexpres\src\App\Exceptions
 */
class NotImplementedException extends StatusCodeException
{
    /**
     * Not Implemented exception
     */
    public function __construct()
    {
        parent::__construct(StatusObject::NOT_IMPLEMENTED, 'Not Implemented');
    }
}