<?php
/**
 * Created by Daniel Bill
 * Date: 12.07.2017
 * Time: 20:44
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class OrderNotFoundException extends StatusCodeException
{
    /**
     * OrderNotFoundException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::NOT_FOUND, $message);
    }
}