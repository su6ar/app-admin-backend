<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 21:12
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class DeliveryTypeHeurekaNotFoundException
 * @package Kominexpres\src\App\Exceptions
 */
class DeliveryTypeHeurekaNotFoundException extends StatusCodeException
{
    /**
     * DeliveryTypeHeurekaNotFoundException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::NOT_FOUND, $message);
    }
}