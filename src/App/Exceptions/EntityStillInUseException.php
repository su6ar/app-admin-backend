<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 16:01
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class EntityStillInUseException extends StatusCodeException
{
    /**
     * EntityStillInUseException constructor.
     * @param string $entity
     * @param $object
     */
    public function __construct(string $entity, $object)
    {
        parent::__construct(StatusObject::FORBIDDEN, $entity . " '{$object}' is in use.");
    }
}