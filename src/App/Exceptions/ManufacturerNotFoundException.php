<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:47
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

class ManufacturerNotFoundException extends StatusCodeException
{
    /**
     * ManufacturerNotFoundException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct(StatusObject::NOT_FOUND, $message);
    }
}