<?php
/**
 * Created by Daniel Bill
 * Date: 04.07.2017
 * Time: 20:46
 */

namespace Kominexpres\src\App\Exceptions;


class PostgreSQLDatabaseException extends StatusCodeException
{
    public const DELETING = "deleting";
    public const INSERTING = "inserting";
    public const MESSAGE = "Something went wrong while";
    public const SORTING = "sorting";
    public const UPDATING = "updating";

    public function __construct($statusCode, $message)
    {
        parent::__construct($statusCode, self::MESSAGE . " " . $message);
    }
}