<?php
/**
 * Created by Daniel Bill
 * Date: 03.07.2017
 * Time: 19:38
 */

namespace Kominexpres\src\App\Exceptions;


use Psr\Log\LoggerInterface;
use Kominexpres\src\App\Logger\LOGGER;

class ExceptionHandler
{
    /** @var LoggerInterface */
    protected static $logger;

    public static function setExceptionHandler(){
        self::$logger = LOGGER::getLogger();
        set_exception_handler(array('\Kominexpres\src\App\Exceptions\ExceptionHandler','exception_handler'));
        set_error_handler(array('\Kominexpres\src\App\Exceptions\ExceptionHandler','error_handler'));
    }

    /**
     * @param \Throwable $exception
     */
    public static function exception_handler($exception) {
        http_response_code(500);
        header('Content-Type: application/json;charset=utf-8');
        print json_encode(self::formatException($exception));
        self::$logger->error(dechex($exception->getCode())." ".$exception->getMessage()." File: ".$exception->getFile()." Line: ".$exception->getLine()." Traceback: ".$exception->getTraceAsString());
        die();
    }

    public static function formatException(\Throwable $throwable){
        return [
            'code'=>dechex($throwable->getCode()),
            'message'=>$throwable->getMessage()
        ];
    }

    /**
     * @param $errno
     * @param $errstr
     */
    public static function error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
        http_response_code(500);
        header('Content-Type: application/json;charset=utf-8');
        print(json_encode(["message"=>"Error: [$errno] $errstr"]));
        self::$logger->error("Error: [$errno] $errstr File: $errfile Line: $errline");
        die();
    }
}