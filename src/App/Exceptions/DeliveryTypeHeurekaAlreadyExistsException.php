<?php
/**
 * Created by Daniel Bill
 * Date: 03.03.2018
 * Time: 20:18
 */

namespace Kominexpres\src\App\Exceptions;


use Kominexpres\src\App\POPO\StatusObject;

/**
 * Class DeliveryTypeHeurekaAlreadyExistsException
 * @package Kominexpres\src\App\Exceptions
 */
class DeliveryTypeHeurekaAlreadyExistsException extends StatusCodeException
{
    /**
     * DeliveryTypeHeurekaAlreadyExistsException constructor.
     * @param string $dtaName
     */
    public function __construct(string $dtaName)
    {
        parent::__construct(StatusObject::CONFLICT, "Delivery '{$dtaName}' already exists.");
    }
}