<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 23.01.2017
 * Time: 22:25
 */

namespace Kominexpres\src\App\Storage;

/**
 * Class DbWrapper
 * @package Kominexpres\src\App\Storage
 */
class Database extends \PDO
{
    /**
     * DbWrapper constructor.
     * @param string $dsn
     * @param string $username
     * @param string $password
     */
    public function __construct(string $dsn, string $username, string $password)
    {
        parent::__construct($dsn, $username, $password);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param $sql
     * @param array $inputParams
     * @param int $fetchStyle
     * @param string $className
     * @return array
     */
    public function sendQueryAndFetchAll($sql, array $inputParams = [], $fetchStyle = \PDO::FETCH_ASSOC, string $className = null)
    {
        $stmt = $this->sendQuery($sql, $inputParams);
        return $className === null ? $stmt->fetchAll($fetchStyle) : $stmt->fetchAll($fetchStyle);
    }

    /**
     * @param $sql
     * @param array|null $inputParams
     * @param int $fetchStyle
     * @return mixed
     */
    public function sendQueryAndFetch($sql, array $inputParams = [], $fetchStyle = \PDO::FETCH_ASSOC)
    {
        $stmt = $this->sendQuery($sql, $inputParams);
        return $stmt->fetch($fetchStyle);
    }

    /**
     * Function for multiple sql queries with same SQL, but different input parameters
     *
     * @param string $sql SQL query
     * @param array  $arrayOfInputParams Array of input params, eg. [["param1", "param2"], ["param1", "param2"]]
     * @param int    $fetchStyle PDO fetch style
     *
     * @return array
     */
    public function sendMultipleQueryAndFetchAll($sql, array $arrayOfInputParams = [], $fetchStyle = \PDO::FETCH_ASSOC): array
    {
        $stmt = $this->prepare($sql);
        $data = [];
        foreach ($arrayOfInputParams as $inputParams) {
            $stmt->execute($inputParams);
            $data[] = $stmt->fetchAll($fetchStyle);
        }

        return $data;
    }

    /**
     * @param string $sql SQL query
     * @param array  $inputParams
     *
     * @return int Row count affected by query
     */
    public function sendQueryOnly($sql, array $inputParams = [])
    {
        $stmt = $this->sendQuery($sql, $inputParams);
        return $stmt->rowCount();
    }

    /**
     * @param string $sql
     * @param array  $inputParams
     * @return \PDOStatement
     */
    private function sendQuery($sql, array $inputParams)
    {
        $stmt = $this->prepare($sql);
        $stmt->execute($inputParams);

        return $stmt;
    }
}