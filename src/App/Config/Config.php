<?php
/**
 * Created by Daniel Bill
 * Date: 26.07.2017
 * Time: 20:26
 */

namespace Kominexpres\src\App\Config;


use Dotenv\Dotenv;
use Dotenv\Exception\ValidationException;

class Config
{
    /** @var Dotenv */
    private $config;
    /** @var string */
    private $secretKey;
    /** @var string */
    private $dbCharset;
    /** @var string */
    private $dbMysqlName;
    /** @var string */
    private $dbMysqlHost;
    /** @var string */
    private $dbMysqlDSN;
    /** @var string */
    private $dbMysqlUsername;
    /** @var string */
    private $dbMysqlPassword;
    /** @var string */
    private $dbPgsqlName;
    /** @var string */
    private $dbPgsqlHost;
    /** @var string */
    private $dbPgsqlDSN;
    /** @var string */
    private $dbPgsqlUsername;
    /** @var string */
    private $dbPgsqlPassword;
    /** @var string */
    private $mailCharSet;
    /** @var string */
    private $mailSmtpDebug;
    /** @var string */
    private $mailSmtpSecure;
    /** @var string */
    private $mailSmtpAuth;
    /** @var string */
    private $mailHost;
    /** @var string */
    private $mailPort;
    /** @var string */
    private $mailHtml;
    /** @var string */
    private $mailEshopUsername;
    /** @var string */
    private $mailEshopPassword;
    /** @var string */
    private $mailInfoUsername;
    /** @var string */
    private $mailInfoPassword;

    /**
     * Config constructor.
     * @param Dotenv $config
     */
    public function __construct(Dotenv $config)
    {
        $this->config = $config;
        $this->validate();

        $this->secretKey = getenv('SECRET_KEY');
        $this->dbCharset = getenv('DB_CHARSET');
        $this->dbMysqlDSN = getenv('DB_MYSQL_DSN');
        $this->dbMysqlHost = getenv('DB_MYSQL_HOST');
        $this->dbMysqlName = getenv('DB_MYSQL_NAME');
        $this->dbMysqlPassword = getenv('DB_MYSQL_PASSWORD');
        $this->dbMysqlUsername = getenv('DB_MYSQL_USERNAME');
        $this->dbPgsqlDSN = getenv('DB_PGSQL_DSN');
        $this->dbPgsqlHost = getenv('DB_PGSQL_HOST');
        $this->dbPgsqlName = getenv('DB_PGSQL_NAME');
        $this->dbPgsqlPassword = getenv('DB_PGSQL_PASSWORD');
        $this->dbPgsqlUsername = getenv('DB_PGSQL_USERNAME');
        $this->mailCharSet = getenv('MAIL_CHAR_SET');
        $this->mailEshopPassword = getenv('MAIL_ESHOP_PASSWORD');
        $this->mailEshopUsername = getenv('MAIL_ESHOP_USERNAME');
        $this->mailHost = getenv('MAIL_HOST');
        $this->mailHtml = getenv('MAIL_HTML');
        $this->mailPort = getenv('MAIL_PORT');
        $this->mailInfoPassword = getenv('MAIL_INFO_PASSWORD');
        $this->mailInfoUsername = getenv('MAIL_INFO_USERNAME');
        $this->mailSmtpAuth = getenv('MAIL_SMTP_AUTH');
        $this->mailSmtpDebug = getenv('MAIL_SMTP_DEBUG');
        $this->mailSmtpSecure = getenv('MAIL_SMTP_SECURE');
    }

    private function validate()
    {
        $keys = [
            'SECRET_KEY',
            'DB_CHARSET',
            'DB_MYSQL_NAME',
            'DB_MYSQL_HOST',
            'DB_MYSQL_DSN',
            'DB_MYSQL_USERNAME',
            'DB_MYSQL_PASSWORD',
            'DB_PGSQL_NAME',
            'DB_PGSQL_HOST',
            'DB_PGSQL_DSN',
            'DB_PGSQL_USERNAME',
            'DB_PGSQL_PASSWORD',
            'MAIL_CHAR_SET',
            'MAIL_SMTP_DEBUG',
            'MAIL_SMTP_SECURE',
            'MAIL_SMTP_AUTH',
            'MAIL_HOST',
            'MAIL_PORT',
            'MAIL_HTML',
            'MAIL_ESHOP_USERNAME',
            'MAIL_ESHOP_PASSWORD',
            'MAIL_INFO_USERNAME',
            'MAIL_INFO_PASSWORD'
        ];

        try {
            $this->config->required($keys)->notEmpty();
        } catch (ValidationException $e) {
            header("Content-Type: application/json", true, 500);
            die(json_encode(["error"=>"{$e->getMessage()}"]));
        }
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    /**
     * @return string
     */
    public function getDbMysqlDSN(): string
    {
        return $this->dbMysqlDSN;
    }

    /**
     * @return string
     */
    public function getDbMysqlUsername(): string
    {
        return $this->dbMysqlUsername;
    }

    /**
     * @return string
     */
    public function getDbMysqlPassword(): string
    {
        return $this->dbMysqlPassword;
    }

    /**
     * @return string
     */
    public function getDbPgsqlDSN(): string
    {
        return $this->dbPgsqlDSN;
    }

    /**
     * @return string
     */
    public function getDbPgsqlUsername(): string
    {
        return $this->dbPgsqlUsername;
    }

    /**
     * @return string
     */
    public function getDbPgsqlPassword(): string
    {
        return $this->dbPgsqlPassword;
    }

    /**
     * @return string
     */
    public function getMailCharSet(): string
    {
        return $this->mailCharSet;
    }

    /**
     * @return string
     */
    public function getMailSmtpDebug(): string
    {
        return $this->mailSmtpDebug;
    }

    /**
     * @return string
     */
    public function getMailSmtpSecure(): string
    {
        return $this->mailSmtpSecure;
    }

    /**
     * @return string
     */
    public function getMailSmtpAuth(): string
    {
        return $this->mailSmtpAuth;
    }

    /**
     * @return string
     */
    public function getMailHost(): string
    {
        return $this->mailHost;
    }

    /**
     * @return string
     */
    public function getMailPort(): string
    {
        return $this->mailPort;
    }

    /**
     * @return string
     */
    public function getMailHtml(): string
    {
        return $this->mailHtml;
    }

    /**
     * @return string
     */
    public function getMailEshopUsername(): string
    {
        return $this->mailEshopUsername;
    }

    /**
     * @return string
     */
    public function getMailEshopPassword(): string
    {
        return $this->mailEshopPassword;
    }

    /**
     * @return string
     */
    public function getMailInfoUsername(): string
    {
        return $this->mailInfoUsername;
    }

    /**
     * @return string
     */
    public function getMailInfoPassword(): string
    {
        return $this->mailInfoPassword;
    }
}