<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:27
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\DeliveryTypeHeureka;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\DeliveryTypeHeurekaGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ShippingTypeInterface extends AbstractInterface
{
    private const ID = "id";

    /** @var DeliveryTypeHeurekaGateway */
    private $deliveryTypeHeurekaGateway;

    /**
     * ShippingTypeInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->deliveryTypeHeurekaGateway = new DeliveryTypeHeurekaGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getShippingTypeListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $shippingTypeList = $this->deliveryTypeHeurekaGateway->selectAll();
            $so = StatusObject::create($shippingTypeList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getShippingTypeSingleResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->deliveryTypeHeurekaGateway->existsById($id);
            $shippingType = $this->deliveryTypeHeurekaGateway->findWhere($id);
            $so = StatusObject::create($shippingType);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postShippingTypeResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $dta = new DeliveryTypeHeureka();
            $dta->setDtaName($body[DeliveryTypeHeureka::DTA_NAME]);
            $this->deliveryTypeHeurekaGateway->existsByName($dta->getDtaName());
            $this->deliveryTypeHeurekaGateway->insert($dta);
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteShippingTypeResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            foreach ($body as $id)
            {
                $this->deliveryTypeHeurekaGateway->isUsed($id);
                $this->deliveryTypeHeurekaGateway->delete($id);
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putShippingTypeResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $dta = new DeliveryTypeHeureka();
            $dta->setId($id);
            $dta->setDtaName($body[DeliveryTypeHeureka::DTA_NAME]);
            $this->deliveryTypeHeurekaGateway->existsByName($dta->getDtaName());
            $this->deliveryTypeHeurekaGateway->update($dta);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}