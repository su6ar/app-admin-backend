<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 0:45
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\CompanyGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Response;
use Slim\Http\Request;

class CompanyDataInterface extends AbstractInterface
{
    private const MIM_ID = "mimId";

    /** @var CompanyGateway */
    private $companyGateway;

    /**
     * CompanyDataInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->companyGateway = new CompanyGateway($this->dbPgsql, $this->dbMysql);
    }

    public function getCompanyContactListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->companyGateway->selectCompanyContacts();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}