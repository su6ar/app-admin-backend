<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2018
 * Time: 19:37
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Article;
use Kominexpres\src\App\BO\ArticleHistory;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\AccountGateway;
use Kominexpres\src\App\TableDataGateway\ArticleGateway;
use Kominexpres\src\App\TableDataGateway\ArticleHistoryGateway;
use Kominexpres\src\App\TableDataGateway\MenuItemGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ArticlesInterface extends AbstractInterface
{
    private const ATE_ID = "ateId";

    /** @var ArticleGateway  */
    private $articleGateway; 
    /** @var ArticleHistoryGateway  */
    private $articleHistoryGateway;
    /** @var AccountGateway  */
    private $accountGateway;
    /** @var MenuItemGateway  */
    private $menuItemGateway;

    /**
     * ArticlesInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->articleGateway = new ArticleGateway($this->dbPgsql, $this->dbMysql);
        $this->articleHistoryGateway = new ArticleHistoryGateway($this->dbPgsql, $this->dbMysql);
        $this->accountGateway = new AccountGateway($this->dbPgsql, $this->dbMysql);
        $this->menuItemGateway = new MenuItemGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getArticleListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->articleGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getArticleResponse(Request $request, Response $response)
    {
        $ateId = $request->getAttribute(self::ATE_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $item = $this->articleGateway->findWhere($ateId);
            $item["history"] = $this->articleHistoryGateway->findWhere($ateId);
            $so = StatusObject::create($item);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteArticleResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->delete($body);
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param $items
     * @throws \Kominexpres\src\App\Exceptions\TokenNotFoundException
     */
    private function delete($items)
    {
        $ahy = new ArticleHistory();
        $ahy->setActId($this->accountGateway->findByToken($this->token)[Administrator::ACT_ID]);
        foreach ($items as $ateId)
        {
            $ahy->setAteId($ateId);
            $this->articleGateway->delete($ateId);
            $this->articleHistoryGateway->delete($ahy);
            $this->menuItemGateway->deleteByAteId($ateId);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putArticleResponse(Request $request, Response $response)
    {
        $ateId = $request->getAttribute(self::ATE_ID);
        $body = $request->getParsedBody();
        $data = [];
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->articleGateway->findWhere($ateId);
            $actId = $this->accountGateway->findByToken($this->token)[Administrator::ACT_ID];
            $ate = new Article();
            $ate->setAteName($body[Article::ATE_NAME])
                ->setAteId($ateId);
            $ahy = new ArticleHistory();
            $ahy->setAteId($ateId)
                ->setActId($actId)
                ->setAhyContent($body[ArticleHistory::AHY_CONTENT]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $exists = $this->articleGateway->existsByAteName($ate->getAteName(), $ate->getAteId());
                if (!empty($exists))
                {
                    if ($exists[Article::ATE_ACTIVE] == 1)
                    {
                        throw new EntityAlreadyExistsException("Article", $ate->getAteName());
                    }
                    else
                    {
                        $this->menuItemGateway->updateAteId($exists[Article::ATE_ID], $ate->getAteId());
                        $this->delete([$ate->getAteId()]);
                        $ate->setAteId($exists[Article::ATE_ID]);
                        $ahy->setAteId($exists[Article::ATE_ID]);
                        $data = [
                            "ate_id" => $exists[Article::ATE_ID]
                        ];
                    }
                }
                $this->articleGateway->update($ate);
                $this->articleHistoryGateway->delete($ahy);
                $this->articleHistoryGateway->insert($ahy);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " article. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " article");
            }
            return $response->withJson($data, StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postArticleResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $ate = new Article();
            $ate->setAteName($body[Article::ATE_NAME]);
            $ahy = new ArticleHistory();
            try
            {
                $this->dbPgsql->beginTransaction();
                $actId = $this->accountGateway->findByToken($this->token)[Administrator::ACT_ID];
                $arr = $this->articleGateway->existsByAteName($ate->getAteName());
                if (!empty($arr))
                {
                    if ($arr[Article::ATE_ACTIVE] === true)
                    {
                        throw new EntityAlreadyExistsException("Article", $ate->getAteName());
                    }
                    else
                    {
                        $ate->setAteId($arr[Article::ATE_ID]);
                        $this->articleGateway->update($ate);
                    }
                }
                else
                {
                    $ateId = $this->articleGateway->insert($ate)[Article::ATE_ID];
                    $ate->setAteId($ateId);
                }
                $ahy->setAteId($ate->getAteId())
                    ->setActId($actId)
                    ->setAhyContent($body[ArticleHistory::AHY_CONTENT]);
                $this->articleHistoryGateway->insert($ahy);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " article. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " article");
            } 
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}