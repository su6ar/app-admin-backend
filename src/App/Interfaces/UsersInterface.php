<?php
/**
 * Created by Daniel Bill
 * Date: 13.08.2018
 * Time: 15:40
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\AccountGroupHistory;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\Exceptions\UnauthorizedException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\AccountGroupHistoryGateway;
use Kominexpres\src\App\TableDataGateway\OrdersGateway;
use Kominexpres\src\App\TableDataGateway\TokenGateway;
use Kominexpres\src\App\TableDataGateway\UsersGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class UsersInterface extends AbstractInterface
{
    private const ACT_ID = "actId";
    private const GRP_ID = "grpId";
    private const AMR_RIGHTS = "amrRights";

    /** @var UsersGateway  */
    private $usersGateway;
    /** @var OrdersGateway  */
    private $ordersGateway;
    /** @var AccountGroupHistoryGateway  */
    private $accountGroupHistoryGateway;
    /** @var TokenGateway  */
    private $tokenGateway;

    /**
     * UsersInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->usersGateway = new UsersGateway($this->dbPgsql, $this->dbMysql);
        $this->ordersGateway = new OrdersGateway($this->dbPgsql, $this->dbMysql);
        $this->accountGroupHistoryGateway = new AccountGroupHistoryGateway($this->dbPgsql, $this->dbMysql);
        $this->tokenGateway = new TokenGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getCustomerListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->usersGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getCustomerResponse(Request $request, Response $response)
    {
        $actId = $request->getAttribute(self::ACT_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $item = $this->usersGateway->findWhere($actId);
            $item["orders"] = $this->ordersGateway->findWhereActId($actId);
            $so = StatusObject::create($item);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getAdminListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->usersGateway->selectAllAdmins();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getAdminResponse(Request $request, Response $response)
    {
        $actId = $request->getAttribute(self::ACT_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $item = $this->usersGateway->findWhereAdmin($actId);
            $so = StatusObject::create($item);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putAdminResponse(Request $request, Response $response)
    {
        $amrRights = $request->getAttribute(self::AMR_RIGHTS);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_SUPER]);
            $jwt = $this->token->getJwt();
            $tokenRights = $this->tokenGateway->findWhere($jwt)[Administrator::AMR_RIGHTS];
            if ($tokenRights !== "2")
                throw new UnauthorizedException("You are not allowed to be here!");
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $actId)
                {
                    $item = $this->usersGateway->findWhereAdmin($actId);
                    $this->usersGateway->updateAdminRights($amrRights, $actId);
                    $this->tokenGateway->delete($item[Administrator::ACT_EMAIL]);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " admin. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " admin");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putCustomerResponse(Request $request, Response $response)
    {
        $grpId = $request->getAttribute(self::GRP_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $agy = new AccountGroupHistory();
                $agy->setGrpId($grpId);
                foreach ($body as $actId)
                {
                    $this->usersGateway->findWhere($actId);
                    $this->accountGroupHistoryGateway->deleteByActId($actId);
                    $agy->setActId($actId);
                    $this->accountGroupHistoryGateway->insert($agy);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " customer. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " customer");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}
