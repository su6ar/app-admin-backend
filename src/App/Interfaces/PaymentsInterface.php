<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:42
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Payment;
use Kominexpres\src\App\BO\PaymentOptionHistory;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\PaymentGateway;
use Kominexpres\src\App\TableDataGateway\PaymentOptionHistoryGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class PaymentsInterface extends AbstractInterface
{
    private const ID = "id";

    /** @var PaymentGateway */
    private $paymentGateway;
    /** @var PaymentOptionHistoryGateway */
    private $paymentOptionHistoryGateway;

    /**
     * ShippingTypeInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->paymentGateway = new PaymentGateway($this->dbPgsql, $this->dbMysql);
        $this->paymentOptionHistoryGateway = new PaymentOptionHistoryGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getPaymentsListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $paymentsList = $this->paymentGateway->selectAll();
            $so = StatusObject::create($paymentsList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getPaymentSingleResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->paymentGateway->existsById($id);
            $payment = $this->paymentGateway->findWhere($id);
            $payment[PaymentOptionHistory::ADD_PERCENTAGE_OOTT_PRICE] = doubleval($payment[PaymentOptionHistory::ADD_PERCENTAGE_OOTT_PRICE]);
            $payment[PaymentOptionHistory::PRICE] = doubleval($payment[PaymentOptionHistory::PRICE]);
            $so = StatusObject::create($payment);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putPaymentResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->paymentOptionHistoryGateway->existsByPmtId($id);
            $pmt = new Payment();
            $pmt->setId($id)
                ->setCashOnDelivery($body[Payment::CASH_ON_DELIVERY]);
            $poy = new PaymentOptionHistory();
            $poy->setPmtId($id)
                ->setPrice($body[PaymentOptionHistory::PRICE])
                ->setAddPercentageOottPrice(floatval($body[PaymentOptionHistory::ADD_PERCENTAGE_OOTT_PRICE]))
                ->setPoyName($body[PaymentOptionHistory::POY_NAME]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->paymentGateway->update($pmt);
                $this->paymentOptionHistoryGateway->delete($id);
                $this->paymentOptionHistoryGateway->update($poy);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " payment. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " payment");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postPaymentResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $pmt = new Payment();
            $pmt->setCashOnDelivery($body[Payment::CASH_ON_DELIVERY]);
            $poy = new PaymentOptionHistory();
            $poy->setPrice($body[PaymentOptionHistory::PRICE])
                ->setAddPercentageOottPrice(floatval($body[PaymentOptionHistory::ADD_PERCENTAGE_OOTT_PRICE]))
                ->setPoyName($body[PaymentOptionHistory::POY_NAME]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $pmtId = $this->paymentGateway->insert($pmt)[Payment::ID];
                $poy->setPmtId($pmtId);
                $this->paymentOptionHistoryGateway->insert($poy);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " payment. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " payment");
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deletePaymentResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            foreach ($body as $id)
            {
                $this->paymentGateway->delete($id);
                $this->paymentOptionHistoryGateway->delete($id);
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}