<?php
/**
 * Created by Daniel Bill
 * Date: 14.07.2018
 * Time: 14:51
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Image;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\CategoryGateway;
use Kominexpres\src\App\TableDataGateway\ImageGateway;
use Kominexpres\src\App\TableDataGateway\ImageMarkingGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ImagesInterface extends AbstractInterface
{
    private const ICY_ID = "icyId";

    /** @var ImageGateway */
    private $imageGateway;
    /** @var ImageMarkingGateway */
    private $imageMarkingGateway;
    /** @var CategoryGateway */
    private $categoryGateway;

    /**
     * ImagesInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->imageGateway = new ImageGateway($this->dbPgsql, $this->dbMysql);
        $this->imageMarkingGateway = new ImageMarkingGateway($this->dbPgsql, $this->dbMysql);
        $this->categoryGateway = new CategoryGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getImageListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $imageList = $this->imageGateway->selectAll();
            $so = StatusObject::create($imageList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postImageResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $image = new Image();
                $image->setIaeName($body[Image::IAE_NAME])
                    ->setIaePath($body[Image::IAE_PATH])
                    ->setIaeSize($body[Image::IAE_SIZE])
                    ->setIaeType($body[Image::IAE_TYPE])
                    ->setIcyId($body[Image::ICY_ID]);
                $iaeId = $this->imageGateway->insert($image)[Image::IAE_ID];
                $image->setIaeId($iaeId);
                foreach ($body["iae_marking"] as $marking)
                {
                    $this->imageMarkingGateway->insert($iaeId, $marking);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " image. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " image");
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function patchImageResponse(Request $request, Response $response)
    {
        $icyId = $request->getAttribute(self::ICY_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $iaeId)
                {
                    $this->imageGateway->update($iaeId, $icyId);
                    $this->imageMarkingGateway->delete($iaeId);
                    $this->imageMarkingGateway->insertByIcyId($iaeId, $icyId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " image. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " image");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getImageByIcyIdResponse(Request $request, Response $response)
    {
        $icyId = $request->getAttribute(self::ICY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $images = $this->imageGateway->selectByIcyId($icyId);
            $so = StatusObject::create($images);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteImageResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $iaeId)
                {
                    $this->categoryGateway->deleteIaeId($iaeId);
                    $this->imageGateway->delete($iaeId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " image. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " image");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}