<?php
/**
 * Created by PhpStorm.
 * User: Daniel Bill
 * Date: 03.07.2017
 * Time: 15:56
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\Config\Config;
use Kominexpres\src\App\Exceptions\UnauthorizedException;
use Kominexpres\src\App\BO\Token;
use Kominexpres\src\App\Storage\Database;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Response;
use Kominexpres\src\App\Exceptions\StatusCodeException;

/**
 * Class AbstractInterface
 * @package Kominexpres\src\App\Interfaces
 */
abstract class AbstractInterface
{
    const ActInterface = 'AccountsInterface';
    const AdsInterface = 'AddressesInterface';
    const AteInterface = 'ArticlesInterface';
    const CeyInterface = 'CategoryInterface';
    const CdaInterface = 'CompanyDataInterface';
    const DbdInterface = 'DashboardInterface';
    const FieInterface = 'FilesInterface';
    const FcyInterface = 'FileCategoriesInterface';
    const GrpInterface = 'GroupsInterface';
    const IaeInterface = 'ImagesInterface';
    const IcyInterface = 'ImageCategoriesInterface';
    const MurInterface = 'ManufacturerInterface';
    const MigInterface = 'MarkingInterface';
    const MimInterface = 'MenuItemsInterface';
    const MkdInterface = 'MetaKeywordsInterface';
    const OrdInterface = 'OrdersInterface';
    const PgpInterface = 'ParameterGroupsInterface';
    const PmtInterface = 'PaymentsInterface';
    const PrrInterface = 'ParametersInterface';
    const PutInterface = 'ProductInterface';
    const SerInterface = 'StickersInterface';
    const SpgInterface = 'ShippingInterface';
    const SteInterface = 'ShippingTypeInterface';
    const SycInterface = 'SyncInterface';
    const UerInterface = 'UsersInterface';

    /**
     * Instance of container. Can be use to retrieve data, eg. $this->ci->get('settings')
     * @var ContainerInterface
     */
    protected $ci;
    /** @var LoggerInterface */
    protected $logger;
    /** @var Token */
    protected $token;
    /** @var Config */
    protected $config;
    /** @var Database */
    protected $dbMysql;
    /** @var Database */
    protected $dbPgsql;

    /**
     * AbstractInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->ci = $container;
        $this->logger = $container->get('logger');
        $this->config = $container->get('config');
        $this->token = $container->get('token');
        $this->dbMysql = $container->get('mysql');
        $this->dbPgsql = $container->get('pgsql');
    }

    /**
     * @param Response $response
     * @param StatusCodeException $exception
     * @return Response
     */
    protected function exceptionResponse(Response $response, StatusCodeException $exception): Response
    {
        $this->logger->debug(
            "An exception occurred Code:".dechex($exception->getCode()).
            "Message: ".$exception->getMessage().
            " File: ".$exception->getFile().
            " Line: ".$exception->getLine().
            " Stack Trace: ".$exception->getTraceAsString());
        return $response->withJson($exception, $exception->getStatusCode());
    }

    /**
     * @param array $scope
     * @throws UnauthorizedException
     */
    public function checkAuthorization(array $scope): void
    {
        if ($this->token->hasScope($scope) !== true)
            throw new UnauthorizedException("You are not allowed to be here!");
    }
}