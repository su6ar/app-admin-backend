<?php
/**
 * Created by Daniel Bill
 * Date: 10.08.2018
 * Time: 14:29
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\ParameterGroup;
use Kominexpres\src\App\BO\ParameterGroupParameter;
use Kominexpres\src\App\BO\ParameterValues;
use Kominexpres\src\App\BO\Product;
use Kominexpres\src\App\BO\ProductParameterValue;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\ParameterGroupParametersGateway;
use Kominexpres\src\App\TableDataGateway\ParameterGroupsGateway;
use Kominexpres\src\App\TableDataGateway\ParametersGateway;
use Kominexpres\src\App\TableDataGateway\ParameterValuesGateway;
use Kominexpres\src\App\TableDataGateway\ProductGateway;
use Kominexpres\src\App\TableDataGateway\ProductParameterValuesGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ParameterGroupsInterface extends AbstractInterface
{
    private const PGP_ID = "pgpId";

    /** @var ParametersGateway */
    private $parametersGateway;
    /** @var ParameterGroupsGateway */
    private $parameterGroupsGateway;
    /** @var ParameterGroupParametersGateway */
    private $parameterGroupParametersGateway;
    /** @var ProductGateway  */
    private $productGateway;
    /** @var ProductParameterValuesGateway */
    private $productParameterValuesGateway;
    /** @var ParameterValuesGateway  */
    private $parameterValuesGateway;

    /**
     * ParameterGroupsInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->parametersGateway = new ParametersGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterGroupsGateway = new ParameterGroupsGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterGroupParametersGateway = new ParameterGroupParametersGateway($this->dbPgsql, $this->dbMysql);
        $this->productGateway = new ProductGateway($this->dbPgsql, $this->dbMysql);
        $this->productParameterValuesGateway = new ProductParameterValuesGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterValuesGateway = new ParameterValuesGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getParameterGroupListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $parameterGroups = $this->parameterGroupsGateway->selectAll();
            $list = [];
            foreach ($parameterGroups as $key => $pgp)
            {
                $list[$key] = $parameterGroups[$key];
                $list[$key]["parameters"] = $this->parametersGateway->selectByPgpId($pgp[ParameterGroup::PGP_ID]);
                foreach ($list[$key]["parameters"] as $key1 => $option)
                {
                    $list[$key]["parameters"][$key1]["options"] = json_decode($list[$key]["parameters"][$key1]["options"], true);
                }
            }
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getParameterGroupResponse(Request $request, Response $response)
    {
        $pgpId = $request->getAttribute(self::PGP_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $parameterGroup = $this->parameterGroupsGateway->findWhere($pgpId);
            $parameterGroup["parameters"] = $this->parametersGateway->selectByPgpId($pgpId);
            $so = StatusObject::create($parameterGroup);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteParameterGroupResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $pgpId)
                {
                    $arr = $this->productGateway->selectByPgpId($pgpId);
                    foreach ($arr as $item)
                    {
                        $this->productParameterValuesGateway->delete($item[Product::PUT_ID]);
                    }
                    $this->productGateway->deletePgpId($pgpId);
                    $this->parameterGroupParametersGateway->delete($pgpId);
                    $this->parameterGroupsGateway->delete($pgpId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " parameter group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " parameter group");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putParameterGroupResponse(Request $request, Response $response)
    {
        $pgpId = $request->getAttribute(self::PGP_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->parameterGroupsGateway->findWhere($pgpId);
            $pgp = new ParameterGroup();
            $pgp->setPgpId($pgpId)
                ->setPgpName($body[ParameterGroup::PGP_NAME]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->parameterGroupsGateway->update($pgp);

                $prrIds = $this->parameterGroupParametersGateway->selectByPgpId($pgpId);
                $unchangedPrrIds = [];
                $deletedPrrIds = [];
                foreach ($prrIds as $oldPrrId)
                {
                    $unchanged = false;
                    foreach ($body["parameters"] as $newPrrId)
                    {
                        if ($oldPrrId["prr_id"] == $newPrrId)
                        {
                            array_push($unchangedPrrIds, $newPrrId);
                            $unchanged = true;
                            break;
                        }
                    }
                    if (!$unchanged)
                    {
                        array_push($deletedPrrIds, $oldPrrId["prr_id"]);
                    }
                }
                $newPrrIds = array_diff($body["parameters"], $unchangedPrrIds);

                if (count($deletedPrrIds)>0||count($newPrrIds)>0)
                {
                    $this->parameterGroupParametersGateway->delete($pgpId);
                    $pgr = new ParameterGroupParameter();
                    $pgr->setPgpId($pgpId);
                    foreach ($body["parameters"] as $prrId)
                    {
                        $pgr->setPrrId($prrId);
                        $this->parameterGroupParametersGateway->insert($pgr);
                    }
                    $pve = new ParameterValues();
                    foreach ($newPrrIds as $prrId)
                    {
                        $pve->setPveValue(NULL)
                            ->setPrrId($prrId);
                        try
                        {
                            $pveId = $this->parameterValuesGateway->findWhere($pve)[ParameterValues::PVE_ID];
                        }
                        catch (EntityNotFoundException $e)
                        {
                            $pveId = $this->parameterValuesGateway->insert($pve)[ParameterValues::PVE_ID];
                        }
                        $this->productParameterValuesGateway->insertBulk($pveId, $pgpId);
                    }
                    foreach ($deletedPrrIds as $prrId)
                    {
                        $this->productParameterValuesGateway->deleteByPgpIdAndPrrId($pgpId, $prrId);
                    }
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " parameter group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " parameter group");
            }

            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postParameterGroupResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $pgp = new ParameterGroup();
            $pgp->setPgpName($body[ParameterGroup::PGP_NAME]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $pgpId = $this->parameterGroupsGateway->insert($pgp)[ParameterGroup::PGP_ID];
                $pgr = new ParameterGroupParameter();
                $pgr->setPgpId($pgpId);
                foreach ($body["parameters"] as $prrId)
                {
                    $pgr->setPrrId($prrId);
                    $this->parameterGroupParametersGateway->insert($pgr);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " parameter group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " parameter group");
            }

            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}