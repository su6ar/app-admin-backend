<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2017
 * Time: 17:06
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\DashboardGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class DashboardInterface
 * @package Kominexpres\src\App\Interfaces
 */
class DashboardInterface extends AbstractInterface
{
    private const DATE_FROM = "date_from";
    private const DATE_TO = "date_to";

    /** @var DashboardGateway */
    private $client;

    /**
     * DashboardInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->client = new DashboardGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getDashboardStatsResponse(Request $request, Response $response)
    {
        $date_from = $request->getQueryParam(self::DATE_FROM);
        $date_to = $request->getQueryParam(self::DATE_TO);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $dashboardStats = $this->client->getDashboardStats($date_from, $date_to);
            $so = StatusObject::create([
                "users" => $dashboardStats["users_count"],
                "orders" => [
                    "count" => $dashboardStats["orders_count"],
                    "sum" => $dashboardStats["orders_total_sum"]
                ]
            ]);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}