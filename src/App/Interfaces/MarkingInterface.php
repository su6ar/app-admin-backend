<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 14:31
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\MarkingGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class MarkingInterface extends AbstractInterface
{
    /** @var MarkingGateway */
    private $markingGateway;

    /**
     * MarkingInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->markingGateway = new MarkingGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getMarkingListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $markingList = $this->markingGateway->selectAll();
            $so = StatusObject::create($markingList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}