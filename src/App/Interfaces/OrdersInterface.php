<?php
/**
 * Created by PhpStorm.
 * User: Danie
 * Date: 28.01.2017
 * Time: 17:36
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\OrderHistory;
use Kominexpres\src\App\BO\OrderNote;
use Kominexpres\src\App\Exceptions\JsonInvalidFormatException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\POPO\MailMessenger\EshopMailMessenger;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\POPO\TwigObject;
use Kominexpres\src\App\TableDataGateway\AccountGateway;
use Kominexpres\src\App\TableDataGateway\CompanyGateway;
use Kominexpres\src\App\TableDataGateway\OrdersGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class OrdersInterface
 * @package Kominexpres\src\App\Interfaces
 */
class OrdersInterface extends AbstractInterface
{
    private const SEND_EMAIL = 'sendemail';
    private const STATUS = 'status';
    private const ODR_ID = 'odrId';

    /** @var OrdersGateway */
    private $ordersGateway;
    /** @var CompanyGateway */
    private $companyGateway;
    /** @var AccountGateway */
    private $accountGateway;

    /**
     * OrdersInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->ordersGateway = new OrdersGateway($this->dbPgsql, $this->dbMysql);
        $this->companyGateway = new CompanyGateway($this->dbPgsql, $this->dbMysql);
        $this->accountGateway = new AccountGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getOrdersListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $orders = $this->ordersGateway->selectAll();
            $so = StatusObject::create($orders);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \phpmailerException
     */
    public function patchOrdersStatusResponse(Request $request, Response $response)
    {
        $sendEmail = $request->getQueryParam(self::SEND_EMAIL);
        $status = $request->getQueryParam(self::STATUS);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $account = $this->accountGateway->findByToken($this->token);
            $company = $this->companyGateway->getCompanyInfo(true, true);
            foreach ($body as $odrId)
            {
                if(!is_numeric($odrId)) throw new JsonInvalidFormatException("Order id is of a wrong data type");
                $order = $this->ordersGateway->findWhere($odrId);
                $orderHistory = new OrderHistory();
                $orderHistory->setActId($account[Administrator::ACT_ID])
                    ->setStatus($status)
                    ->setOdrId($odrId);
                $rowCount = $this->ordersGateway->update($orderHistory)["update_status_order_prc"];
                $this->logger->info("odrId: " . $odrId . ", status: " . $status . ", account[act_id]: " . $account[Administrator::ACT_ID] . ", sendEmail: " . $sendEmail . ", rowCount: " . $rowCount);

                if ($rowCount != 0)
                {
                    $this->logger->info("logger was here to send email");
                    $statusWord = OrderHistory::getStatusWord($status, $odrId);
                    $body = TwigObject::renderTwig(TwigObject::TEMPLATE_PATH_ORDERS, TwigObject::FILENAME_CHANGE_STATUS, array(
                        "odrId" => $odrId,
                        "statusWord" => $statusWord["status"],
                        "orderInfo" => $order["order_info"],
                        "orderItems" => $order["order_items"],
                        "company" => $company
                    ));
                    $eshopMail = EshopMailMessenger::createMail($this->config);
                    $eshopMail->sendMail($body, mb_strtolower($order["order_info"]["act_email"]), $statusWord["subject"]);
                }
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getOrderResponse(Request $request, Response $response)
    {
        $odrId = $request->getAttribute(self::ODR_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $order = $this->ordersGateway->findWhere($odrId);
            $so = StatusObject::create($order);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function postOrderNoteResponse(Request $request, Response $response)
    {
        $odrId = $request->getAttribute(self::ODR_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $orderNote = new OrderNote();
            $orderNote->setOdrId($odrId)
                ->setActId($body[OrderNote::ACT_ID])
                ->setOneContent($body[OrderNote::ONE_CONTENT])
                ->setTitle($body[OrderNote::TITLE]);
            $newNote = $this->ordersGateway->insert($orderNote);
            $so = StatusObject::create($newNote);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteOrderResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->ordersGateway->delete($body);
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

}
