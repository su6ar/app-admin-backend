<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:39
 */

namespace Kominexpres\src\App\Interfaces;


use const Grpc\CALL_ERROR_TOO_MANY_OPERATIONS;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Category;
use Kominexpres\src\App\BO\CategoryGroupDiscount;
use Kominexpres\src\App\BO\Group;
use Kominexpres\src\App\BO\Parameter;
use Kominexpres\src\App\BO\ParameterGroup;
use Kominexpres\src\App\BO\ProductCategory;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\CategoryGateway;
use Kominexpres\src\App\TableDataGateway\CategoryGroupDiscountsGateway;
use Kominexpres\src\App\TableDataGateway\ParameterGroupsGateway;
use Kominexpres\src\App\TableDataGateway\ParametersGateway;
use Kominexpres\src\App\TableDataGateway\ProductCategoriesGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class CategoryInterface extends AbstractInterface
{
    private const CEY_ID = "ceyId";

    /** @var CategoryGateway */
    private $categoryGateway;
    /** @var CategoryGroupDiscountsGateway */
    private $categoryGroupDiscountsGateway;
    /** @var ParameterGroupsGateway */
    private $parameterGroupsGateway;
    /** @var ParametersGateway */
    private $parametersGateway;
    /** @var ProductCategoriesGateway */
    private $productCategoriesGateway;

    /**
     * ImagesInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->categoryGateway = new CategoryGateway($this->dbPgsql, $this->dbMysql);
        $this->categoryGroupDiscountsGateway = new CategoryGroupDiscountsGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterGroupsGateway = new ParameterGroupsGateway($this->dbPgsql, $this->dbMysql);
        $this->parametersGateway = new ParametersGateway($this->dbPgsql, $this->dbMysql);
        $this->productCategoriesGateway = new ProductCategoriesGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getCategoryListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->categoryGateway->selectAll();
            $resList = Category::generateList($list);
            $so = StatusObject::create($resList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getCategoryResponse(Request $request, Response $response)
    {
        $ceyId = $request->getAttribute(self::CEY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $category = $this->categoryGateway->findWhere($ceyId);
            $category["discounts"] = $this->categoryGroupDiscountsGateway->selectByCeyId($ceyId);
            foreach ($category["discounts"] as $key => $discount)
            {
                $category["discounts"][$key][CategoryGroupDiscount::DISCOUNT] = doubleval($category["discounts"][$key][CategoryGroupDiscount::DISCOUNT]);
            }
            $parameterGroups = $this->parameterGroupsGateway->selectAll();
            foreach ($parameterGroups as $key => $pgp)
            {
                $category["parameter_groups"][$key] = $parameterGroups[$key];
                $category["parameter_groups"][$key]["parameters"] = $this->parametersGateway->selectByPgpId($pgp[ParameterGroup::PGP_ID]);
            }
            $so = StatusObject::create($category);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $ids = [];
            $this->getAllChildren($body, $ids);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->delete($ids);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error("Something went wrong while deleting category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, 'Something went wrong while deleting category');
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function delete($ids)
    {
        foreach ($ids as $ceyId)
        {
            $this->categoryGateway->delete($ceyId);
            $cgy = new CategoryGroupDiscount();
            $cgy->setCeyId($ceyId);
            $this->categoryGroupDiscountsGateway->update($cgy);
            $this->productCategoriesGateway->delete($ceyId);
        }
    }

    /**
     * @param $items
     * @param $ids
     */
    public function getAllChildren($items, &$ids)
    {
        foreach ($items as $item)
        {
            array_push($ids, $item);
            $children = $this->categoryGateway->selectAllChildren($item);
            if (count($children) > 0)
            {
                $this->getAllChildren($children, $ids);
            }
        }
    }

    /**
     * @param $items
     * @param $ceyCeyId
     */
    public function updateSortItems($items, $ceyCeyId)
    {
        $placeNew = 0;
        $place = $this->categoryGateway->selectAvailablePlace($ceyCeyId);
        $placeMax = $place["cey_place_max"];
        $placeMin = $place["cey_place_min"];
        if ((count($items) - $placeMin) > 0)
        {
            $placeNew = $placeMax;
        }
        foreach ($items as $item)
        {
            $placeNew++;
            $category = new Category();
            $category->setCeyCeyId($ceyCeyId)
                ->setCeyId($item[Category::CEY_ID])
                ->setCeyPlace($placeNew)
                ->setCeyName($item[Category::CEY_NAME]);
            $parent = $ceyCeyId;
            while ($parent !== NULL)
            {
                $par = $this->categoryGateway->selectParent($parent);
                $parent = $par[Category::CEY_CEY_ID];
                $category->setCeyUrl(Category::generateUrl($par[Category::CEY_NAME])."/".$category->getCeyUrl());
            }
            $this->categoryGateway->updateSort($category);
            $this->updateSortItems($item["items"], $item[Category::CEY_ID]);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putCategoriesResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->updateSortItems($body, null);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::SORTING . " categories. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::SORTING . " categories");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putCategoryResponse(Request $request, Response $response)
    {
        $ceyId = $request->getAttribute(self::CEY_ID);
        $body = $request->getParsedBody();
        $data = [];
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $category = new Category();
            $category->setCeyId($ceyId)
                ->setIaeId($body[Category::IAE_ID])
                ->setCeyName($body[Category::CEY_NAME])
                ->setCeyCeyId($body[Category::CEY_CEY_ID])
                ->setShowParameters($body[Category::SHOW_PARAMETERS])
                ->setPgpId($body[Category::PGP_ID])
                ->setCeyDescription($body[Category::CEY_DESCRIPTION]);
            $this->categoryGateway->findWhere($ceyId);
            $parent = $category->getCeyCeyId();

            while ($parent !== NULL)
            {
                $par = $this->categoryGateway->selectParent($parent);
                $parent = $par[Category::CEY_CEY_ID];
                $category->setCeyUrl(Category::generateUrl($par[Category::CEY_NAME])."/".$category->getCeyUrl());
            }

            try
            {
                $this->dbPgsql->beginTransaction();
                $exists = $this->categoryGateway->existsByUrl($category->getCeyUrl(), $category->getCeyId());
                if (!empty($exists))
                {
                    if ($exists[Category::CEY_ACTIVE] == 1)
                    {
                        throw new EntityAlreadyExistsException("Category", $category->getCeyUrl());
                    }
                    else
                    {
                        $products = $this->productCategoriesGateway->selectByCeyId($category->getCeyId());
                        $pcy = new ProductCategory();
                        $pcy->setCeyId($exists[Category::CEY_ID]);
                        foreach ($products as $product)
                        {
                            $pcy->setPceMain($product[ProductCategory::PCE_MAIN])
                                ->setPutId($product[ProductCategory::PUT_ID]);
                            $this->productCategoriesGateway->insert($pcy);
                        }
                        $this->delete([$category->getCeyId()]);
                        $category->setCeyId($exists[Category::CEY_ID]);
                        $data = [
                            "cey_id" => $exists[Category::CEY_ID]
                        ];
                    }
                }

                if ($category->getCeyCeyId() !== $body["cey_cey_id_original"])
                {
                    $this->categoryGateway->updateOne($category, true);
                }
                else
                {
                    $this->categoryGateway->updateOne($category, false);
                }
                $cgd = new CategoryGroupDiscount();
                $cgd->setCeyId($category->getCeyId());
                $this->categoryGroupDiscountsGateway->update($cgd);
                foreach ($body["discounts"] as $discount)
                {
                    $cgd->setDiscount(number_format(floatval($discount[CategoryGroupDiscount::DISCOUNT]), 2))
                    ->setGrpId($discount[Group::GRP_ID]);
                    $this->categoryGroupDiscountsGateway->insert($cgd);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT,  PostgreSQLDatabaseException::UPDATING . " category");
            }
            return $response->withJson($data, StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $category = new Category();
            $category->setCeyName($body[Category::CEY_NAME])
                ->setCeyCeyId($body[Category::CEY_CEY_ID])
                ->setIaeId($body[Category::IAE_ID])
                ->setShowParameters($body[Category::SHOW_PARAMETERS])
                ->setPgpId($body[Category::PGP_ID])
                ->setCeyDescription($body[Category::CEY_DESCRIPTION]);
            $parent = $category->getCeyCeyId();

            while ($parent !== NULL)
            {
                $par = $this->categoryGateway->selectParent($parent);
                $parent = $par[Category::CEY_CEY_ID];
                $category->setCeyUrl(Category::generateUrl($par[Category::CEY_NAME])."/".$category->getCeyUrl());
            }

            try
            {
                $this->dbPgsql->beginTransaction();
                $exists = $this->categoryGateway->existsByUrl($category->getCeyUrl(), -1);
                if (!empty($exists))
                {
                    if ($exists[Category::CEY_ACTIVE] == 1)
                    {
                        throw new EntityAlreadyExistsException("Category", $category->getCeyUrl());
                    }
                    else
                    {
                        $category->setCeyId($exists[Category::CEY_ID]);
                        $this->categoryGateway->update($category);
                    }
                }
                else
                {
                    $ceyId = $this->categoryGateway->insert($category)[Category::CEY_ID];
                    $category->setCeyId($ceyId);
                }

                $cgd = new CategoryGroupDiscount();
                $cgd->setCeyId($category->getCeyId());
                $this->categoryGroupDiscountsGateway->update($cgd);
                foreach ($body["discounts"] as $discount)
                {
                    $cgd->setDiscount(number_format(floatval($discount[CategoryGroupDiscount::DISCOUNT]), 2))
                        ->setGrpId($discount[Group::GRP_ID]);
                    $this->categoryGroupDiscountsGateway->insert($cgd);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error("Something went wrong while inserting category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, 'Something went wrong while inserting category');
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}