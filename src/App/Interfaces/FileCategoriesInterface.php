<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 23:03
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\FileCategory;
use Kominexpres\src\App\Exceptions\EntityStillInUseException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\FileCategoryGateway;
use Kominexpres\src\App\TableDataGateway\FileGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class FileCategoriesInterface extends AbstractInterface
{
    private const FCY_ID = "fcyId";

    /** @var FileCategoryGateway */
    private $fileCategoryGateway;
    /** @var FileGateway */
    private $fileGateway;

    /**
     * ImagesInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->fileCategoryGateway = new FileCategoryGateway($this->dbPgsql, $this->dbMysql);
        $this->fileGateway = new FileGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getFileCategoryListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->fileCategoryGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getFileCategoryResponse(Request $request, Response $response)
    {
        $fcyId = (int)$request->getAttribute(self::FCY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->fileCategoryGateway->existsByFcyId($fcyId);
            $so = StatusObject::create($this->fileCategoryGateway->findWhere($fcyId));
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function deleteFileCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $fcyId)
                {
                    $this->fileGateway->existsByFcyId($fcyId);
                    $this->fileCategoryGateway->delete($fcyId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " file category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " file category");
            }
            catch (EntityStillInUseException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error("{$e->getMessage()} Trace: {$e->getTraceAsString()}");
                throw $e;
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postFileCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $fcy = new FileCategory();
            $fcy->setFcyName($body[FileCategory::FCY_NAME]);
            $this->fileCategoryGateway->existsByFcyName($fcy->getFcyName());
            $this->fileCategoryGateway->insert((Object)$fcy);
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putFileCategoryResponse(Request $request, Response $response)
    {
        $fcyId = (int)$request->getAttribute(self::FCY_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $fcy = new FileCategory();
            $fcy->setFcyName($body[FileCategory::FCY_NAME])
                ->setFcyId($fcyId);
            $this->fileCategoryGateway->existsByFcyId($fcyId);
            $fcyName = $this->fileCategoryGateway->findWhere($fcyId)[FileCategory::FCY_NAME];
            if (strcmp($fcyName, $fcy->getFcyName()) != 0) {
                $this->fileCategoryGateway->existsByFcyName($fcy->getFcyName());
            }
            $this->fileCategoryGateway->update((Object)$fcy);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}