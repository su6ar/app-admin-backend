<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:27
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Manufactory;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\ManufacturerGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ManufacturerInterface extends AbstractInterface
{
    private const ID = "id";

    /** @var ManufacturerGateway */
    private $manufacturerGateway;

    /**
     * ShippingTypeInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->manufacturerGateway = new ManufacturerGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getManufacturerListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $mur = $this->manufacturerGateway->selectAll();
            $so = StatusObject::create($mur);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getManufacturerSingleResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->manufacturerGateway->existsById($id);
            $mur = $this->manufacturerGateway->findWhere($id);
            $so = StatusObject::create($mur);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postManufacturerResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $mur = new Manufactory();
            $mur->setMurName($body[Manufactory::MUR_NAME]);
            $this->manufacturerGateway->existsByName($mur->getMurName());
            $this->manufacturerGateway->insert($mur);
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteManufacturerResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            foreach ($body as $id)
            {
                $this->manufacturerGateway->isUsed($id);
                $this->manufacturerGateway->delete($id);
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putManufacturerResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $mur = new Manufactory();
            $mur->setId($id);
            $mur->setMurName($body[Manufactory::MUR_NAME]);
            $this->manufacturerGateway->existsByName($mur->getMurName());
            $this->manufacturerGateway->update($mur);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}