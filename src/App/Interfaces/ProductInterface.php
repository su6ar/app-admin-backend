<?php
/**
 * Created by Daniel Bill
 * Date: 24.08.2018
 * Time: 15:21
 */

namespace Kominexpres\src\App\Interfaces;

use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Category;
use Kominexpres\src\App\BO\MetaKeyword;
use Kominexpres\src\App\BO\ParameterValues;
use Kominexpres\src\App\BO\Payment;
use Kominexpres\src\App\BO\Product;
use Kominexpres\src\App\BO\ProductCategory;
use Kominexpres\src\App\BO\ProductFile;
use Kominexpres\src\App\BO\ProductGroupDiscount;
use Kominexpres\src\App\BO\ProductHistory;
use Kominexpres\src\App\BO\ProductImage;
use Kominexpres\src\App\BO\ProductMenuOption;
use Kominexpres\src\App\BO\ProductMetaKeyword;
use Kominexpres\src\App\BO\ProductParameterValue;
use Kominexpres\src\App\BO\ProductPayment;
use Kominexpres\src\App\BO\ProductShipping;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\AccountGateway;
use Kominexpres\src\App\TableDataGateway\CategoryGateway;
use Kominexpres\src\App\TableDataGateway\ParameterValuesGateway;
use Kominexpres\src\App\TableDataGateway\ProductCategoriesGateway;
use Kominexpres\src\App\TableDataGateway\ProductFilesGateway;
use Kominexpres\src\App\TableDataGateway\ProductGateway;
use Kominexpres\src\App\TableDataGateway\ProductGroupDiscountsGateway;
use Kominexpres\src\App\TableDataGateway\ProductHistoriesGateway;
use Kominexpres\src\App\TableDataGateway\ProductImagesGateway;
use Kominexpres\src\App\TableDataGateway\ProductMenuOptionsGateway;
use Kominexpres\src\App\TableDataGateway\ProductMetaKeywordsGateway;
use Kominexpres\src\App\TableDataGateway\ProductParameterValuesGateway;
use Kominexpres\src\App\TableDataGateway\ProductPaymentsGateway;
use Kominexpres\src\App\TableDataGateway\ProductShippingsGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ProductInterface extends AbstractInterface
{
    private const NOT_SELECTED = "0x6e6564";
    private const DELETE = "delete";
    private const CEY_ID = "ceyId";
    private const ITEMS = "items";
    private const ENABLE = "enable";
    private const PUT_ID = "putId";
    private const SHIPPINGS = "shippings";
    private const CATEGORIES = "categories";
    private const FILES = "files";
    private const IMAGES = "images";
    private const GROUP_DISCOUNTS = "group_discounts";
    private const PAYMENTS = "payments";
    private const MENU_OPTIONS = "menu_options";
    private const PARAMETER_VALUES = "parameter_values";
    private const META_KEYWORDS = "meta_keywords";
    private const HISTORY = "history";

    /** @var ParameterValuesGateway */
    private $parameterValuesGateway;
    /** @var ProductGateway */
    private $productGateway;
    /** @var ProductCategoriesGateway  */
    private $productCategoriesGateway;
    /** @var ProductHistoriesGateway  */
    private $productHistoriesGateway;
    /** @var ProductShippingsGateway  */
    private $productShippingsGateway;
    /** @var AccountGateway  */
    private $accountGateway;
    /** @var ProductFilesGateway  */
    private $productFilesGateway;
    /** @var ProductImagesGateway  */
    private $productImagesGateway;
    /** @var ProductGroupDiscountsGateway  */
    private $productGroupDiscountsGateway;
    /** @var ProductPaymentsGateway  */
    private $productPaymentsGateway;
    /** @var ProductMenuOptionsGateway  */
    private $productMenuOptionsGateway;
    /** @var ProductParameterValuesGateway  */
    private $productParameterValuesGateway;
    /** @var ProductMetaKeywordsGateway */
    private $productMetaKeywordsGateway;
    /** @var CategoryGateway  */
    private $categoryGateway;

    /**
     * ProductInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->productGateway = new ProductGateway($this->dbPgsql, $this->dbMysql);
        $this->productCategoriesGateway = new ProductCategoriesGateway($this->dbPgsql, $this->dbMysql);
        $this->productGroupDiscountsGateway = new ProductGroupDiscountsGateway($this->dbPgsql, $this->dbMysql);
        $this->productFilesGateway = new ProductFilesGateway($this->dbPgsql, $this->dbMysql);
        $this->productMetaKeywordsGateway = new ProductMetaKeywordsGateway($this->dbPgsql, $this->dbMysql);
        $this->productParameterValuesGateway = new ProductParameterValuesGateway($this->dbPgsql, $this->dbMysql);
        $this->productMenuOptionsGateway = new ProductMenuOptionsGateway($this->dbPgsql, $this->dbMysql);
        $this->productPaymentsGateway = new ProductPaymentsGateway($this->dbPgsql, $this->dbMysql);
        $this->productImagesGateway = new ProductImagesGateway($this->dbPgsql, $this->dbMysql);
        $this->productHistoriesGateway = new ProductHistoriesGateway($this->dbPgsql, $this->dbMysql);
        $this->productShippingsGateway = new ProductShippingsGateway($this->dbPgsql, $this->dbMysql);
        $this->accountGateway = new AccountGateway($this->dbPgsql, $this->dbMysql);
        $this->categoryGateway = new CategoryGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterValuesGateway = new ParameterValuesGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getProductListResponse(Request $request, Response $response)
    {
        $ceyId = $request->getQueryParam(ProductCategory::CEY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->productGateway->selectAllByCeyId($ceyId);
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getProductResponse(Request $request, Response $response)
    {
        $putId = $request->getAttribute(self::PUT_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->productGateway->findWhere($putId);
            $list[Product::AVAILABILITY] = intval($list[Product::AVAILABILITY]);
            $list[ProductHistory::WEIGHT] = $list[ProductHistory::WEIGHT] === null ? null : doubleval($list[ProductHistory::WEIGHT]);
            $list[ProductHistory::PRICE_NO_VAT] = doubleval($list[ProductHistory::PRICE_NO_VAT]);
            $list[ProductHistory::PRICE_VAT] = doubleval($list[ProductHistory::PRICE_VAT]);
            $list[self::CATEGORIES] = json_decode($list[self::CATEGORIES], true);
            $list[self::SHIPPINGS] = json_decode($list[self::SHIPPINGS], true);
            $list[self::FILES] = json_decode($list[self::FILES], true);
            $list[self::IMAGES] = json_decode($list[self::IMAGES], true);
            $list[self::GROUP_DISCOUNTS] = json_decode($list[self::GROUP_DISCOUNTS], true);
            $list[self::PAYMENTS] = json_decode($list[self::PAYMENTS], true);
            $list[self::MENU_OPTIONS] = json_decode($list[self::MENU_OPTIONS], true);
            $list[self::PARAMETER_VALUES] = json_decode($list[self::PARAMETER_VALUES], true);
            $list[self::HISTORY] = json_decode($list[self::HISTORY], true);
            $list[self::META_KEYWORDS] = json_decode($list[self::META_KEYWORDS], true);
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteProductResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $putId)
                {
                    $this->productGateway->delete($putId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " product. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " product");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param $haystack
     * @param ProductCategory $pcy
     * @return bool
     */
    private function isParent($haystack, ProductCategory $pcy): bool
    {
        foreach ($haystack as $item)
        {
            try
            {
                if ($item[ProductCategory::CEY_ID] == $pcy->getCeyId() || !empty($this->productCategoriesGateway->findWhere($pcy)))
                    return TRUE;
            }
            catch (EntityNotFoundException $e) {}
        }
        return false;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postProductResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $phy = new ProductHistory();
            $phy->setPriceVat($body[ProductHistory::PRICE_VAT])
                ->setPriceNoVat($body[ProductHistory::PRICE_NO_VAT])
                ->setActId($this->accountGateway->findByEmail($this->token->getSub())[Administrator::ACT_ID])
                ->setOptionTitle($body[ProductHistory::OPTION_TITLE])
                ->setPhyName($body[ProductHistory::PHY_NAME])
                ->setWeight($body[ProductHistory::WEIGHT]);
            $put = new Product();
            $put->setMurId($body[Product::MUR_ID])
                ->setPgpId($body[Product::PGP_ID])
                ->setCode($body[Product::CODE])
                ->setAvailability($body[Product::AVAILABILITY])
                ->setActive($body[Product::ACTIVE])
                ->setBarcode($body[Product::BARCODE])
                ->setDescriptionShort($body[Product::DESCRIPTION_SHORT])
                ->setDescriptionLong($body[Product::DESCRIPTION_LONG])
                ->setWarranty($body[Product::WARRANTY])
                ->setDescriptionMeta($body[Product::DESCRIPTION_META])
                ->setSellable($body[Product::SELLABLE])
                ->setSale($body[Product::SALE])
                ->setPutUrl(Product::generateUrl($phy->getPhyName()));
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->productGateway->existsByCode($put->getCode());
                $putId = $this->productGateway->insert($put)[Product::PUT_ID];
                $phy->setPutId($putId);
                $this->productHistoriesGateway->insert($phy);
                $psg = new ProductShipping();
                $psg->setPutId($putId);
                foreach ($body[self::SHIPPINGS] as $shipping)
                {
                    $psg->setSpgId($shipping[ProductShipping::SPG_ID])
                        ->setShowShipping($shipping[ProductShipping::SHOW_SHIPPING])
                        ->setFixedPrice($shipping[ProductShipping::FIXED_PRICE]);
                    $this->productShippingsGateway->insert($psg);
                }
                $pcy = new ProductCategory();
                $pcy->setPutId($putId);
                foreach ($body[self::CATEGORIES] as $category)
                {
                    $pcy->setCeyId($category[ProductCategory::CEY_ID])
                        ->setPceMain($category[ProductCategory::PCE_MAIN]);
                    $this->productCategoriesGateway->insert($pcy);
                    $parent = $this->categoryGateway->selectParent($pcy->getCeyId())[Category::CEY_CEY_ID];
                    while ($parent !== null)
                    {
                        $pcy->setCeyId($parent)
                            ->setPceMain(false);
                        if (!$this->isParent($body[self::CATEGORIES], $pcy))
                        {
                            $this->productCategoriesGateway->insert($pcy);
                        }
                        $parent = $this->categoryGateway->selectParent($parent)[Category::CEY_CEY_ID];
                    }
                }
                $pfe = new ProductFile();
                $pfe->setPutId($putId);
                foreach ($body[self::FILES] as $fieId)
                {
                    $pfe->setFieId($fieId);
                    $this->productFilesGateway->insert($pfe);
                }
                $pie = new ProductImage();
                $pie->setPutId($putId);
                foreach ($body[self::IMAGES] as $image)
                {
                    $pie->setIaeId($image[ProductImage::IAE_ID])
                        ->setIaeMain($image[ProductImage::IAE_MAIN]);
                    $this->productImagesGateway->insert($pie);
                }
                $pgt = new ProductGroupDiscount();
                $pgt->setPutId($putId);
                foreach ($body[self::GROUP_DISCOUNTS] as $item)
                {
                    $pgt->setGrpId($item[ProductGroupDiscount::GRP_ID])
                        ->setDiscount($item[ProductGroupDiscount::DISCOUNT]);
                    $this->productGroupDiscountsGateway->insert($pgt);
                }
                $ppt = new ProductPayment();
                $ppt->setPutId($putId);
                foreach ($body[self::PAYMENTS] as $item)
                {
                    $ppt->setPmtId($item);
                    $this->productPaymentsGateway->insert($ppt);
                }
                $pmn = new ProductMenuOption();
                $pmn->setPutId($putId);
                foreach ($body[self::MENU_OPTIONS] as $item)
                {
                    $pmn->setPmnName($item[ProductMenuOption::PMN_NAME]);
                    $this->productMenuOptionsGateway->insert($pmn);
                }
                $ppe = new ProductParameterValue();
                $ppe->setPutId($putId);
                $pve = new ParameterValues();
                foreach ($body[self::PARAMETER_VALUES] as $item)
                {
                    $pve->setPrrId($item[ParameterValues::PRR_ID])
                        ->setPveValue($item[ParameterValues::PVE_VALUE]);
                    try
                    {
                        $pveId = $this->parameterValuesGateway->findWhere($pve)[ProductParameterValue::PVE_ID];
                    }
                    catch (EntityNotFoundException $e)
                    {
                        $pveId = $this->parameterValuesGateway->insert($pve)[ProductParameterValue::PVE_ID];
                    }
                    $ppe->setPveId($pveId);
                    $this->productParameterValuesGateway->insert($ppe);
                }
                $pmd = new ProductMetaKeyword();
                $pmd->setPutId($putId);
                foreach ($body[self::META_KEYWORDS] as $item)
                {
                    $pmd->setMkdId($item[MetaKeyword::ID]);
                    $this->productMetaKeywordsGateway->insert($pmd);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " product. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " product");
            }

            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putProductResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $putId = $request->getAttribute(self::PUT_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $phy = new ProductHistory();
            $phy->setPutId($putId)
                ->setPriceVat($body[ProductHistory::PRICE_VAT])
                ->setPriceNoVat($body[ProductHistory::PRICE_NO_VAT])
                ->setActId($this->accountGateway->findByEmail($this->token->getSub())[Administrator::ACT_ID])
                ->setOptionTitle($body[ProductHistory::OPTION_TITLE])
                ->setPhyName($body[ProductHistory::PHY_NAME])
                ->setWeight($body[ProductHistory::WEIGHT]);
            $put = new Product();
            $put->setPutId($putId)
                ->setMurId($body[Product::MUR_ID])
                ->setPgpId($body[Product::PGP_ID])
                ->setCode($body[Product::CODE])
                ->setAvailability($body[Product::AVAILABILITY])
                ->setActive($body[Product::ACTIVE])
                ->setBarcode($body[Product::BARCODE])
                ->setDescriptionShort($body[Product::DESCRIPTION_SHORT])
                ->setDescriptionLong($body[Product::DESCRIPTION_LONG])
                ->setWarranty($body[Product::WARRANTY])
                ->setDescriptionMeta($body[Product::DESCRIPTION_META])
                ->setSellable($body[Product::SELLABLE])
                ->setSale($body[Product::SALE])
                ->setSyncFeedStatus($body[Product::SYNC_FEED_STATUS])
                ->setPutUrl(Product::generateUrl($phy->getPhyName()));
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->productGateway->existsByPutId($putId);
                $this->productGateway->existsByCode($put->getCode(), $putId);
                $psg = new ProductShipping();
                $psg->setPutId($putId);
                $this->productShippingsGateway->delete([$putId]);
                foreach ($body[self::SHIPPINGS] as $shipping)
                {
                    $psg->setSpgId($shipping[ProductShipping::SPG_ID])
                        ->setShowShipping($shipping[ProductShipping::SHOW_SHIPPING])
                        ->setFixedPrice($shipping[ProductShipping::FIXED_PRICE]);
                    $this->productShippingsGateway->insert($psg);
                }
                $pcy = new ProductCategory();
                $pcy->setPutId($putId);
                $this->productCategoriesGateway->deleteByPutId($putId);
                foreach ($body[self::CATEGORIES] as $category)
                {
                    $pcy->setCeyId($category[ProductCategory::CEY_ID])
                        ->setPceMain($category[ProductCategory::PCE_MAIN]);
                    $this->productCategoriesGateway->insert($pcy);
                    $parent = $this->categoryGateway->selectParent($pcy->getCeyId())[Category::CEY_CEY_ID];
                    while ($parent !== null)
                    {
                        $pcy->setCeyId($parent)
                            ->setPceMain(false);
                        if (!$this->isParent($body[self::CATEGORIES], $pcy))
                        {
                            $this->productCategoriesGateway->insert($pcy);
                        }
                        $parent = $this->categoryGateway->selectParent($parent)[Category::CEY_CEY_ID];
                    }
                }
                $pfe = new ProductFile();
                $pfe->setPutId($putId);
                $this->productFilesGateway->delete($putId);
                foreach ($body[self::FILES] as $fieId)
                {
                    $pfe->setFieId($fieId);
                    $this->productFilesGateway->insert($pfe);
                }
                $pie = new ProductImage();
                $pie->setPutId($putId);
                $this->productImagesGateway->delete($putId);
                foreach ($body[self::IMAGES] as $image)
                {
                    $pie->setIaeId($image[ProductImage::IAE_ID])
                        ->setIaeMain($image[ProductImage::IAE_MAIN]);
                    $this->productImagesGateway->insert($pie);
                }
                $pgt = new ProductGroupDiscount();
                $pgt->setPutId($putId);
                $this->productGroupDiscountsGateway->delete($putId);
                foreach ($body[self::GROUP_DISCOUNTS] as $item)
                {
                    $pgt->setGrpId($item[ProductGroupDiscount::GRP_ID])
                        ->setDiscount($item[ProductGroupDiscount::DISCOUNT]);
                    $this->productGroupDiscountsGateway->insert($pgt);
                }
                $ppt = new ProductPayment();
                $ppt->setPutId($putId);
                $this->productPaymentsGateway->delete($putId);
                foreach ($body[self::PAYMENTS] as $item)
                {
                    $ppt->setPmtId($item);
                    $this->productPaymentsGateway->insert($ppt);
                }
                $pmn = new ProductMenuOption();
                $pmn->setPutId($putId);
                $this->productMenuOptionsGateway->delete($putId);
                foreach ($body[self::MENU_OPTIONS] as $item)
                {
                    $pmn->setPmnName($item[ProductMenuOption::PMN_NAME]);
                    $this->productMenuOptionsGateway->insert($pmn);
                }
                $ppe = new ProductParameterValue();
                $ppe->setPutId($putId);
                $this->productParameterValuesGateway->delete($putId);
                $pve = new ParameterValues();
                foreach ($body[self::PARAMETER_VALUES] as $item)
                {
                    $pve->setPrrId($item[ParameterValues::PRR_ID])
                        ->setPveValue($item[ParameterValues::PVE_VALUE]);
                    try
                    {
                        $pveId = $this->parameterValuesGateway->findWhere($pve)[ProductParameterValue::PVE_ID];
                    }
                    catch (EntityNotFoundException $e)
                    {
                        $pveId = $this->parameterValuesGateway->insert($pve)[ProductParameterValue::PVE_ID];
                    }
                    $ppe->setPveId($pveId);
                    $this->productParameterValuesGateway->insert($ppe);
                }
                $pmd = new ProductMetaKeyword();
                $pmd->setPutId($putId);
                $this->productMetaKeywordsGateway->delete($putId);
                foreach ($body[self::META_KEYWORDS] as $item)
                {
                    $pmd->setMkdId($item[MetaKeyword::ID]);
                    $this->productMetaKeywordsGateway->insert($pmd);
                }
                $this->productHistoriesGateway->delete([$phy->getPutId()]);
                $this->productHistoriesGateway->insert($phy);
                $this->productGateway->update($put);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " product. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " product");
            }

            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param array $array
     * @return bool
     */
    private function isHistorySelected(array $array): bool
    {
        if (is_array($array[ProductHistory::PHY_NAME]) ||
            strcmp($array[ProductHistory::PRICE_VAT], self::NOT_SELECTED) != 0 ||
            strcmp($array[ProductHistory::PRICE_NO_VAT], self::NOT_SELECTED) != 0 ||
            strcmp($array[ProductHistory::WEIGHT], self::NOT_SELECTED) != 0 ||
            strcmp($array[ProductHistory::OPTION_TITLE], self::NOT_SELECTED) != 0)
        {
            return true;
        }
        return false;
    }

    /**
     * @param array $array
     * @return bool
     */
    private function isProductSelected(array $array): bool
    {
        if (strcmp($array[Product::MUR_ID], self::NOT_SELECTED) != 0 ||
            is_array($array[Product::CODE]) ||
            strcmp($array[Product::AVAILABILITY], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::ACTIVE], self::NOT_SELECTED) != 0 ||
            is_array($array[Product::BARCODE]) ||
            strcmp($array[Product::BARCODE], self::NOT_SELECTED) != 0 ||
            is_array($array[Product::DESCRIPTION_SHORT]) ||
            strcmp($array[Product::DESCRIPTION_SHORT], self::NOT_SELECTED) != 0 ||
            is_array($array[Product::DESCRIPTION_LONG]) ||
            strcmp($array[Product::DESCRIPTION_LONG], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::WARRANTY], self::NOT_SELECTED) != 0 ||
            is_array($array[Product::DESCRIPTION_META]) ||
            strcmp($array[Product::DESCRIPTION_META], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::SELLABLE], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::SYNC_FEED_STATUS], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::SALE], self::NOT_SELECTED) != 0 ||
            strcmp($array[Product::PGP_ID], self::NOT_SELECTED) != 0)
        {
            return true;
        }
        return false;
    }

    /**
     * @param $new
     * @param $original
     * @param int $index
     * @return mixed
     */
    private function getDynamicValue($new, $original, int $index)
    {
        if (is_array($new))
        {
            return $new[$index];
        }
        return $original;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMultipleProductResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                for ($i = 0; $i < count($body[self::ITEMS]); $i++)
                {
                    $putId = $body[self::ITEMS][$i]["id"];
                    $phyArray = $this->productHistoriesGateway->findWhere($putId);
                    $phy = new ProductHistory();
                    $phy->setPutId($putId)
                        ->setPriceVat(strcmp($body[ProductHistory::PRICE_VAT], self::NOT_SELECTED) == 0 ? $phyArray[ProductHistory::PRICE_VAT] : $body[ProductHistory::PRICE_VAT])
                        ->setPriceNoVat(strcmp($body[ProductHistory::PRICE_NO_VAT], self::NOT_SELECTED) == 0 ? $phyArray[ProductHistory::PRICE_NO_VAT] : $body[ProductHistory::PRICE_NO_VAT])
                        ->setActId($this->accountGateway->findByEmail($this->token->getSub())[Administrator::ACT_ID])
                        ->setOptionTitle(strcmp($body[ProductHistory::OPTION_TITLE], self::NOT_SELECTED) == 0 ? $phyArray[ProductHistory::OPTION_TITLE] : $body[ProductHistory::OPTION_TITLE])
                        ->setPhyName($this->getDynamicValue($body[ProductHistory::PHY_NAME], $phyArray[ProductHistory::PHY_NAME], $i))
                        ->setWeight(strcmp($body[ProductHistory::WEIGHT], self::NOT_SELECTED) == 0 ? $phyArray[ProductHistory::WEIGHT] : $body[ProductHistory::WEIGHT]);
                    $putArray = $this->productGateway->selectByPutId($putId);
                    $put = new Product();
                    $put->setPutId($putId)
                        ->setMurId(strcmp($body[Product::MUR_ID], self::NOT_SELECTED) == 0 ? $putArray[Product::MUR_ID] : $body[Product::MUR_ID])
                        ->setPgpId(strcmp($body[Product::PGP_ID], self::NOT_SELECTED) == 0 ? $putArray[Product::PGP_ID] : $body[Product::PGP_ID])
                        ->setCode($this->getDynamicValue($body[Product::CODE], $putArray[Product::CODE], $i))
                        ->setAvailability(strcmp($body[Product::AVAILABILITY], self::NOT_SELECTED) == 0 ? $putArray[Product::AVAILABILITY] : $body[Product::AVAILABILITY])
                        ->setActive(strcmp($body[Product::ACTIVE], self::NOT_SELECTED) == 0 ? $putArray[Product::ACTIVE] : $body[Product::ACTIVE])
                        ->setBarcode($body[Product::BARCODE] === null ? null : $this->getDynamicValue($body[Product::BARCODE], $putArray[Product::BARCODE], $i))
                        ->setDescriptionShort($body[Product::DESCRIPTION_SHORT] === null ? null : $this->getDynamicValue($body[Product::DESCRIPTION_SHORT], $putArray[Product::DESCRIPTION_SHORT], $i))
                        ->setDescriptionLong($body[Product::DESCRIPTION_LONG] === null ? null : $this->getDynamicValue($body[Product::DESCRIPTION_LONG], $putArray[Product::DESCRIPTION_LONG], $i))
                        ->setWarranty(strcmp($body[Product::WARRANTY], self::NOT_SELECTED) == 0 ? $putArray[Product::WARRANTY] : $body[Product::WARRANTY])
                        ->setDescriptionMeta($body[Product::DESCRIPTION_META] === null ? null : $this->getDynamicValue($body[Product::DESCRIPTION_META], $putArray[Product::DESCRIPTION_META], $i))
                        ->setSellable(strcmp($body[Product::SELLABLE], self::NOT_SELECTED) == 0 ? $putArray[Product::SELLABLE] : $body[Product::SELLABLE])
                        ->setSyncFeedStatus(strcmp($body[Product::SYNC_FEED_STATUS], self::NOT_SELECTED) == 0
                            ? $putArray[Product::SYNC_FEED_STATUS]
                            : $body[Product::SYNC_FEED_STATUS])
                        ->setSale(strcmp($body[Product::SALE], self::NOT_SELECTED) == 0 ? $putArray[Product::SALE] : $body[Product::SALE])
                        ->setPutUrl(Product::generateUrl($phy->getPhyName()));

                    $this->productGateway->existsByPutId($putId);
                    $this->productGateway->existsByCode($put->getCode(), $putId);

                    if (is_array($body[self::SHIPPINGS]))
                    {
                        $psg = new ProductShipping();
                        $psg->setPutId($putId);
                        foreach ($body[self::SHIPPINGS] as $shipping)
                        {
                            $psg->setSpgId($shipping[ProductShipping::SPG_ID])
                                ->setShowShipping($shipping[ProductShipping::SHOW_SHIPPING])
                                ->setFixedPrice($shipping[ProductShipping::FIXED_PRICE]);
                            $this->productShippingsGateway->deleteBySpgId([$putId], $psg->getSpgId());
                            if (!$shipping[self::DELETE])
                            {
                                $this->productShippingsGateway->insert($psg);
                            }
                        }
                    }

                    if (is_array($body[self::CATEGORIES]))
                    {
                        $pcy = new ProductCategory();
                        $pcy->setPutId($putId);
                        $this->productCategoriesGateway->deleteByPutId($putId);
                        foreach ($body[self::CATEGORIES] as $category)
                        {
                            $pcy->setCeyId($category[ProductCategory::CEY_ID])
                                ->setPceMain($category[ProductCategory::PCE_MAIN]);
                            $this->productCategoriesGateway->insert($pcy);
                            $parent = $this->categoryGateway->selectParent($pcy->getCeyId())[Category::CEY_CEY_ID];
                            while ($parent !== NULL)
                            {
                                $pcy->setCeyId($parent)
                                    ->setPceMain(FALSE);
                                if (!$this->isParent($body[self::CATEGORIES], $pcy))
                                {
                                    $this->productCategoriesGateway->insert($pcy);
                                }
                                $parent = $this->categoryGateway->selectParent($parent)[Category::CEY_CEY_ID];
                            }
                        }
                    }

                    if (is_array($body[self::FILES]))
                    {
                        $pfe = new ProductFile();
                        $pfe->setPutId($putId);
                        $this->productFilesGateway->delete($putId);
                        foreach ($body[self::FILES] as $fieId)
                        {
                            $pfe->setFieId($fieId);
                            $this->productFilesGateway->insert($pfe);
                        }
                    }

                    if (is_array($body[self::IMAGES]))
                    {
                        $pie = new ProductImage();
                        $pie->setPutId($putId);
                        $this->productImagesGateway->delete($putId);
                        foreach ($body[self::IMAGES] as $image)
                        {
                            $pie->setIaeId($image[ProductImage::IAE_ID])
                                ->setIaeMain($image[ProductImage::IAE_MAIN]);
                            $this->productImagesGateway->insert($pie);
                        }
                    }

                    if (is_array($body[self::GROUP_DISCOUNTS]))
                    {
                        $pgt = new ProductGroupDiscount();
                        $pgt->setPutId($putId);
                        foreach ($body[self::GROUP_DISCOUNTS] as $item)
                        {
                            $pgt->setGrpId($item[ProductGroupDiscount::GRP_ID]);
                            $this->productGroupDiscountsGateway->delete([$putId], $pgt->getGrpId());
                            if (!$item[self::DELETE])
                            {
                                $pgt->setDiscount($item[ProductGroupDiscount::DISCOUNT]);
                                $this->productGroupDiscountsGateway->insert($pgt);
                            }
                        }
                    }

                    if (is_array($body[self::PAYMENTS]))
                    {
                        $ppt = new ProductPayment();
                        $ppt->setPutId($putId);
                        foreach ($body[self::PAYMENTS] as $item)
                        {
                            $ppt->setPmtId($item[Payment::ID]);
                            $this->productPaymentsGateway->deleteByPmtId($putId, $ppt->getPmtId());
                            if (!$item[self::ENABLE])
                            {
                                $this->productPaymentsGateway->insert($ppt);
                            }
                        }
                    }

                    if (is_array($body[self::MENU_OPTIONS]))
                    {
                        $pmn = new ProductMenuOption();
                        $pmn->setPutId($putId);
                        $this->productMenuOptionsGateway->delete($putId);
                        foreach ($body[self::MENU_OPTIONS] as $item)
                        {
                            $pmn->setPmnName($item[ProductMenuOption::PMN_NAME]);
                            $this->productMenuOptionsGateway->insert($pmn);
                        }
                    }

                    if (is_array($body[self::PARAMETER_VALUES]))
                    {
                        $ppe = new ProductParameterValue();
                        $ppe->setPutId($putId);
                        $pve = new ParameterValues();
                        $this->productParameterValuesGateway->delete($putId);
                        foreach ($body[self::PARAMETER_VALUES][$i] as $item)
                        {
                            $pve->setPrrId($item[ParameterValues::PRR_ID])
                                ->setPveValue($item[ParameterValues::PVE_VALUE]);
                            try
                            {
                                $pveId = $this->parameterValuesGateway->findWhere($pve)[ProductParameterValue::PVE_ID];
                            }
                            catch (EntityNotFoundException $e)
                            {
                                $pveId = $this->parameterValuesGateway->insert($pve)[ProductParameterValue::PVE_ID];
                            }
                            $ppe->setPveId($pveId);
                            $this->productParameterValuesGateway->insert($ppe);
                        }
                    }

                    if (is_array($body[self::META_KEYWORDS]))
                    {
                        $pmd = new ProductMetaKeyword();
                        $pmd->setPutId($putId);
                        $this->productMetaKeywordsGateway->delete($putId);
                        foreach ($body[self::META_KEYWORDS] as $item)
                        {
                            $pmd->setMkdId($item[MetaKeyword::ID]);
                            $this->productMetaKeywordsGateway->insert($pmd);
                        }
                    }

                    if ($this->isHistorySelected($body))
                    {
                        $this->productHistoriesGateway->delete([$putId]);
                        $this->productHistoriesGateway->insert($phy);
                    }
                    if ($this->isProductSelected($body) || $this->isHistorySelected($body)) $this->productGateway->update($put);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " products. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " products");
            }
            return $response->withStatus(StatusObject::OK);
            //return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}