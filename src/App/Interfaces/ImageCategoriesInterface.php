<?php
/**
 * Created by Daniel Bill
 * Date: 17.07.2018
 * Time: 19:37
 */
namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\ImageCategory;
use Kominexpres\src\App\BO\ImageCategoryMarking;
use Kominexpres\src\App\Exceptions\ImageCategoryStillInUseException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\ImageCategoriesGateway;
use Kominexpres\src\App\TableDataGateway\ImageCategoryMarkingsGateway;
use Kominexpres\src\App\TableDataGateway\ImageGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ImageCategoriesInterface extends AbstractInterface
{
    private const ICY_ID = "icyId";

    /** @var ImageCategoriesGateway */
    private $imageCategoriesGateway;
    /** @var ImageGateway */
    private $imageGateway;
    /** @var ImageCategoryMarkingsGateway */
    private $imageCategoryMarkingsGateway;

    /**
     * ImageCategoriesInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->imageCategoriesGateway = new ImageCategoriesGateway($this->dbPgsql, $this->dbMysql);
        $this->imageGateway = new ImageGateway($this->dbPgsql, $this->dbMysql);
        $this->imageCategoryMarkingsGateway = new ImageCategoryMarkingsGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getImageCategoriesListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $imageList = $this->imageCategoriesGateway->selectAll();
            foreach ($imageList as $key => $image)
            {
                $imageList[$key]["mig"] = $this->imageCategoriesGateway->selectMarkingsByCategory($image[ImageCategory::ICY_ID]);
            }
            $so = StatusObject::create($imageList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function getImageCategoryResponse(Request $request, Response $response)
    {
        $icyId = $request->getAttribute(self::ICY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->imageCategoriesGateway->existsByIcyId($icyId);
            $icy = $this->imageCategoriesGateway->findWhere($icyId);
            $icy["mig_markings"] = [];
            foreach ($this->imageCategoryMarkingsGateway->selectMarkingsByIcyId($icyId) as $migMarking)
            {
                array_push($icy["mig_markings"], $migMarking["mig_marking"]);
            }
            $so = StatusObject::create($icy);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function getImageMarkingsByCategoryListResponse(Request $request, Response $response)
    {
        $icyId = $request->getAttribute(self::ICY_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $imageList = $this->imageCategoriesGateway->selectMarkingsByCategory($icyId);
            $so = StatusObject::create($imageList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function deleteImageCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $icyId)
                {
                    $this->imageGateway->existsByIcyId($icyId);
                    $this->imageCategoryMarkingsGateway->delete($icyId);
                    $this->imageCategoriesGateway->delete($icyId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " image category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " image category");
            }
            catch (ImageCategoryStillInUseException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error("{$e->getMessage()} Trace: {$e->getTraceAsString()}");
                throw $e;
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function postImageCategoryResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $icy = new ImageCategory();
            $icy->setIcyName($body[ImageCategory::ICY_NAME]);
            $this->imageCategoriesGateway->existsByIcyName($icy->getIcyName());
            try
            {
                $this->dbPgsql->beginTransaction();
                $icyId = $this->imageCategoriesGateway->insert((Object)$icy)[ImageCategory::ICY_ID];
                $icy->setIcyId($icyId);
                foreach ($body["mig_markings"] as $migId)
                {
                    $icg = new ImageCategoryMarking();
                    $icg->setIcyId($icy->getIcyId())
                        ->setMigId($migId);
                    $this->imageCategoryMarkingsGateway->insert((Object)$icg);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " image category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " image category");
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function putImageCategoryResponse(Request $request, Response $response)
    {
        $icyId = $request->getAttribute(self::ICY_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $icy = new ImageCategory();
            $icy->setIcyName($body[ImageCategory::ICY_NAME])
                ->setIcyId($icyId);
            $this->imageCategoriesGateway->existsByIcyId($icyId);
            $icyName = $this->imageCategoriesGateway->findWhere($icyId)[ImageCategory::ICY_NAME];
            if (strcmp($icyName, $icy->getIcyName()) != 0) {
                $this->imageCategoriesGateway->existsByIcyName($icy->getIcyName());
            }
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->imageCategoriesGateway->update((Object)$icy);
                $this->imageCategoryMarkingsGateway->delete($icy->getIcyId());
                foreach ($body["mig_markings"] as $migId)
                {
                    $icg = new ImageCategoryMarking();
                    $icg->setIcyId($icy->getIcyId())
                        ->setMigId($migId);
                    $this->imageCategoryMarkingsGateway->insert((Object)$icg);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " image category. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " image category");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}