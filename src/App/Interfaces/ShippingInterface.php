<?php
/**
 * Created by Daniel Bill
 * Date: 05.01.2018
 * Time: 20:58
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Shipping;
use Kominexpres\src\App\BO\ShippingHistory;
use Kominexpres\src\App\BO\ShippingHistoryWeight;
use Kominexpres\src\App\BO\ShippingPayment;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\DeliveryTypeHeurekaGateway;
use Kominexpres\src\App\TableDataGateway\ProductShippingsGateway;
use Kominexpres\src\App\TableDataGateway\ShippingGateway;
use Kominexpres\src\App\TableDataGateway\ShippingHistoryGateway;
use Kominexpres\src\App\TableDataGateway\ShippingHistoryWeightsGateway;
use Kominexpres\src\App\TableDataGateway\ShippingPaymentGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ShippingInterface
 * @package Kominexpres\src\App\Interfaces
 */
class ShippingInterface extends AbstractInterface
{
    private const SHT_ID = "shtId";
    private const SHY_ID = "shyId";
    private const SPG_ID = "spgId";

    /** @var ShippingHistoryWeightsGateway */
    private $shippingHistoryWeightsGateway;
    /** @var ShippingGateway */
    private $shippingGateway;
    /** @var ShippingHistoryGateway */
    private $shippingHistoryGateway;
    /** @var ShippingPaymentGateway */
    private $shippingPaymentGateway;
    /** @var DeliveryTypeHeurekaGateway */
    private $deliveryTypeHeurekaGateway;
    /** @var ProductShippingsGateway  */
    private $productShippingsGateway;
    /**
     * ShippingInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->shippingHistoryWeightsGateway = new ShippingHistoryWeightsGateway($this->dbPgsql, $this->dbMysql);
        $this->shippingGateway = new ShippingGateway($this->dbPgsql, $this->dbMysql);
        $this->shippingHistoryGateway = new ShippingHistoryGateway($this->dbPgsql, $this->dbMysql);
        $this->shippingPaymentGateway = new ShippingPaymentGateway($this->dbPgsql, $this->dbMysql);
        $this->deliveryTypeHeurekaGateway = new DeliveryTypeHeurekaGateway($this->dbPgsql, $this->dbMysql);
        $this->productShippingsGateway = new ProductShippingsGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getShippingListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $shippingList = $this->shippingHistoryWeightsGateway->selectAll();
            $so = StatusObject::create($shippingList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getShippingSingleResponse(Request $request, Response $response)
    {
        $spgId = $request->getAttribute(self::SPG_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->shippingHistoryGateway->existsBySpgId($spgId);
            $shipping = $this->shippingHistoryGateway->findWhere($spgId);
            $shippingPayments = $this->shippingPaymentGateway->findWhere($shipping[ShippingHistory::SPG_ID]);
            $so = StatusObject::create(array_merge($shipping, ["payments" => $shippingPayments]));
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteShippingResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            foreach ($body[ShippingHistory::SPG_ID] as $spgId)
            {
                $this->shippingHistoryWeightsGateway->deleteBySpgId($spgId);
                $this->shippingHistoryGateway->delete($spgId);
                $this->productShippingsGateway->deleteOnlyBySpgId($spgId);
            }
            foreach ($body[ShippingHistoryWeight::SHT_ID] as $shtId)
            {
                $this->shippingHistoryWeightsGateway->delete($shtId);
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postShippingWeightResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $shyId = $request->getAttribute(self::SHY_ID);

        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $sht = new ShippingHistoryWeight();
            $sht->setActive(TRUE)
                ->setWeightMin($body[ShippingHistoryWeight::WEIGHT_MIN])
                ->setWeightMax($body[ShippingHistoryWeight::WEIGHT_MAX])
                ->setShyId($shyId)
                ->setPrice($body[ShippingHistoryWeight::PRICE]);
            $this->shippingHistoryGateway->existsByShyId($shyId);
            $this->shippingHistoryWeightsGateway->existsByShyIdAndWeight($shyId, $sht->getWeightMin(), $sht->getWeightMax());
            $this->shippingHistoryWeightsGateway->insert($sht);

            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postShippingResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->shippingHistoryGateway->existsByShyName($body[ShippingHistory::SHY_NAME]);
            $spg = new Shipping();
            $spg->setDtaId($body[Shipping::DTA_ID]);
            $spg->setShowOnDefault($body[Shipping::SHOW_ON_DEFAULT]);
            $spg->setShowTillMaxWeight($body[Shipping::SHOW_TILL_MAX_WEIGHT]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $spgId = $this->shippingGateway->insert($spg)[Shipping::ID];
                $shy = new ShippingHistory();
                $shy->setSpgId($spgId)
                    ->setFreePriceAbove($body[ShippingHistory::FREE_PRICE_ABOVE])
                    ->setSumWeightPrice($body[ShippingHistory::SUM_WEIGHT_PRICE])
                    ->setShyName($body[ShippingHistory::SHY_NAME]);
                $shyId = $this->shippingHistoryGateway->insert($shy)[ShippingHistory::SHY_ID];
                $shy->setShyId($shyId);
                $sht = new ShippingHistoryWeight();
                $sht->setShyId($shyId);
                foreach ($body["items"] as $item)
                {
                    $this->shippingHistoryWeightsGateway->existsByShyIdAndWeight($shyId, $item[ShippingHistoryWeight::WEIGHT_MIN], $item[ShippingHistoryWeight::WEIGHT_MAX]);
                    $sht->setPrice($item[ShippingHistoryWeight::PRICE])
                        ->setWeightMax($item[ShippingHistoryWeight::WEIGHT_MAX])
                        ->setWeightMin($item[ShippingHistoryWeight::WEIGHT_MIN])
                        ->setShyId($shyId)
                        ->setActive(TRUE);
                    $this->shippingHistoryWeightsGateway->insert($sht);
                }
                foreach ($body["payments"] as $payment)
                {
                    $spt = new ShippingPayment();
                    $spt->setPmtId($payment[ShippingPayment::PMT_ID])
                        ->setSpgId($spgId);
                    $this->shippingPaymentGateway->insert($spt);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " shipping. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " shipping");
            }

            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putShippingSpgResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $spgId = $request->getAttribute(self::SPG_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->shippingHistoryGateway->existsBySpgId($spgId);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->shippingGateway->update($body, $spgId);
                foreach ($body["payments"] as $pmtId)
                {
                    $spt = new ShippingPayment();
                    $spt->setSpgId($spgId)
                        ->setPmtId($pmtId[ShippingPayment::PMT_ID]);
                    $this->shippingPaymentGateway->insert($spt);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " shipping. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " shipping");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putShippingShtResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $shtId = $request->getAttribute(self::SHT_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->shippingHistoryWeightsGateway->existsByShtId($shtId);
            $sht = new ShippingHistoryWeight();
            $sht->setWeightMax($body[ShippingHistoryWeight::WEIGHT_MAX])
                ->setWeightMin($body[ShippingHistoryWeight::WEIGHT_MIN])
                ->setPrice($body[ShippingHistoryWeight::PRICE])
                ->setShyId($body[ShippingHistoryWeight::SHY_ID])
                ->setActive(TRUE)
                ->setShtId($shtId);
            $this->shippingHistoryWeightsGateway->existsByShyIdAndWeightNotShtId($sht->getShyId(), $sht->getWeightMin(), $sht->getWeightMax(), $shtId);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->shippingHistoryWeightsGateway->update($sht);
                $this->shippingHistoryWeightsGateway->insert($sht);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " shipping. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " shipping");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}