<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:39
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Category;
use Kominexpres\src\App\BO\CategoryGroupDiscount;
use Kominexpres\src\App\BO\Group;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\AccountGroupHistoryGateway;
use Kominexpres\src\App\TableDataGateway\CategoryGroupDiscountsGateway;
use Kominexpres\src\App\TableDataGateway\GroupsGateway;
use Kominexpres\src\App\TableDataGateway\ProductGroupDiscountsGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class GroupsInterface
 * @package Kominexpres\src\App\Interfaces
 */
class GroupsInterface extends AbstractInterface
{
    private const GRP_ID = "grpId";

    /** @var GroupsGateway */
    private $groupsGateway;
    /** @var AccountGroupHistoryGateway */
    private $accountGroupHistoryGateway;
    /** @var CategoryGroupDiscountsGateway */
    private $categoryGroupDiscountsGateway;
    /** @var ProductGroupDiscountsGateway */
    private $productGroupDiscountsGateway;

    /**
     * GroupsInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->groupsGateway = new GroupsGateway($this->dbPgsql, $this->dbMysql);
        $this->accountGroupHistoryGateway = new AccountGroupHistoryGateway($this->dbPgsql, $this->dbMysql);
        $this->categoryGroupDiscountsGateway = new CategoryGroupDiscountsGateway($this->dbPgsql, $this->dbMysql);
        $this->productGroupDiscountsGateway = new ProductGroupDiscountsGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getGroupListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->groupsGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getGroupResponse(Request $request, Response $response)
    {
        $grpId = $request->getAttribute(self::GRP_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $group = $this->groupsGateway->findWhere($grpId);
            $list = $this->categoryGroupDiscountsGateway->selectByGrpId($grpId);
            $group["categories"] = Category::generateList($list);
            $so = StatusObject::create($group);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteGroupResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->delete($body);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " group");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param $items
     * @throws \Kominexpres\src\App\Exceptions\DefaultGroupException
     */
    private function delete($items)
    {
        foreach ($items as $grpId)
        {
            $this->groupsGateway->existsDefault($grpId);
            $this->accountGroupHistoryGateway->deleteByGrpId($grpId);
            $this->categoryGroupDiscountsGateway->deleteByGrpId($grpId);
            $this->productGroupDiscountsGateway->deleteByGrpId($grpId);
            $this->groupsGateway->delete($grpId);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putGroupResponse(Request $request, Response $response)
    {
        $grpId = $request->getAttribute(self::GRP_ID);
        $body = $request->getParsedBody();
        $data = [];
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $grp = new Group();
            $grp->setGrpName($body[Group::GRP_NAME])
                ->setGrpDefault($body[Group::GRP_DEFAULT])
                ->setGrpId($grpId);
            try
            {
                $this->dbPgsql->beginTransaction();
                if ($grp->isGrpDefault())
                    $this->groupsGateway->updateDefault();
                $exists = $this->groupsGateway->existsByGrpName($grp->getGrpName(), $grp->getGrpId());
                if (!empty($exists))
                {
                    if ($exists[Group::GRP_ACTIVE] == 1)
                    {
                        throw new EntityAlreadyExistsException("Group", $grp->getGrpName());
                    }
                    else
                    {
                        $this->delete([$grp->getGrpId()]);
                        $grp->setGrpId($exists[Group::GRP_ID]);
                        $data = [
                            "grp_id" => $exists[Group::GRP_ID]
                        ];
                    }
                }

                $this->groupsGateway->update($grp);
                $this->categoryGroupDiscountsGateway->deleteByGrpId($grpId);
                $cgt = new CategoryGroupDiscount();
                $cgt->setGrpId($grpId);
                foreach ($body["categories"] as $item)
                {
                    $cgt->setCeyId($item[Category::CEY_ID])
                        ->setDiscount(doubleval($item[CategoryGroupDiscount::DISCOUNT]));
                    $this->categoryGroupDiscountsGateway->insert($cgt);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING . " group");
            }

            return $response->withJson($data, StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postGroupResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $grp = new Group();
            $grp->setGrpName($body[Group::GRP_NAME])
                ->setGrpDefault($body[Group::GRP_DEFAULT]);
            try
            {
                $this->dbPgsql->beginTransaction();
                if ($grp->isGrpDefault())
                    $this->groupsGateway->updateDefault();
                $exists = $this->groupsGateway->existsByGrpName($grp->getGrpName());
                if (!empty($exists))
                {
                    if ($exists[Group::GRP_ACTIVE] == 1)
                    {
                        throw new EntityAlreadyExistsException("Group", $grp->getGrpName());
                    }
                    else
                    {
                        $grp->setGrpId($exists[Group::GRP_ID]);
                        $this->groupsGateway->update($grp);
                    }
                }
                else
                {
                    $grpId = $this->groupsGateway->insert($grp)[Group::GRP_ID];
                    $grp->setGrpId($grpId);
                }

                $cgt = new CategoryGroupDiscount();
                $cgt->setGrpId($grp->getGrpId());
                foreach ($body["categories"] as $item)
                {
                    $cgt->setCeyId($item[Category::CEY_ID])
                        ->setDiscount(doubleval($item[CategoryGroupDiscount::DISCOUNT]));
                    $this->categoryGroupDiscountsGateway->insert($cgt);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " group. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " group");
            }

            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}