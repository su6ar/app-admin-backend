<?php
/**
 * Created by PhpStorm.
 * User: Danie
 * Date: 28.01.2017
 * Time: 17:36
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\Exceptions\AccountAlreadyExistsException;
use Kominexpres\src\App\Exceptions\AccountNotFoundException;
use Kominexpres\src\App\Exceptions\AccountVerificationException;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\Exceptions\UnauthorizedException;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\Logger\LOGGER;
use Kominexpres\src\App\POPO\MailMessenger\EshopMailMessenger;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\BO\Token;
use Kominexpres\src\App\POPO\TwigObject;
use Kominexpres\src\App\TableDataGateway\AccountGateway;
use Kominexpres\src\App\TableDataGateway\TokenGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
/**
 * Class AccountsInterface
 * @package Kominexpres\src\App\Interfaces
 */
class AccountsInterface extends AbstractInterface
{
    private const ACT_EMAIL = 'actemail';
    private const REFRESH_CODE = 'refreshcode';
    private const NEW_PASSWORD = 'newpass';
    private const RUR_PASSWORD = 'rurpassword';
    private const ACT_ID = 'actid';

    /** @var AccountGateway */
    private $accountGateway;
    /** @var TokenGateway */
    private $tokenGateway;

    /**
     * AccountsInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->accountGateway = new AccountGateway($this->dbPgsql, $this->dbMysql);
        $this->tokenGateway = new TokenGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getAccountsListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $accounts = $this->accountGateway->selectAll();
            $so = StatusObject::create($accounts);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function postAccountResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $administrator = new Administrator();
            $administrator->setAmrAvatar($body[Administrator::AMR_AVATAR])
                ->setActEmail($body[Administrator::ACT_EMAIL])
                ->setAmrFirstName($body[Administrator::AMR_FIRST_NAME])
                ->setAmrLastName($body[Administrator::AMR_LAST_NAME])
                ->setRurPassword($body[Administrator::RUR_PASSWORD])
                ->setAmrPrimaryColor(null)
                ->setAmrHeaderStyle(null)
                ->setHashedPassword();
            try
            {
                $this->accountGateway->existsByEmail($administrator->getActEmail());
                throw new AccountAlreadyExistsException($administrator->getActEmail());
            }
            catch (AccountNotFoundException $e)
            {
                $this->accountGateway->insert($administrator);
                return $response->withStatus(StatusObject::CREATED);
            }
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \phpmailerException
     */
    public function patchAccountForgotPasswordResponse(Request $request, Response $response)
    {
        $email = strtolower($request->getAttribute(self::ACT_EMAIL));
        try
        {
            $amr = Administrator::createFromJson($this->accountGateway->findByEmail($email));
            $amr->setGeneratedRurCodeRefresh();
            $this->accountGateway->updateCodeRefresh($amr);
            $body = TwigObject::renderTwig(TwigObject::TEMPLATE_PATH_USERS, TwigObject::FILENAME_REFRESH_PASS, array("amr" => $amr));
            $eshopMail = EshopMailMessenger::createMail($this->config);
            $eshopMail->sendMail($body, $amr->getActEmail(), "Administrace - obnova hesla");
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getAccountRefreshCodeExistsResponse(Request $request, Response $response)
    {
        $refreshCode = $request->getQueryParam(self::REFRESH_CODE);
        try
        {
            $this->accountGateway->existsByCodeRefresh($refreshCode);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function patchAccountPasswordResponse(Request $request, Response $response)
    {
        $newPassword = $request->getQueryParam(self::NEW_PASSWORD);
        $refreshCode = $request->getQueryParam(self::REFRESH_CODE);
        try
        {
            $amr = Administrator::createFromJson($this->accountGateway->findByCodeRefresh($refreshCode));
            $amr->setHashedPassword($newPassword);
            $this->accountGateway->updatePassword($amr);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     * @throws \Exception
     */
    public function authAccountResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            Administrator::validateUserCredentialsJson($body);
            try
            {
                $account = $this->accountGateway->findByEmail($body[Administrator::ACT_EMAIL]);
            }
            catch (AccountNotFoundException $exception)
            {
                throw new AccountVerificationException();
            }
            $amr = Administrator::createFromJson($account);
            $amr->verifyPassword($body[Administrator::RUR_PASSWORD]);
            if($amr->getAmrRights() == 0) throw new UnauthorizedException("Your account is not activated!");
            $token = Token::createToken($amr->getActEmail(), Administrator::getScope($amr->getAmrRights()));
            $this->logger->info("token: " . $token->getJwt() . ", " . $token->getSub() . ", " .(int)$token->isInvalid());
            $this->tokenGateway->insert($token);
            return $response->withJson(["token" => $token->getJwt()], StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function getAuthAccountResponse(Request $request, Response $response)
    {
        try
        {
            $account = $this->accountGateway->findByToken($this->token);
            return $response->withJson($account, StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function putAccountResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $actEmail = $this->tokenGateway->findWhere($this->token->getJwt())["sub"];
            $find = $this->accountGateway->findByEmail($actEmail);
            try
            {
                $exists = $this->accountGateway->findByEmail($body[Administrator::ACT_EMAIL]);
                if (!empty($exists) && $exists[Administrator::ACT_ID] != $find[Administrator::ACT_ID])
                {
                    throw new EntityAlreadyExistsException("E-mail", $body[Administrator::ACT_EMAIL]);
                }
            }
            catch (AccountNotFoundException $e)
            {

            }
            $account = new Administrator();
            $account->setActId($find[Administrator::ACT_ID])
                ->setActEmail($body[Administrator::ACT_EMAIL])
                ->setAmrDeletedAdminUser($body[Administrator::AMR_DELETED_ADMIN_USER])
                ->setAmrDeletedOrderShow($body[Administrator::AMR_DELETED_ORDER_SHOW])
                ->setAmrAvatar($body[Administrator::AMR_AVATAR])
                ->setAmrFirstName($body[Administrator::AMR_FIRST_NAME])
                ->setAmrLastName($body[Administrator::AMR_LAST_NAME])
                ->setAmrHeaderStyle($body[Administrator::AMR_HEADER_STYLE])
                ->setAmrPrimaryColor($body[Administrator::AMR_PRIMARY_COLOR])
                ->setRurPassword($find[Administrator::RUR_PASSWORD]);
            $account->verifyPassword($body[Administrator::RUR_PASSWORD]);
            if ($body["new_password"] !== null)
            {
                $account->setHashedPassword($body["new_password"]);
            }
            $this->tokenGateway->deleteForce($find[Administrator::ACT_EMAIL]);
            $this->accountGateway->update($account);
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}