<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:39
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Parameter;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\ParameterGroupParametersGateway;
use Kominexpres\src\App\TableDataGateway\ParametersGateway;
use Kominexpres\src\App\TableDataGateway\ParameterValuesGateway;
use Kominexpres\src\App\TableDataGateway\ProductParameterValuesGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ParametersInterface extends AbstractInterface
{
    private const PRR_ID = "prrId";

    /** @var ParametersGateway */
    private $parametersGateway;
    /** @var ParameterGroupParametersGateway */
    private $parameterGroupParametersGateway;
    /** @var ProductParameterValuesGateway */
    private $productParameterValuesGateway;
    /** @var ParameterValuesGateway  */
    private $parameterValuesGateway;

    /**
     * ParametersInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->parametersGateway = new ParametersGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterGroupParametersGateway = new ParameterGroupParametersGateway($this->dbPgsql, $this->dbMysql);
        $this->productParameterValuesGateway = new ProductParameterValuesGateway($this->dbPgsql, $this->dbMysql);
        $this->parameterValuesGateway = new ParameterValuesGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getParameterListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->parametersGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getParameterResponse(Request $request, Response $response)
    {
        $prrId = $request->getAttribute(self::PRR_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $parameter = $this->parametersGateway->findWhere($prrId);
            $so = StatusObject::create($parameter);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteParameterResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $prrId)
                {
                    $this->parameterGroupParametersGateway->deleteByPrrId($prrId);
                    $this->parameterValuesGateway->deleteByPrrId($prrId);
                    $this->parametersGateway->delete($prrId);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " parameters. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING . " parameters");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    public function putParameterResponse(Request $request, Response $response)
    {
        $prrId = $request->getAttribute(self::PRR_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $this->parametersGateway->findWhere($prrId);
            $prr = new Parameter();
            $prr->setPrrId($prrId)
                ->setPrrUnit($body[Parameter::PRR_UNIT])
                ->setPrrName($body[Parameter::PRR_NAME]);
            $this->parametersGateway->update($prr);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postParameterResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $prr = new Parameter();
            $prr->setPrrName($body[Parameter::PRR_NAME])
                ->setPrrUnit($body[Parameter::PRR_UNIT]);
            $this->parametersGateway->insert($prr);
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}