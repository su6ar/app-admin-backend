<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 0:50
 */

namespace Kominexpres\src\App\Interfaces;

use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\MenuItemMetaKeyword;
use Kominexpres\src\App\BO\MetaKeyword;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\MenuItemMetaKeywordGateway;
use Kominexpres\src\App\TableDataGateway\MetaKeywordsGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class MetaKeywordsInterface extends AbstractInterface
{
    private const ID = "id";

    /** @var MetaKeywordsGateway */
    private $metaKeywordsGateway;
    /** @var MenuItemMetaKeywordGateway */
    private $menuItemMetaKeywordGateway;

    /**
     * MetaKeywordsInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->metaKeywordsGateway = new MetaKeywordsGateway($this->dbPgsql, $this->dbMysql);
        $this->menuItemMetaKeywordGateway = new MenuItemMetaKeywordGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMetaKeywordListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->metaKeywordsGateway->selectAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMetaKeywordResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->metaKeywordsGateway->findWhere($id);
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postMetaKeywordResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $meta = new MetaKeyword();
            $meta->setMkdName($body[MetaKeyword::MKD_NAME]);
            $exists = $this->metaKeywordsGateway->existsByName($meta->getMkdName());
            if (!empty($exists))
            {
                throw new EntityAlreadyExistsException("Meta keyword", $meta->getMkdName());
            }
            $this->metaKeywordsGateway->insert($meta);
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMetaKeywordResponse(Request $request, Response $response)
    {
        $id = $request->getAttribute(self::ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $meta = new MetaKeyword();
            $meta->setMkdName($body[MetaKeyword::MKD_NAME])
                ->setId($id);
            $exists = $this->metaKeywordsGateway->existsByName($meta->getMkdName());
            if (!empty($exists) && $exists[MetaKeyword::ID] != $id)
            {
                throw new EntityAlreadyExistsException("Meta keyword", $meta->getMkdName());
            }
            $this->metaKeywordsGateway->update($meta);
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteMetaKeywordResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $menuMeta = new MenuItemMetaKeyword();
                foreach ($body as $id)
                {
                    $menuMeta->setMkdId($id);
                    $this->menuItemMetaKeywordGateway->delete($menuMeta);
                    $this->metaKeywordsGateway->delete($id);
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " meta keyword. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING. " meta keyword");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}