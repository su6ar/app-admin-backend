<?php
/**
 * Created by Daniel Bill
 * Date: 17.08.2018
 * Time: 15:10
 */

namespace Kominexpres\src\App\Interfaces;


use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\MenuItem;
use Kominexpres\src\App\BO\MenuItemMetaKeyword;
use Kominexpres\src\App\Exceptions\EntityAlreadyExistsException;
use Kominexpres\src\App\Exceptions\EntityNotFoundException;
use Kominexpres\src\App\Exceptions\PostgreSQLDatabaseException;
use Kominexpres\src\App\Exceptions\StatusCodeException;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\App\TableDataGateway\MenuFooterTitlesGateway;
use Kominexpres\src\App\TableDataGateway\MenuItemGateway;
use Kominexpres\src\App\TableDataGateway\MenuItemMetaKeywordGateway;
use Kominexpres\src\App\TableDataGateway\MetaKeywordsGateway;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class MenuItemsInterface extends AbstractInterface
{
    private const MIM_ID = "mimId";

    /** @var MenuItemGateway */
    private $menuItemGateway;
    /** @var MenuItemMetaKeywordGateway  */
    private $menuItemMetaKeywordGateway;
    /** @var MetaKeywordsGateway  */
    private $metaKeywordsGateway;
    /** @var MenuFooterTitlesGateway */
    private $menuFooterTitlesGateway;

    /**
     * MenuItemsInterface constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->menuItemGateway = new MenuItemGateway($this->dbPgsql, $this->dbMysql);
        $this->menuItemMetaKeywordGateway = new MenuItemMetaKeywordGateway($this->dbPgsql, $this->dbMysql);
        $this->metaKeywordsGateway = new MetaKeywordsGateway($this->dbPgsql, $this->dbMysql);
        $this->menuFooterTitlesGateway = new MenuFooterTitlesGateway($this->dbPgsql, $this->dbMysql);
    }

    /**
     * @param $items
     * @param $mimMimId
     * @throws EntityAlreadyExistsException
     * @throws \Kominexpres\src\App\Exceptions\EntityNotFoundException
     */
    public function updateSortItems($items, $mimMimId)
    {
        $placeNew = 0;
        $place = $this->menuItemGateway->selectAvailablePlace($mimMimId, -1);
        $placeMax = $place["man_place_max"];
        $placeMin = $place["man_place_min"];
        if ((count($items) - $placeMin) > 0)
        {
            $placeNew = $placeMax;
        }
        foreach ($items as $item)
        {
            $placeNew++;
            $menu = new MenuItem();
            $menu->setMimMimId($mimMimId)
                ->setMimId($item[MenuItem::MIM_ID])
                ->setManPlace($placeNew)
                ->setMimName($item[MenuItem::MIM_NAME]);

            if ($mimMimId !== null)
            {
                $mimMimIdObject = $this->menuItemGateway->selectByMimMimId($mimMimId);
                $menuItem = $this->menuItemGateway->findWhere($item[MenuItem::MIM_ID]);

                foreach ($mimMimIdObject as $object)
                {
                    if ($object[MenuItem::MIM_ID] == $menuItem[MenuItem::MIM_ID]) continue;
                    if ($object[MenuItem::ATE_ID] !== NULL && $menuItem[MenuItem::ATE_ID] !== NULL && $object[MenuItem::ATE_ID] == $menuItem[MenuItem::ATE_ID])
                    {
                        throw new EntityAlreadyExistsException("Article", "id: " . $menuItem[MenuItem::ATE_ID] . ", menu name: " . $item[MenuItem::MIM_NAME]);
                    }
                }
            }

            $parent = $mimMimId;
            while ($parent !== NULL)
            {
                $par = $this->menuItemGateway->selectParent($parent);
                $parent = $par[MenuItem::MIM_MIM_ID];
                $menu->setUrl(MenuItem::generateUrl($par[MenuItem::MIM_NAME])."/".$menu->getUrl());
            }
            $this->menuItemGateway->updateSort($menu);
            $this->updateSortItems($item["items"], $item[MenuItem::MIM_ID]);
        }
    }

    private function setUrl(MenuItem $item)
    {
        if ($item->isMimHome())
        {
            $item->setUrl(MenuItem::URL_HOME);
            return;
        }
        if ($item->getMimEcommerce())
        {
            $item->setUrl(MenuItem::URL_ECOMMERCE);
            return;
        }
        $parent = $item->getMimMimId();
        while ($parent !== NULL)
        {
            $par = $this->menuItemGateway->selectParent($parent);
            $parent = $par[MenuItem::MIM_MIM_ID];
            $item->setUrl(MenuItem::generateUrl($par[MenuItem::MIM_NAME])."/".$item->getUrl());
        }
    }

    public function updateSortFooterItems($items)
    {
        foreach ($items as $fer)
        {
            $mfeId = $fer["id"];
            $placeNew = 0;
            $place = $this->menuItemGateway->selectAvailablePlace(-1, $mfeId);
            $placeMax = $place["fer_place_max"];
            $placeMin = $place["fer_place_min"];
            if ((count($fer["items"]) - $placeMin) > 0)
            {
                $placeNew = $placeMax;
            }
            foreach ($fer["items"] as $mim)
            {
                $placeNew++;
                $menu = new MenuItem();
                $menu->setMimId($mim[MenuItem::MIM_ID])
                    ->setMfeId($mim[MenuItem::MFE_ID])
                    ->setFerPlace($placeNew);
                $this->menuItemGateway->updateSortFooter($menu);
            }
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuMainListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->menuItemGateway->selectMainAll();
            $resList = MenuItem::generateList($list);
            $so = StatusObject::create($resList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuMainOnlyListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->menuItemGateway->selectMainOnlyAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuMainResponse(Request $request, Response $response)
    {
        $mimId = $request->getAttribute(self::MIM_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $item = $this->menuItemGateway->findWhere($mimId);
            $item["meta_keywords"] = $this->metaKeywordsGateway->findWhereMimId($mimId);
            $so = StatusObject::create($item);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postMenuMainResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $item = new MenuItem();
                $item->setMimMimId($body[MenuItem::MIM_MIM_ID])
                    ->setManPlace($this->menuItemGateway->selectAvailablePlace($item->getMimMimId())["man_place_max"])
                    ->setAteId($body[MenuItem::ATE_ID]);
                if ($body[MenuItem::MIM_ID] !== null)
                {
                    $body = $this->menuItemGateway->findWhere($body[MenuItem::MIM_ID]);
                    $item->setMimType(MenuItem::MIM_TYPE_MFR)
                        ->setMimId($body[MenuItem::MIM_ID])
                        ->setMfeId($body[MenuItem::MFE_ID])
                        ->setFerPlace($body[MenuItem::FER_PLACE])
                        ->setMimName($body[MenuItem::MIM_NAME]);
                }
                else
                {
                    $item->setMimName($body[MenuItem::MIM_NAME])
                        ->setMimType(MenuItem::MIM_TYPE_MAN);
                }
                $item->setCctId($body[MenuItem::CCT_ID])
                    ->setMetaDescription($body[MenuItem::META_DESCRIPTION])
                    ->setMimEcommerce($body[MenuItem::MIM_ECOMMERCE])
                    ->setMimHome($body[MenuItem::MIM_HOME]);
                $this->setUrl($item);

                if (!$item->getMimEcommerce() && $item->getAteId() === null)
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "Article must be chosen");
                }

                if ($item->getMimEcommerce() && $item->isMimHome())
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "The same item can't be home and ecommerce");
                }

                if ($item->getMimMimId() !== null)
                {
                    $mimMimIdObject = $this->menuItemGateway->selectByMimMimId($item->getMimMimId());
                    foreach ($mimMimIdObject as $object)
                    {
                        if ($object[MenuItem::ATE_ID] == $item->getAteId())
                        {
                            throw new EntityAlreadyExistsException("Article", $item->getAteId());
                        }
                    }
                }

                $exists = $this->menuItemGateway->existsByUrl($item->getUrl());
                if (!empty($exists) || ($item->getMimId() !== null && $item->getMimMimId() !== null))
                {
                    if (!empty($exists) && ($exists[MenuItem::MIM_ID] != $item->getMimId() || $exists[MenuItem::MIM_TYPE] != [MenuItem::MIM_TYPE_FER]))
                        throw new EntityAlreadyExistsException("Menu", $item->getUrl());

                    $this->menuItemGateway->update($item);
                }
                else
                {
                    $mimId = $this->menuItemGateway->insert($item)[MenuItem::MIM_ID];
                    $mid = new MenuItemMetaKeyword();
                    $mid->setMimId($mimId);
                    foreach ($body["meta_keywords"] as $mkdId)
                    {
                        $mid->setMkdId($mkdId);
                        $this->menuItemMetaKeywordGateway->insert($mid);
                    }
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " menu");
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMenuMainsResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->updateSortItems($body, null);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::SORTING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::SORTING . " menu");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMenuMainResponse(Request $request, Response $response)
    {
        $mimId = $request->getAttribute(self::MIM_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $mimOld = $this->menuItemGateway->findWhere($mimId);
                if (!in_array($mimOld[MenuItem::MIM_TYPE], [MenuItem::MIM_TYPE_MFR, MenuItem::MIM_TYPE_MAN]))
                {
                    throw new EntityNotFoundException("Menu", $mimId);
                }
                $menuItem = new MenuItem();
                $menuItem->setMimId($mimId)
                    ->setAteId($body[MenuItem::ATE_ID])
                    ->setMfeId($mimOld[MenuItem::MFE_ID])
                    ->setMimMimId($body[MenuItem::MIM_MIM_ID])
                    ->setMimType($mimOld[MenuItem::MIM_TYPE])
                    ->setMetaDescription($body[MenuItem::META_DESCRIPTION])
                    ->setManPlace($mimOld[MenuItem::MAN_PLACE])
                    ->setFerPlace($mimOld[MenuItem::FER_PLACE])
                    ->setCctId($body[MenuItem::CCT_ID])
                    ->setMimEcommerce($body[MenuItem::MIM_ECOMMERCE])
                    ->setMimHome($body[MenuItem::MIM_HOME])
                    ->setMimName($body[MenuItem::MIM_NAME]);
                if ($mimOld[MenuItem::MIM_MIM_ID] != $menuItem->getMimMimId())
                {
                    $menuItem->setManPlace($this->menuItemGateway->selectAvailablePlace($body[MenuItem::MIM_MIM_ID], -1)["man_place_max"]);
                }
                if ($menuItem->getMimEcommerce() && $menuItem->isMimHome())
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "The same item can't be home and ecommerce");
                }
                $this->setUrl($menuItem);
                if (!$menuItem->getMimEcommerce() && $menuItem->getAteId() === NULL)
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "Article must be chosen");
                }
                $exists = $this->menuItemGateway->existsByUrl($menuItem->getUrl());
                if (!empty($exists))
                {
                    if ($exists[MenuItem::MIM_ID] != $menuItem->getMimId())
                        throw new EntityAlreadyExistsException("Menu", $menuItem->getUrl());
                }
                $children = $this->menuItemGateway->selectChildren($mimId);
                if ($menuItem->getMimMimId() !== NULL)
                {
                    if (!empty($children))
                    {
                        throw new StatusCodeException(StatusObject::INVALID_INPUT, "Parent menu can't become a child");
                    }

                    $parent = $this->menuItemGateway->findWhere($menuItem->getMimMimId());
                    if ($parent[MenuItem::MIM_MIM_ID] !== NULL)
                        throw new StatusCodeException(StatusObject::INVALID_INPUT, "Only 2 level of submenu are allowed");

                    $parentChildren = $this->menuItemGateway->selectChildren($parent[MenuItem::MIM_ID]);
                    if ($menuItem->getAteId() !== NULL)
                    {
                        foreach ($parentChildren as $parentChild)
                        {
                            if ($parentChild[MenuItem::MIM_ID] == $menuItem->getMimId()) continue;
                            if ($parentChild[MenuItem::ATE_ID] == $menuItem->getAteId())
                                throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                        }
                        if ($parent[MenuItem::ATE_ID] == $menuItem->getAteId())
                            throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                    }
                }
                if ($menuItem->getAteId() !== NULL)
                {
                    foreach ($children as $child)
                    {
                        if ($child[MenuItem::ATE_ID] == $menuItem->getAteId())
                        {
                            throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                        }
                    }
                }
                foreach ($children as $child)
                {
                    $newUrl = $menuItem->getUrl() . "/" . MenuItem::generateUrl($child[MenuItem::MIM_NAME]);
                    $this->menuItemGateway->updateUrl($newUrl, $child[MenuItem::MIM_ID]);
                }
                $meta = new MenuItemMetaKeyword();
                $meta->setMimId($mimId);
                $this->menuItemMetaKeywordGateway->delete($meta);
                foreach ($body["meta_keywords"] as $metaKeyword)
                {
                    $meta->setMkdId($metaKeyword);
                    $this->menuItemMetaKeywordGateway->insert($meta);
                }
                $this->menuItemGateway->update($menuItem);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING. " menu");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteMenuMainResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $mimId)
                {
                    $mim = $this->menuItemGateway->findWhere($mimId);
                    $children = $this->menuItemGateway->selectChildren($mimId);

                    foreach ($children as $child)
                    {
                        if ($child[MenuItem::MIM_TYPE] == MenuItem::MIM_TYPE_MAN)
                        {
                            $meta = new MenuItemMetaKeyword();
                            $meta->setMimId($child[MenuItem::MIM_ID]);
                            $this->menuItemMetaKeywordGateway->delete($meta);
                            $this->menuItemGateway->delete($child[MenuItem::MIM_ID]);
                        }
                        else
                        {
                            $childMenu = new MenuItem();
                            $childMenu->setMimId($child[MenuItem::MIM_ID])
                                ->setAteId($child[MenuItem::ATE_ID])
                                ->setFerPlace($child[MenuItem::FER_PLACE])
                                ->setCctId($child[MenuItem::CCT_ID])
                                ->setMimEcommerce($child[MenuItem::MIM_ECOMMERCE])
                                ->setMimHome($child[MenuItem::MIM_HOME])
                                ->setMfeId($child[MenuItem::MFE_ID])
                                ->setMimMimId(null)
                                ->setManPlace(null)
                                ->setMimName($child[MenuItem::MIM_NAME])
                                ->setMimType(MenuItem::MIM_TYPE_FER)
                                ->setMetaDescription($child[MenuItem::META_DESCRIPTION]);
                            $exists = $this->menuItemGateway->existsByUrl($childMenu->getUrl());
                            if (!empty($exists) && $exists[MenuItem::MIM_ID] != $childMenu->getMimId())
                            {
                                throw new EntityAlreadyExistsException("Menu", $childMenu->getUrl());
                            }
                            $this->menuItemGateway->update($childMenu);
                        }
                    }

                    if ($mim[MenuItem::MIM_TYPE] == MenuItem::MIM_TYPE_MAN)
                    {
                        $metaMim = new MenuItemMetaKeyword();
                        $metaMim->setMimId($mimId);
                        $this->menuItemMetaKeywordGateway->delete($metaMim);
                        $this->menuItemGateway->delete($mimId);
                    }
                    else if ($mim[MenuItem::MIM_TYPE] == MenuItem::MIM_TYPE_MFR)
                    {
                        $ParentMenu = new MenuItem();
                        $ParentMenu->setMimId($mim[MenuItem::MIM_ID])
                            ->setAteId($mim[MenuItem::ATE_ID])
                            ->setFerPlace($mim[MenuItem::FER_PLACE])
                            ->setCctId($mim[MenuItem::CCT_ID])
                            ->setMimEcommerce($mim[MenuItem::MIM_ECOMMERCE])
                            ->setMimHome($mim[MenuItem::MIM_HOME])
                            ->setMfeId($mim[MenuItem::MFE_ID])
                            ->setMimMimId(null)
                            ->setManPlace(null)
                            ->setMimName($mim[MenuItem::MIM_NAME])
                            ->setMimType(MenuItem::MIM_TYPE_FER)
                            ->setMetaDescription($mim[MenuItem::META_DESCRIPTION]);
                        $exists = $this->menuItemGateway->existsByUrl($ParentMenu->getUrl());
                        if (!empty($exists) && $exists[MenuItem::MIM_ID] != $ParentMenu->getMimId())
                        {
                            throw new EntityAlreadyExistsException("Menu", $ParentMenu->getUrl());
                        }
                        $this->menuItemGateway->update($ParentMenu);
                    }
                    else
                    {
                        throw new EntityNotFoundException("Menu", $mimId);
                    }
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING. " menu");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuFooterListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $resList = $this->menuFooterTitlesGateway->selectAll();
            $list = $this->menuItemGateway->selectFooterAll();
            foreach ($resList as $key => $fte)
            {
                foreach ($list as $item)
                {
                    if ($fte["id"] == $item[MenuItem::MFE_ID])
                    {
                        $resList[$key]["items"][] = $item;
                    }
                }
            }

            $so = StatusObject::create($resList);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuFooterResponse(Request $request, Response $response)
    {
        $mimId = $request->getAttribute(self::MIM_ID);
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $item = $this->menuItemGateway->findWhere($mimId);
            $item["meta_keywords"] = $this->metaKeywordsGateway->findWhereMimId($mimId);
            $so = StatusObject::create($item);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function getMenuFooterOnlyListResponse(Request $request, Response $response)
    {
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            $list = $this->menuItemGateway->selectFooterOnlyAll();
            $so = StatusObject::create($list);
            return $response->withJson($so->getData(), $so->getCode());
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function postMenuFooterResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $item = new MenuItem();
                $item->setMfeId($body[MenuItem::MFE_ID])
                    ->setFerPlace($this->menuItemGateway->selectAvailablePlace(-1, $item->getMfeId())["fer_place_max"]);

                if ($body[MenuItem::MIM_ID] !== null)
                {
                    $body = $this->menuItemGateway->findWhere($body[MenuItem::MIM_ID]);
                    $item->setMimType(MenuItem::MIM_TYPE_MFR)
                        ->setMimId($body[MenuItem::MIM_ID])
                        ->setMimMimId($body[MenuItem::MIM_MIM_ID])
                        ->setManPlace($body[MenuItem::FER_PLACE])
                        ->setMimName($body[MenuItem::MIM_NAME])
                        ->setUrl($body[MenuItem::URL]);
                }
                else
                {
                    $item->setMimName($body[MenuItem::MIM_NAME])
                        ->setMimType(MenuItem::MIM_TYPE_FER);
                }
                $item->setAteId($body[MenuItem::ATE_ID])
                    ->setCctId($body[MenuItem::CCT_ID])
                    ->setMetaDescription($body[MenuItem::META_DESCRIPTION])
                    ->setMimEcommerce($body[MenuItem::MIM_ECOMMERCE])
                    ->setMimHome($body[MenuItem::MIM_HOME]);

                if (!$item->getMimEcommerce() && $item->getAteId() === null)
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "Article must be chosen");
                }

                $exists = $this->menuItemGateway->existsByUrl($item->getUrl());
                if (!empty($exists))
                {
                    if ($exists[MenuItem::MIM_ID] != $item->getMimId() || $exists[MenuItem::MIM_TYPE] != MenuItem::MIM_TYPE_MAN)
                        throw new EntityAlreadyExistsException("Menu", $item->getUrl());
                    $this->menuItemGateway->update($item);
                }
                else
                {
                    $mimId = $this->menuItemGateway->insert($item)[MenuItem::MIM_ID];
                    $mid = new MenuItemMetaKeyword();
                    $mid->setMimId($mimId);
                    foreach ($body["meta_keywords"] as $mkdId)
                    {
                        $mid->setMkdId($mkdId);
                        $this->menuItemMetaKeywordGateway->insert($mid);
                    }
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::INSERTING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::INSERTING . " menu");
            }
            return $response->withStatus(StatusObject::CREATED);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMenuFootersResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $this->updateSortFooterItems($body);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::SORTING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::SORTING . " menu");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function putMenuFooterResponse(Request $request, Response $response)
    {
        $mimId = $request->getAttribute(self::MIM_ID);
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                $mimOld = $this->menuItemGateway->findWhere($mimId);
                if (!in_array($mimOld[MenuItem::MIM_TYPE], [MenuItem::MIM_TYPE_MFR, MenuItem::MIM_TYPE_FER]))
                {
                    throw new EntityNotFoundException("Menu", $mimId);
                }
                $menuItem = new MenuItem();
                $menuItem->setMimId($mimId)
                    ->setAteId($body[MenuItem::ATE_ID])
                    ->setMfeId($body[MenuItem::MFE_ID])
                    ->setMimMimId($mimOld[MenuItem::MIM_MIM_ID])
                    ->setMimType($mimOld[MenuItem::MIM_TYPE])
                    ->setMetaDescription($body[MenuItem::META_DESCRIPTION])
                    ->setManPlace($mimOld[MenuItem::MAN_PLACE])
                    ->setFerPlace($mimOld[MenuItem::FER_PLACE])
                    ->setCctId($body[MenuItem::CCT_ID])
                    ->setMimEcommerce($body[MenuItem::MIM_ECOMMERCE])
                    ->setMimHome($body[MenuItem::MIM_HOME])
                    ->setMimName($body[MenuItem::MIM_NAME]);
                if ($mimOld[MenuItem::MFE_ID] != $menuItem->getMfeId())
                {
                    $menuItem->setFerPlace($this->menuItemGateway->selectAvailablePlace(-1, $body[MenuItem::MFE_ID])["fer_place_max"]);
                }
                $this->setUrl($menuItem);
                if (!$menuItem->getMimEcommerce() && $menuItem->getAteId() === NULL)
                {
                    throw new StatusCodeException(StatusObject::INVALID_INPUT, "Article must be chosen");
                }
                $exists = $this->menuItemGateway->existsByUrl($menuItem->getUrl());
                if (!empty($exists))
                {
                    if ($exists[MenuItem::MIM_ID] != $menuItem->getMimId())
                        throw new EntityAlreadyExistsException("Menu", $menuItem->getUrl());
                }
                $children = $this->menuItemGateway->selectChildren($mimId);
                if ($menuItem->getMimMimId() !== NULL)
                {
                    $parent = $this->menuItemGateway->findWhere($menuItem->getMimMimId());
                    $parentChildren = $this->menuItemGateway->selectChildren($parent[MenuItem::MIM_ID]);
                    if ($menuItem->getAteId() !== NULL)
                    {
                        foreach ($parentChildren as $parentChild)
                        {
                            if ($parentChild[MenuItem::MIM_ID] == $menuItem->getMimId()) continue;
                            if ($parentChild[MenuItem::ATE_ID] == $menuItem->getAteId())
                                throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                        }
                        if ($parent[MenuItem::ATE_ID] == $menuItem->getAteId())
                            throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                    }
                }
                if ($menuItem->getAteId() !== NULL)
                {
                    foreach ($children as $child)
                    {
                        if ($child[MenuItem::ATE_ID] == $menuItem->getAteId())
                        {
                            throw new EntityAlreadyExistsException("Article", $menuItem->getAteId());
                        }
                    }
                }
                foreach ($children as $child)
                {
                    $newUrl = $menuItem->getUrl() . "/" . MenuItem::generateUrl($child[MenuItem::MIM_NAME]);
                    $this->menuItemGateway->updateUrl($newUrl, $child[MenuItem::MIM_ID]);
                }
                $meta = new MenuItemMetaKeyword();
                $meta->setMimId($mimId);
                $this->menuItemMetaKeywordGateway->delete($meta);
                foreach ($body["meta_keywords"] as $metaKeyword)
                {
                    $meta->setMkdId($metaKeyword);
                    $this->menuItemMetaKeywordGateway->insert($meta);
                }
                $this->menuItemGateway->update($menuItem);
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::UPDATING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::UPDATING. " menu");
            }
            return $response->withStatus(StatusObject::NO_CONTENT);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response|static
     */
    public function deleteMenuFooterResponse(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        try
        {
            $this->checkAuthorization([Administrator::USER_BASIC, Administrator::USER_SUPER]);
            try
            {
                $this->dbPgsql->beginTransaction();
                foreach ($body as $mimId)
                {
                    $mim = $this->menuItemGateway->findWhere($mimId);

                    if ($mim[MenuItem::MIM_TYPE] == MenuItem::MIM_TYPE_FER)
                    {
                        $metaMim = new MenuItemMetaKeyword();
                        $metaMim->setMimId($mimId);
                        $this->menuItemMetaKeywordGateway->delete($metaMim);
                        $this->menuItemGateway->delete($mimId);
                    }
                    else if ($mim[MenuItem::MIM_TYPE] == MenuItem::MIM_TYPE_MFR)
                    {
                        $ParentMenu = new MenuItem();
                        $ParentMenu->setMimId($mim[MenuItem::MIM_ID])
                            ->setAteId($mim[MenuItem::ATE_ID])
                            ->setFerPlace(null)
                            ->setCctId($mim[MenuItem::CCT_ID])
                            ->setMimEcommerce($mim[MenuItem::MIM_ECOMMERCE])
                            ->setMimHome($mim[MenuItem::MIM_HOME])
                            ->setMfeId(null)
                            ->setMimMimId($mim[MenuItem::MIM_MIM_ID])
                            ->setManPlace($mim[MenuItem::MAN_PLACE])
                            ->setMimName($mim[MenuItem::MIM_NAME])
                            ->setUrl($mim[MenuItem::URL])
                            ->setMimType(MenuItem::MIM_TYPE_MAN)
                            ->setMetaDescription($mim[MenuItem::META_DESCRIPTION]);
                        $exists = $this->menuItemGateway->existsByUrl($ParentMenu->getUrl());
                        if (!empty($exists) && $exists[MenuItem::MIM_ID] != $ParentMenu->getMimId())
                        {
                            throw new EntityAlreadyExistsException("Menu", $ParentMenu->getUrl());
                        }
                        $this->menuItemGateway->update($ParentMenu);
                    }
                    else
                    {
                        throw new EntityNotFoundException("Menu", $mimId);
                    }
                }
                $this->dbPgsql->commit();
            }
            catch (\PDOException $e)
            {
                $this->dbPgsql->rollBack();
                $this->logger->Error(PostgreSQLDatabaseException::MESSAGE . " " . PostgreSQLDatabaseException::DELETING . " menu. Exception Message: {$e->getMessage()}\n Trace: {$e->getTraceAsString()}");
                throw new PostgreSQLDatabaseException(StatusObject::INVALID_INPUT, PostgreSQLDatabaseException::DELETING. " menu");
            }
            return $response->withStatus(StatusObject::OK);
        }
        catch (StatusCodeException $e)
        {
            return $this->exceptionResponse($response, $e);
        }
    }
}