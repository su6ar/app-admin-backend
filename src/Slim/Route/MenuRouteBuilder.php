<?php
/**
 * Created by Daniel Bill
 * Date: 17.08.2018
 * Time: 15:07
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class MenuRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class MenuRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::MimInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::MimInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/menu/main', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::MimInterface . ':getMenuMainListResponse');
            $slimApp->get('/only', AbstractInterface::MimInterface . ':getMenuMainOnlyListResponse');
            $slimApp->delete('', AbstractInterface::MimInterface . ':deleteMenuMainResponse');
            $slimApp->put('/{mimId}', AbstractInterface::MimInterface . ':putMenuMainResponse');
            $slimApp->put('', AbstractInterface::MimInterface . ':putMenuMainsResponse');
            $slimApp->post('', AbstractInterface::MimInterface . ':postMenuMainResponse');
            $slimApp->get('/{mimId}', AbstractInterface::MimInterface . ':getMenuMainResponse');
        });

        $slimApp->group(Main::$REST_PREFIX.'/menu/footer', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::MimInterface . ':getMenuFooterListResponse');
            $slimApp->get('/only', AbstractInterface::MimInterface . ':getMenuFooterOnlyListResponse');
            $slimApp->delete('', AbstractInterface::MimInterface . ':deleteMenuFooterResponse');
            $slimApp->put('/{mimId}', AbstractInterface::MimInterface . ':putMenuFooterResponse');
            $slimApp->put('', AbstractInterface::MimInterface . ':putMenuFootersResponse');
            $slimApp->post('', AbstractInterface::MimInterface . ':postMenuFooterResponse');
            $slimApp->get('/{mimId}', AbstractInterface::MimInterface . ':getMenuFooterResponse');
        });
    }
}