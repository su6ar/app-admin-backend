<?php
/**
 * Created by Daniel Bill
 * Date: 14.07.2018
 * Time: 13:47
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;


class ImageCategoryRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::IcyInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::IcyInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/image-categories', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::IcyInterface.':getImageCategoriesListResponse');
            $slimApp->get('/{icyId}', AbstractInterface::IcyInterface.':getImageCategoryResponse');
            $slimApp->get('/marking/{icyId}', AbstractInterface::IcyInterface.':getImageMarkingsByCategoryListResponse');
            $slimApp->delete('', AbstractInterface::IcyInterface.':deleteImageCategoryResponse');
            $slimApp->post('', AbstractInterface::IcyInterface.':postImageCategoryResponse');
            $slimApp->put('/{icyId}', AbstractInterface::IcyInterface.':putImageCategoryResponse');
        });
    }
}