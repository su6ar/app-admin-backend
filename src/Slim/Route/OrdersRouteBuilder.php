<?php

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class OrdersRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class OrdersRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::OrdInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::OrdInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/orders', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::OrdInterface.':getOrdersListResponse');
            $slimApp->get('/{odrId}', AbstractInterface::OrdInterface.':getOrderResponse');
            $slimApp->delete('', AbstractInterface::OrdInterface.':deleteOrderResponse');
            $slimApp->post('/{odrId}/note', AbstractInterface::OrdInterface.':postOrderNoteResponse');
            $slimApp->patch('', AbstractInterface::OrdInterface.':patchOrdersStatusResponse');
        });
    }
}