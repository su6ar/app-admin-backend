<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:08
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class CategoryRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class CategoryRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::CeyInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::CeyInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/categories', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::CeyInterface . ':getCategoryListResponse');
            $slimApp->delete('', AbstractInterface::CeyInterface . ':deleteCategoryResponse');
            $slimApp->put('/{ceyId}', AbstractInterface::CeyInterface . ':putCategoryResponse');
            $slimApp->put('', AbstractInterface::CeyInterface . ':putCategoriesResponse');
            $slimApp->post('', AbstractInterface::CeyInterface . ':postCategoryResponse');
            $slimApp->get('/{ceyId}', AbstractInterface::CeyInterface . ':getCategoryResponse');
        });
    }
}