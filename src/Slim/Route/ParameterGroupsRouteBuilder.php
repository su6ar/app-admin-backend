<?php
/**
 * Created by Daniel Bill
 * Date: 10.08.2018
 * Time: 14:32
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class ParameterGroupsRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::PgpInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::PgpInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/parameter-groups', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::PgpInterface . ':getParameterGroupListResponse');
            $slimApp->delete('', AbstractInterface::PgpInterface . ':deleteParameterGroupResponse');
            $slimApp->put('/{pgpId}', AbstractInterface::PgpInterface . ':putParameterGroupResponse');
            $slimApp->post('', AbstractInterface::PgpInterface . ':postParameterGroupResponse');
            $slimApp->get('/{pgpId}', AbstractInterface::PgpInterface . ':getParameterGroupResponse');
        });
    }
}