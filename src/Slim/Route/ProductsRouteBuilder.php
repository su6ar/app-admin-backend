<?php
/**
 * Created by Daniel Bill
 * Date: 24.08.2018
 * Time: 15:19
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class ProductsRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::PutInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::PutInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/products', function () use ($slimApp) {
            $slimApp->get('/category', AbstractInterface::PutInterface . ':getProductListResponse');
            $slimApp->delete('', AbstractInterface::PutInterface . ':deleteProductResponse');
            $slimApp->put('/single/{putId}', AbstractInterface::PutInterface . ':putProductResponse');
            $slimApp->put('/multiple', AbstractInterface::PutInterface . ':putMultipleProductResponse');
            $slimApp->post('', AbstractInterface::PutInterface . ':postProductResponse');
            $slimApp->get('/{putId}', AbstractInterface::PutInterface . ':getProductResponse');
        });
    }
}