<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:08
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class GroupRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class ParametersRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::PrrInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::PrrInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/parameters', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::PrrInterface . ':getParameterListResponse');
            $slimApp->delete('', AbstractInterface::PrrInterface . ':deleteParameterResponse');
            $slimApp->put('/{prrId}', AbstractInterface::PrrInterface . ':putParameterResponse');
            $slimApp->post('', AbstractInterface::PrrInterface . ':postParameterResponse');
            $slimApp->get('/{prrId}', AbstractInterface::PrrInterface . ':getParameterResponse');
        });
    }
}