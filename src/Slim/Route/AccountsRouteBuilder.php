<?php
/**
 * Created by PhpStorm.
 * User: Daniel Bill
 * Date: 03.07.2017
 * Time: 15:16
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class AccountsRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class AccountsRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::ActInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::ActInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/accounts', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::ActInterface.':getAccountsListResponse');
            $slimApp->post('/create', AbstractInterface::ActInterface.':postAccountResponse');
            $slimApp->patch('/forgotpass/{actemail}', AbstractInterface::ActInterface.':patchAccountForgotPasswordResponse');
            $slimApp->post('/auth', AbstractInterface::ActInterface.':authAccountResponse');
            $slimApp->get('/token', AbstractInterface::ActInterface.':getAuthAccountResponse');
            $slimApp->put('', AbstractInterface::ActInterface.':putAccountResponse');
        });

        $slimApp->group(Main::$REST_PREFIX.'/accounts/updatepass', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::ActInterface.':getAccountRefreshCodeExistsResponse');
            $slimApp->patch('', AbstractInterface::ActInterface.':patchAccountPasswordResponse');
        });
    }
}