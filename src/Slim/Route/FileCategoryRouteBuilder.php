<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 23:01
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class FileCategoryRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::FcyInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::FcyInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/file-categories', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::FcyInterface.':getFileCategoryListResponse');
            $slimApp->get('/{fcyId}', AbstractInterface::FcyInterface.':getFileCategoryResponse');
            $slimApp->delete('', AbstractInterface::FcyInterface.':deleteFileCategoryResponse');
            $slimApp->post('', AbstractInterface::FcyInterface.':postFileCategoryResponse');
            $slimApp->put('/{fcyId}', AbstractInterface::FcyInterface.':putFileCategoryResponse');
        });
    }
}