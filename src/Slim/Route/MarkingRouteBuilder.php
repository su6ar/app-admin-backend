<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 14:28
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class MarkingRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::MigInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::MigInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/markings', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::MigInterface.':getMarkingListResponse');
        });
    }
}