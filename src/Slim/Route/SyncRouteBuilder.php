<?php
/**
 * Created by Daniel Bill
 * Date: 02.09.2018
 * Time: 17:32
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class DbSyncRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class SyncRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::SycInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::SycInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/db/sync', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::SycInterface . ':syncResponse');
        });
    }
}
