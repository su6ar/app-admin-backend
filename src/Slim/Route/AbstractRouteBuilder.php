<?php
/**
 * Created by PhpStorm.
 * User: Daniel Bill
 * Date: 03.07.2017
 * Time: 15:00
 */

namespace Kominexpres\src\Slim\Route;


use Psr\Container\ContainerInterface;
use Monolog\Logger;
use Slim\App as Slim;

/**
 * Class AbstractRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
abstract class AbstractRouteBuilder
{
    /** @var Logger */
    protected $logger;

    /** @var string */
    protected $interface;

    /**
     * AbstractRouteBuilder constructor.
     * @param ContainerInterface $ci
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $ci)
    {
        $this->logger = $ci->get('logger');
        $this->interface = '\\Kominexpres\\src\\App\\Interfaces\\';
    }

    /**
     * @param Slim $slimApp
     */
    public abstract function build(Slim $slimApp);
}