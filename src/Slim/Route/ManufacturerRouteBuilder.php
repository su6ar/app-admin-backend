<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:40
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class ManufacturerRouteBuilder extends AbstractRouteBuilder
{
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::MurInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::MurInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/manufacturer', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::MurInterface.':getManufacturerListResponse');
            $slimApp->get('/{id}', AbstractInterface::MurInterface.':getManufacturerSingleResponse');
            $slimApp->delete('', AbstractInterface::MurInterface.':deleteManufacturerResponse');
            $slimApp->put('/{id}', AbstractInterface::MurInterface.':putManufacturerResponse');
            $slimApp->post('', AbstractInterface::MurInterface.':postManufacturerResponse');
        });
    }
}