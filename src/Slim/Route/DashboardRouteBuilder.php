<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2017
 * Time: 17:07
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class DashboardRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class DashboardRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::DbdInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::DbdInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/dashboard', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::DbdInterface.':getDashboardStatsResponse');
        });
    }
}