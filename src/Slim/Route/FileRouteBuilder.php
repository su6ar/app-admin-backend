<?php
/**
 * Created by Daniel Bill
 * Date: 24.07.2018
 * Time: 17:24
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class FileRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::FieInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::FieInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/files', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::FieInterface.':getFileListResponse');
            $slimApp->delete('', AbstractInterface::FieInterface.':deleteFileResponse');
            $slimApp->patch('/{fcyId}', AbstractInterface::FieInterface.':patchFileResponse');
            $slimApp->post('', AbstractInterface::FieInterface.':postFileResponse');
        });
    }
}