<?php
/**
 * Created by Daniel Bill
 * Date: 14.07.2018
 * Time: 13:47
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;


class ImageRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::IaeInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::IaeInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/images', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::IaeInterface.':getImageListResponse');
            $slimApp->post('', AbstractInterface::IaeInterface.':postImageResponse');
            $slimApp->patch('/category/{icyId}', AbstractInterface::IaeInterface.':patchImageResponse');
            $slimApp->delete('', AbstractInterface::IaeInterface.':deleteImageResponse');
            $slimApp->get('/category/{icyId}', AbstractInterface::IaeInterface.':getImageByIcyIdResponse');
        });
    }
}