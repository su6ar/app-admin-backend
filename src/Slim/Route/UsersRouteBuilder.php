<?php
/**
 * Created by Daniel Bill
 * Date: 13.08.2018
 * Time: 15:19
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class UsersRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class UsersRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::UerInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::UerInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/users/admins', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::UerInterface . ':getAdminListResponse');
            $slimApp->put('/rights/{amrRights}', AbstractInterface::UerInterface . ':putAdminResponse');
            $slimApp->get('/{actId}', AbstractInterface::UerInterface . ':getAdminResponse');
        });

        $slimApp->group(Main::$REST_PREFIX.'/users/customers', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::UerInterface . ':getCustomerListResponse');
            $slimApp->put('/group/{grpId}', AbstractInterface::UerInterface . ':putCustomerResponse');
            $slimApp->get('/{actId}', AbstractInterface::UerInterface . ':getCustomerResponse');
        });
    }
}