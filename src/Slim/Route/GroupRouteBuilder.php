<?php
/**
 * Created by Daniel Bill
 * Date: 25.07.2018
 * Time: 18:08
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class GroupsRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class GroupRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::GrpInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::GrpInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/groups', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::GrpInterface . ':getGroupListResponse');
            $slimApp->delete('', AbstractInterface::GrpInterface . ':deleteGroupResponse');
            $slimApp->put('/{grpId}', AbstractInterface::GrpInterface . ':putGroupResponse');
            $slimApp->post('', AbstractInterface::GrpInterface . ':postGroupResponse');
            $slimApp->get('/{grpId}', AbstractInterface::GrpInterface . ':getGroupResponse');
        });
    }
}