<?php
/**
 * Created by Daniel Bill
 * Date: 15.08.2018
 * Time: 19:25
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class ArticlesRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class ArticlesRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::AteInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::AteInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/articles', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::AteInterface . ':getArticleListResponse');
            $slimApp->delete('', AbstractInterface::AteInterface . ':deleteArticleResponse');
            $slimApp->put('/{ateId}', AbstractInterface::AteInterface . ':putArticleResponse');
            $slimApp->post('', AbstractInterface::AteInterface . ':postArticleResponse');
            $slimApp->get('/{ateId}', AbstractInterface::AteInterface . ':getArticleResponse');
        });
    }
}