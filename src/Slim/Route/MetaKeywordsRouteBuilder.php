<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 0:49
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class MetaKeywordsRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::MkdInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::MkdInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/meta-keywords', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::MkdInterface . ':getMetaKeywordListResponse');
            $slimApp->get('/{id}', AbstractInterface::MkdInterface . ':getMetaKeywordResponse');
            $slimApp->put('/{id}', AbstractInterface::MkdInterface . ':putMetaKeywordResponse');
            $slimApp->post('', AbstractInterface::MkdInterface . ':postMetaKeywordResponse');
            $slimApp->delete('', AbstractInterface::MkdInterface . ':deleteMetaKeywordResponse');
        });
    }
}