<?php
/**
 * Created by Daniel Bill
 * Date: 17.03.2018
 * Time: 13:28
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class ShippingTypeRouteBuilder extends AbstractRouteBuilder
{
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::SteInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::SteInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/shipping-type', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::SteInterface.':getShippingTypeListResponse');
            $slimApp->get('/{id}', AbstractInterface::SteInterface.':getShippingTypeSingleResponse');
            $slimApp->delete('', AbstractInterface::SteInterface.':deleteShippingTypeResponse');
            $slimApp->put('/{id}', AbstractInterface::SteInterface.':putShippingTypeResponse');
            $slimApp->post('', AbstractInterface::SteInterface.':postShippingTypeResponse');
        });
    }
}