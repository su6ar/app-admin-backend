<?php
/**
 * Created by Daniel Bill
 * Date: 05.01.2018
 * Time: 20:53
 */

namespace Kominexpres\src\Slim\Route;


use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

/**
 * Class ShippingRouteBuilder
 * @package Kominexpres\src\Slim\Route
 */
class ShippingRouteBuilder extends AbstractRouteBuilder
{
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::SpgInterface] = function ($container) {
            $class = $this->interface.AbstractInterface::SpgInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX.'/shipping', function () use ($slimApp) {
            $slimApp->get('', AbstractInterface::SpgInterface.':getShippingListResponse');
            $slimApp->get('/{spgId}', AbstractInterface::SpgInterface.':getShippingSingleResponse');
            $slimApp->delete('', AbstractInterface::SpgInterface.':deleteShippingResponse');
            $slimApp->post('', AbstractInterface::SpgInterface.':postShippingResponse');
            $slimApp->post('/{shyId}', AbstractInterface::SpgInterface.':postShippingWeightResponse');
            $slimApp->put('/spg/{spgId}', AbstractInterface::SpgInterface.':putShippingSpgResponse');
            $slimApp->put('/sht/{shtId}', AbstractInterface::SpgInterface.':putShippingShtResponse');
        });
    }
}