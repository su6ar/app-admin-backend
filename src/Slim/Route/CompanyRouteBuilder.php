<?php
/**
 * Created by Daniel Bill
 * Date: 19.08.2018
 * Time: 0:43
 */

namespace Kominexpres\src\Slim\Route;

use Kominexpres\src\App\Interfaces\AbstractInterface;
use Kominexpres\src\Slim\Main\Main;
use Slim\App as Slim;

class CompanyRouteBuilder extends AbstractRouteBuilder
{
    /**
     * @param Slim $slimApp
     */
    public function build(Slim $slimApp)
    {
        $slimApp->getContainer()[AbstractInterface::CdaInterface] = function ($container) {
            $class = $this->interface . AbstractInterface::CdaInterface;
            return new $class($container);
        };

        $slimApp->group(Main::$REST_PREFIX . '/company', function () use ($slimApp) {
            $slimApp->get('/contacts', AbstractInterface::CdaInterface . ':getCompanyContactListResponse');
        });
    }
}