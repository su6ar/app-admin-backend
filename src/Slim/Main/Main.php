<?php
/**
 * Created by PhpStorm.
 * User: Daniel Bill
 * Date: 03.07.2017
 * Time: 14:50
 */

namespace Kominexpres\src\Slim\Main;


use Firebase\JWT\JWT;
use Kominexpres\src\App\BO\Administrator;
use Kominexpres\src\App\BO\Token;
use Kominexpres\src\App\Config\Config;
use Kominexpres\src\App\Exceptions\TokenNotFoundException;
use Kominexpres\src\App\Exceptions\UnauthorizedException;
use Kominexpres\src\App\Storage\Database;
use Kominexpres\src\App\TableDataGateway\TokenGateway;
use Kominexpres\src\Slim\Route\ArticlesRouteBuilder;
use Kominexpres\src\Slim\Route\CategoryRouteBuilder;
use Kominexpres\src\Slim\Route\CompanyRouteBuilder;
use Kominexpres\src\Slim\Route\DashboardRouteBuilder;
use Kominexpres\src\Slim\Route\FileCategoryRouteBuilder;
use Kominexpres\src\Slim\Route\FileRouteBuilder;
use Kominexpres\src\Slim\Route\GroupRouteBuilder;
use Kominexpres\src\Slim\Route\ImageCategoryRouteBuilder;
use Kominexpres\src\Slim\Route\ImageRouteBuilder;
use Kominexpres\src\Slim\Route\ManufacturerRouteBuilder;
use Kominexpres\src\Slim\Route\MarkingRouteBuilder;
use Kominexpres\src\Slim\Route\MenuRouteBuilder;
use Kominexpres\src\Slim\Route\MetaKeywordsRouteBuilder;
use Kominexpres\src\Slim\Route\OrdersRouteBuilder;
use Kominexpres\src\Slim\Route\ParameterGroupsRouteBuilder;
use Kominexpres\src\Slim\Route\ParametersRouteBuilder;
use Kominexpres\src\Slim\Route\PaymentRouteBuilder;
use Kominexpres\src\Slim\Route\ProductsRouteBuilder;
use Kominexpres\src\Slim\Route\ShippingRouteBuilder;
use Kominexpres\src\Slim\Route\ShippingTypeRouteBuilder;
use Kominexpres\src\Slim\Route\SyncRouteBuilder;
use Kominexpres\src\Slim\Route\UsersRouteBuilder;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Kominexpres\src\App\Exceptions\ExceptionHandler;
use Kominexpres\src\App\Exceptions\HTTP\MethodNotAllowedException;
use Kominexpres\src\App\Exceptions\HTTP\RouteNotFoundException;
use Kominexpres\src\App\Logger\LOGGER;
use Kominexpres\src\App\POPO\StatusObject;
use Kominexpres\src\Slim\Route\AccountsRouteBuilder;
use Slim\Middleware\JwtAuthentication;
use Unirest\Request as UNIRequest;

class Main
{
    /** @var string */
    public static $REST_PREFIX = "/app/v1";

    /** @var Config */
    private $config;

    /** @var array */
    private $builders;

    /** @var App */
    private $slimApp;

    /** @var \Psr\Container\ContainerInterface */
    private $container;

    /**
     * Main constructor.
     * @param Config $config
     * @param array $settings
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(Config $config, array $settings)
    {
        $this->config = $config;
        $this->slimApp = new App($settings);
        $this->container = $this->slimApp->getContainer();

        $this->setLogger();
        $this->setAuth();
        $this->setConfig();
        $this->setRouteNotFoundHandler();
        $this->setMethodNotAllowedHandler();
        $this->setSlimErrHandler();
        $this->setDatabase();

        $this->builders = [
            new AccountsRouteBuilder($this->container),
            new ArticlesRouteBuilder($this->container),
            new CategoryRouteBuilder($this->container),
            new CompanyRouteBuilder($this->container),
            new DashboardRouteBuilder($this->container),
            new FileCategoryRouteBuilder($this->container),
            new FileRouteBuilder($this->container),
            new GroupRouteBuilder($this->container),
            new ImageCategoryRouteBuilder($this->container),
            new ImageRouteBuilder($this->container),
            new ManufacturerRouteBuilder($this->container),
            new MarkingRouteBuilder($this->container),
            new MenuRouteBuilder($this->container),
            new MetaKeywordsRouteBuilder($this->container),
            new OrdersRouteBuilder($this->container),
            new ParameterGroupsRouteBuilder($this->container),
            new ParametersRouteBuilder($this->container),
            new PaymentRouteBuilder($this->container),
            new ProductsRouteBuilder($this->container),
            new ShippingRouteBuilder($this->container),
            new ShippingTypeRouteBuilder($this->container),
            new SyncRouteBuilder($this->container),
            new UsersRouteBuilder($this->container)
        ];
    }

    /**
     * @throws \Exception
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function runApp()
    {
        $this->slimApp->add(new LogRequestMiddleware());
        $this->slimApp->add("JwtAuthentication");
        $this->slimApp->options('/{routes:.+}', function ($request, $response, $args) {
            return $response;
        });
        $this->slimApp->add(function ($req, $res, callable $next) {
            $response = $next($req, $res);
            return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
        });
        $this->slimApp->run();
    }

    public function createRoutes()
    {
        foreach ($this->builders as $builder)
        {
            $builder->build($this->slimApp);
        }
    }

    private function setConfig()
    {
        $this->container['config'] = $this->config;
    }

    private function setSlimErrHandler()
    {
        $func = function ($c)
        {
            return function (Request $request, Response $response, \Throwable $exception) use ($c) 
            {
                $logger = LOGGER::getLogger();
                $logger->error($exception->getCode()." ".$exception->getMessage()." File: ".$exception->getFile()." Line: ".$exception->getLine()." Traceback: ".$exception->getTraceAsString());
                return $response->withStatus(StatusObject::INTERNAL_ERROR)
                    ->withHeader('Content-Type', 'application/json;charset=utf-8')
                    ->write(json_encode(ExceptionHandler::formatException($exception)));
            };
        };
        $this->container['errorHandler'] = $func;
        $this->container['phpErrorHandler'] = $func;
    }

    private function setRouteNotFoundHandler()
    {
        $this->container['notFoundHandler'] = function ($c)
        {
            return function (Request $request, Response $response) use ($c)
            {
                return $response
                    ->withStatus(StatusObject::NOT_FOUND)
                    ->withHeader('Content-Type', 'application/json;charset=utf-8')
                    ->write(json_encode(new RouteNotFoundException()));
            };
        };
    }

    private function setMethodNotAllowedHandler()
    {
        $this->container['notAllowedHandler'] = function ($c) 
        {
            return function (Request $request, Response $response, $methods) use ($c) 
            {
                return $response
                    ->withStatus(StatusObject::METHOD_NOT_ALLOWED)
                    ->withHeader('Allow', implode(', ', $methods))
                    ->withHeader('Content-Type', 'application/json;charset=utf-8')
                    ->write(json_encode(new MethodNotAllowedException($methods)));
            };
        };
    }

    /**
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function setLogger()
    {
        $settings = $this->container->get('settings')['logger'];
        LOGGER::setLogger($settings);
        $this->container['logger'] = LOGGER::getLogger();
    }

    private function setDatabase()
    {
        $this->container['mysql'] = null;
        //$this->container['mysql'] = new Database($this->config->getDbMysqlDSN(), $this->config->getDbMysqlUsername(), $this->config->getDbMysqlPassword());
        $this->container['pgsql'] = new Database($this->config->getDbPgsqlDSN(), $this->config->getDbPgsqlUsername(), $this->config->getDbPgsqlPassword());
    }

    private function setAuth()
    {
        $this->container["token"] = function ($c) {
            return new Token;
        };
        $this->container["JwtAuthentication"] = function ($c)
        {
            return new JwtAuthentication([
                "path" => Main::$REST_PREFIX,
                "secure" => "true",
                "passthrough" => [
                    Main::$REST_PREFIX . "/accounts/auth",
                    Main::$REST_PREFIX . "/accounts/create",
                    Main::$REST_PREFIX . "/accounts/forgotpass",
                    Main::$REST_PREFIX . "/accounts/updatepass"
                ],
                "secret" => $this->config->getSecretKey(),
                "logger" => $this->container->get("logger"),
                "attribute" => false,
                "relaxed" => [],
                "error" => function (Request $request, Response $response, $arguments) {
                    $logger = LOGGER::getLogger();
                    $logger->error("token error");
                    $logger->info("secret key token: " . $this->config->getSecretKey());
                    if (strcmp($arguments["message"], "Expired token") == 0)
                    {
                        $jwt = $arguments["token"];
                        try
                        {
                            $oToken = new Token();
                            $tokenGateway = new TokenGateway($this->container->get('pgsql'), null);
                            $data = $tokenGateway->findWhere($jwt);
                            $oToken->setId($data["token_id"])
                                ->setJwt($data["jwt"])
                                ->setScope(Administrator::getScope($data["amr_rights"]))
                                ->setSub($data["sub"])
                                ->setInvalid($data["invalid"]);

                            if ($oToken->isInvalid() === true)
                            {
                                $exception = new UnauthorizedException($arguments["message"]);
                                return $response->withJson(ExceptionHandler::formatException($exception));
                            }
                            $oToken->setInvalid(true);
                            $tokenGateway->patch($oToken);
                            $nToken = Token::CreateToken($oToken->getSub(), $oToken->getScope());
                            $tokenGateway->insert($nToken);
                            UNIRequest::jsonOpts(true);
                            UNIRequest::verifyPeer(false);
                            UNIRequest::verifyHost(false);
                            $UNIresponse = UNIRequest::send(
                                $request->getMethod(),
                                "https://" . $request->getUri()->getHost() . $request->getUri()->getPath(),
                                $request->getParsedBody(),
                                ["Authorization" => "Bearer " . $nToken->getJwt()]
                            );
                            return $response->withJson($UNIresponse->body, $UNIresponse->code)->withHeader("Authorization","Bearer " . $nToken->getJwt());
                        }
                        catch (TokenNotFoundException $exception)
                        {
                            return $response->withJson(ExceptionHandler::formatException($exception));
                        }
                    }

                    $exception = new UnauthorizedException($arguments["message"]);
                    return $response->withJson(ExceptionHandler::formatException($exception));
                },
                "callback" => function ($request, $response, $arguments) use ($c) {
                    $c["token"]->setDecoded($arguments["decoded"]);
                },
            ]);
        };
    }
}