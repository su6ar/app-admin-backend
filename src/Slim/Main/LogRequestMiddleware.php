<?php
/**
 * Created by Danie
 * Date: 03.07.2017
 * Time: 22:17
 */

namespace Kominexpres\src\Slim\Main;


use Kominexpres\src\App\Logger\LOGGER;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LogRequestMiddleware
{
    /** @var \Psr\Log\LoggerInterface */
    private $logger;

    /**
     * LogRequestMiddleware constructor.
     */
    public function __construct()
    {
        $this->logger = LOGGER::getLogger();
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return ResponseInterface
     */
    function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $url = $request->getUri()->getPath();
        $method = $request->getMethod();
        $this->logger->info("Incoming message to route '$method $url'", ['method'=>$method, 'query'=>$request->getUri()->getQuery(), 'body'=>$request->getParsedBody()]);

        $response = $next($request, $response);

        $this->logger->info("Outgoing message with status code '{$response->getStatusCode()}'");
        $this->logger->debug("Outgoing message body", ['body'=>$response->getBody()]);

        return $response;
    }
}